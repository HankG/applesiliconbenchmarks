Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro17,1
      Processor Name: Unknown
      Processor Speed: 2.4 GHz
      Number of Processors: 1
      Total Number of Cores: 8
      L2 Cache (per Core): 4 MB
      Memory: 16 GB
      Serial Number (system): C02DP2NCQ05P
      Hardware UUID: 4844558B-FD89-511C-80D0-8EA44F05829D
      Provisioning UDID: 4844558B-FD89-511C-80D0-8EA44F05829D
      Activation Lock Status: Enabled

Memory:

      Upgradeable Memory: Yes

        Memory Slot:

          Size: Empty
          Type: Empty
          Speed: Empty
          Status: Empty
          Manufacturer: Empty
          Part Number: Empty
          Serial Number: Empty

Graphics/Displays:

    Apple M1:

      Chipset Model: Apple M1
      Type: GPU
      Bus: Built-In
      Total Number of Cores: 8
      Vendor: Apple (0x106b)
      Metal Family: Supported, Metal GPUFamily Apple 7
      Displays:
        Color LCD:
          Display Type: Built-In Retina LCD
          Resolution: 2560 x 1600 Retina
          Main Display: Yes
          Mirror: Off
          Online: Yes
          Automatically Adjust Brightness: No
          Connection Type: Internal

