``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                           Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|       EnumerateActivityTagsSmall |     52.89 ns |   0.926 ns |   0.866 ns |     52.54 ns |     51.78 ns |     54.56 ns | 0.0132 |     - |     - |      56 B |
|       EnumerateActivityTagsLarge |  6,820.01 ns | 107.184 ns | 100.260 ns |  6,840.94 ns |  6,696.97 ns |  7,005.23 ns |      - |     - |     - |      56 B |
| EnumerateActivityTagObjectsSmall |     63.92 ns |   1.286 ns |   1.430 ns |     63.28 ns |     62.46 ns |     67.61 ns | 0.0094 |     - |     - |      40 B |
| EnumerateActivityTagObjectsLarge | 11,788.27 ns | 121.803 ns | 113.935 ns | 11,811.66 ns | 11,624.87 ns | 12,032.14 ns |      - |     - |     - |      40 B |
|      EnumerateActivityLinksSmall |    120.94 ns |   1.774 ns |   1.660 ns |    120.55 ns |    119.00 ns |    124.56 ns | 0.0151 |     - |     - |      64 B |
|      EnumerateActivityLinksLarge | 23,228.98 ns | 313.812 ns | 293.540 ns | 23,153.53 ns | 22,842.21 ns | 23,956.76 ns |      - |     - |     - |      64 B |
|     EnumerateActivityEventsSmall |     88.30 ns |   1.750 ns |   1.945 ns |     87.62 ns |     86.12 ns |     92.65 ns | 0.0132 |     - |     - |      56 B |
|     EnumerateActivityEventsLarge | 17,443.51 ns | 242.138 ns | 214.649 ns | 17,347.75 ns | 17,180.68 ns | 17,870.34 ns |      - |     - |     - |      56 B |
|   EnumerateActivityLinkTagsSmall |     58.22 ns |   1.095 ns |   1.025 ns |     57.96 ns |     57.04 ns |     60.78 ns | 0.0113 |     - |     - |      48 B |
|   EnumerateActivityLinkTagsLarge |  9,789.11 ns |  65.172 ns |  50.882 ns |  9,784.71 ns |  9,707.62 ns |  9,881.77 ns |      - |     - |     - |      48 B |
