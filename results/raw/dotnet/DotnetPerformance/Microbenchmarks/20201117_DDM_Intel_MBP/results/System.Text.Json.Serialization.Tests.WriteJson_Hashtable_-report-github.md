``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 17.02 μs | 0.335 μs | 0.329 μs | 16.95 μs | 16.62 μs | 17.77 μs | 2.8743 |     - |     - |   12064 B |
|    SerializeToUtf8Bytes | 16.45 μs | 0.265 μs | 0.247 μs | 16.40 μs | 16.15 μs | 16.99 μs | 1.4293 |     - |     - |    6152 B |
|       SerializeToStream | 15.86 μs | 0.185 μs | 0.173 μs | 15.80 μs | 15.64 μs | 16.20 μs |      - |     - |     - |     208 B |
| SerializeObjectProperty | 17.36 μs | 0.228 μs | 0.202 μs | 17.37 μs | 17.07 μs | 17.78 μs | 2.9340 |     - |     - |   12392 B |
