``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method |   level |             file |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |-------- |----------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|   **Compress** | **Optimal** | **TestDocument.pdf** | **2,067.0 μs** | **26.32 μs** | **23.33 μs** | **2,058.4 μs** | **2,031.8 μs** | **2,099.7 μs** |      **-** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Optimal | TestDocument.pdf |   304.9 μs |  4.42 μs |  3.92 μs |   304.9 μs |   298.4 μs |   311.3 μs | 1.2500 |     - |     - |   8.23 KB |
|   **Compress** | **Optimal** |      **alice29.txt** | **6,398.6 μs** | **82.87 μs** | **73.46 μs** | **6,385.5 μs** | **6,279.2 μs** | **6,525.0 μs** |      **-** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Optimal |      alice29.txt |   486.6 μs |  5.31 μs |  4.97 μs |   484.7 μs |   479.8 μs |   496.7 μs | 1.9531 |     - |     - |   8.23 KB |
|   **Compress** | **Optimal** |              **sum** | **1,300.7 μs** | **14.84 μs** | **13.88 μs** | **1,295.2 μs** | **1,282.7 μs** | **1,325.4 μs** |      **-** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Optimal |              sum |   115.0 μs |  1.18 μs |  1.11 μs |   115.2 μs |   113.6 μs |   117.0 μs | 1.8382 |     - |     - |   8.23 KB |
|   **Compress** | **Fastest** | **TestDocument.pdf** | **2,005.6 μs** | **12.47 μs** | **11.06 μs** | **2,002.3 μs** | **1,991.4 μs** | **2,026.1 μs** |      **-** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Fastest | TestDocument.pdf |   310.0 μs |  4.15 μs |  3.46 μs |   309.3 μs |   305.3 μs |   318.4 μs | 1.2500 |     - |     - |   8.23 KB |
|   **Compress** | **Fastest** |      **alice29.txt** | **1,622.2 μs** | **26.13 μs** | **24.44 μs** | **1,613.8 μs** | **1,596.5 μs** | **1,685.3 μs** |      **-** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Fastest |      alice29.txt |   525.1 μs |  5.82 μs |  5.45 μs |   525.0 μs |   517.4 μs |   534.2 μs |      - |     - |     - |   8.23 KB |
|   **Compress** | **Fastest** |              **sum** |   **368.1 μs** |  **5.09 μs** |  **4.76 μs** |   **366.6 μs** |   **361.1 μs** |   **377.3 μs** | **1.4535** |     **-** |     **-** |   **8.23 KB** |
| Decompress | Fastest |              sum |   128.2 μs |  1.88 μs |  1.76 μs |   128.2 μs |   125.4 μs |   130.7 μs | 1.5369 |     - |     - |   8.23 KB |
