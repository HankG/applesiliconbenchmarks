``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    DeserializeFromString | 461.1 ns |  6.62 ns |  6.19 ns | 461.2 ns | 452.4 ns | 472.3 ns | 0.0392 |     - |     - |     168 B |
| DeserializeFromUtf8Bytes | 402.7 ns |  4.13 ns |  3.66 ns | 401.6 ns | 397.2 ns | 409.9 ns | 0.0388 |     - |     - |     168 B |
|    DeserializeFromStream | 701.9 ns | 13.14 ns | 12.29 ns | 696.3 ns | 682.6 ns | 721.7 ns | 0.0552 |     - |     - |     240 B |
