``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|          XmlSerializer | 2.922 μs | 0.0331 μs | 0.0258 μs | 2.921 μs | 2.881 μs | 2.956 μs | 1.4438 |     - |     - |    5.9 KB |
| DataContractSerializer | 2.049 μs | 0.0340 μs | 0.0302 μs | 2.048 μs | 2.008 μs | 2.117 μs | 1.7400 |     - |     - |   7.12 KB |
