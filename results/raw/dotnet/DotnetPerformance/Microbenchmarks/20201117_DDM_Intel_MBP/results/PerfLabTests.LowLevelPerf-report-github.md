``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                      Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |     Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|----------:|------:|------:|----------:|
|                         EmptyStaticFunction |  2,436.25 μs |  31.445 μs |  29.414 μs |  2,427.08 μs |  2,390.46 μs |  2,489.93 μs |         - |     - |     - |       3 B |
|                     EmptyStaticFunction5Arg |  1,367.57 μs |  18.802 μs |  16.667 μs |  1,364.37 μs |  1,342.81 μs |  1,396.17 μs |         - |     - |     - |       2 B |
|                       EmptyInstanceFunction |  1,365.84 μs |  17.336 μs |  16.216 μs |  1,361.72 μs |  1,348.09 μs |  1,400.22 μs |         - |     - |     - |       2 B |
|                    InterfaceInterfaceMethod |  2,721.81 μs |  30.207 μs |  28.256 μs |  2,711.02 μs |  2,692.77 μs |  2,777.63 μs |         - |     - |     - |       3 B |
|       InterfaceInterfaceMethodLongHierarchy |    281.08 μs |   3.557 μs |   3.327 μs |    280.90 μs |    277.24 μs |    287.01 μs |         - |     - |     - |         - |
|      InterfaceInterfaceMethodSwitchCallType |    636.50 μs |   6.965 μs |   6.515 μs |    635.57 μs |    628.62 μs |    649.44 μs |         - |     - |     - |       1 B |
|                          ClassVirtualMethod |    113.83 μs |   1.406 μs |   1.246 μs |    113.33 μs |    112.49 μs |    116.22 μs |         - |     - |     - |         - |
|                  SealedClassInterfaceMethod |    113.66 μs |   1.119 μs |   1.047 μs |    113.49 μs |    112.20 μs |    115.63 μs |         - |     - |     - |         - |
|          StructWithInterfaceInterfaceMethod |    136.40 μs |   1.458 μs |   1.292 μs |    136.03 μs |    134.93 μs |    139.35 μs |         - |     - |     - |         - |
|                               StaticIntPlus |    120.21 μs |   0.901 μs |   0.752 μs |    120.27 μs |    119.00 μs |    121.64 μs |         - |     - |     - |         - |
|                        ObjectStringIsString |    102.45 μs |   2.448 μs |   2.819 μs |    102.34 μs |     97.28 μs |    108.19 μs |         - |     - |     - |         - |
|             NewDelegateClassEmptyInstanceFn |    640.11 μs |  13.390 μs |  14.883 μs |    638.55 μs |    622.11 μs |    664.07 μs | 1530.0000 |     - |     - | 6400001 B |
|               NewDelegateClassEmptyStaticFn |    634.59 μs |  12.457 μs |  13.329 μs |    632.00 μs |    616.75 μs |    658.21 μs | 1528.8462 |     - |     - | 6400001 B |
|                            InstanceDelegate |    159.14 μs |   1.670 μs |   1.480 μs |    158.54 μs |    157.38 μs |    161.81 μs |         - |     - |     - |         - |
|                              StaticDelegate |    227.87 μs |   3.078 μs |   2.879 μs |    226.41 μs |    224.63 μs |    232.75 μs |         - |     - |     - |         - |
|                               MeasureEvents | 13,195.59 μs | 148.424 μs | 131.574 μs | 13,165.49 μs | 13,044.71 μs | 13,446.35 μs |         - |     - |     - |      15 B |
|     GenericClassWithIntGenericInstanceField |     61.29 μs |   1.002 μs |   0.938 μs |     61.48 μs |     58.99 μs |     62.67 μs |         - |     - |     - |         - |
|              GenericClassGenericStaticField |     45.54 μs |   0.452 μs |   0.423 μs |     45.47 μs |     45.02 μs |     46.42 μs |         - |     - |     - |         - |
|           GenericClassGenericInstanceMethod |    162.78 μs |   2.722 μs |   2.413 μs |    162.19 μs |    159.66 μs |    167.47 μs |         - |     - |     - |         - |
|             GenericClassGenericStaticMethod |    136.55 μs |   1.389 μs |   1.300 μs |    136.76 μs |    134.53 μs |    138.91 μs |         - |     - |     - |         - |
|                        GenericGenericMethod |    137.71 μs |   1.671 μs |   1.563 μs |    136.92 μs |    136.25 μs |    141.29 μs |         - |     - |     - |         - |
| GenericClassWithSTringGenericInstanceMethod |    182.97 μs |   3.024 μs |   2.681 μs |    182.26 μs |    180.15 μs |    188.99 μs |         - |     - |     - |         - |
|                  ForeachOverList100Elements | 23,823.46 μs | 239.723 μs | 224.237 μs | 23,776.78 μs | 23,452.28 μs | 24,237.27 μs |         - |     - |     - |      22 B |
|                 TypeReflectionObjectGetType |    182.27 μs |   2.480 μs |   2.320 μs |    181.41 μs |    179.61 μs |    186.25 μs |         - |     - |     - |         - |
|                  TypeReflectionArrayGetType |    182.41 μs |   2.409 μs |   2.253 μs |    182.40 μs |    179.59 μs |    185.91 μs |         - |     - |     - |         - |
|                           IntegerFormatting |  2,064.65 μs |  28.630 μs |  26.781 μs |  2,055.62 μs |  2,023.21 μs |  2,106.26 μs | 1140.6250 |     - |     - | 4800002 B |
