``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Count |      Mean |     Error |    StdDev |    Median |       Min |       Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|--------:|-------:|------:|----------:|
|           Dictionary |   512 |  4.387 μs | 0.0881 μs | 0.0943 μs |  4.367 μs |  4.249 μs |  4.595 μs |  2.5152 |      - |     - |   10.3 KB |
| ConcurrentDictionary |   512 | 26.366 μs | 0.4795 μs | 0.4485 μs | 26.245 μs | 25.788 μs | 27.148 μs | 12.3720 | 0.1067 |     - |  50.62 KB |
