``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|      GetHit | 110.56 ns | 2.187 ns | 2.046 ns | 109.62 ns | 108.30 ns | 115.72 ns |      - |     - |     - |         - |
|     GetMiss |  72.47 ns | 0.784 ns | 0.733 ns |  72.59 ns |  71.38 ns |  73.66 ns |      - |     - |     - |         - |
| SetOverride | 315.05 ns | 5.985 ns | 5.878 ns | 314.14 ns | 307.25 ns | 326.38 ns | 0.1001 |     - |     - |     424 B |
