``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |         Mean |       Error |      StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|                Array |  512 |     151.7 ns |     2.52 ns |     2.36 ns |     150.6 ns |     149.4 ns |     157.3 ns |     - |     - |     - |         - |
|                 Span |  512 |     193.0 ns |     2.97 ns |     2.63 ns |     192.2 ns |     189.7 ns |     197.2 ns |     - |     - |     - |         - |
|                 List |  512 |     589.9 ns |     5.42 ns |     5.07 ns |     589.4 ns |     581.5 ns |     597.8 ns |     - |     - |     - |         - |
|                IList |  512 |   2,652.6 ns |    24.44 ns |    20.41 ns |   2,656.4 ns |   2,612.5 ns |   2,684.4 ns |     - |     - |     - |         - |
|           Dictionary |  512 |  11,033.5 ns |   128.31 ns |   113.74 ns |  11,047.5 ns |  10,781.4 ns |  11,213.3 ns |     - |     - |     - |         - |
|           SortedList |  512 | 215,268.2 ns | 2,833.26 ns | 2,511.61 ns | 214,872.2 ns | 211,514.5 ns | 219,490.3 ns |     - |     - |     - |         - |
|     SortedDictionary |  512 | 251,691.4 ns | 3,230.76 ns | 2,863.98 ns | 251,484.5 ns | 247,158.7 ns | 257,283.1 ns |     - |     - |     - |         - |
| ConcurrentDictionary |  512 |  30,317.8 ns |   392.86 ns |   367.48 ns |  30,253.6 ns |  29,883.7 ns |  31,070.3 ns |     - |     - |     - |         - |
