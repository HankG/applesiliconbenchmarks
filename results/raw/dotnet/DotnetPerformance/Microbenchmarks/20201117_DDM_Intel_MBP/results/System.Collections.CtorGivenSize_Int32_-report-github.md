``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                Array |  512 |  72.36 ns | 2.058 ns | 2.288 ns |  71.98 ns |  68.13 ns |  77.20 ns | 0.4952 |     - |     - |   2.02 KB |
|                 List |  512 |  80.21 ns | 1.623 ns | 1.594 ns |  79.76 ns |  78.52 ns |  84.05 ns | 0.5029 |     - |     - |   2.05 KB |
|              HashSet |  512 | 319.96 ns | 6.570 ns | 7.566 ns | 318.63 ns | 306.28 ns | 335.23 ns | 2.0211 |     - |     - |   8.27 KB |
|           Dictionary |  512 | 403.80 ns | 8.648 ns | 9.959 ns | 400.98 ns | 389.02 ns | 422.11 ns | 2.5246 |     - |     - |   10.3 KB |
|                Queue |  512 |  84.37 ns | 2.836 ns | 3.266 ns |  84.32 ns |  78.20 ns |  89.42 ns | 0.5048 |     - |     - |   2.06 KB |
|                Stack |  512 |  80.93 ns | 1.177 ns | 1.044 ns |  80.73 ns |  79.48 ns |  82.36 ns | 0.5029 |     - |     - |   2.05 KB |
|           SortedList |  512 | 158.48 ns | 3.144 ns | 3.495 ns | 157.76 ns | 152.96 ns | 166.46 ns | 1.0055 |     - |     - |   4.11 KB |
| ConcurrentDictionary |  512 | 203.22 ns | 4.093 ns | 4.020 ns | 201.92 ns | 197.64 ns | 209.87 ns | 1.0476 |     - |     - |   4.28 KB |
