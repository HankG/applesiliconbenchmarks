``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
| Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |        Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |---------:|---------:|---------:|---------:|---------:|---------:|-------------:|------:|------:|----------:|
|   Walk | 32.86 ms | 1.190 ms | 1.370 ms | 32.75 ms | 29.81 ms | 35.38 ms | 1000000.0000 |     - |     - |      32 B |
