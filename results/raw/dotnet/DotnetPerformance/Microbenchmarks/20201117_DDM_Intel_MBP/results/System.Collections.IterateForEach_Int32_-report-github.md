``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|                     Array |  512 |    216.0 ns |   2.41 ns |   2.25 ns |    215.4 ns |    213.2 ns |    220.1 ns |      - |     - |     - |         - |
|                      Span |  512 |    216.3 ns |   1.47 ns |   1.31 ns |    216.2 ns |    214.0 ns |    219.1 ns |      - |     - |     - |         - |
|              ReadOnlySpan |  512 |    218.1 ns |   4.15 ns |   4.08 ns |    216.6 ns |    213.7 ns |    227.8 ns |      - |     - |     - |         - |
|               IEnumerable |  512 |  2,013.3 ns |  22.64 ns |  21.17 ns |  2,008.1 ns |  1,981.2 ns |  2,057.4 ns |      - |     - |     - |      32 B |
|                      List |  512 |  1,176.7 ns |  11.80 ns |  11.04 ns |  1,172.4 ns |  1,162.2 ns |  1,196.9 ns |      - |     - |     - |         - |
|                LinkedList |  512 |  1,782.1 ns |  18.93 ns |  17.71 ns |  1,775.6 ns |  1,753.9 ns |  1,819.2 ns |      - |     - |     - |         - |
|                   HashSet |  512 |  1,014.3 ns |  11.50 ns |  10.76 ns |  1,010.6 ns |    999.7 ns |  1,030.5 ns |      - |     - |     - |         - |
|                Dictionary |  512 |  1,196.8 ns |  18.72 ns |  16.60 ns |  1,193.4 ns |  1,175.3 ns |  1,232.5 ns |      - |     - |     - |         - |
|                     Queue |  512 |  1,571.4 ns |  17.49 ns |  15.51 ns |  1,569.4 ns |  1,547.4 ns |  1,598.6 ns |      - |     - |     - |         - |
|                     Stack |  512 |  1,698.5 ns |  20.88 ns |  19.53 ns |  1,700.1 ns |  1,665.3 ns |  1,726.9 ns |      - |     - |     - |         - |
|                SortedList |  512 |  4,437.1 ns |  46.82 ns |  41.50 ns |  4,424.8 ns |  4,385.5 ns |  4,521.7 ns |      - |     - |     - |      48 B |
|                 SortedSet |  512 |  7,865.8 ns |  71.82 ns |  59.97 ns |  7,854.9 ns |  7,785.2 ns |  8,013.7 ns | 0.0316 |     - |     - |     216 B |
|          SortedDictionary |  512 |  8,632.7 ns |  91.89 ns |  81.46 ns |  8,617.6 ns |  8,536.1 ns |  8,791.6 ns | 0.0341 |     - |     - |     216 B |
|      ConcurrentDictionary |  512 | 11,433.8 ns | 145.39 ns | 136.00 ns | 11,381.7 ns | 11,247.9 ns | 11,699.6 ns |      - |     - |     - |      56 B |
|           ConcurrentQueue |  512 |  3,449.7 ns |  39.89 ns |  37.31 ns |  3,435.6 ns |  3,402.6 ns |  3,508.6 ns | 0.0137 |     - |     - |      72 B |
|           ConcurrentStack |  512 |  2,535.2 ns |  30.34 ns |  28.38 ns |  2,525.8 ns |  2,500.3 ns |  2,584.1 ns |      - |     - |     - |      40 B |
|             ConcurrentBag |  512 |  2,406.0 ns |  44.99 ns |  44.18 ns |  2,396.4 ns |  2,354.0 ns |  2,510.5 ns | 0.5027 |     - |     - |    2104 B |
|            ImmutableArray |  512 |    214.0 ns |   2.89 ns |   2.70 ns |    212.6 ns |    210.9 ns |    218.3 ns |      - |     - |     - |         - |
|       ImmutableDictionary |  512 | 24,638.9 ns | 245.45 ns | 204.96 ns | 24,629.2 ns | 24,396.1 ns | 25,166.3 ns |      - |     - |     - |         - |
|          ImmutableHashSet |  512 | 23,924.4 ns | 278.97 ns | 260.95 ns | 23,846.0 ns | 23,559.7 ns | 24,376.9 ns |      - |     - |     - |         - |
|             ImmutableList |  512 | 11,446.3 ns | 138.68 ns | 129.72 ns | 11,408.9 ns | 11,261.8 ns | 11,653.7 ns |      - |     - |     - |         - |
|            ImmutableQueue |  512 |  3,894.5 ns |  47.64 ns |  42.23 ns |  3,899.0 ns |  3,816.9 ns |  3,943.1 ns |      - |     - |     - |         - |
|            ImmutableStack |  512 |  3,681.2 ns |  58.89 ns |  55.08 ns |  3,691.6 ns |  3,583.5 ns |  3,748.8 ns |      - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 12,581.4 ns | 118.52 ns | 110.87 ns | 12,587.3 ns | 12,389.3 ns | 12,726.7 ns |      - |     - |     - |         - |
|        ImmutableSortedSet |  512 | 12,503.9 ns | 273.01 ns | 314.40 ns | 12,587.4 ns | 12,043.1 ns | 13,070.6 ns |      - |     - |     - |         - |
