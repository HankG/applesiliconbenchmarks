``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |         Mean |       Error |      StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|  ArrayList |  512 |     541.8 ns |    15.95 ns |    18.37 ns |     539.2 ns |     519.5 ns |     582.6 ns | 1.9750 |     - |     - |   8.08 KB |
|  Hashtable |  512 |  29,610.1 ns |   502.78 ns |   470.30 ns |  29,518.2 ns |  29,063.7 ns |  30,574.3 ns | 4.3550 |     - |     - |  17.98 KB |
|      Queue |  512 |   8,739.3 ns |   160.97 ns |   150.57 ns |   8,723.7 ns |   8,530.2 ns |   8,980.9 ns | 1.0019 |     - |     - |    4.1 KB |
|      Stack |  512 |   7,909.7 ns |   140.33 ns |   131.26 ns |   7,843.9 ns |   7,765.0 ns |   8,140.2 ns | 0.9771 |     - |     - |   4.09 KB |
| SortedList |  512 | 245,298.5 ns | 2,978.11 ns | 2,785.72 ns | 244,636.7 ns | 240,954.8 ns | 249,819.8 ns | 1.9231 |     - |     - |   8.13 KB |
