``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|--------:|-------:|------:|----------:|
|                Array |  512 |    441.2 ns |  16.97 ns |  19.55 ns |    432.9 ns |    420.3 ns |    484.2 ns |  0.4936 |      - |     - |   2.02 KB |
|                 Span |  512 |    698.2 ns |  10.73 ns |   9.51 ns |    698.7 ns |    683.5 ns |    715.0 ns |  0.4930 |      - |     - |   2.02 KB |
|                 List |  512 |    945.9 ns |  17.09 ns |  15.99 ns |    942.9 ns |    928.4 ns |    977.9 ns |  1.0254 |      - |     - |    4.2 KB |
|          ICollection |  512 |  1,533.0 ns |  26.45 ns |  22.09 ns |  1,524.6 ns |  1,501.2 ns |  1,571.0 ns |  1.0271 |      - |     - |    4.2 KB |
|           LinkedList |  512 |  7,462.6 ns | 139.89 ns | 130.85 ns |  7,450.5 ns |  7,305.0 ns |  7,755.1 ns |  5.8603 | 0.0297 |     - |  24.04 KB |
|              HashSet |  512 |  7,078.7 ns | 175.11 ns | 201.66 ns |  7,075.3 ns |  6,769.5 ns |  7,411.4 ns |  6.6050 |      - |     - |  27.07 KB |
|           Dictionary |  512 |  7,608.4 ns | 132.25 ns | 123.71 ns |  7,580.2 ns |  7,443.1 ns |  7,805.2 ns |  8.2181 | 0.0303 |     - |  33.69 KB |
|          IDictionary |  512 |  9,788.0 ns | 192.20 ns | 188.76 ns |  9,734.4 ns |  9,558.6 ns | 10,128.4 ns |  8.2123 | 0.0395 |     - |  33.69 KB |
|           SortedList |  512 | 48,849.5 ns | 749.46 ns | 701.05 ns | 48,650.3 ns | 48,003.3 ns | 50,143.7 ns |  1.9231 |      - |     - |   8.41 KB |
|            SortedSet |  512 | 40,063.9 ns | 797.86 ns | 783.60 ns | 39,874.6 ns | 39,288.5 ns | 41,870.0 ns |  4.7710 | 0.1590 |     - |  20.05 KB |
|     SortedDictionary |  512 | 62,937.4 ns | 970.98 ns | 860.75 ns | 63,072.5 ns | 61,610.9 ns | 64,603.8 ns |  5.6818 | 0.2470 |     - |  24.11 KB |
| ConcurrentDictionary |  512 | 36,114.1 ns | 693.25 ns | 741.77 ns | 36,021.8 ns | 34,915.1 ns | 37,477.3 ns | 15.7670 | 0.1420 |     - |  64.91 KB |
|                Stack |  512 |  1,174.5 ns |  21.89 ns |  20.48 ns |  1,168.3 ns |  1,151.9 ns |  1,221.4 ns |  1.0259 |      - |     - |    4.2 KB |
|      ConcurrentStack |  512 |  6,924.8 ns | 114.95 ns | 107.53 ns |  6,909.8 ns |  6,777.4 ns |  7,116.1 ns |  3.9167 |      - |     - |  16.02 KB |
|                Queue |  512 |  1,629.9 ns |  31.95 ns |  31.38 ns |  1,622.2 ns |  1,581.5 ns |  1,695.6 ns |  1.0295 |      - |     - |   4.21 KB |
|      ConcurrentQueue |  512 |  6,222.6 ns |  91.99 ns |  86.05 ns |  6,197.4 ns |  6,117.1 ns |  6,362.8 ns |  2.3319 |      - |     - |   9.56 KB |
|        ConcurrentBag |  512 | 12,118.6 ns | 213.74 ns | 189.47 ns | 12,119.3 ns | 11,861.6 ns | 12,488.7 ns |  1.9952 | 0.9744 |     - |   8.25 KB |
