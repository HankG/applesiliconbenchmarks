``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|  Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    ctor | 2.740 ns | 0.0818 ns | 0.0726 ns | 2.739 ns | 2.632 ns | 2.881 ns | 0.0057 |     - |     - |      24 B |
| GetType | 1.617 ns | 0.0928 ns | 0.0868 ns | 1.586 ns | 1.534 ns | 1.841 ns |      - |     - |     - |         - |
