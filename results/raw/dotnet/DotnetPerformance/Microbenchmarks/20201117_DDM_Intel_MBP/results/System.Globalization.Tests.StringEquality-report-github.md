``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method | Count |                    Options |          Mean |       Error |      StdDev |        Median |           Min |           Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |------ |--------------------------- |--------------:|------------:|------------:|--------------:|--------------:|--------------:|------:|------:|------:|----------:|
|               **Compare_Same** |  **1024** |             **(, IgnoreCase)** |    **534.461 ns** |   **6.8615 ns** |   **6.4183 ns** |    **533.837 ns** |    **526.448 ns** |    **547.318 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |             (, IgnoreCase) |  7,454.583 ns | 101.8490 ns |  90.2865 ns |  7,435.122 ns |  7,330.145 ns |  7,613.632 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |             (, IgnoreCase) |     33.135 ns |   0.3868 ns |   0.3429 ns |     33.062 ns |     32.701 ns |     33.813 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |                   **(, None)** |    **534.194 ns** |   **6.7261 ns** |   **6.2916 ns** |    **532.483 ns** |    **526.075 ns** |    **545.003 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |                   (, None) |  7,457.322 ns |  88.2947 ns |  82.5909 ns |  7,426.105 ns |  7,366.635 ns |  7,649.431 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |                   (, None) |     33.694 ns |   0.5192 ns |   0.4602 ns |     33.519 ns |     33.124 ns |     34.568 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |        **(en-US, IgnoreCase)** |    **533.937 ns** |   **5.0735 ns** |   **4.7457 ns** |    **531.537 ns** |    **527.126 ns** |    **543.022 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |        (en-US, IgnoreCase) |  7,438.731 ns |  65.3060 ns |  61.0873 ns |  7,426.304 ns |  7,349.119 ns |  7,536.828 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |        (en-US, IgnoreCase) |     33.171 ns |   0.4666 ns |   0.4364 ns |     33.027 ns |     32.702 ns |     34.028 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |    **(en-US, IgnoreNonSpace)** |    **533.525 ns** |   **5.8353 ns** |   **5.4583 ns** |    **532.174 ns** |    **527.097 ns** |    **547.000 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |    (en-US, IgnoreNonSpace) |  2,248.944 ns |  31.2342 ns |  26.0819 ns |  2,237.758 ns |  2,218.112 ns |  2,297.559 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |    (en-US, IgnoreNonSpace) |     33.155 ns |   0.4489 ns |   0.4199 ns |     33.007 ns |     32.659 ns |     33.956 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |     **(en-US, IgnoreSymbols)** |    **533.814 ns** |   **5.9903 ns** |   **5.6034 ns** |    **534.765 ns** |    **525.477 ns** |    **541.377 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |     (en-US, IgnoreSymbols) | 10,710.920 ns | 126.4300 ns | 118.2627 ns | 10,681.214 ns | 10,565.457 ns | 10,975.827 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |     (en-US, IgnoreSymbols) | 20,481.244 ns | 286.8172 ns | 268.2890 ns | 20,421.834 ns | 20,150.046 ns | 21,032.854 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |              **(en-US, None)** |    **533.439 ns** |   **6.7802 ns** |   **6.3422 ns** |    **532.327 ns** |    **526.255 ns** |    **543.342 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |              (en-US, None) |  7,451.844 ns |  81.4411 ns |  76.1801 ns |  7,435.517 ns |  7,353.722 ns |  7,599.693 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |              (en-US, None) |     33.488 ns |   0.4973 ns |   0.4652 ns |     33.295 ns |     33.035 ns |     34.371 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |           **(en-US, Ordinal)** |     **43.755 ns** |   **1.0065 ns** |   **1.1590 ns** |     **43.818 ns** |     **40.678 ns** |     **45.393 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |           (en-US, Ordinal) |     14.219 ns |   0.1560 ns |   0.1303 ns |     14.219 ns |     14.056 ns |     14.516 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |           (en-US, Ordinal) |     10.603 ns |   0.1244 ns |   0.1163 ns |     10.574 ns |     10.426 ns |     10.805 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** | **(en-US, OrdinalIgnoreCase)** |    **713.693 ns** |   **5.7634 ns** |   **4.8127 ns** |    **714.468 ns** |    **705.165 ns** |    **723.099 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 | (en-US, OrdinalIgnoreCase) |  1,013.322 ns |  13.6679 ns |  12.7849 ns |  1,012.778 ns |    992.254 ns |  1,035.605 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 | (en-US, OrdinalIgnoreCase) |      9.207 ns |   0.1502 ns |   0.1332 ns |      9.198 ns |      9.036 ns |      9.433 ns |     - |     - |     - |         - |
|               **Compare_Same** |  **1024** |              **(pl-PL, None)** |    **534.378 ns** |   **4.2565 ns** |   **3.9815 ns** |    **533.606 ns** |    **528.052 ns** |    **541.220 ns** |     **-** |     **-** |     **-** |         **-** |
|         Compare_Same_Upper |  1024 |              (pl-PL, None) | 16,668.192 ns | 190.2084 ns | 177.9211 ns | 16,656.083 ns | 16,420.893 ns | 16,967.930 ns |     - |     - |     - |         - |
| Compare_DifferentFirstChar |  1024 |              (pl-PL, None) |     33.102 ns |   0.3698 ns |   0.3459 ns |     32.983 ns |     32.682 ns |     33.744 ns |     - |     - |     - |         - |
