``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
| Method | Formatted | SkipValidation |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |---------- |--------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|   **Ctor** |     **False** |          **False** | **19.17 ns** | **0.363 ns** | **0.340 ns** | **19.10 ns** | **18.78 ns** | **19.89 ns** | **0.0287** |     **-** |     **-** |     **120 B** |
|   **Ctor** |     **False** |           **True** | **19.04 ns** | **0.361 ns** | **0.338 ns** | **18.98 ns** | **18.61 ns** | **19.59 ns** | **0.0287** |     **-** |     **-** |     **120 B** |
|   **Ctor** |      **True** |          **False** | **19.61 ns** | **0.417 ns** | **0.464 ns** | **19.54 ns** | **18.98 ns** | **20.55 ns** | **0.0286** |     **-** |     **-** |     **120 B** |
|   **Ctor** |      **True** |           **True** | **19.43 ns** | **0.320 ns** | **0.299 ns** | **19.32 ns** | **18.98 ns** | **19.97 ns** | **0.0287** |     **-** |     **-** |     **120 B** |
