``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |      Gen 0 | Gen 1 | Gen 2 |   Allocated |
|------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|-----------:|------:|------:|------------:|
|        Where00LinqQueryX | 590.5 ms | 5.03 ms | 4.46 ms | 590.5 ms | 583.8 ms | 596.2 ms | 17000.0000 |     - |     - |  72000288 B |
|       Where00LinqMethodX | 579.0 ms | 3.06 ms | 2.55 ms | 578.8 ms | 573.5 ms | 583.0 ms | 17000.0000 |     - |     - |  72000288 B |
|              Where00ForX | 391.1 ms | 5.36 ms | 5.01 ms | 389.0 ms | 384.3 ms | 400.1 ms | 42000.0000 |     - |     - | 176000288 B |
|        Where01LinqQueryX | 269.4 ms | 3.07 ms | 2.72 ms | 269.0 ms | 266.6 ms | 274.3 ms |  4000.0000 |     - |     - |  18000288 B |
|       Where01LinqMethodX | 268.6 ms | 3.37 ms | 3.16 ms | 267.5 ms | 264.8 ms | 274.8 ms |  4000.0000 |     - |     - |  18000288 B |
| Where01LinqMethodNestedX | 329.3 ms | 3.86 ms | 3.42 ms | 329.4 ms | 323.3 ms | 335.3 ms | 14000.0000 |     - |     - |  60000288 B |
|              Where01ForX | 218.0 ms | 2.28 ms | 2.02 ms | 218.0 ms | 214.7 ms | 221.4 ms |  5000.0000 |     - |     - |  22000288 B |
|       Count00LinqMethodX | 763.6 ms | 5.16 ms | 4.31 ms | 762.8 ms | 756.3 ms | 771.0 ms |  9000.0000 |     - |     - |  40000288 B |
|              Count00ForX | 301.7 ms | 3.34 ms | 3.12 ms | 301.1 ms | 297.6 ms | 308.8 ms |          - |     - |     - |       288 B |
|        Order00LinqQueryX | 115.8 ms | 1.37 ms | 1.21 ms | 115.7 ms | 114.1 ms | 118.1 ms | 14000.0000 |     - |     - |  58600144 B |
|       Order00LinqMethodX | 116.6 ms | 1.93 ms | 1.81 ms | 116.7 ms | 114.3 ms | 120.5 ms | 14000.0000 |     - |     - |  58600144 B |
|           Order00ManualX | 138.3 ms | 1.87 ms | 1.66 ms | 137.9 ms | 136.1 ms | 141.8 ms |  3000.0000 |     - |     - |  16000288 B |
