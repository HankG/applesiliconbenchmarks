``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QZKIVK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MaxWarmupIterationCount=10  
MinIterationCount=15  MinWarmupIterationCount=6  UnrollFactor=1  
WarmupCount=-1  

```
|          Method |    Size |     Mean |   Error |  StdDev |   Median |      Min |      Max |      Gen 0 | Gen 1 | Gen 2 |    Allocated |
|---------------- |-------- |---------:|--------:|--------:|---------:|---------:|---------:|-----------:|------:|------:|-------------:|
|   ConcurrentBag | 2000000 | 284.2 ms | 5.22 ms | 4.88 ms | 284.6 ms | 276.2 ms | 293.4 ms |          - |     - |     - |      1.41 KB |
| ConcurrentStack | 2000000 | 129.1 ms | 5.89 ms | 6.54 ms | 128.8 ms | 117.8 ms | 143.8 ms | 30000.0000 |     - |     - | 125947.88 KB |
| ConcurrentQueue | 2000000 | 139.3 ms | 5.70 ms | 6.56 ms | 141.6 ms | 128.4 ms | 147.3 ms |          - |     - |     - |      8.91 KB |
