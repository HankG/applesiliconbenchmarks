``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | Formatted | SkipValidation |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |---------- |--------------- |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
| **WriteDateTimes** |     **False** |          **False** | **5.332 ms** | **0.0627 ms** | **0.0556 ms** | **5.319 ms** | **5.256 ms** | **5.439 ms** |     **-** |     **-** |     **-** |     **126 B** |
| **WriteDateTimes** |     **False** |           **True** | **5.312 ms** | **0.0694 ms** | **0.0649 ms** | **5.303 ms** | **5.245 ms** | **5.428 ms** |     **-** |     **-** |     **-** |     **126 B** |
| **WriteDateTimes** |      **True** |          **False** | **5.903 ms** | **0.0727 ms** | **0.0680 ms** | **5.882 ms** | **5.817 ms** | **6.003 ms** |     **-** |     **-** |     **-** |     **147 B** |
| **WriteDateTimes** |      **True** |           **True** | **5.826 ms** | **0.0902 ms** | **0.0799 ms** | **5.788 ms** | **5.732 ms** | **5.984 ms** |     **-** |     **-** |     **-** |     **147 B** |
