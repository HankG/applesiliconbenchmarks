``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|        benchFFT | 686.6 ms | 3.56 ms | 2.97 ms | 685.9 ms | 683.0 ms | 692.9 ms |     - |     - |     - |     288 B |
|        benchSOR | 853.8 ms | 2.74 ms | 2.56 ms | 854.1 ms | 848.4 ms | 858.3 ms |     - |     - |     - |    5520 B |
| benchMonteCarlo | 722.1 ms | 3.57 ms | 2.79 ms | 722.6 ms | 718.4 ms | 725.8 ms |     - |     - |     - |     464 B |
| benchSparseMult | 559.9 ms | 4.08 ms | 3.82 ms | 559.3 ms | 553.1 ms | 567.0 ms |     - |     - |     - |     288 B |
|     benchmarkLU | 687.5 ms | 4.31 ms | 3.82 ms | 688.6 ms | 682.1 ms | 694.7 ms |     - |     - |     - |     288 B |
