``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
| Method | Size |       Mean |    Error |   StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |----- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|  Array |  512 |   468.9 ns |  3.79 ns |  3.36 ns |   468.6 ns |   465.1 ns |   476.2 ns |     - |     - |     - |         - |
|   Span |  512 |   214.7 ns |  2.85 ns |  2.67 ns |   213.7 ns |   211.4 ns |   219.7 ns |     - |     - |     - |         - |
|   List |  512 |   596.0 ns |  7.56 ns |  7.08 ns |   594.2 ns |   588.8 ns |   610.4 ns |     - |     - |     - |         - |
|  IList |  512 | 1,547.7 ns | 29.34 ns | 27.45 ns | 1,541.3 ns | 1,512.6 ns | 1,605.4 ns |     - |     - |     - |         - |
