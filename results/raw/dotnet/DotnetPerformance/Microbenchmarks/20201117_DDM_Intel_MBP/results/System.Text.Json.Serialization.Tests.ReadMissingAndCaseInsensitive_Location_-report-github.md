``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                   Baseline | 1,233.6 ns | 15.92 ns | 14.11 ns | 1,234.0 ns | 1,210.3 ns | 1,264.6 ns | 0.1034 |     - |     - |     448 B |
|          MissingProperties |   812.7 ns | 11.04 ns | 10.33 ns |   811.9 ns |   799.1 ns |   832.2 ns | 0.0190 |     - |     - |      88 B |
|    CaseInsensitiveMatching | 1,231.9 ns | 19.87 ns | 18.59 ns | 1,227.9 ns | 1,204.3 ns | 1,264.6 ns | 0.1028 |     - |     - |     448 B |
| CaseInsensitiveNotMatching | 1,223.9 ns | 23.34 ns | 22.92 ns | 1,219.2 ns | 1,198.4 ns | 1,266.6 ns | 0.1025 |     - |     - |     448 B |
