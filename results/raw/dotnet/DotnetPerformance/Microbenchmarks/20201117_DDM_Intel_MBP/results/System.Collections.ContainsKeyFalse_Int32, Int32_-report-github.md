``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  4.062 μs | 0.0531 μs | 0.0497 μs |  4.064 μs |  3.962 μs |  4.133 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  4.787 μs | 0.0590 μs | 0.0551 μs |  4.771 μs |  4.715 μs |  4.891 μs |     - |     - |     - |         - |
|                SortedList |  512 | 24.379 μs | 0.3110 μs | 0.2757 μs | 24.322 μs | 23.947 μs | 24.832 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 49.069 μs | 0.4656 μs | 0.4355 μs | 48.934 μs | 48.436 μs | 49.757 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  2.608 μs | 0.0356 μs | 0.0316 μs |  2.601 μs |  2.570 μs |  2.674 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 | 15.231 μs | 0.2104 μs | 0.1968 μs | 15.268 μs | 14.939 μs | 15.530 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 30.051 μs | 0.4510 μs | 0.4219 μs | 29.952 μs | 29.513 μs | 30.815 μs |     - |     - |     - |         - |
