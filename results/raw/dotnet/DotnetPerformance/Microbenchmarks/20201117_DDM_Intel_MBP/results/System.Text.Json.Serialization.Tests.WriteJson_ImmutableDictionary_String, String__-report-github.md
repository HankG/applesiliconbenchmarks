``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|       SerializeToString | 24.31 μs | 0.219 μs | 0.171 μs | 24.34 μs | 24.03 μs | 24.59 μs | 2.8891 |      - |     - |   12312 B |
|    SerializeToUtf8Bytes | 23.46 μs | 0.372 μs | 0.348 μs | 23.39 μs | 22.97 μs | 24.28 μs | 1.4771 |      - |     - |    6400 B |
|       SerializeToStream | 23.29 μs | 0.360 μs | 0.337 μs | 23.17 μs | 22.91 μs | 24.12 μs | 0.0929 |      - |     - |     456 B |
| SerializeObjectProperty | 23.83 μs | 0.338 μs | 0.316 μs | 23.81 μs | 23.38 μs | 24.39 μs | 2.9940 | 0.0936 |     - |   12640 B |
