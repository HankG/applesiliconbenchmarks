``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                 Clear |  512 |  16.846 ns | 0.3435 ns | 0.3045 ns |  16.855 ns |  16.413 ns |  17.470 ns |      - |     - |     - |         - |
|                  Fill |  512 | 116.358 ns | 1.1059 ns | 0.9235 ns | 116.259 ns | 115.179 ns | 118.437 ns |      - |     - |     - |         - |
|               Reverse |  512 | 122.409 ns | 1.2527 ns | 1.1718 ns | 122.209 ns | 120.877 ns | 125.076 ns |      - |     - |     - |         - |
|               ToArray |  512 |  62.115 ns | 1.2856 ns | 1.2626 ns |  61.778 ns |  59.942 ns |  64.058 ns | 0.2504 |     - |     - |    1048 B |
|         SequenceEqual |  512 |  16.415 ns | 0.2562 ns | 0.2139 ns |  16.400 ns |  16.014 ns |  16.767 ns |      - |     - |     - |         - |
|     SequenceCompareTo |  512 |  20.455 ns | 0.2355 ns | 0.2203 ns |  20.420 ns |  20.144 ns |  20.830 ns |      - |     - |     - |         - |
|            StartsWith |  512 |  10.951 ns | 0.1260 ns | 0.1178 ns |  10.898 ns |  10.821 ns |  11.156 ns |      - |     - |     - |         - |
|              EndsWith |  512 |  11.245 ns | 0.1530 ns | 0.1431 ns |  11.185 ns |  11.068 ns |  11.530 ns |      - |     - |     - |         - |
|          IndexOfValue |  512 |  11.695 ns | 0.2593 ns | 0.2547 ns |  11.638 ns |  11.351 ns |  12.319 ns |      - |     - |     - |         - |
|   IndexOfAnyTwoValues |  512 |  11.244 ns | 0.1141 ns | 0.0953 ns |  11.228 ns |  11.135 ns |  11.485 ns |      - |     - |     - |         - |
| IndexOfAnyThreeValues |  512 |  13.504 ns | 0.1640 ns | 0.1534 ns |  13.431 ns |  13.343 ns |  13.756 ns |      - |     - |     - |         - |
|  IndexOfAnyFourValues |  512 |  21.785 ns | 0.3018 ns | 0.2823 ns |  21.715 ns |  21.405 ns |  22.424 ns |      - |     - |     - |         - |
|      LastIndexOfValue |  512 |  16.536 ns | 0.2502 ns | 0.1953 ns |  16.584 ns |  16.224 ns |  16.892 ns |      - |     - |     - |         - |
|  LastIndexOfAnyValues |  512 | 147.291 ns | 1.5198 ns | 1.3473 ns | 146.914 ns | 145.552 ns | 150.058 ns |      - |     - |     - |         - |
|          BinarySearch |  512 |  11.458 ns | 0.1885 ns | 0.1671 ns |  11.389 ns |  11.259 ns |  11.773 ns |      - |     - |     - |         - |
|  GetPinnableReference |  512 |   1.358 ns | 0.0157 ns | 0.0147 ns |   1.354 ns |   1.338 ns |   1.381 ns |      - |     - |     - |         - |
