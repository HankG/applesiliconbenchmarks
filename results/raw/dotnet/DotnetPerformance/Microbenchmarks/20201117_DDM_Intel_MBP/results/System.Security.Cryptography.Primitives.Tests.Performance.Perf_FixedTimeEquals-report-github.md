``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|             FixedTimeEquals_256Bit_Equal | 124.0 ns | 1.49 ns | 1.39 ns | 123.5 ns | 122.2 ns | 126.6 ns |     - |     - |     - |         - |
|  FixedTimeEquals_256Bit_LastBitDifferent | 124.6 ns | 1.14 ns | 1.06 ns | 125.1 ns | 122.3 ns | 126.3 ns |     - |     - |     - |         - |
| FixedTimeEquals_256Bit_FirstBitDifferent | 123.5 ns | 1.36 ns | 1.27 ns | 123.2 ns | 121.8 ns | 125.6 ns |     - |     - |     - |         - |
|   FixedTimeEquals_256Bit_CascadingErrors | 123.9 ns | 1.26 ns | 1.18 ns | 123.6 ns | 122.3 ns | 126.0 ns |     - |     - |     - |         - |
|  FixedTimeEquals_256Bit_AllBitsDifferent | 123.8 ns | 1.31 ns | 1.23 ns | 123.6 ns | 122.1 ns | 125.9 ns |     - |     - |     - |         - |
|        FixedTimeEquals_256Bit_VersusZero | 123.8 ns | 1.81 ns | 1.69 ns | 123.0 ns | 122.2 ns | 127.2 ns |     - |     - |     - |         - |
|     FixedTimeEquals_256Bit_SameReference | 138.3 ns | 1.65 ns | 1.47 ns | 138.1 ns | 136.3 ns | 141.6 ns |     - |     - |     - |         - |
