``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|    DeserializeFromString | 46.73 μs | 0.721 μs | 0.639 μs | 46.51 μs | 45.71 μs | 47.84 μs | 7.6923 | 0.1923 |     - |  31.61 KB |
| DeserializeFromUtf8Bytes | 45.74 μs | 0.891 μs | 0.833 μs | 45.44 μs | 44.81 μs | 47.37 μs | 7.6754 | 0.7310 |     - |  31.61 KB |
|    DeserializeFromStream | 48.05 μs | 0.772 μs | 0.722 μs | 47.78 μs | 46.90 μs | 49.33 μs | 7.6722 | 0.9356 |     - |  31.68 KB |
