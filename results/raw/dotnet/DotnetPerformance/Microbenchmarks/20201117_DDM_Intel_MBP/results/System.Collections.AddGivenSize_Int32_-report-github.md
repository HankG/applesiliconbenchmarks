``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|--------:|-------:|------:|----------:|
|                 List |  512 |    694.0 ns |  11.91 ns |  10.56 ns |    691.8 ns |    679.0 ns |    717.6 ns |  0.5030 |      - |     - |   2.05 KB |
|          ICollection |  512 |  1,276.7 ns |  19.04 ns |  17.81 ns |  1,270.6 ns |  1,252.2 ns |  1,312.1 ns |  0.5024 |      - |     - |   2.05 KB |
|              HashSet |  512 |  4,061.9 ns |  76.53 ns |  85.07 ns |  4,050.8 ns |  3,916.7 ns |  4,189.1 ns |  2.0075 |      - |     - |   8.27 KB |
|           Dictionary |  512 |  4,244.3 ns |  82.95 ns |  73.54 ns |  4,216.3 ns |  4,160.3 ns |  4,398.2 ns |  2.5170 |      - |     - |   10.3 KB |
|          IDictionary |  512 |  5,937.2 ns | 115.94 ns | 124.05 ns |  5,907.1 ns |  5,800.0 ns |  6,237.7 ns |  2.5169 |      - |     - |   10.3 KB |
|           SortedList |  512 | 46,890.4 ns | 521.69 ns | 462.47 ns | 46,763.7 ns | 46,148.5 ns | 47,811.4 ns |  0.9246 |      - |     - |   4.11 KB |
|                Queue |  512 |  1,646.1 ns |  20.50 ns |  18.17 ns |  1,641.9 ns |  1,622.7 ns |  1,687.4 ns |  0.5044 |      - |     - |   2.06 KB |
|                Stack |  512 |    813.8 ns |  16.13 ns |  17.93 ns |    807.5 ns |    791.6 ns |    851.9 ns |  0.5028 |      - |     - |   2.05 KB |
| ConcurrentDictionary |  512 | 26,741.4 ns | 456.48 ns | 426.99 ns | 26,594.4 ns | 26,193.9 ns | 27,481.4 ns | 12.3057 | 0.1079 |     - |  50.62 KB |
