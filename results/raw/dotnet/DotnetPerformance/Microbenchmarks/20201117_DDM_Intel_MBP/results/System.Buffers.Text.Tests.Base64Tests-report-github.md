``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                          Method | NumberOfBytes |        Mean |     Error |   StdDev |      Median |       Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |-------------- |------------:|----------:|---------:|------------:|----------:|------------:|------:|------:|------:|----------:|
|                    Base64Encode |          1000 |    59.41 ns |  0.920 ns | 0.816 ns |    59.11 ns |  58.41 ns |    61.14 ns |     - |     - |     - |         - |
| Base64EncodeDestinationTooSmall |          1000 |    63.86 ns |  0.941 ns | 0.880 ns |    63.71 ns |  62.73 ns |    65.36 ns |     - |     - |     - |         - |
|        ConvertToBase64CharArray |          1000 |   928.24 ns | 10.561 ns | 9.362 ns |   925.57 ns | 917.89 ns |   947.76 ns |     - |     - |     - |         - |
|                    Base64Decode |          1000 |    10.25 ns |  0.127 ns | 0.119 ns |    10.25 ns |  10.06 ns |    10.46 ns |     - |     - |     - |         - |
|  Base64DecodeDetinationTooSmall |          1000 |    10.30 ns |  0.229 ns | 0.224 ns |    10.24 ns |  10.05 ns |    10.82 ns |     - |     - |     - |         - |
|       ConvertTryFromBase64Chars |          1000 | 1,002.54 ns |  9.663 ns | 8.069 ns | 1,000.13 ns | 990.57 ns | 1,018.43 ns |     - |     - |     - |         - |
