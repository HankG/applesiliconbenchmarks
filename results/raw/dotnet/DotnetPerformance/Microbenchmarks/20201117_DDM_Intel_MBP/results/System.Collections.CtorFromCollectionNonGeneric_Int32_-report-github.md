``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|----------- |----- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|-------:|------:|----------:|
|  ArrayList |  512 | 24.36 μs | 0.462 μs | 0.432 μs | 24.36 μs | 23.80 μs | 25.15 μs |  4.8642 | 0.0918 |     - |  20.08 KB |
|  Hashtable |  512 | 25.04 μs | 0.462 μs | 0.433 μs | 24.92 μs | 24.49 μs | 25.98 μs | 10.2163 | 2.0032 |     - |  41.98 KB |
|      Queue |  512 | 36.26 μs | 0.582 μs | 0.516 μs | 36.15 μs | 35.67 μs | 37.39 μs |  3.8793 | 0.1437 |     - |   16.1 KB |
|      Stack |  512 | 35.77 μs | 0.634 μs | 0.593 μs | 35.48 μs | 35.04 μs | 36.83 μs |  3.8007 | 0.1408 |     - |  16.09 KB |
| SortedList |  512 | 84.47 μs | 1.179 μs | 1.103 μs | 84.33 μs | 83.18 μs | 86.73 μs | 10.6952 | 1.6711 |     - |  44.13 KB |
