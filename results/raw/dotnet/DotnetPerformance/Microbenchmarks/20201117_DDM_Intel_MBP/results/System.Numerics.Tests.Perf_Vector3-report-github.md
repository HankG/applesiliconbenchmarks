``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                               Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                     CreateFromScalar | 0.3260 ns | 0.0184 ns | 0.0172 ns | 0.3206 ns | 0.3053 ns | 0.3600 ns |     - |     - |     - |         - |
| CreateFromVector2WithScalarBenchmark | 0.1744 ns | 0.0141 ns | 0.0132 ns | 0.1719 ns | 0.1532 ns | 0.1946 ns |     - |     - |     - |         - |
|         CreateFromScalarXYZBenchmark | 0.2847 ns | 0.0174 ns | 0.0155 ns | 0.2786 ns | 0.2644 ns | 0.3164 ns |     - |     - |     - |         - |
|                         OneBenchmark | 0.0707 ns | 0.0151 ns | 0.0141 ns | 0.0660 ns | 0.0548 ns | 0.0948 ns |     - |     - |     - |         - |
|                       UnitXBenchmark | 0.1313 ns | 0.0192 ns | 0.0170 ns | 0.1274 ns | 0.1113 ns | 0.1642 ns |     - |     - |     - |         - |
|                       UnitYBenchmark | 0.1695 ns | 0.0161 ns | 0.0150 ns | 0.1638 ns | 0.1488 ns | 0.1965 ns |     - |     - |     - |         - |
|                       UnitZBenchmark | 0.1885 ns | 0.0222 ns | 0.0207 ns | 0.1820 ns | 0.1582 ns | 0.2230 ns |     - |     - |     - |         - |
|                        ZeroBenchmark | 0.1649 ns | 0.0168 ns | 0.0140 ns | 0.1619 ns | 0.1430 ns | 0.2003 ns |     - |     - |     - |         - |
|                 AddOperatorBenchmark | 0.6238 ns | 0.0390 ns | 0.0365 ns | 0.6121 ns | 0.5776 ns | 0.7079 ns |     - |     - |     - |         - |
|     DivideByVector3OperatorBenchmark | 0.7213 ns | 0.0277 ns | 0.0259 ns | 0.7105 ns | 0.6964 ns | 0.7758 ns |     - |     - |     - |         - |
|      DivideByScalarOperatorBenchmark | 0.5137 ns | 0.0258 ns | 0.0241 ns | 0.5064 ns | 0.4840 ns | 0.5545 ns |     - |     - |     - |         - |
|            EqualityOperatorBenchmark | 0.8246 ns | 0.0226 ns | 0.0212 ns | 0.8179 ns | 0.8011 ns | 0.8684 ns |     - |     - |     - |         - |
|          InequalityOperatorBenchmark | 0.8339 ns | 0.0260 ns | 0.0243 ns | 0.8265 ns | 0.8069 ns | 0.8831 ns |     - |     - |     - |         - |
|            MultiplyOperatorBenchmark | 0.7443 ns | 0.0334 ns | 0.0296 ns | 0.7341 ns | 0.7146 ns | 0.8127 ns |     - |     - |     - |         - |
|    MultiplyByScalarOperatorBenchmark | 0.3526 ns | 0.0193 ns | 0.0180 ns | 0.3448 ns | 0.3344 ns | 0.3890 ns |     - |     - |     - |         - |
|            SubtractOperatorBenchmark | 0.7486 ns | 0.0239 ns | 0.0223 ns | 0.7530 ns | 0.7118 ns | 0.7882 ns |     - |     - |     - |         - |
|              NegateOperatorBenchmark | 0.4470 ns | 0.0281 ns | 0.0249 ns | 0.4377 ns | 0.4091 ns | 0.5088 ns |     - |     - |     - |         - |
|                         AbsBenchmark | 0.4397 ns | 0.0189 ns | 0.0176 ns | 0.4425 ns | 0.4135 ns | 0.4678 ns |     - |     - |     - |         - |
|                 AddFunctionBenchmark | 0.7581 ns | 0.0309 ns | 0.0274 ns | 0.7506 ns | 0.7154 ns | 0.8116 ns |     - |     - |     - |         - |
|                       ClampBenchmark | 0.7438 ns | 0.0267 ns | 0.0236 ns | 0.7407 ns | 0.7146 ns | 0.7896 ns |     - |     - |     - |         - |
|                       CrossBenchmark | 1.4500 ns | 0.0371 ns | 0.0329 ns | 1.4444 ns | 1.3947 ns | 1.5271 ns |     - |     - |     - |         - |
|                    DistanceBenchmark | 1.2175 ns | 0.0352 ns | 0.0329 ns | 1.2092 ns | 1.1750 ns | 1.2813 ns |     - |     - |     - |         - |
|             DistanceSquaredBenchmark | 0.6111 ns | 0.0284 ns | 0.0252 ns | 0.6067 ns | 0.5826 ns | 0.6714 ns |     - |     - |     - |         - |
|             DivideByVector3Benchmark | 0.8748 ns | 0.0251 ns | 0.0235 ns | 0.8664 ns | 0.8421 ns | 0.9205 ns |     - |     - |     - |         - |
|              DivideByScalarBenchmark | 7.5665 ns | 0.0932 ns | 0.0872 ns | 7.5434 ns | 7.4495 ns | 7.7107 ns |     - |     - |     - |         - |
|                         DotBenchmark | 0.6170 ns | 0.0205 ns | 0.0182 ns | 0.6160 ns | 0.5973 ns | 0.6543 ns |     - |     - |     - |         - |
|                      EqualsBenchmark | 0.8114 ns | 0.0257 ns | 0.0241 ns | 0.8046 ns | 0.7854 ns | 0.8528 ns |     - |     - |     - |         - |
|                 GetHashCodeBenchmark | 3.2286 ns | 0.0434 ns | 0.0406 ns | 3.2225 ns | 3.1693 ns | 3.3019 ns |     - |     - |     - |         - |
|                      LengthBenchmark | 0.2571 ns | 0.0227 ns | 0.0212 ns | 0.2529 ns | 0.2285 ns | 0.2959 ns |     - |     - |     - |         - |
|               LengthSquaredBenchmark | 0.1689 ns | 0.0191 ns | 0.0178 ns | 0.1647 ns | 0.1459 ns | 0.2003 ns |     - |     - |     - |         - |
|                        LerpBenchmark | 7.1189 ns | 0.0757 ns | 0.0709 ns | 7.1098 ns | 7.0224 ns | 7.2487 ns |     - |     - |     - |         - |
|                         MaxBenchmark | 0.7481 ns | 0.0268 ns | 0.0250 ns | 0.7399 ns | 0.7083 ns | 0.8011 ns |     - |     - |     - |         - |
|                         MinBenchmark | 0.7372 ns | 0.0296 ns | 0.0277 ns | 0.7394 ns | 0.7033 ns | 0.7856 ns |     - |     - |     - |         - |
|            MultiplyFunctionBenchmark | 0.7457 ns | 0.0298 ns | 0.0279 ns | 0.7461 ns | 0.7033 ns | 0.7943 ns |     - |     - |     - |         - |
|            MultiplyByScalarBenchmark | 5.5644 ns | 0.1233 ns | 0.1154 ns | 5.5113 ns | 5.4308 ns | 5.7600 ns |     - |     - |     - |         - |
|                      NegateBenchmark | 5.4835 ns | 0.0865 ns | 0.0767 ns | 5.4533 ns | 5.4008 ns | 5.6311 ns |     - |     - |     - |         - |
|                   NormalizeBenchmark | 1.4588 ns | 0.0350 ns | 0.0327 ns | 1.4488 ns | 1.4232 ns | 1.5344 ns |     - |     - |     - |         - |
|                     ReflectBenchmark | 1.1932 ns | 0.0294 ns | 0.0275 ns | 1.1939 ns | 1.1568 ns | 1.2320 ns |     - |     - |     - |         - |
|                  SquareRootBenchmark | 0.4792 ns | 0.0238 ns | 0.0211 ns | 0.4709 ns | 0.4570 ns | 0.5300 ns |     - |     - |     - |         - |
|            SubtractFunctionBenchmark | 0.7473 ns | 0.0184 ns | 0.0172 ns | 0.7466 ns | 0.7217 ns | 0.7842 ns |     - |     - |     - |         - |
|        TransformByMatrix4x4Benchmark | 6.1963 ns | 0.0617 ns | 0.0577 ns | 6.1737 ns | 6.1147 ns | 6.2992 ns |     - |     - |     - |         - |
|       TransformByQuaternionBenchmark | 5.1888 ns | 0.1055 ns | 0.0987 ns | 5.1627 ns | 5.0576 ns | 5.3700 ns |     - |     - |     - |         - |
|  TransformNormalByMatrix4x4Benchmark | 5.7042 ns | 0.0653 ns | 0.0579 ns | 5.6919 ns | 5.6201 ns | 5.8003 ns |     - |     - |     - |         - |
