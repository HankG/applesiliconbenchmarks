``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| GetReference | 0.2083 ns | 0.0185 ns | 0.0173 ns | 0.2004 ns | 0.1910 ns | 0.2391 ns |     - |     - |     - |         - |
|      AsBytes | 0.4507 ns | 0.0256 ns | 0.0227 ns | 0.4428 ns | 0.4271 ns | 0.5039 ns |     - |     - |     - |         - |
|   CastToByte | 0.4766 ns | 0.0212 ns | 0.0198 ns | 0.4706 ns | 0.4530 ns | 0.5090 ns |     - |     - |     - |         - |
|    CastToInt | 0.2624 ns | 0.0194 ns | 0.0182 ns | 0.2570 ns | 0.2405 ns | 0.2963 ns |     - |     - |     - |         - |
|  TryGetArray | 3.7876 ns | 0.1061 ns | 0.1222 ns | 3.7464 ns | 3.6604 ns | 4.0443 ns |     - |     - |     - |         - |
|         Read | 1.4279 ns | 0.0307 ns | 0.0328 ns | 1.4239 ns | 1.3844 ns | 1.5004 ns |     - |     - |     - |         - |
