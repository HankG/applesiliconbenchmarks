``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |  value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **Parse** | **-32768** | **14.394 ns** | **0.2077 ns** | **0.1943 ns** | **14.382 ns** | **13.989 ns** | **14.637 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse | -32768 | 15.147 ns | 0.2078 ns | 0.1842 ns | 15.141 ns | 14.857 ns | 15.439 ns |      - |     - |     - |         - |
| ToString | -32768 | 23.790 ns | 0.4398 ns | 0.3899 ns | 23.753 ns | 23.012 ns | 24.455 ns | 0.0096 |     - |     - |      40 B |
|    **Parse** |      **0** | **10.575 ns** | **0.1399 ns** | **0.1309 ns** | **10.550 ns** | **10.356 ns** | **10.811 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |      0 | 10.744 ns | 0.1473 ns | 0.1306 ns | 10.725 ns | 10.562 ns | 11.029 ns |      - |     - |     - |         - |
| ToString |      0 |  3.135 ns | 0.0803 ns | 0.0751 ns |  3.113 ns |  3.036 ns |  3.277 ns |      - |     - |     - |         - |
|    **Parse** |  **32767** | **13.424 ns** | **0.2101 ns** | **0.1862 ns** | **13.398 ns** | **13.210 ns** | **13.781 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |  32767 | 14.168 ns | 0.1954 ns | 0.1828 ns | 14.144 ns | 13.960 ns | 14.492 ns |      - |     - |     - |         - |
| ToString |  32767 | 14.096 ns | 0.3290 ns | 0.3077 ns | 14.023 ns | 13.690 ns | 14.707 ns | 0.0076 |     - |     - |      32 B |
