``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 3,835.1 ns | 48.81 ns | 45.66 ns | 3,841.9 ns | 3,771.6 ns | 3,926.6 ns | 1.0185 |     - |     - |    4304 B |
|    protobuf-net |   615.9 ns | 11.36 ns | 11.15 ns |   613.8 ns |   602.9 ns |   644.0 ns | 0.0488 |     - |     - |     208 B |
|     MessagePack |   285.4 ns |  3.57 ns |  3.34 ns |   284.2 ns |   281.5 ns |   293.4 ns |      - |     - |     - |         - |
