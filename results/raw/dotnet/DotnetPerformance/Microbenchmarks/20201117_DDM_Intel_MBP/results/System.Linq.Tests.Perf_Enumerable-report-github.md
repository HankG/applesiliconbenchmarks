``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                  Method |              input |         Mean |       Error |      StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------------------- |------------------- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|                                   **Range** |                  **?** |   **396.156 ns** |   **4.2207 ns** |   **3.9480 ns** |   **394.879 ns** |   **391.770 ns** |   **403.004 ns** | **0.0095** |     **-** |     **-** |      **40 B** |
|                                  Repeat |                  ? |   414.734 ns |   5.0189 ns |   4.4491 ns |   412.760 ns |   408.462 ns |   423.670 ns | 0.0066 |     - |     - |      32 B |
|                  EmptyTakeSelectToArray |                  ? |    25.296 ns |   0.3439 ns |   0.3049 ns |    25.257 ns |    24.780 ns |    25.760 ns |      - |     - |     - |         - |
|                                  **Select** |              **Array** |   **580.496 ns** |   **6.7695 ns** |   **6.3322 ns** |   **578.642 ns** |   **572.036 ns** |   **592.276 ns** | **0.0092** |     **-** |     **-** |      **48 B** |
|                                   Where |              Array |   588.251 ns |   5.5384 ns |   4.9096 ns |   587.513 ns |   581.728 ns |   597.387 ns | 0.0094 |     - |     - |      48 B |
|                             WhereSelect |              Array |   759.730 ns |  13.4491 ns |  11.9223 ns |   756.485 ns |   746.378 ns |   785.071 ns | 0.0240 |     - |     - |     104 B |
|           WhereFirst_LastElementMatches |              Array |   252.044 ns |   3.9028 ns |   3.2591 ns |   251.539 ns |   247.012 ns |   257.234 ns | 0.0111 |     - |     - |      48 B |
|            WhereLast_LastElementMatches |              Array |   192.815 ns |   2.9097 ns |   2.5794 ns |   192.189 ns |   190.126 ns |   199.211 ns | 0.0109 |     - |     - |      48 B |
|   FirstWithPredicate_LastElementMatches |              Array |   497.496 ns |   5.1586 ns |   4.8253 ns |   497.137 ns |   491.125 ns |   506.117 ns | 0.0060 |     - |     - |      32 B |
|             WhereAny_LastElementMatches |              Array |   234.310 ns |   3.5856 ns |   3.3540 ns |   233.162 ns |   230.142 ns |   239.923 ns | 0.0113 |     - |     - |      48 B |
|     AnyWithPredicate_LastElementMatches |              Array |   520.820 ns |   5.1199 ns |   4.5387 ns |   519.839 ns |   515.195 ns |   528.964 ns | 0.0063 |     - |     - |      32 B |
|          WhereSingle_LastElementMatches |              Array |   233.177 ns |   3.7290 ns |   3.4881 ns |   231.414 ns |   229.347 ns |   239.319 ns | 0.0113 |     - |     - |      48 B |
| WhereSingleOrDefault_LastElementMatches |              Array |   222.344 ns |   3.5907 ns |   3.3588 ns |   221.885 ns |   217.801 ns |   227.684 ns | 0.0108 |     - |     - |      48 B |
|  SingleWithPredicate_LastElementMatches |              Array |   477.983 ns |   6.1310 ns |   5.4349 ns |   475.841 ns |   471.222 ns |   486.795 ns | 0.0076 |     - |     - |      32 B |
| SingleWithPredicate_FirstElementMatches |              Array |   507.792 ns |   5.6467 ns |   5.2820 ns |   506.768 ns |   501.120 ns |   516.723 ns | 0.0060 |     - |     - |      32 B |
|                           SelectToArray |              Array |   218.612 ns |   4.4204 ns |   4.9133 ns |   217.936 ns |   212.790 ns |   230.893 ns | 0.1124 |     - |     - |     472 B |
|                            SelectToList |              Array |   230.337 ns |   4.2777 ns |   4.0014 ns |   229.060 ns |   225.240 ns |   237.281 ns | 0.1202 |     - |     - |     504 B |
|                            ToDictionary |              Array |   773.170 ns |  12.6442 ns |  11.8274 ns |   768.031 ns |   757.680 ns |   793.435 ns | 0.5422 |     - |     - |    2272 B |
|                                 **ToArray** |        **ICollection** |    **41.954 ns** |   **0.5789 ns** |   **0.4834 ns** |    **41.825 ns** |    **41.161 ns** |    **43.140 ns** | **0.1013** |     **-** |     **-** |     **424 B** |
|                                  ToList |        ICollection |    47.491 ns |   1.7025 ns |   1.9606 ns |    47.257 ns |    43.801 ns |    51.585 ns | 0.1089 |     - |     - |     456 B |
|                Contains_ElementNotFound |        ICollection |    25.424 ns |   0.1358 ns |   0.1060 ns |    25.428 ns |    25.261 ns |    25.601 ns |      - |     - |     - |         - |
|                                   Count |        ICollection |     3.884 ns |   0.0580 ns |   0.0543 ns |     3.879 ns |     3.768 ns |     3.998 ns |      - |     - |     - |         - |
|                                  **Select** |        **IEnumerable** |   **928.386 ns** |  **12.9877 ns** |  **12.1487 ns** |   **925.259 ns** |   **914.882 ns** |   **951.193 ns** | **0.0186** |     **-** |     **-** |      **88 B** |
|                                   Where |        IEnumerable |   948.297 ns |  12.3102 ns |  10.9127 ns |   945.456 ns |   936.044 ns |   969.791 ns | 0.0188 |     - |     - |      88 B |
|                             WhereSelect |        IEnumerable | 1,107.274 ns |  11.8606 ns |  11.0944 ns | 1,106.175 ns | 1,090.941 ns | 1,127.633 ns | 0.0349 |     - |     - |     152 B |
|           WhereFirst_LastElementMatches |        IEnumerable |   604.001 ns |   8.1731 ns |   7.6451 ns |   603.307 ns |   591.622 ns |   617.143 ns | 0.0194 |     - |     - |      88 B |
|            WhereLast_LastElementMatches |        IEnumerable |   586.514 ns |   6.2033 ns |   5.4991 ns |   587.032 ns |   578.814 ns |   598.057 ns | 0.0187 |     - |     - |      88 B |
|   FirstWithPredicate_LastElementMatches |        IEnumerable |   478.601 ns |   6.2189 ns |   5.8171 ns |   475.994 ns |   472.116 ns |   488.187 ns | 0.0058 |     - |     - |      32 B |
|   LastWithPredicate_FirstElementMatches |        IEnumerable |   533.932 ns |  10.6544 ns |  10.4641 ns |   530.925 ns |   522.479 ns |   556.269 ns | 0.0064 |     - |     - |      32 B |
|             WhereAny_LastElementMatches |        IEnumerable |   609.681 ns |   6.4959 ns |   5.7585 ns |   608.898 ns |   601.316 ns |   620.812 ns | 0.0194 |     - |     - |      88 B |
|     AnyWithPredicate_LastElementMatches |        IEnumerable |   526.366 ns |   5.5860 ns |   5.2251 ns |   524.170 ns |   520.114 ns |   534.117 ns | 0.0063 |     - |     - |      32 B |
|                    All_AllElementsMatch |        IEnumerable |   502.450 ns |   6.1566 ns |   5.7589 ns |   500.366 ns |   495.207 ns |   513.788 ns | 0.0060 |     - |     - |      32 B |
|          WhereSingle_LastElementMatches |        IEnumerable |   593.758 ns |   6.1542 ns |   5.4555 ns |   594.465 ns |   584.641 ns |   605.462 ns | 0.0188 |     - |     - |      88 B |
| WhereSingleOrDefault_LastElementMatches |        IEnumerable |   606.456 ns |  11.7707 ns |  12.5946 ns |   600.430 ns |   592.463 ns |   638.708 ns | 0.0192 |     - |     - |      88 B |
|  SingleWithPredicate_LastElementMatches |        IEnumerable |   501.196 ns |   6.2888 ns |   5.8825 ns |   500.720 ns |   493.439 ns |   510.837 ns | 0.0060 |     - |     - |      32 B |
| SingleWithPredicate_FirstElementMatches |        IEnumerable |   520.853 ns |   4.6586 ns |   3.8902 ns |   520.603 ns |   515.427 ns |   529.358 ns | 0.0063 |     - |     - |      32 B |
|                         CastToBaseClass |        IEnumerable | 2,154.400 ns |  33.6836 ns |  29.8596 ns | 2,151.059 ns | 2,114.479 ns | 2,217.650 ns | 0.5927 |     - |     - |    2488 B |
|                          CastToSameType |        IEnumerable |   433.228 ns |   6.0020 ns |   5.0119 ns |   432.416 ns |   428.055 ns |   445.267 ns | 0.0069 |     - |     - |      32 B |
|                                 OrderBy |        IEnumerable | 3,675.610 ns |  60.8081 ns |  56.8799 ns | 3,653.608 ns | 3,606.146 ns | 3,783.145 ns | 0.5414 |     - |     - |    2272 B |
|                       OrderByDescending |        IEnumerable | 5,000.089 ns |  81.4606 ns |  76.1983 ns | 4,980.961 ns | 4,890.259 ns | 5,143.111 ns | 0.5318 |     - |     - |    2272 B |
|                           OrderByThenBy |        IEnumerable | 3,913.321 ns |  70.2793 ns |  65.7393 ns | 3,884.675 ns | 3,833.981 ns | 4,046.863 ns | 0.6647 |     - |     - |    2808 B |
|                                 Reverse |        IEnumerable | 1,067.910 ns |  18.4585 ns |  17.2661 ns | 1,060.288 ns | 1,043.341 ns | 1,096.513 ns | 0.2939 |     - |     - |    1232 B |
|                                Skip_One |        IEnumerable |   946.409 ns |  11.3781 ns |   9.5012 ns |   943.657 ns |   935.974 ns |   966.148 ns | 0.0188 |     - |     - |      88 B |
|                                Take_All |        IEnumerable |   874.621 ns |  11.4059 ns |  10.6691 ns |   871.205 ns |   863.832 ns |   896.857 ns | 0.0210 |     - |     - |      88 B |
|                            TakeLastHalf |        IEnumerable | 1,353.430 ns |  20.5280 ns |  19.2019 ns | 1,351.718 ns | 1,326.122 ns | 1,382.743 ns | 0.1784 |     - |     - |     752 B |
|                        SkipHalfTakeHalf |        IEnumerable |   628.245 ns |   8.1826 ns |   7.6540 ns |   626.545 ns |   617.764 ns |   645.254 ns | 0.0323 |     - |     - |     144 B |
|                                 ToArray |        IEnumerable |   690.090 ns |  13.5919 ns |  12.7139 ns |   685.398 ns |   675.193 ns |   713.798 ns | 0.2816 |     - |     - |    1184 B |
|                           SelectToArray |        IEnumerable |   855.966 ns |  16.5872 ns |  16.2909 ns |   851.176 ns |   835.609 ns |   888.608 ns | 0.2954 |     - |     - |    1240 B |
|                                  ToList |        IEnumerable |   610.453 ns |  10.5419 ns |   9.8609 ns |   609.047 ns |   597.469 ns |   633.708 ns | 0.2894 |     - |     - |    1216 B |
|                            SelectToList |        IEnumerable |   770.861 ns |  13.7592 ns |  12.8703 ns |   768.169 ns |   754.690 ns |   796.515 ns | 0.3031 |     - |     - |    1272 B |
|                            ToDictionary |        IEnumerable | 1,887.125 ns |  16.8070 ns |  15.7213 ns | 1,892.929 ns | 1,857.662 ns | 1,910.902 ns | 1.7701 |     - |     - |    7424 B |
|                Contains_ElementNotFound |        IEnumerable |   437.210 ns |   5.9734 ns |   5.2953 ns |   436.043 ns |   430.423 ns |   447.721 ns | 0.0070 |     - |     - |      32 B |
|                             Concat_Once |        IEnumerable | 1,629.857 ns |  23.0321 ns |  20.4174 ns | 1,626.434 ns | 1,600.631 ns | 1,670.286 ns | 0.0257 |     - |     - |     120 B |
|                         Concat_TenTimes |        IEnumerable | 8,857.489 ns | 109.9361 ns | 102.8343 ns | 8,826.423 ns | 8,712.959 ns | 9,034.988 ns | 0.2113 |     - |     - |     984 B |
|                                     Sum |        IEnumerable |   429.089 ns |   5.6353 ns |   5.2712 ns |   427.802 ns |   424.128 ns |   439.809 ns | 0.0069 |     - |     - |      32 B |
|                                     Min |        IEnumerable |   452.547 ns |   5.3844 ns |   4.7732 ns |   451.676 ns |   446.287 ns |   461.419 ns | 0.0072 |     - |     - |      32 B |
|                                     Max |        IEnumerable |   429.536 ns |   5.8701 ns |   4.9018 ns |   428.368 ns |   423.568 ns |   439.276 ns | 0.0069 |     - |     - |      32 B |
|                                 Average |        IEnumerable |   451.590 ns |   3.5520 ns |   2.9661 ns |   451.808 ns |   447.119 ns |   457.726 ns | 0.0072 |     - |     - |      32 B |
|                                   Count |        IEnumerable |   278.386 ns |   3.5163 ns |   3.2891 ns |   277.856 ns |   274.178 ns |   285.644 ns | 0.0066 |     - |     - |      32 B |
|                               Aggregate |        IEnumerable |   508.954 ns |   6.2905 ns |   5.5764 ns |   506.857 ns |   502.130 ns |   522.245 ns | 0.0061 |     - |     - |      32 B |
|                          Aggregate_Seed |        IEnumerable |   477.365 ns |   5.6166 ns |   5.2537 ns |   476.321 ns |   472.003 ns |   488.594 ns | 0.0076 |     - |     - |      32 B |
|                                Distinct |        IEnumerable | 2,423.764 ns |  47.8199 ns |  55.0695 ns | 2,413.409 ns | 2,351.506 ns | 2,544.928 ns | 1.0279 |     - |     - |    4312 B |
|                               ElementAt |        IEnumerable |   131.967 ns |   2.5621 ns |   2.3966 ns |   132.318 ns |   128.012 ns |   135.409 ns | 0.0075 |     - |     - |      32 B |
|                                 GroupBy |        IEnumerable | 2,508.872 ns |  33.2004 ns |  31.0556 ns | 2,506.655 ns | 2,454.765 ns | 2,558.963 ns | 0.8118 |     - |     - |    3432 B |
|                                     Zip |        IEnumerable | 1,362.420 ns |  17.9300 ns |  16.7717 ns | 1,358.099 ns | 1,341.055 ns | 1,400.879 ns | 0.0377 |     - |     - |     160 B |
|                               Intersect |        IEnumerable | 3,769.773 ns |  80.5851 ns |  89.5701 ns | 3,738.352 ns | 3,660.437 ns | 3,999.757 ns | 1.0347 |     - |     - |    4376 B |
|                                  Except |        IEnumerable | 3,155.205 ns |  59.1769 ns |  52.4587 ns | 3,142.005 ns | 3,090.356 ns | 3,262.983 ns | 1.0438 |     - |     - |    4376 B |
|                                  Append |        IEnumerable | 4,083.418 ns |  64.6030 ns |  53.9464 ns | 4,063.927 ns | 4,015.615 ns | 4,209.031 ns | 2.7810 |     - |     - |   11664 B |
|                                 Prepend |        IEnumerable | 3,633.377 ns |  70.7295 ns |  75.6798 ns | 3,607.633 ns | 3,544.054 ns | 3,799.999 ns | 2.6782 |     - |     - |   11208 B |
|                           AppendPrepend |        IEnumerable | 3,897.763 ns |  72.8811 ns |  64.6072 ns | 3,882.041 ns | 3,822.354 ns | 4,029.649 ns | 2.7373 |     - |     - |   11464 B |
|                                  **Select** |              **IList** |   **937.377 ns** |  **16.6585 ns** |  **14.7673 ns** |   **935.767 ns** |   **914.812 ns** |   **970.077 ns** | **0.0186** |     **-** |     **-** |      **88 B** |
|   LastWithPredicate_FirstElementMatches |              IList |    12.408 ns |   0.1528 ns |   0.1429 ns |    12.359 ns |    12.227 ns |    12.748 ns |      - |     - |     - |         - |
|                           SelectToArray |              IList |   406.423 ns |   6.7736 ns |   6.3360 ns |   404.377 ns |   398.485 ns |   416.171 ns | 0.1144 |     - |     - |     480 B |
|                            SelectToList |              IList |   483.187 ns |   8.5864 ns |   8.0317 ns |   483.758 ns |   472.647 ns |   501.571 ns | 0.1220 |     - |     - |     512 B |
|                               ElementAt |              IList |     7.673 ns |   0.0917 ns |   0.0813 ns |     7.652 ns |     7.524 ns |     7.819 ns |      - |     - |     - |         - |
|   **FirstWithPredicate_LastElementMatches** | **IOrderedEnumerable** | **3,212.619 ns** |  **44.5229 ns** |  **41.6468 ns** | **3,206.577 ns** | **3,154.881 ns** | **3,300.881 ns** | **0.3354** |     **-** |     **-** |    **1456 B** |
|   LastWithPredicate_FirstElementMatches | IOrderedEnumerable | 1,047.518 ns |  13.6020 ns |  12.7233 ns | 1,043.573 ns | 1,032.385 ns | 1,068.978 ns | 0.0167 |     - |     - |      72 B |
|     AnyWithPredicate_LastElementMatches | IOrderedEnumerable | 3,279.055 ns |  54.1032 ns |  50.6081 ns | 3,255.494 ns | 3,218.220 ns | 3,371.213 ns | 0.3354 |     - |     - |    1456 B |
|                                  **Select** |               **List** |   **667.895 ns** |   **8.5960 ns** |   **8.0407 ns** |   **664.912 ns** |   **656.266 ns** |   **681.388 ns** | **0.0159** |     **-** |     **-** |      **72 B** |
|                                   Where |               List |   785.616 ns |  12.7031 ns |  11.2610 ns |   784.077 ns |   766.328 ns |   805.483 ns | 0.0156 |     - |     - |      72 B |
|                             WhereSelect |               List |   913.905 ns |   8.7848 ns |   7.3357 ns |   912.634 ns |   905.570 ns |   929.949 ns | 0.0328 |     - |     - |     152 B |
|           WhereFirst_LastElementMatches |               List |   447.114 ns |   5.4141 ns |   5.0643 ns |   447.298 ns |   439.517 ns |   457.918 ns | 0.0163 |     - |     - |      72 B |
|            WhereLast_LastElementMatches |               List |   433.469 ns |   4.0785 ns |   3.6155 ns |   433.863 ns |   426.916 ns |   438.494 ns | 0.0172 |     - |     - |      72 B |
|   FirstWithPredicate_LastElementMatches |               List |   732.527 ns |   8.8413 ns |   7.8376 ns |   730.970 ns |   721.758 ns |   747.792 ns | 0.0087 |     - |     - |      40 B |
|             WhereAny_LastElementMatches |               List |   372.703 ns |   3.4971 ns |   2.7303 ns |   372.503 ns |   367.390 ns |   377.212 ns | 0.0164 |     - |     - |      72 B |
|     AnyWithPredicate_LastElementMatches |               List |   773.338 ns |   4.6655 ns |   4.3641 ns |   772.185 ns |   763.689 ns |   779.909 ns | 0.0092 |     - |     - |      40 B |
|          WhereSingle_LastElementMatches |               List |   379.424 ns |   3.8567 ns |   3.4189 ns |   379.246 ns |   374.102 ns |   385.326 ns | 0.0168 |     - |     - |      72 B |
| WhereSingleOrDefault_LastElementMatches |               List |   382.448 ns |   5.0845 ns |   4.7560 ns |   381.344 ns |   375.347 ns |   389.581 ns | 0.0165 |     - |     - |      72 B |
|  SingleWithPredicate_LastElementMatches |               List |   687.578 ns |   8.2644 ns |   7.3262 ns |   685.735 ns |   678.248 ns |   700.943 ns | 0.0082 |     - |     - |      40 B |
| SingleWithPredicate_FirstElementMatches |               List |   692.714 ns |   7.6351 ns |   7.1419 ns |   691.492 ns |   683.024 ns |   706.481 ns | 0.0083 |     - |     - |      40 B |
|                            TakeLastHalf |               List |   408.740 ns |   4.9876 ns |   4.6654 ns |   408.002 ns |   402.134 ns |   417.499 ns | 0.0113 |     - |     - |      48 B |
|                           SelectToArray |               List |   218.995 ns |   4.3900 ns |   4.3116 ns |   218.436 ns |   212.921 ns |   228.239 ns | 0.1179 |     - |     - |     496 B |
|                            SelectToList |               List |   278.666 ns |   4.6826 ns |   4.3801 ns |   277.286 ns |   273.285 ns |   288.420 ns | 0.1256 |     - |     - |     528 B |
|                            ToDictionary |               List |   952.928 ns |  14.1459 ns |  13.2321 ns |   951.808 ns |   934.215 ns |   981.533 ns | 0.5399 |     - |     - |    2272 B |
|                           **SelectToArray** |              **Range** |   **190.967 ns** |   **2.9774 ns** |   **2.6394 ns** |   **190.152 ns** |   **187.547 ns** |   **197.487 ns** | **0.1128** |     **-** |     **-** |     **472 B** |
|                            SelectToList |              Range |   270.781 ns |   5.3687 ns |   5.0219 ns |   269.594 ns |   263.901 ns |   277.943 ns | 0.1195 |     - |     - |     504 B |
