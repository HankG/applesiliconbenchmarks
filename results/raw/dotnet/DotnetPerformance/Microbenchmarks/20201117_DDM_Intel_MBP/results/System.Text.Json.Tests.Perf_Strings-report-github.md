``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method | Formatted | SkipValidation |     Escaped |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |---------- |--------------- |------------ |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|  **WriteStringsUtf8** |     **False** |          **False** |  **AllEscaped** | **53.026 ms** | **0.5802 ms** | **0.4845 ms** | **53.009 ms** | **52.311 ms** | **54.143 ms** |     **-** |     **-** |     **-** |     **240 B** |
| WriteStringsUtf16 |     False |          False |  AllEscaped | 64.578 ms | 0.6145 ms | 0.5748 ms | 64.588 ms | 63.842 ms | 65.734 ms |     - |     - |     - |     264 B |
|  **WriteStringsUtf8** |     **False** |          **False** |  **OneEscaped** | **10.468 ms** | **0.0966 ms** | **0.0904 ms** | **10.483 ms** | **10.326 ms** | **10.598 ms** |     **-** |     **-** |     **-** |     **176 B** |
| WriteStringsUtf16 |     False |          False |  OneEscaped | 13.315 ms | 0.2626 ms | 0.2919 ms | 13.477 ms | 12.843 ms | 13.639 ms |     - |     - |     - |     251 B |
|  **WriteStringsUtf8** |     **False** |          **False** | **NoneEscaped** |  **5.070 ms** | **0.0982 ms** | **0.0964 ms** |  **5.074 ms** |  **4.817 ms** |  **5.217 ms** |     **-** |     **-** |     **-** |     **126 B** |
| WriteStringsUtf16 |     False |          False | NoneEscaped |  8.893 ms | 0.0752 ms | 0.0703 ms |  8.878 ms |  8.806 ms |  9.065 ms |     - |     - |     - |     130 B |
|  **WriteStringsUtf8** |     **False** |           **True** |  **AllEscaped** | **54.445 ms** | **0.7397 ms** | **0.6557 ms** | **54.440 ms** | **53.710 ms** | **55.791 ms** |     **-** |     **-** |     **-** |     **192 B** |
| WriteStringsUtf16 |     False |           True |  AllEscaped | 65.137 ms | 1.0741 ms | 1.0047 ms | 65.146 ms | 63.582 ms | 66.482 ms |     - |     - |     - |     264 B |
|  **WriteStringsUtf8** |     **False** |           **True** |  **OneEscaped** |  **9.482 ms** | **0.1100 ms** | **0.1029 ms** |  **9.529 ms** |  **9.268 ms** |  **9.626 ms** |     **-** |     **-** |     **-** |     **131 B** |
| WriteStringsUtf16 |     False |           True |  OneEscaped | 13.388 ms | 0.1084 ms | 0.1014 ms | 13.363 ms | 13.258 ms | 13.650 ms |     - |     - |     - |     152 B |
|  **WriteStringsUtf8** |     **False** |           **True** | **NoneEscaped** |  **4.921 ms** | **0.1193 ms** | **0.1374 ms** |  **4.934 ms** |  **4.643 ms** |  **5.126 ms** |     **-** |     **-** |     **-** |     **166 B** |
| WriteStringsUtf16 |     False |           True | NoneEscaped |  8.531 ms | 0.0556 ms | 0.0520 ms |  8.520 ms |  8.454 ms |  8.612 ms |     - |     - |     - |     130 B |
|  **WriteStringsUtf8** |      **True** |          **False** |  **AllEscaped** | **54.706 ms** | **0.6715 ms** | **0.6282 ms** | **54.487 ms** | **54.010 ms** | **56.098 ms** |     **-** |     **-** |     **-** |     **192 B** |
| WriteStringsUtf16 |      True |          False |  AllEscaped | 64.904 ms | 0.7803 ms | 0.7299 ms | 64.583 ms | 64.186 ms | 66.510 ms |     - |     - |     - |     264 B |
|  **WriteStringsUtf8** |      **True** |          **False** |  **OneEscaped** | **10.033 ms** | **0.2010 ms** | **0.2315 ms** | **10.123 ms** |  **9.590 ms** | **10.293 ms** |     **-** |     **-** |     **-** |     **199 B** |
| WriteStringsUtf16 |      True |          False |  OneEscaped | 14.235 ms | 0.0920 ms | 0.0860 ms | 14.254 ms | 14.050 ms | 14.364 ms |     - |     - |     - |     152 B |
|  **WriteStringsUtf8** |      **True** |          **False** | **NoneEscaped** |  **5.333 ms** | **0.1017 ms** | **0.1044 ms** |  **5.368 ms** |  **5.085 ms** |  **5.461 ms** |     **-** |     **-** |     **-** |     **126 B** |
| WriteStringsUtf16 |      True |          False | NoneEscaped |  9.410 ms | 0.0383 ms | 0.0358 ms |  9.405 ms |  9.355 ms |  9.471 ms |     - |     - |     - |     131 B |
|  **WriteStringsUtf8** |      **True** |           **True** |  **AllEscaped** | **53.551 ms** | **0.6530 ms** | **0.6108 ms** | **53.448 ms** | **52.723 ms** | **54.613 ms** |     **-** |     **-** |     **-** |     **192 B** |
| WriteStringsUtf16 |      True |           True |  AllEscaped | 63.838 ms | 0.7754 ms | 0.7253 ms | 63.664 ms | 63.046 ms | 65.266 ms |     - |     - |     - |     264 B |
|  **WriteStringsUtf8** |      **True** |           **True** |  **OneEscaped** | **10.754 ms** | **0.0945 ms** | **0.0884 ms** | **10.761 ms** | **10.578 ms** | **10.898 ms** |     **-** |     **-** |     **-** |     **163 B** |
| WriteStringsUtf16 |      True |           True |  OneEscaped | 13.843 ms | 0.1248 ms | 0.1106 ms | 13.870 ms | 13.535 ms | 13.984 ms |     - |     - |     - |     191 B |
|  **WriteStringsUtf8** |      **True** |           **True** | **NoneEscaped** |  **5.278 ms** | **0.0916 ms** | **0.0857 ms** |  **5.299 ms** |  **5.102 ms** |  **5.381 ms** |     **-** |     **-** |     **-** |     **141 B** |
| WriteStringsUtf16 |      True |           True | NoneEscaped |  9.092 ms | 0.0586 ms | 0.0548 ms |  9.096 ms |  8.970 ms |  9.172 ms |     - |     - |     - |     130 B |
