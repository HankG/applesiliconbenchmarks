``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |   level |             file |           Mean |         Error |        StdDev |         Median |            Min |            Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------ |-------- |----------------- |---------------:|--------------:|--------------:|---------------:|---------------:|---------------:|--------:|-------:|------:|----------:|
|      **Compress_WithState** | **Optimal** | **TestDocument.pdf** | **437,302.167 μs** | **7,210.3142 μs** | **6,391.7557 μs** | **435,963.005 μs** | **428,596.251 μs** | **448,713.984 μs** |       **-** |      **-** |     **-** |     **320 B** |
|    Decompress_WithState | Optimal | TestDocument.pdf |     710.073 μs |     8.4154 μs |     7.8718 μs |     707.647 μs |     699.273 μs |     725.489 μs |       - |      - |     - |      33 B |
|   Compress_WithoutState | Optimal | TestDocument.pdf | 434,495.958 μs | 3,386.0991 μs | 3,167.3592 μs | 435,769.537 μs | 429,946.405 μs | 439,034.898 μs |       - |      - |     - |     888 B |
| Decompress_WithoutState | Optimal | TestDocument.pdf |     710.066 μs |     8.3892 μs |     7.8473 μs |     708.743 μs |     698.735 μs |     721.248 μs |       - |      - |     - |       1 B |
|                Compress | Optimal | TestDocument.pdf |      22.481 μs |     0.2142 μs |     0.1789 μs |      22.439 μs |      22.252 μs |      22.884 μs | 15.6250 | 0.0903 |     - |   65688 B |
|              Decompress | Optimal | TestDocument.pdf |     762.656 μs |    13.2060 μs |    11.7068 μs |     758.740 μs |     749.317 μs |     787.764 μs | 14.8810 | 2.9762 |     - |   65689 B |
|      **Compress_WithState** | **Optimal** |      **alice29.txt** | **198,089.184 μs** | **1,470.9762 μs** | **1,228.3318 μs** | **198,168.803 μs** | **196,238.534 μs** | **200,548.119 μs** |       **-** |      **-** |     **-** |     **320 B** |
|    Decompress_WithState | Optimal |      alice29.txt |     418.727 μs |     6.8383 μs |     6.3966 μs |     415.877 μs |     411.463 μs |     431.837 μs |       - |      - |     - |      32 B |
|   Compress_WithoutState | Optimal |      alice29.txt | 198,301.156 μs | 2,042.0418 μs | 1,705.1974 μs | 198,227.820 μs | 195,755.976 μs | 202,487.262 μs |       - |      - |     - |     288 B |
| Decompress_WithoutState | Optimal |      alice29.txt |     419.369 μs |     5.8589 μs |     5.4805 μs |     417.928 μs |     411.751 μs |     430.293 μs |       - |      - |     - |         - |
|                Compress | Optimal |      alice29.txt |      29.168 μs |     0.3127 μs |     0.2772 μs |      29.207 μs |      28.774 μs |      29.737 μs | 15.6250 | 3.1250 |     - |   65688 B |
|              Decompress | Optimal |      alice29.txt |     470.186 μs |     8.3814 μs |     7.8399 μs |     468.512 μs |     460.661 μs |     487.230 μs | 15.1515 | 1.8939 |     - |   65689 B |
|      **Compress_WithState** | **Optimal** |              **sum** |  **45,184.318 μs** |   **729.5697 μs** |   **646.7445 μs** |  **45,010.766 μs** |  **44,447.229 μs** |  **46,657.821 μs** |       **-** |      **-** |     **-** |      **80 B** |
|    Decompress_WithState | Optimal |              sum |     143.120 μs |     1.9743 μs |     1.7502 μs |     142.286 μs |     141.264 μs |     147.010 μs |       - |      - |     - |      32 B |
|   Compress_WithoutState | Optimal |              sum |  45,201.562 μs |   560.7096 μs |   524.4881 μs |  45,046.599 μs |  44,469.394 μs |  46,280.338 μs |       - |      - |     - |      48 B |
| Decompress_WithoutState | Optimal |              sum |     143.033 μs |     1.8351 μs |     1.7165 μs |     142.587 μs |     140.923 μs |     146.966 μs |       - |      - |     - |         - |
|                Compress | Optimal |              sum |       7.198 μs |     0.2092 μs |     0.2410 μs |       7.204 μs |       6.760 μs |       7.772 μs | 15.6183 | 3.1076 |     - |   65688 B |
|              Decompress | Optimal |              sum |     160.325 μs |     3.1471 μs |     2.9438 μs |     159.400 μs |     156.728 μs |     165.197 μs | 15.3061 | 2.5510 |     - |   65688 B |
|      **Compress_WithState** | **Fastest** | **TestDocument.pdf** |     **303.928 μs** |     **4.5900 μs** |     **4.2935 μs** |     **302.560 μs** |     **298.294 μs** |     **313.202 μs** |       **-** |      **-** |     **-** |      **32 B** |
|    Decompress_WithState | Fastest | TestDocument.pdf |     314.471 μs |     3.9767 μs |     3.5252 μs |     313.508 μs |     309.334 μs |     322.007 μs |       - |      - |     - |      32 B |
|   Compress_WithoutState | Fastest | TestDocument.pdf |     304.151 μs |     3.8035 μs |     3.5578 μs |     304.512 μs |     298.736 μs |     310.830 μs |       - |      - |     - |         - |
| Decompress_WithoutState | Fastest | TestDocument.pdf |     312.991 μs |     4.5265 μs |     3.7798 μs |     312.545 μs |     309.380 μs |     322.148 μs |       - |      - |     - |         - |
|                Compress | Fastest | TestDocument.pdf |     521.664 μs |     6.9318 μs |     6.4840 μs |     521.882 μs |     510.687 μs |     533.728 μs | 14.1129 | 2.0161 |     - |   65689 B |
|              Decompress | Fastest | TestDocument.pdf |     354.094 μs |     4.3428 μs |     4.0622 μs |     353.456 μs |     347.725 μs |     362.116 μs | 15.6250 | 1.4205 |     - |   65688 B |
|      **Compress_WithState** | **Fastest** |      **alice29.txt** |     **750.663 μs** |     **9.1487 μs** |     **8.5577 μs** |     **750.951 μs** |     **736.766 μs** |     **766.681 μs** |       **-** |      **-** |     **-** |      **33 B** |
|    Decompress_WithState | Fastest |      alice29.txt |     461.665 μs |     5.6695 μs |     5.3033 μs |     458.848 μs |     455.000 μs |     469.748 μs |       - |      - |     - |      33 B |
|   Compress_WithoutState | Fastest |      alice29.txt |     747.049 μs |     9.8967 μs |     8.7731 μs |     747.673 μs |     733.712 μs |     764.383 μs |       - |      - |     - |       1 B |
| Decompress_WithoutState | Fastest |      alice29.txt |     463.233 μs |     6.3242 μs |     5.6062 μs |     462.219 μs |     456.724 μs |     475.836 μs |       - |      - |     - |       1 B |
|                Compress | Fastest |      alice29.txt |   1,029.846 μs |    11.2051 μs |     8.7482 μs |   1,032.041 μs |   1,012.456 μs |   1,039.708 μs | 15.6250 |      - |     - |   65689 B |
|              Decompress | Fastest |      alice29.txt |     513.865 μs |     6.6862 μs |     6.2543 μs |     511.395 μs |     506.321 μs |     524.713 μs | 14.1129 | 2.0161 |     - |   65689 B |
|      **Compress_WithState** | **Fastest** |              **sum** |     **137.543 μs** |     **1.9996 μs** |     **1.8705 μs** |     **136.680 μs** |     **135.234 μs** |     **141.282 μs** |       **-** |      **-** |     **-** |      **32 B** |
|    Decompress_WithState | Fastest |              sum |     118.367 μs |     1.8951 μs |     1.7727 μs |     118.108 μs |     116.450 μs |     122.075 μs |       - |      - |     - |      32 B |
|   Compress_WithoutState | Fastest |              sum |     137.507 μs |     2.2762 μs |     1.9007 μs |     137.249 μs |     134.870 μs |     141.286 μs |       - |      - |     - |         - |
| Decompress_WithoutState | Fastest |              sum |     117.873 μs |     1.4984 μs |     1.4016 μs |     117.342 μs |     116.237 μs |     120.581 μs |       - |      - |     - |         - |
|                Compress | Fastest |              sum |     236.619 μs |     3.5893 μs |     3.3574 μs |     237.418 μs |     230.753 μs |     240.806 μs | 15.6250 | 2.7574 |     - |   65688 B |
|              Decompress | Fastest |              sum |     134.179 μs |     1.7986 μs |     1.6824 μs |     134.638 μs |     131.648 μs |     137.077 μs | 15.2311 | 2.6261 |     - |   65688 B |
