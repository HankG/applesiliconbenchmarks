``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                 Clear |  512 |   6.341 ns | 0.0774 ns | 0.0724 ns |   6.361 ns |   6.234 ns |   6.433 ns |      - |     - |     - |         - |
|                  Fill |  512 |   7.863 ns | 0.0876 ns | 0.0820 ns |   7.848 ns |   7.737 ns |   8.022 ns |      - |     - |     - |         - |
|               Reverse |  512 | 122.212 ns | 1.4823 ns | 1.3865 ns | 121.725 ns | 120.512 ns | 124.919 ns |      - |     - |     - |         - |
|               ToArray |  512 |  32.758 ns | 0.7019 ns | 0.7208 ns |  32.606 ns |  31.463 ns |  34.202 ns | 0.1281 |     - |     - |     536 B |
|         SequenceEqual |  512 |   9.736 ns | 0.1404 ns | 0.1313 ns |   9.703 ns |   9.585 ns |   9.991 ns |      - |     - |     - |         - |
|     SequenceCompareTo |  512 |  12.974 ns | 0.1783 ns | 0.1668 ns |  12.924 ns |  12.807 ns |  13.351 ns |      - |     - |     - |         - |
|            StartsWith |  512 |   7.340 ns | 0.1012 ns | 0.0947 ns |   7.306 ns |   7.241 ns |   7.537 ns |      - |     - |     - |         - |
|              EndsWith |  512 |   7.067 ns | 0.0883 ns | 0.0826 ns |   7.061 ns |   6.960 ns |   7.225 ns |      - |     - |     - |         - |
|          IndexOfValue |  512 |   8.197 ns | 0.1495 ns | 0.1325 ns |   8.186 ns |   7.991 ns |   8.455 ns |      - |     - |     - |         - |
|   IndexOfAnyTwoValues |  512 |   8.760 ns | 0.1380 ns | 0.1223 ns |   8.765 ns |   8.569 ns |   8.966 ns |      - |     - |     - |         - |
| IndexOfAnyThreeValues |  512 |  16.660 ns | 0.2278 ns | 0.2019 ns |  16.681 ns |  16.337 ns |  17.014 ns |      - |     - |     - |         - |
|  IndexOfAnyFourValues |  512 |  38.794 ns | 0.3386 ns | 0.3002 ns |  38.763 ns |  38.243 ns |  39.386 ns |      - |     - |     - |         - |
|      LastIndexOfValue |  512 |  15.694 ns | 0.3183 ns | 0.3126 ns |  15.711 ns |  15.293 ns |  16.449 ns |      - |     - |     - |         - |
|  LastIndexOfAnyValues |  512 |  16.856 ns | 0.1883 ns | 0.1761 ns |  16.762 ns |  16.623 ns |  17.125 ns |      - |     - |     - |         - |
|          BinarySearch |  512 |   9.487 ns | 0.1252 ns | 0.1171 ns |   9.525 ns |   9.285 ns |   9.640 ns |      - |     - |     - |         - |
|  GetPinnableReference |  512 |   1.334 ns | 0.0195 ns | 0.0183 ns |   1.327 ns |   1.312 ns |   1.367 ns |      - |     - |     - |         - |
