``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|                        Jil | 35.24 μs | 0.507 μs | 0.475 μs | 35.16 μs | 34.47 μs | 36.15 μs |      - |     - |     - |      96 B |
|                   JSON.NET | 36.54 μs | 0.646 μs | 0.572 μs | 36.41 μs | 35.92 μs | 37.68 μs | 0.4437 |     - |     - |    2448 B |
|                   Utf8Json | 22.60 μs | 0.313 μs | 0.293 μs | 22.53 μs | 22.04 μs | 23.17 μs |      - |     - |     - |         - |
| DataContractJsonSerializer | 89.09 μs | 0.950 μs | 0.888 μs | 88.95 μs | 87.66 μs | 90.66 μs | 0.3551 |     - |     - |    2424 B |
