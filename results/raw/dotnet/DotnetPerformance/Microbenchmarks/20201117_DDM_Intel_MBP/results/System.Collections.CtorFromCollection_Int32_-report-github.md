``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |         Mean |       Error |      StdDev |       Median |          Min |          Max |   Gen 0 |  Gen 1 |  Gen 2 | Allocated |
|-------------------------- |----- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|--------:|-------:|-------:|----------:|
|                      List |  512 |     134.4 ns |     1.64 ns |     1.46 ns |     134.0 ns |     131.5 ns |     136.9 ns |  0.5026 |      - |      - |   2.05 KB |
|                LinkedList |  512 |   8,920.7 ns |   179.01 ns |   191.54 ns |   8,886.9 ns |   8,680.3 ns |   9,310.5 ns |  5.8861 | 0.6064 |      - |  24.07 KB |
|                   HashSet |  512 |   7,255.1 ns |   103.05 ns |    86.06 ns |   7,258.5 ns |   7,155.3 ns |   7,426.0 ns |  2.0060 |      - |      - |    8.3 KB |
|                Dictionary |  512 |   6,106.6 ns |    91.18 ns |    85.29 ns |   6,085.6 ns |   5,979.5 ns |   6,276.7 ns |  2.5098 |      - |      - |   10.3 KB |
|                     Queue |  512 |     138.2 ns |     2.76 ns |     2.84 ns |     137.3 ns |     134.8 ns |     143.2 ns |  0.5047 |      - |      - |   2.06 KB |
|                     Stack |  512 |     140.4 ns |     2.56 ns |     2.27 ns |     140.3 ns |     137.6 ns |     144.9 ns |  0.5025 |      - |      - |   2.05 KB |
|                SortedList |  512 |   8,687.7 ns |   158.16 ns |   140.20 ns |   8,668.4 ns |   8,502.2 ns |   8,947.3 ns |  0.9782 |      - |      - |   4.11 KB |
|                 SortedSet |  512 |   9,704.4 ns |   179.64 ns |   168.04 ns |   9,688.7 ns |   9,479.8 ns |   9,994.6 ns |  5.3866 | 0.6494 |      - |  22.07 KB |
|          SortedDictionary |  512 |  66,287.8 ns | 1,038.34 ns |   971.27 ns |  66,135.1 ns |  64,970.2 ns |  67,916.4 ns |  5.8017 | 0.7911 |      - |  24.16 KB |
|      ConcurrentDictionary |  512 |  50,690.5 ns |   926.55 ns |   866.70 ns |  50,389.4 ns |  49,608.8 ns |  52,473.1 ns | 27.6672 | 6.7675 |      - | 113.53 KB |
|           ConcurrentQueue |  512 |   5,931.0 ns |   118.04 ns |   126.30 ns |   5,908.8 ns |   5,772.8 ns |   6,254.8 ns |  1.0597 |      - |      - |   4.34 KB |
|           ConcurrentStack |  512 |   4,554.5 ns |    80.58 ns |    75.37 ns |   4,535.5 ns |   4,463.6 ns |   4,687.7 ns |  3.9151 | 0.0178 |      - |  16.05 KB |
|             ConcurrentBag |  512 |   8,300.3 ns |   162.15 ns |   159.25 ns |   8,277.8 ns |   8,121.5 ns |   8,551.5 ns |  1.9737 | 0.9868 | 0.0329 |   8.28 KB |
|            ImmutableArray |  512 |     139.5 ns |     2.74 ns |     2.56 ns |     139.2 ns |     135.3 ns |     144.7 ns |  0.4949 |      - |      - |   2.02 KB |
|       ImmutableDictionary |  512 | 127,610.5 ns | 1,598.97 ns | 1,495.68 ns | 127,390.1 ns | 124,626.3 ns | 130,584.3 ns |  6.5524 | 1.0081 |      - |  28.09 KB |
|          ImmutableHashSet |  512 | 115,521.0 ns | 2,273.53 ns | 2,126.66 ns | 115,817.7 ns | 112,900.9 ns | 119,655.5 ns |  6.7935 | 0.9058 |      - |  28.08 KB |
|             ImmutableList |  512 |   9,295.8 ns |   145.22 ns |   135.84 ns |   9,243.1 ns |   9,149.4 ns |   9,570.7 ns |  5.8824 | 0.2206 |      - |  24.05 KB |
|            ImmutableQueue |  512 |   2,847.2 ns |    44.97 ns |    42.06 ns |   2,832.3 ns |   2,792.2 ns |   2,945.3 ns |  3.9202 | 0.0112 |      - |  16.04 KB |
|            ImmutableStack |  512 |   4,374.9 ns |    66.13 ns |    58.62 ns |   4,365.0 ns |   4,298.9 ns |   4,487.4 ns |  3.9149 | 0.0174 |      - |  16.03 KB |
| ImmutableSortedDictionary |  512 |  83,978.4 ns | 1,556.65 ns | 1,456.09 ns |  83,772.4 ns |  82,020.9 ns |  87,272.4 ns | 12.6330 | 0.9973 |      - |  52.56 KB |
|        ImmutableSortedSet |  512 |  16,041.1 ns |   299.44 ns |   294.09 ns |  16,006.9 ns |  15,622.2 ns |  16,680.7 ns |  6.3358 |      - |      - |  26.11 KB |
