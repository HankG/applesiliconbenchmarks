``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                              Method |               kind |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------ |------------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                       **ThrowAndCatch** |           **Software** |  **10.57 μs** | **0.125 μs** | **0.117 μs** |  **10.54 μs** |  **10.41 μs** |  **10.78 μs** | **0.0419** |     **-** |     **-** |     **320 B** |
|       ThrowAndCatch_ManyCatchBlocks |           Software |  10.83 μs | 0.127 μs | 0.118 μs |  10.79 μs |  10.60 μs |  11.00 μs | 0.0429 |     - |     - |     320 B |
|                ThrowAndCatchFinally |           Software |  10.53 μs | 0.141 μs | 0.132 μs |  10.50 μs |  10.29 μs |  10.78 μs | 0.0427 |     - |     - |     320 B |
|                   ThrowAndCatchWhen |           Software |  10.97 μs | 0.207 μs | 0.193 μs |  10.91 μs |  10.68 μs |  11.31 μs | 0.0431 |     - |     - |     320 B |
|            ThrowAndCatchWhenFinally |           Software |  11.14 μs | 0.167 μs | 0.156 μs |  11.11 μs |  10.82 μs |  11.43 μs | 0.0440 |     - |     - |     320 B |
|                   ThrowAndCatchDeep |           Software |  19.69 μs | 0.202 μs | 0.179 μs |  19.71 μs |  19.39 μs |  20.04 μs | 0.3906 |     - |     - |    1736 B |
|          ThrowAndCatchDeepRecursive |           Software |  20.57 μs | 0.276 μs | 0.258 μs |  20.46 μs |  20.23 μs |  21.02 μs | 0.4037 |     - |     - |    1736 B |
| MultipleNestedTryCatch_FirstCatches |           Software |  10.61 μs | 0.207 μs | 0.222 μs |  10.54 μs |  10.32 μs |  10.97 μs | 0.0436 |     - |     - |     320 B |
|  MultipleNestedTryCatch_LastCatches |           Software |  10.79 μs | 0.200 μs | 0.196 μs |  10.71 μs |  10.57 μs |  11.20 μs | 0.0433 |     - |     - |     320 B |
|            MultipleNestedTryFinally |           Software |  10.74 μs | 0.143 μs | 0.127 μs |  10.72 μs |  10.53 μs |  10.97 μs | 0.0422 |     - |     - |     320 B |
|                 CatchAndRethrowDeep |           Software | 236.38 μs | 4.663 μs | 4.789 μs | 234.11 μs | 231.13 μs | 248.61 μs |      - |     - |     - |    1736 B |
|              CatchAndThrowOtherDeep |           Software | 245.00 μs | 4.832 μs | 5.371 μs | 245.03 μs | 238.43 μs | 255.34 μs |      - |     - |     - |    3520 B |
|                   TryAndFinallyDeep |           Software |  21.34 μs | 0.419 μs | 0.449 μs |  21.30 μs |  20.87 μs |  22.30 μs | 0.3378 |     - |     - |    1736 B |
|       TryAndCatchDeep_CaugtAtTheTop |           Software |  20.97 μs | 0.333 μs | 0.312 μs |  20.87 μs |  20.47 μs |  21.54 μs | 0.3351 |     - |     - |    1736 B |
|                       **ThrowAndCatch** |           **Hardware** |  **25.53 μs** | **0.393 μs** | **0.368 μs** |  **25.45 μs** |  **25.04 μs** |  **26.12 μs** |      **-** |     **-** |     **-** |     **320 B** |
|       ThrowAndCatch_ManyCatchBlocks |           Hardware |  25.73 μs | 0.302 μs | 0.283 μs |  25.67 μs |  25.33 μs |  26.30 μs |      - |     - |     - |     320 B |
|                ThrowAndCatchFinally |           Hardware |  25.45 μs | 0.472 μs | 0.442 μs |  25.29 μs |  24.98 μs |  26.35 μs |      - |     - |     - |     320 B |
|                   ThrowAndCatchWhen |           Hardware |  25.34 μs | 0.400 μs | 0.355 μs |  25.40 μs |  24.77 μs |  26.10 μs |      - |     - |     - |     320 B |
|            ThrowAndCatchWhenFinally |           Hardware |  25.05 μs | 0.270 μs | 0.226 μs |  25.06 μs |  24.74 μs |  25.54 μs |      - |     - |     - |     320 B |
|                   ThrowAndCatchDeep |           Hardware |  34.91 μs | 0.454 μs | 0.403 μs |  34.80 μs |  34.43 μs |  35.82 μs | 0.2784 |     - |     - |    1736 B |
|          ThrowAndCatchDeepRecursive |           Hardware |  36.33 μs | 0.635 μs | 0.594 μs |  36.04 μs |  35.72 μs |  37.48 μs | 0.2860 |     - |     - |    1736 B |
| MultipleNestedTryCatch_FirstCatches |           Hardware |  26.29 μs | 0.520 μs | 0.486 μs |  26.12 μs |  25.73 μs |  27.22 μs |      - |     - |     - |     320 B |
|  MultipleNestedTryCatch_LastCatches |           Hardware |  26.10 μs | 0.641 μs | 0.738 μs |  26.15 μs |  25.01 μs |  27.71 μs |      - |     - |     - |     320 B |
|            MultipleNestedTryFinally |           Hardware |  25.34 μs | 0.250 μs | 0.209 μs |  25.30 μs |  25.10 μs |  25.83 μs |      - |     - |     - |     320 B |
|                 CatchAndRethrowDeep |           Hardware | 319.47 μs | 6.275 μs | 6.975 μs | 320.27 μs | 306.97 μs | 331.42 μs |      - |     - |     - |    1736 B |
|              CatchAndThrowOtherDeep |           Hardware | 328.27 μs | 5.878 μs | 5.498 μs | 327.33 μs | 320.13 μs | 339.57 μs |      - |     - |     - |    3520 B |
|                   TryAndFinallyDeep |           Hardware |  36.19 μs | 0.279 μs | 0.217 μs |  36.18 μs |  35.87 μs |  36.48 μs | 0.2880 |     - |     - |    1736 B |
|       TryAndCatchDeep_CaugtAtTheTop |           Hardware |  37.01 μs | 0.655 μs | 0.613 μs |  37.23 μs |  36.11 μs |  38.11 μs | 0.2955 |     - |     - |    1736 B |
|                       **ThrowAndCatch** | **ReflectionSoftware** |  **38.14 μs** | **0.513 μs** | **0.455 μs** |  **38.12 μs** |  **37.48 μs** |  **38.89 μs** | **0.1510** |     **-** |     **-** |     **952 B** |
|       ThrowAndCatch_ManyCatchBlocks | ReflectionSoftware |  38.69 μs | 0.490 μs | 0.459 μs |  38.70 μs |  37.96 μs |  39.35 μs | 0.1543 |     - |     - |     952 B |
|                   ThrowAndCatchDeep | ReflectionSoftware |  48.26 μs | 0.465 μs | 0.435 μs |  48.30 μs |  47.65 μs |  49.03 μs | 0.3894 |     - |     - |    2152 B |
|          ThrowAndCatchDeepRecursive | ReflectionSoftware |  49.62 μs | 0.981 μs | 1.007 μs |  49.38 μs |  48.60 μs |  51.83 μs | 0.3906 |     - |     - |    2152 B |
|                       **ThrowAndCatch** | **ReflectionHardware** |  **60.63 μs** | **0.867 μs** | **0.811 μs** |  **60.30 μs** |  **59.80 μs** |  **62.62 μs** |      **-** |     **-** |     **-** |     **952 B** |
|       ThrowAndCatch_ManyCatchBlocks | ReflectionHardware |  60.02 μs | 0.981 μs | 0.917 μs |  59.77 μs |  58.83 μs |  61.64 μs |      - |     - |     - |     952 B |
|                   ThrowAndCatchDeep | ReflectionHardware |  69.99 μs | 1.176 μs | 1.100 μs |  69.78 μs |  68.57 μs |  71.99 μs | 0.2753 |     - |     - |    2152 B |
|          ThrowAndCatchDeepRecursive | ReflectionHardware |  70.46 μs | 0.830 μs | 0.693 μs |  70.33 μs |  69.70 μs |  72.30 μs | 0.2841 |     - |     - |    2152 B |
