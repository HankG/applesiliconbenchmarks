``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|---------:|--------:|------:|----------:|
| BinaryFormatter | 1,056.9 μs | 15.00 μs | 14.03 μs | 1,052.4 μs | 1,036.9 μs | 1,083.3 μs | 112.5000 | 25.0000 |     - |  491951 B |
|    protobuf-net |   132.3 μs |  2.12 μs |  1.88 μs |   132.7 μs |   129.5 μs |   135.9 μs |        - |       - |     - |     209 B |
|     MessagePack |   168.6 μs |  2.28 μs |  2.02 μs |   167.9 μs |   166.3 μs |   172.4 μs |        - |       - |     - |       1 B |
