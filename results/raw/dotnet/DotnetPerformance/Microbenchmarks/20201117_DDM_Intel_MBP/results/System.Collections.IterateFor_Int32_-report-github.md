``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|              Array |  512 |    216.7 ns |   2.52 ns |   2.35 ns |    215.4 ns |    214.1 ns |    220.4 ns |     - |     - |     - |         - |
|               Span |  512 |    215.7 ns |   2.57 ns |   2.41 ns |    214.9 ns |    213.4 ns |    220.6 ns |     - |     - |     - |         - |
|       ReadOnlySpan |  512 |    214.7 ns |   0.96 ns |   0.80 ns |    214.6 ns |    213.7 ns |    215.8 ns |     - |     - |     - |         - |
|               List |  512 |    239.0 ns |   3.57 ns |   3.34 ns |    237.1 ns |    235.0 ns |    243.4 ns |     - |     - |     - |         - |
|              IList |  512 |  2,003.5 ns |  31.70 ns |  31.14 ns |  1,994.9 ns |  1,959.4 ns |  2,075.4 ns |     - |     - |     - |         - |
|     ImmutableArray |  512 |    216.3 ns |   1.78 ns |   1.67 ns |    216.4 ns |    214.1 ns |    218.7 ns |     - |     - |     - |         - |
|      ImmutableList |  512 | 11,093.6 ns | 137.40 ns | 128.53 ns | 11,048.6 ns | 10,877.3 ns | 11,344.4 ns |     - |     - |     - |         - |
| ImmutableSortedSet |  512 | 11,148.6 ns |  91.22 ns |  76.17 ns | 11,156.0 ns | 10,968.6 ns | 11,283.4 ns |     - |     - |     - |         - |
