``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                        Jil |   644.6 ns |  6.77 ns |  5.29 ns |   645.3 ns |   637.0 ns |   655.1 ns | 0.8469 |     - |     - |    3544 B |
|                   JSON.NET | 1,358.2 ns | 27.15 ns | 26.66 ns | 1,345.2 ns | 1,330.2 ns | 1,405.5 ns | 1.3856 |     - |     - |    5800 B |
|                   Utf8Json |   346.6 ns |  4.89 ns |  4.33 ns |   345.9 ns |   339.2 ns |   353.9 ns | 0.0400 |     - |     - |     168 B |
| DataContractJsonSerializer | 4,125.2 ns | 79.74 ns | 78.31 ns | 4,092.6 ns | 4,045.6 ns | 4,273.3 ns | 3.1396 |     - |     - |   13152 B |
