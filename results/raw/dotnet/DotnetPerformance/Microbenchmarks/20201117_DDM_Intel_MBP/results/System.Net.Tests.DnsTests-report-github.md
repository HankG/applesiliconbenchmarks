``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |         Mean |        Error |        StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |-------------:|-------------:|--------------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
| GetHostEntry | 550,344.5 ns | 90,163.38 ns | 100,216.36 ns | 515,171.0 ns | 444,941.0 ns | 766,515.0 ns |      - |     - |     - |     720 B |
|  GetHostName |     821.6 ns |     13.18 ns |      12.32 ns |     818.6 ns |     806.7 ns |     843.7 ns | 0.0165 |     - |     - |      72 B |
