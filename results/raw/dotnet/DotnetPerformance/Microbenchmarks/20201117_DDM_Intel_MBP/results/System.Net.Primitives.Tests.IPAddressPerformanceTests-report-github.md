``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |                              address |        Mean |     Error |    StdDev |      Median |         Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |------------------------------------- |------------:|----------:|----------:|------------:|------------:|-----------:|-------:|------:|------:|----------:|
| **GetAddressBytes** | **1020:3040:506(...)112:1314:1516 [39]** |  **20.6988 ns** | **0.5447 ns** | **0.6054 ns** |  **20.5062 ns** |  **20.0381 ns** |  **22.007 ns** | **0.0096** |     **-** |     **-** |      **40 B** |
|        ToString | 1020:3040:506(...)112:1314:1516 [39] |   1.1389 ns | 0.0593 ns | 0.0554 ns |   1.1225 ns |   1.0813 ns |   1.267 ns |      - |     - |     - |         - |
|       TryFormat | 1020:3040:506(...)112:1314:1516 [39] | 142.8437 ns | 2.0355 ns | 1.9040 ns | 142.1530 ns | 140.6615 ns | 146.414 ns |      - |     - |     - |         - |
|   TryWriteBytes | 1020:3040:506(...)112:1314:1516 [39] |  11.4048 ns | 0.1762 ns | 0.1648 ns |  11.3552 ns |  11.1994 ns |  11.677 ns |      - |     - |     - |         - |
| **GetAddressBytes** |                         **143.24.20.36** |   **5.5356 ns** | **0.1537 ns** | **0.1437 ns** |   **5.5083 ns** |   **5.3487 ns** |   **5.746 ns** | **0.0076** |     **-** |     **-** |      **32 B** |
|        ToString |                         143.24.20.36 |   0.8970 ns | 0.0843 ns | 0.0828 ns |   0.8958 ns |   0.8110 ns |   1.047 ns |      - |     - |     - |         - |
|       TryFormat |                         143.24.20.36 |  20.2393 ns | 0.1743 ns | 0.1630 ns |  20.2158 ns |  19.9247 ns |  20.458 ns |      - |     - |     - |         - |
|   TryWriteBytes |                         143.24.20.36 |   2.9389 ns | 0.0680 ns | 0.0603 ns |   2.9211 ns |   2.8630 ns |   3.058 ns |      - |     - |     - |         - |
|      **Ctor_Bytes** |                             **Byte[16]** |  **18.7520 ns** | **0.5146 ns** | **0.5926 ns** |  **18.6809 ns** |  **17.9319 ns** |  **19.973 ns** | **0.0191** |     **-** |     **-** |      **80 B** |
|       Ctor_Span |                             Byte[16] |  18.5069 ns | 0.4222 ns | 0.3949 ns |  18.3426 ns |  17.9893 ns |  19.180 ns | 0.0190 |     - |     - |      80 B |
|      **Ctor_Bytes** |                              **Byte[4]** |   **6.6130 ns** | **0.1946 ns** | **0.2082 ns** |   **6.5429 ns** |   **6.3561 ns** |   **7.013 ns** | **0.0095** |     **-** |     **-** |      **40 B** |
|       Ctor_Span |                              Byte[4] |   6.6639 ns | 0.1736 ns | 0.1623 ns |   6.6215 ns |   6.4577 ns |   6.955 ns | 0.0096 |     - |     - |      40 B |
