``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                            Method |  TestCase | numberOfBytes | segmentSize |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------------- |---------- |-------------- |------------ |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|      **ReadSingleSegmentSequenceByN** |   **Json4KB** |          **4096** |           **?** |     **8.016 μs** |  **0.0783 μs** |  **0.0733 μs** |     **8.014 μs** |     **7.892 μs** |     **8.135 μs** |     **-** |     **-** |     **-** |         **-** |
|          **ReadMultiSegmentSequence** |   **Json4KB** |             **?** |        **4096** |     **8.987 μs** |  **0.1196 μs** |  **0.1118 μs** |     **8.940 μs** |     **8.859 μs** |     **9.170 μs** |     **-** |     **-** |     **-** |         **-** |
| ReadMultiSegmentSequenceUsingSpan |   Json4KB |             ? |        4096 |     8.059 μs |  0.1083 μs |  0.1013 μs |     8.049 μs |     7.951 μs |     8.238 μs |     - |     - |     - |         - |
|      **ReadSingleSegmentSequenceByN** |   **Json4KB** |          **8192** |           **?** |     **7.980 μs** |  **0.1177 μs** |  **0.1043 μs** |     **7.937 μs** |     **7.873 μs** |     **8.202 μs** |     **-** |     **-** |     **-** |         **-** |
|          **ReadMultiSegmentSequence** |   **Json4KB** |             **?** |        **8192** |     **7.931 μs** |  **0.1012 μs** |  **0.0946 μs** |     **7.902 μs** |     **7.804 μs** |     **8.077 μs** |     **-** |     **-** |     **-** |         **-** |
| ReadMultiSegmentSequenceUsingSpan |   Json4KB |             ? |        8192 |     8.066 μs |  0.1125 μs |  0.0997 μs |     8.048 μs |     7.950 μs |     8.252 μs |     - |     - |     - |         - |
|         **ReadSingleSegmentSequence** |   **Json4KB** |             **?** |           **?** |     **8.023 μs** |  **0.1417 μs** |  **0.1257 μs** |     **8.019 μs** |     **7.864 μs** |     **8.281 μs** |     **-** |     **-** |     **-** |         **-** |
|      **ReadSingleSegmentSequenceByN** |  **Json40KB** |          **4096** |           **?** |    **89.588 μs** |  **1.1246 μs** |  **1.0519 μs** |    **89.176 μs** |    **88.321 μs** |    **91.402 μs** |     **-** |     **-** |     **-** |         **-** |
|          **ReadMultiSegmentSequence** |  **Json40KB** |             **?** |        **4096** |    **98.996 μs** |  **1.3648 μs** |  **1.1397 μs** |    **98.754 μs** |    **97.632 μs** |   **101.942 μs** |     **-** |     **-** |     **-** |         **-** |
| ReadMultiSegmentSequenceUsingSpan |  Json40KB |             ? |        4096 |    91.041 μs |  1.0469 μs |  0.9281 μs |    90.794 μs |    89.931 μs |    92.707 μs |     - |     - |     - |         - |
|      **ReadSingleSegmentSequenceByN** |  **Json40KB** |          **8192** |           **?** |    **89.325 μs** |  **1.4145 μs** |  **1.2540 μs** |    **88.863 μs** |    **88.047 μs** |    **91.703 μs** |     **-** |     **-** |     **-** |         **-** |
|          **ReadMultiSegmentSequence** |  **Json40KB** |             **?** |        **8192** |    **99.011 μs** |  **1.3405 μs** |  **1.2539 μs** |    **98.688 μs** |    **97.503 μs** |   **101.424 μs** |     **-** |     **-** |     **-** |         **-** |
| ReadMultiSegmentSequenceUsingSpan |  Json40KB |             ? |        8192 |    91.273 μs |  1.5537 μs |  1.4533 μs |    90.442 μs |    89.799 μs |    93.894 μs |     - |     - |     - |         - |
|         **ReadSingleSegmentSequence** |  **Json40KB** |             **?** |           **?** |    **89.051 μs** |  **1.3617 μs** |  **1.2738 μs** |    **88.456 μs** |    **87.561 μs** |    **91.705 μs** |     **-** |     **-** |     **-** |         **-** |
|      **ReadSingleSegmentSequenceByN** | **Json400KB** |          **4096** |           **?** |   **926.457 μs** | **11.3145 μs** | **10.5836 μs** |   **924.169 μs** |   **912.051 μs** |   **947.116 μs** |     **-** |     **-** |     **-** |       **1 B** |
|          **ReadMultiSegmentSequence** | **Json400KB** |             **?** |        **4096** | **1,019.592 μs** | **12.3490 μs** | **11.5513 μs** | **1,016.758 μs** | **1,005.099 μs** | **1,044.738 μs** |     **-** |     **-** |     **-** |       **1 B** |
| ReadMultiSegmentSequenceUsingSpan | Json400KB |             ? |        4096 |   942.567 μs | 13.2504 μs | 12.3944 μs |   936.775 μs |   929.744 μs |   965.858 μs |     - |     - |     - |       1 B |
|      **ReadSingleSegmentSequenceByN** | **Json400KB** |          **8192** |           **?** |   **917.788 μs** | **10.3984 μs** |  **9.7267 μs** |   **914.903 μs** |   **906.346 μs** |   **938.661 μs** |     **-** |     **-** |     **-** |       **1 B** |
|          **ReadMultiSegmentSequence** | **Json400KB** |             **?** |        **8192** | **1,013.469 μs** | **11.4966 μs** | **10.1914 μs** | **1,011.171 μs** |   **999.318 μs** | **1,032.970 μs** |     **-** |     **-** |     **-** |       **1 B** |
| ReadMultiSegmentSequenceUsingSpan | Json400KB |             ? |        8192 |   931.982 μs | 16.2262 μs | 15.1780 μs |   925.987 μs |   915.615 μs |   961.717 μs |     - |     - |     - |       1 B |
|         **ReadSingleSegmentSequence** | **Json400KB** |             **?** |           **?** |   **908.248 μs** | **12.5233 μs** | **11.7143 μs** |   **904.957 μs** |   **895.487 μs** |   **927.627 μs** |     **-** |     **-** |     **-** |       **1 B** |
