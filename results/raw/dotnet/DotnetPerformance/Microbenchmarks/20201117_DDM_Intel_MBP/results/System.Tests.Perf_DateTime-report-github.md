``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | format |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |------- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|         **GetNow** |      **?** | **107.5517 ns** | **1.5957 ns** | **1.4145 ns** | **106.8709 ns** | **105.9046 ns** | **110.3801 ns** |      **-** |     **-** |     **-** |         **-** |
|      GetUtcNow |      ? |  25.9847 ns | 0.2779 ns | 0.2599 ns |  25.9459 ns |  25.6668 ns |  26.4802 ns |      - |     - |     - |         - |
| op_Subtraction |      ? |   0.7687 ns | 0.0288 ns | 0.0255 ns |   0.7628 ns |   0.7357 ns |   0.8305 ns |      - |     - |     - |         - |
|         ParseR |      ? |  57.0080 ns | 0.8736 ns | 0.8171 ns |  56.6622 ns |  56.1374 ns |  58.9414 ns |      - |     - |     - |         - |
|         ParseO |      ? |  54.5457 ns | 0.8330 ns | 0.7384 ns |  54.2723 ns |  53.5740 ns |  55.7818 ns |      - |     - |     - |         - |
|       **ToString** |      **?** | **242.2709 ns** | **2.2968 ns** | **1.9179 ns** | **241.9963 ns** | **239.7750 ns** | **246.8481 ns** | **0.0148** |     **-** |     **-** |      **64 B** |
|       **ToString** |      **G** | **241.4686 ns** | **3.7741 ns** | **3.3457 ns** | **241.4476 ns** | **237.4502 ns** | **246.5380 ns** | **0.0147** |     **-** |     **-** |      **64 B** |
|       **ToString** |      **o** |  **49.1062 ns** | **0.9264 ns** | **0.8665 ns** |  **49.1010 ns** |  **47.8504 ns** |  **51.1015 ns** | **0.0189** |     **-** |     **-** |      **80 B** |
|       **ToString** |      **r** |  **43.9063 ns** | **0.8746 ns** | **0.8181 ns** |  **43.7540 ns** |  **42.9620 ns** |  **45.8173 ns** | **0.0191** |     **-** |     **-** |      **80 B** |
|       **ToString** |      **s** | **241.5529 ns** | **2.4748 ns** | **2.1939 ns** | **240.9576 ns** | **239.0790 ns** | **246.2333 ns** | **0.0145** |     **-** |     **-** |      **64 B** |
