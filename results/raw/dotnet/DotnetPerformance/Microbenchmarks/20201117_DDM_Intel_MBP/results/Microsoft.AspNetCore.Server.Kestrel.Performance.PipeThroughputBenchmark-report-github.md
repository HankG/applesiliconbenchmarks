``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | Length | Chunks |     Mean |    Error |   StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |------- |------- |---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|   **Parse_ParallelAsync** |    **128** |      **1** | **154.0 ns** |  **3.32 ns** |  **3.82 ns** | **154.6 ns** | **146.9 ns** | **160.6 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |    128 |      1 | 196.2 ns |  2.31 ns |  2.16 ns | 196.7 ns | 192.6 ns | 199.5 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |    **128** |     **16** | **702.0 ns** | **13.91 ns** | **13.01 ns** | **697.9 ns** | **685.4 ns** | **729.4 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |    128 |     16 | 514.1 ns |  7.28 ns |  6.45 ns | 513.1 ns | 502.4 ns | 527.3 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |   **4096** |      **1** | **430.6 ns** |  **4.62 ns** |  **4.33 ns** | **428.6 ns** | **426.2 ns** | **439.1 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |   4096 |      1 | 196.0 ns |  2.56 ns |  2.39 ns | 195.2 ns | 193.4 ns | 201.5 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |   **4096** |     **16** | **944.2 ns** | **17.17 ns** | **16.06 ns** | **939.1 ns** | **927.8 ns** | **979.8 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |   4096 |     16 | 510.3 ns |  5.35 ns |  5.00 ns | 509.4 ns | 503.6 ns | 519.3 ns |     - |     - |     - |         - |
