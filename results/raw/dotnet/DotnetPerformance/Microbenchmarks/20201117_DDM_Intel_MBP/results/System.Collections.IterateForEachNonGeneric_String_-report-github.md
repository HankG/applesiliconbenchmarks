``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |      Mean |     Error |    StdDev |    Median |      Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |----------:|----------:|----------:|----------:|---------:|----------:|-------:|------:|------:|----------:|
|  ArrayList |  512 |  3.295 μs | 0.0357 μs | 0.0317 μs |  3.291 μs | 3.253 μs |  3.358 μs |      - |     - |     - |      48 B |
|  Hashtable |  512 | 10.166 μs | 0.1697 μs | 0.1588 μs | 10.150 μs | 9.978 μs | 10.459 μs | 3.9263 |     - |     - |   16440 B |
|      Queue |  512 |  4.152 μs | 0.0444 μs | 0.0415 μs |  4.151 μs | 4.088 μs |  4.227 μs |      - |     - |     - |      40 B |
|      Stack |  512 |  3.769 μs | 0.0546 μs | 0.0484 μs |  3.758 μs | 3.707 μs |  3.882 μs |      - |     - |     - |      40 B |
| SortedList |  512 |  7.421 μs | 0.1249 μs | 0.1168 μs |  7.441 μs | 7.259 μs |  7.651 μs | 3.9118 |     - |     - |   16448 B |
