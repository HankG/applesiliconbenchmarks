``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Count |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |------ |---------:|---------:|---------:|---------:|---------:|---------:|--------:|-------:|------:|----------:|
|           Dictionary |   512 | 13.44 μs | 0.204 μs | 0.191 μs | 13.47 μs | 13.16 μs | 13.80 μs |  3.4871 | 0.1609 |     - |  14.38 KB |
| ConcurrentDictionary |   512 | 43.95 μs | 0.802 μs | 0.750 μs | 44.00 μs | 42.77 μs | 45.25 μs | 14.1411 | 2.7933 |     - |   58.4 KB |
