``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                             Method |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------- |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|           ContainsValue_17_Int_Int | 4.875 ns | 0.0949 ns | 0.1015 ns | 4.846 ns | 4.773 ns | 5.092 ns |     - |     - |     - |         - |
|     ContainsKey_17_Int_32ByteValue | 4.827 ns | 0.0538 ns | 0.0503 ns | 4.809 ns | 4.763 ns | 4.943 ns |     - |     - |     - |         - |
| ContainsKey_17_Int_32ByteRefsValue | 4.595 ns | 0.0431 ns | 0.0403 ns | 4.583 ns | 4.540 ns | 4.676 ns |     - |     - |     - |         - |
|           ContainsValue_3k_Int_Int | 4.788 ns | 0.0582 ns | 0.0516 ns | 4.768 ns | 4.733 ns | 4.889 ns |     - |     - |     - |         - |
|     ContainsKey_3k_Int_32ByteValue | 4.810 ns | 0.0581 ns | 0.0543 ns | 4.796 ns | 4.756 ns | 4.914 ns |     - |     - |     - |         - |
| ContainsKey_3k_Int_32ByteRefsValue | 4.635 ns | 0.0835 ns | 0.0740 ns | 4.612 ns | 4.540 ns | 4.777 ns |     - |     - |     - |         - |
|             TryGetValue_17_Int_Int | 4.971 ns | 0.0782 ns | 0.0731 ns | 4.951 ns | 4.895 ns | 5.126 ns |     - |     - |     - |         - |
|     TryGetValue_17_Int_32ByteValue | 4.949 ns | 0.0605 ns | 0.0566 ns | 4.924 ns | 4.877 ns | 5.071 ns |     - |     - |     - |         - |
| TryGetValue_17_Int_32ByteRefsValue | 5.411 ns | 0.0450 ns | 0.0421 ns | 5.421 ns | 5.326 ns | 5.474 ns |     - |     - |     - |         - |
|             TryGetValue_3k_Int_Int | 4.612 ns | 0.0667 ns | 0.0591 ns | 4.605 ns | 4.518 ns | 4.731 ns |     - |     - |     - |         - |
|     TryGetValue_3k_Int_32ByteValue | 4.656 ns | 0.0618 ns | 0.0516 ns | 4.680 ns | 4.533 ns | 4.696 ns |     - |     - |     - |         - |
| TryGetValue_3k_Int_32ByteRefsValue | 5.040 ns | 0.0621 ns | 0.0581 ns | 5.023 ns | 4.972 ns | 5.143 ns |     - |     - |     - |         - |
