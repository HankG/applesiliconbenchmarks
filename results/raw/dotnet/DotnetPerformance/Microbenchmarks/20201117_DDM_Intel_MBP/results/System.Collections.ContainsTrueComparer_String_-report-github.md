``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|            HashSet |  512 |  23.54 μs | 0.392 μs | 0.347 μs |  23.42 μs |  23.20 μs |  24.45 μs |     - |     - |     - |         - |
|          SortedSet |  512 | 241.65 μs | 2.382 μs | 2.112 μs | 240.99 μs | 239.53 μs | 245.89 μs |     - |     - |     - |         - |
|   ImmutableHashSet |  512 |  43.66 μs | 0.632 μs | 0.591 μs |  43.42 μs |  42.96 μs |  44.99 μs |     - |     - |     - |         - |
| ImmutableSortedSet |  512 | 262.97 μs | 2.123 μs | 1.773 μs | 262.62 μs | 260.08 μs | 266.29 μs |     - |     - |     - |         - |
