``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                     Method |  Segment |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------- |--------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                        **Slice_StartPosition** |   **Single** |  **6.234 ns** | **0.1201 ns** | **0.1124 ns** |  **6.206 ns** |  **6.093 ns** |  **6.467 ns** |     **-** |     **-** |     **-** |         **-** |
|                                Slice_Start |   Single |  4.056 ns | 0.0594 ns | 0.0527 ns |  4.042 ns |  3.993 ns |  4.185 ns |     - |     - |     - |         - |
|                     Slice_Start_And_Length |   Single |  5.160 ns | 0.1017 ns | 0.0951 ns |  5.111 ns |  5.064 ns |  5.316 ns |     - |     - |     - |         - |
|                Slice_Start_And_EndPosition |   Single |  4.806 ns | 0.1001 ns | 0.0887 ns |  4.788 ns |  4.696 ns |  4.991 ns |     - |     - |     - |         - |
|             Slice_StartPosition_And_Length |   Single |  4.767 ns | 0.0798 ns | 0.0747 ns |  4.758 ns |  4.673 ns |  4.892 ns |     - |     - |     - |         - |
|        Slice_StartPosition_And_EndPosition |   Single |  5.709 ns | 0.0818 ns | 0.0766 ns |  5.706 ns |  5.604 ns |  5.846 ns |     - |     - |     - |         - |
|                               Slice_Repeat |   Single | 32.448 ns | 0.3903 ns | 0.3260 ns | 32.352 ns | 31.999 ns | 32.966 ns |     - |     - |     - |         - |
| Slice_Repeat_StartPosition_And_EndPosition |   Single | 21.069 ns | 0.3183 ns | 0.2822 ns | 21.009 ns | 20.712 ns | 21.586 ns |     - |     - |     - |         - |
|                        **Slice_StartPosition** | **Multiple** | **12.091 ns** | **0.1717 ns** | **0.1606 ns** | **12.021 ns** | **11.926 ns** | **12.409 ns** |     **-** |     **-** |     **-** |         **-** |
|                                Slice_Start | Multiple |  4.184 ns | 0.0752 ns | 0.0703 ns |  4.147 ns |  4.105 ns |  4.327 ns |     - |     - |     - |         - |
|                     Slice_Start_And_Length | Multiple |  9.678 ns | 0.2009 ns | 0.1781 ns |  9.692 ns |  9.459 ns | 10.076 ns |     - |     - |     - |         - |
|                Slice_Start_And_EndPosition | Multiple | 12.934 ns | 0.2602 ns | 0.2434 ns | 12.806 ns | 12.727 ns | 13.463 ns |     - |     - |     - |         - |
|             Slice_StartPosition_And_Length | Multiple | 11.795 ns | 0.1416 ns | 0.1255 ns | 11.789 ns | 11.622 ns | 11.997 ns |     - |     - |     - |         - |
|        Slice_StartPosition_And_EndPosition | Multiple | 13.119 ns | 0.1425 ns | 0.1113 ns | 13.135 ns | 12.955 ns | 13.280 ns |     - |     - |     - |         - |
|                               Slice_Repeat | Multiple | 44.575 ns | 0.4435 ns | 0.3932 ns | 44.460 ns | 44.008 ns | 45.252 ns |     - |     - |     - |         - |
| Slice_Repeat_StartPosition_And_EndPosition | Multiple | 52.655 ns | 0.8477 ns | 0.7515 ns | 52.475 ns | 51.703 ns | 54.403 ns |     - |     - |     - |         - |
