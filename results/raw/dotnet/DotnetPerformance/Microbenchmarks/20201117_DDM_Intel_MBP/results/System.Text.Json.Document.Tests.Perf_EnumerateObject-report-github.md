``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|              Method |          TestCase |         Mean |      Error |    StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |------------------ |-------------:|-----------:|----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|               **Parse** |  **ObjectProperties** | **1,017.261 μs** | **11.0467 μs** | **9.7926 μs** | **1,016.191 μs** | **1,003.821 μs** | **1,033.641 μs** |     **-** |     **-** |     **-** |      **81 B** |
| EnumerateProperties |  ObjectProperties |     3.333 μs |  0.0365 μs | 0.0342 μs |     3.325 μs |     3.288 μs |     3.397 μs |     - |     - |     - |         - |
|     PropertyIndexer |  ObjectProperties |     2.655 μs |  0.0268 μs | 0.0209 μs |     2.653 μs |     2.626 μs |     2.690 μs |     - |     - |     - |         - |
|               **Parse** |  **StringProperties** |    **22.180 μs** |  **0.2210 μs** | **0.2068 μs** |    **22.182 μs** |    **21.902 μs** |    **22.499 μs** |     **-** |     **-** |     **-** |      **80 B** |
| EnumerateProperties |  StringProperties |     3.533 μs |  0.0508 μs | 0.0475 μs |     3.551 μs |     3.465 μs |     3.602 μs |     - |     - |     - |         - |
|     PropertyIndexer |  StringProperties |     1.843 μs |  0.0188 μs | 0.0176 μs |     1.839 μs |     1.814 μs |     1.869 μs |     - |     - |     - |         - |
|               **Parse** | **NumericProperties** |    **24.899 μs** |  **0.2474 μs** | **0.2193 μs** |    **24.835 μs** |    **24.630 μs** |    **25.365 μs** |     **-** |     **-** |     **-** |      **80 B** |
| EnumerateProperties | NumericProperties |     3.489 μs |  0.0269 μs | 0.0224 μs |     3.498 μs |     3.448 μs |     3.522 μs |     - |     - |     - |         - |
|     PropertyIndexer | NumericProperties |     1.725 μs |  0.0231 μs | 0.0216 μs |     1.716 μs |     1.702 μs |     1.771 μs |     - |     - |     - |         - |
