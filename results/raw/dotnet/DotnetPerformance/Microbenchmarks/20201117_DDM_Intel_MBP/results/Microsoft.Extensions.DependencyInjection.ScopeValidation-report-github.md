``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                       Method |     Mean |    Error |   StdDev |   Median |      Min |      Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|------:|--------:|-------:|------:|------:|----------:|
|                    Transient | 27.72 ns | 0.390 ns | 0.346 ns | 27.68 ns | 27.25 ns | 28.55 ns |  1.00 |    0.00 | 0.0057 |     - |     - |      24 B |
| TransientWithScopeValidation | 40.76 ns | 0.804 ns | 0.752 ns | 40.62 ns | 39.84 ns | 42.07 ns |  1.47 |    0.02 | 0.0057 |     - |     - |      24 B |
