``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |         Mean |       Error |      StdDev |       Median |          Min |          Max |   Gen 0 |  Gen 1 |  Gen 2 | Allocated |
|-------------------------- |----- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|--------:|-------:|-------:|----------:|
|                      List |  512 |     263.2 ns |     4.63 ns |     4.33 ns |     261.5 ns |     257.4 ns |     270.6 ns |  0.9911 |      - |      - |   4.05 KB |
|                LinkedList |  512 |   9,439.2 ns |   160.97 ns |   142.69 ns |   9,400.3 ns |   9,272.4 ns |   9,754.2 ns |  5.8920 | 0.6339 |      - |  24.07 KB |
|                   HashSet |  512 |  13,546.4 ns |   256.22 ns |   239.67 ns |  13,555.7 ns |  13,236.5 ns |  14,036.4 ns |  2.4852 |      - |      - |  10.33 KB |
|                Dictionary |  512 |  13,993.1 ns |   265.31 ns |   272.45 ns |  13,913.2 ns |  13,595.6 ns |  14,514.0 ns |  3.4876 | 0.1661 |      - |  14.38 KB |
|                     Queue |  512 |     267.5 ns |     5.12 ns |     4.79 ns |     267.7 ns |     261.0 ns |     279.0 ns |  0.9940 |      - |      - |   4.06 KB |
|                     Stack |  512 |     269.3 ns |     5.33 ns |     5.48 ns |     268.0 ns |     261.3 ns |     280.5 ns |  0.9911 |      - |      - |   4.05 KB |
|                SortedList |  512 | 333,610.2 ns | 4,393.30 ns | 4,109.50 ns | 331,545.9 ns | 329,745.8 ns | 340,691.7 ns |  1.3021 |      - |      - |   8.11 KB |
|                 SortedSet |  512 | 286,872.4 ns | 4,350.04 ns | 3,856.20 ns | 285,927.2 ns | 282,067.3 ns | 295,077.0 ns |  6.6964 |      - |      - |  28.07 KB |
|          SortedDictionary |  512 | 291,975.4 ns | 3,595.24 ns | 3,362.99 ns | 291,075.9 ns | 286,684.3 ns | 297,494.9 ns |  6.8182 | 1.1364 |      - |  28.16 KB |
|      ConcurrentDictionary |  512 |  51,888.8 ns |   917.63 ns |   858.35 ns |  51,597.4 ns |  50,692.3 ns |  53,461.2 ns | 19.1532 | 4.6371 |      - |  78.66 KB |
|           ConcurrentQueue |  512 |   6,471.1 ns |   124.88 ns |   122.65 ns |   6,445.6 ns |   6,336.5 ns |   6,753.7 ns |  2.0294 | 0.1028 |      - |   8.34 KB |
|           ConcurrentStack |  512 |   5,242.2 ns |    97.39 ns |    86.33 ns |   5,232.8 ns |   5,142.3 ns |   5,386.5 ns |  3.9141 |      - |      - |  16.05 KB |
|             ConcurrentBag |  512 |  13,961.2 ns |   274.07 ns |   242.95 ns |  13,944.1 ns |  13,554.9 ns |  14,421.0 ns |  3.9197 | 1.9330 | 0.1074 |  16.16 KB |
|            ImmutableArray |  512 |     268.2 ns |     5.09 ns |     4.76 ns |     267.3 ns |     261.3 ns |     274.8 ns |  0.9842 |      - |      - |   4.02 KB |
|       ImmutableDictionary |  512 | 162,258.4 ns | 2,619.94 ns | 2,450.69 ns | 161,316.0 ns | 159,339.5 ns | 166,198.2 ns |  7.8125 | 1.3021 |      - |  32.09 KB |
|          ImmutableHashSet |  512 | 144,917.4 ns | 2,217.28 ns | 2,074.04 ns | 144,402.6 ns | 142,461.7 ns | 149,218.6 ns |  6.8182 | 1.1364 |      - |  28.08 KB |
|             ImmutableList |  512 |  12,399.5 ns |   222.08 ns |   207.74 ns |  12,359.2 ns |  12,113.9 ns |  12,839.9 ns |  5.8670 | 0.1939 |      - |  24.05 KB |
|            ImmutableQueue |  512 |   4,239.1 ns |    55.99 ns |    46.75 ns |   4,222.1 ns |   4,177.7 ns |   4,317.2 ns |  3.9254 |      - |      - |  16.04 KB |
|            ImmutableStack |  512 |   5,742.0 ns |   109.44 ns |   112.39 ns |   5,697.9 ns |   5,591.1 ns |   5,997.8 ns |  3.9176 | 0.0228 |      - |  16.03 KB |
| ImmutableSortedDictionary |  512 | 315,822.1 ns | 3,408.09 ns | 3,021.18 ns | 315,273.7 ns | 312,174.3 ns | 322,061.7 ns | 14.7059 | 1.2255 |      - |  64.57 KB |
|        ImmutableSortedSet |  512 | 303,450.2 ns | 5,356.26 ns | 4,472.72 ns | 301,936.7 ns | 297,521.6 ns | 312,309.3 ns |  5.8962 |      - |      - |  28.11 KB |
