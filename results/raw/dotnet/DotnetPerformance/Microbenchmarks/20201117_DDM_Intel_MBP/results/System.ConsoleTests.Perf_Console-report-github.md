``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|  OpenStandardInput | 1,177.40 ns | 11.733 ns | 10.975 ns | 1,176.66 ns | 1,164.61 ns | 1,200.17 ns | 0.0187 |     - |     - |      88 B |
| OpenStandardOutput | 1,169.84 ns | 15.976 ns | 14.944 ns | 1,168.45 ns | 1,148.43 ns | 1,202.09 ns | 0.0186 |     - |     - |      88 B |
|  OpenStandardError | 1,176.86 ns | 16.397 ns | 14.535 ns | 1,171.68 ns | 1,161.76 ns | 1,211.80 ns | 0.0186 |     - |     - |      88 B |
|    ForegroundColor |    20.84 ns |  0.280 ns |  0.248 ns |    20.76 ns |    20.50 ns |    21.24 ns |      - |     - |     - |         - |
|    BackgroundColor |    21.05 ns |  0.289 ns |  0.270 ns |    20.90 ns |    20.74 ns |    21.51 ns |      - |     - |     - |         - |
|         ResetColor |    61.06 ns |  0.734 ns |  0.687 ns |    60.79 ns |    60.28 ns |    62.51 ns |      - |     - |     - |         - |
