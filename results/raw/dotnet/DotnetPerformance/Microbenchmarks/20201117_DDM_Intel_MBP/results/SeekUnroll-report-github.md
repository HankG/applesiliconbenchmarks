``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | boxedIndex |    Mean |    Error |   StdDev |  Median |     Min |     Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----------- |--------:|---------:|---------:|--------:|--------:|--------:|------:|------:|------:|----------:|
| **SeekUnroll** |          **1** | **1.134 s** | **0.0027 s** | **0.0023 s** | **1.135 s** | **1.131 s** | **1.138 s** |     **-** |     **-** |     **-** |     **344 B** |
| **SeekUnroll** |          **3** | **1.136 s** | **0.0078 s** | **0.0069 s** | **1.133 s** | **1.130 s** | **1.150 s** |     **-** |     **-** |     **-** |     **344 B** |
| **SeekUnroll** |         **11** | **1.360 s** | **0.0031 s** | **0.0026 s** | **1.360 s** | **1.356 s** | **1.365 s** |     **-** |     **-** |     **-** |     **344 B** |
| **SeekUnroll** |         **19** | **1.590 s** | **0.0070 s** | **0.0062 s** | **1.588 s** | **1.582 s** | **1.602 s** |     **-** |     **-** |     **-** |    **5392 B** |
| **SeekUnroll** |         **27** | **1.645 s** | **0.0058 s** | **0.0048 s** | **1.644 s** | **1.638 s** | **1.655 s** |     **-** |     **-** |     **-** |     **344 B** |
