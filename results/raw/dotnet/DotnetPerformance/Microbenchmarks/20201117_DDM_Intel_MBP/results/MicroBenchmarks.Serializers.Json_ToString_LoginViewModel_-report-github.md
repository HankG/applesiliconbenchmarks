``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|      Jil | 400.6 ns |  6.85 ns |  6.40 ns | 398.9 ns | 390.7 ns | 412.1 ns | 0.1752 |     - |     - |     736 B |
| JSON.NET | 663.5 ns | 13.21 ns | 12.36 ns | 661.3 ns | 649.2 ns | 689.4 ns | 0.3579 |     - |     - |    1504 B |
| Utf8Json | 171.1 ns |  2.90 ns |  2.71 ns | 171.2 ns | 166.6 ns | 175.1 ns | 0.0456 |     - |     - |     192 B |
