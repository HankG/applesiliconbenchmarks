``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    DeserializeFromString | 131.56 ns | 1.177 ns | 1.043 ns | 131.28 ns | 130.14 ns | 133.37 ns |      - |     - |     - |         - |
| DeserializeFromUtf8Bytes |  88.11 ns | 0.750 ns | 0.626 ns |  88.06 ns |  87.15 ns |  89.05 ns |      - |     - |     - |         - |
|    DeserializeFromStream | 280.84 ns | 4.002 ns | 3.744 ns | 280.65 ns | 275.01 ns | 286.32 ns | 0.0169 |     - |     - |      72 B |
