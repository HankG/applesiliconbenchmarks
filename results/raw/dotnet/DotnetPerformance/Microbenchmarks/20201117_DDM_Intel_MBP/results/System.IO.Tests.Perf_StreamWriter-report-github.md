``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | writeLength |             Mean |           Error |          StdDev |           Median |              Min |              Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |------------ |-----------------:|----------------:|----------------:|-----------------:|-----------------:|-----------------:|-------:|------:|------:|----------:|
|           **WriteFormat** |           **?** |         **147.9 ns** |         **2.63 ns** |         **2.34 ns** |         **148.0 ns** |         **144.7 ns** |         **152.9 ns** | **0.0053** |     **-** |     **-** |      **24 B** |
|        **WriteCharArray** |           **2** |  **87,826,046.0 ns** |   **882,829.51 ns** |   **825,799.27 ns** |  **87,483,539.5 ns** |  **86,786,581.5 ns** |  **89,464,456.2 ns** |      **-** |     **-** |     **-** |      **72 B** |
| WritePartialCharArray |           2 |  98,309,379.2 ns |   772,968.29 ns |   645,463.53 ns |  98,342,668.5 ns |  97,165,446.0 ns |  99,404,271.5 ns |      - |     - |     - |     144 B |
|           WriteString |           2 |  88,309,159.5 ns |   802,521.64 ns |   750,679.24 ns |  88,398,289.8 ns |  87,170,415.0 ns |  89,479,914.0 ns |      - |     - |     - |      72 B |
|        **WriteCharArray** |         **100** | **428,674,217.7 ns** | **3,745,409.87 ns** | **3,320,208.29 ns** | **428,666,553.0 ns** | **424,887,342.0 ns** | **435,601,129.0 ns** |      **-** |     **-** |     **-** |     **288 B** |
| WritePartialCharArray |         100 | 453,107,247.9 ns | 4,172,059.61 ns | 3,902,547.15 ns | 454,054,535.0 ns | 447,651,075.0 ns | 459,625,763.0 ns |      - |     - |     - |     288 B |
|           WriteString |         100 | 421,982,616.9 ns | 1,986,028.54 ns | 1,857,732.33 ns | 421,861,319.0 ns | 418,651,737.0 ns | 424,727,004.0 ns |      - |     - |     - |     288 B |
