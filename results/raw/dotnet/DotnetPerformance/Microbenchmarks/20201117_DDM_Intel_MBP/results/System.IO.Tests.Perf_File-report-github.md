``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-VUUYSD : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
WarmupCount=1  

```
| Method |        Job | UnrollFactor |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |----------- |------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| Delete | Job-VUUYSD |            1 | 27.751 μs | 2.3736 μs | 2.5397 μs | 26.645 μs | 25.206 μs | 34.147 μs |     - |     - |     - |      29 B |
| Exists | Job-NXBZBK |           16 |  3.588 μs | 0.0686 μs | 0.0704 μs |  3.559 μs |  3.505 μs |  3.757 μs |     - |     - |     - |         - |
