``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                               Method | NumberOfBytes |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------- |-------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **WriteByteArrayAsBase64_NoEscaping** |           **100** |  **78.14 ns** | **0.937 ns** | **0.782 ns** |  **78.12 ns** |  **77.09 ns** |  **79.89 ns** | **0.0286** |     **-** |     **-** |     **120 B** |
| WriteByteArrayAsBase64_HeavyEscaping |           100 |  78.48 ns | 1.503 ns | 1.333 ns |  78.20 ns |  76.63 ns |  80.56 ns | 0.0286 |     - |     - |     120 B |
|    **WriteByteArrayAsBase64_NoEscaping** |          **1000** | **183.73 ns** | **2.621 ns** | **2.324 ns** | **183.58 ns** | **180.47 ns** | **189.84 ns** | **0.0283** |     **-** |     **-** |     **120 B** |
| WriteByteArrayAsBase64_HeavyEscaping |          1000 | 179.61 ns | 2.945 ns | 2.610 ns | 178.80 ns | 176.13 ns | 184.78 ns | 0.0281 |     - |     - |     120 B |
