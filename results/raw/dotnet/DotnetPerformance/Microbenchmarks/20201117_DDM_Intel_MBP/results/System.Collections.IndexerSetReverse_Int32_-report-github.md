``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
| Method | Size |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |----- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|  Array |  512 | 471.9 ns | 5.56 ns | 5.20 ns | 470.8 ns | 464.6 ns | 481.6 ns |     - |     - |     - |         - |
|   Span |  512 | 238.2 ns | 2.73 ns | 2.55 ns | 237.9 ns | 235.3 ns | 243.4 ns |     - |     - |     - |         - |
|   List |  512 | 557.0 ns | 8.71 ns | 7.72 ns | 555.5 ns | 546.8 ns | 576.0 ns |     - |     - |     - |         - |
|  IList |  512 | 942.4 ns | 9.87 ns | 8.25 ns | 940.7 ns | 930.6 ns | 957.7 ns |     - |     - |     - |         - |
