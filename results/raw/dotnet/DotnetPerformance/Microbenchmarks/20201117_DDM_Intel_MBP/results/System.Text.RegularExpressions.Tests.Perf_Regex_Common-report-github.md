``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method |              Options |          Mean |        Error |       StdDev |        Median |           Min |           Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|----------------- |--------------------- |--------------:|-------------:|-------------:|--------------:|--------------:|--------------:|-------:|-------:|------:|----------:|
|    **Email_IsMatch** |                 **None** |     **427.57 ns** |     **5.127 ns** |     **4.796 ns** |     **425.44 ns** |     **421.09 ns** |     **437.14 ns** |      **-** |      **-** |     **-** |         **-** |
| Email_IsNotMatch |                 None |     718.25 ns |     7.478 ns |     6.994 ns |     717.85 ns |     708.30 ns |     729.63 ns |      - |      - |     - |         - |
|     Date_IsMatch |                 None |     196.82 ns |     1.787 ns |     1.584 ns |     196.79 ns |     194.16 ns |     200.33 ns |      - |      - |     - |         - |
|  Date_IsNotMatch |                 None |     440.85 ns |     4.814 ns |     4.268 ns |     439.37 ns |     435.14 ns |     448.96 ns |      - |      - |     - |         - |
|       IP_IsMatch |                 None |     817.79 ns |    11.470 ns |    10.168 ns |     815.38 ns |     804.71 ns |     837.70 ns |      - |      - |     - |         - |
|    IP_IsNotMatch |                 None |     802.94 ns |     4.516 ns |     3.526 ns |     803.13 ns |     797.05 ns |     808.13 ns |      - |      - |     - |         - |
|      Uri_IsMatch |                 None |     305.73 ns |     4.218 ns |     3.945 ns |     304.71 ns |     301.72 ns |     313.65 ns |      - |      - |     - |         - |
|   Uri_IsNotMatch |                 None |     364.98 ns |     4.698 ns |     4.394 ns |     363.28 ns |     359.17 ns |     372.49 ns |      - |      - |     - |         - |
|       MatchesSet |                 None | 198,463.72 ns | 2,573.205 ns | 2,406.978 ns | 197,623.63 ns | 195,833.00 ns | 202,678.29 ns | 1.5625 |      - |     - |    6696 B |
|  MatchesBoundary |                 None | 152,759.01 ns | 2,276.326 ns | 2,017.903 ns | 152,163.68 ns | 150,647.18 ns | 156,339.31 ns | 1.2136 |      - |     - |    6696 B |
|      MatchesWord |                 None |   3,530.14 ns |    53.525 ns |    50.067 ns |   3,521.23 ns |   3,473.72 ns |   3,620.74 ns | 0.3501 |      - |     - |    1480 B |
|     MatchesWords |                 None |  70,629.98 ns |   850.369 ns |   795.436 ns |  70,493.69 ns |  69,646.73 ns |  71,814.42 ns | 0.8371 |      - |     - |    3504 B |
|        MatchWord |                 None |   1,950.30 ns |    17.758 ns |    15.742 ns |   1,948.83 ns |   1,920.58 ns |   1,975.20 ns | 0.0467 |      - |     - |     208 B |
|     ReplaceWords |                 None |  70,886.35 ns |   601.746 ns |   502.485 ns |  70,955.94 ns |  70,100.61 ns |  71,684.72 ns | 1.4077 |      - |     - |    6848 B |
|       SplitWords |                 None |  71,472.95 ns | 1,149.160 ns | 1,018.700 ns |  71,223.10 ns |  70,207.80 ns |  73,419.81 ns | 1.6968 |      - |     - |    7432 B |
|             Ctor |                 None |   6,349.36 ns |   115.193 ns |   107.752 ns |   6,319.14 ns |   6,221.97 ns |   6,552.03 ns | 1.7935 |      - |     - |    7584 B |
|       CtorInvoke |                 None |   6,945.23 ns |   133.260 ns |   142.587 ns |   6,930.12 ns |   6,759.70 ns |   7,218.54 ns | 1.8406 |      - |     - |    7720 B |
|    **Email_IsMatch** |             **Compiled** |     **144.44 ns** |     **1.616 ns** |     **1.512 ns** |     **144.07 ns** |     **142.31 ns** |     **146.77 ns** |      **-** |      **-** |     **-** |         **-** |
| Email_IsNotMatch |             Compiled |     212.36 ns |     2.343 ns |     2.077 ns |     211.97 ns |     209.45 ns |     216.89 ns |      - |      - |     - |         - |
|     Date_IsMatch |             Compiled |      71.96 ns |     0.616 ns |     0.576 ns |      72.08 ns |      70.98 ns |      72.73 ns |      - |      - |     - |         - |
|  Date_IsNotMatch |             Compiled |     122.83 ns |     1.125 ns |     1.052 ns |     122.77 ns |     121.21 ns |     124.40 ns |      - |      - |     - |         - |
|       IP_IsMatch |             Compiled |     166.36 ns |     1.069 ns |     0.892 ns |     166.40 ns |     164.91 ns |     168.19 ns |      - |      - |     - |         - |
|    IP_IsNotMatch |             Compiled |     166.51 ns |     1.539 ns |     1.364 ns |     166.47 ns |     164.18 ns |     168.68 ns |      - |      - |     - |         - |
|      Uri_IsMatch |             Compiled |      99.49 ns |     1.374 ns |     1.285 ns |      99.06 ns |      97.73 ns |     101.72 ns |      - |      - |     - |         - |
|   Uri_IsNotMatch |             Compiled |     115.03 ns |     1.115 ns |     0.931 ns |     114.95 ns |     113.87 ns |     117.27 ns |      - |      - |     - |         - |
|       MatchesSet |             Compiled |  54,713.66 ns | 1,036.581 ns |   969.618 ns |  54,867.83 ns |  53,325.06 ns |  56,746.86 ns | 1.4932 |      - |     - |    6696 B |
|  MatchesBoundary |             Compiled |  60,141.29 ns |   885.836 ns |   828.611 ns |  59,946.64 ns |  59,097.92 ns |  61,638.18 ns | 1.4259 |      - |     - |    6696 B |
|      MatchesWord |             Compiled |   2,098.99 ns |    35.678 ns |    35.040 ns |   2,087.61 ns |   2,058.79 ns |   2,167.39 ns | 0.3531 |      - |     - |    1480 B |
|     MatchesWords |             Compiled |   6,138.29 ns |   111.492 ns |   104.290 ns |   6,102.37 ns |   6,013.96 ns |   6,323.08 ns | 0.8298 |      - |     - |    3504 B |
|        MatchWord |             Compiled |     176.73 ns |     2.964 ns |     2.772 ns |     176.57 ns |     172.23 ns |     180.98 ns | 0.0492 |      - |     - |     208 B |
|     ReplaceWords |             Compiled |   6,014.03 ns |   113.580 ns |   111.551 ns |   5,978.05 ns |   5,865.33 ns |   6,216.66 ns | 1.6256 |      - |     - |    6848 B |
|       SplitWords |             Compiled |   5,935.02 ns |   116.374 ns |   119.507 ns |   5,901.57 ns |   5,776.36 ns |   6,095.85 ns | 1.7602 |      - |     - |    7432 B |
|             Ctor |             Compiled |  30,094.29 ns |   784.624 ns |   872.107 ns |  29,753.29 ns |  29,036.31 ns |  31,738.82 ns | 6.4576 |      - |     - |   27080 B |
|       CtorInvoke |             Compiled | 124,818.64 ns | 2,394.301 ns | 2,351.524 ns | 124,598.38 ns | 121,200.13 ns | 128,856.10 ns | 6.8898 | 3.4449 |     - |   29958 B |
|    **Email_IsMatch** | **IgnoreCase, Compiled** |     **210.21 ns** |     **3.974 ns** |     **3.523 ns** |     **208.39 ns** |     **206.86 ns** |     **218.97 ns** |      **-** |      **-** |     **-** |         **-** |
| Email_IsNotMatch | IgnoreCase, Compiled |     289.53 ns |     3.473 ns |     3.249 ns |     288.52 ns |     285.30 ns |     295.39 ns |      - |      - |     - |         - |
|     Date_IsMatch | IgnoreCase, Compiled |      84.41 ns |     1.301 ns |     1.153 ns |      84.07 ns |      83.28 ns |      86.64 ns |      - |      - |     - |         - |
|  Date_IsNotMatch | IgnoreCase, Compiled |     181.11 ns |     2.255 ns |     2.110 ns |     180.46 ns |     178.17 ns |     184.51 ns |      - |      - |     - |         - |
|       IP_IsMatch | IgnoreCase, Compiled |     315.38 ns |     4.395 ns |     3.896 ns |     314.11 ns |     310.55 ns |     322.80 ns |      - |      - |     - |         - |
|    IP_IsNotMatch | IgnoreCase, Compiled |     310.28 ns |     3.616 ns |     3.382 ns |     310.20 ns |     305.29 ns |     316.36 ns |      - |      - |     - |         - |
|      Uri_IsMatch | IgnoreCase, Compiled |     143.73 ns |     1.493 ns |     1.396 ns |     143.34 ns |     141.91 ns |     146.94 ns |      - |      - |     - |         - |
|   Uri_IsNotMatch | IgnoreCase, Compiled |     187.07 ns |     2.248 ns |     2.103 ns |     186.90 ns |     184.22 ns |     190.57 ns |      - |      - |     - |         - |
|       MatchesSet | IgnoreCase, Compiled | 117,274.81 ns | 1,161.656 ns | 1,086.613 ns | 117,159.86 ns | 115,646.29 ns | 119,703.05 ns | 1.3993 |      - |     - |    6696 B |
|  MatchesBoundary | IgnoreCase, Compiled | 101,390.60 ns | 1,486.731 ns | 1,390.689 ns | 101,612.20 ns |  98,954.89 ns | 103,529.06 ns | 1.2175 |      - |     - |    6696 B |
|      MatchesWord | IgnoreCase, Compiled |   3,565.14 ns |    51.286 ns |    47.973 ns |   3,565.54 ns |   3,482.14 ns |   3,630.59 ns | 0.3496 |      - |     - |    1480 B |
|     MatchesWords | IgnoreCase, Compiled |  18,150.90 ns |   207.944 ns |   184.337 ns |  18,137.90 ns |  17,930.86 ns |  18,513.01 ns | 0.7866 |      - |     - |    3504 B |
|        MatchWord | IgnoreCase, Compiled |     397.57 ns |     5.849 ns |     5.471 ns |     397.09 ns |     390.02 ns |     408.01 ns | 0.0494 |      - |     - |     208 B |
|     ReplaceWords | IgnoreCase, Compiled |  17,696.11 ns |   248.646 ns |   232.584 ns |  17,679.83 ns |  17,332.00 ns |  18,076.48 ns | 1.6079 |      - |     - |    6848 B |
|       SplitWords | IgnoreCase, Compiled |  17,941.52 ns |   244.871 ns |   229.052 ns |  17,955.02 ns |  17,570.77 ns |  18,275.17 ns | 1.7202 |      - |     - |    7432 B |
|             Ctor | IgnoreCase, Compiled |  36,767.72 ns |   631.795 ns |   590.981 ns |  36,525.65 ns |  36,052.27 ns |  37,977.11 ns | 6.8955 |      - |     - |   29000 B |
|       CtorInvoke | IgnoreCase, Compiled | 163,489.64 ns | 2,476.824 ns | 2,195.640 ns | 163,514.04 ns | 159,697.64 ns | 167,384.40 ns | 7.6531 | 3.8265 |     - |   32232 B |
