``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method | value |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------ |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|    **Parse** | **False** | **9.325 ns** | **0.1392 ns** | **0.1302 ns** | **9.296 ns** | **9.178 ns** | **9.546 ns** |     **-** |     **-** |     **-** |         **-** |
| TryParse | False | 6.957 ns | 0.1639 ns | 0.1610 ns | 6.915 ns | 6.748 ns | 7.330 ns |     - |     - |     - |         - |
| ToString | False | 1.260 ns | 0.0530 ns | 0.0496 ns | 1.243 ns | 1.194 ns | 1.348 ns |     - |     - |     - |         - |
|    **Parse** |  **True** | **7.714 ns** | **0.0990 ns** | **0.0878 ns** | **7.697 ns** | **7.616 ns** | **7.886 ns** |     **-** |     **-** |     **-** |         **-** |
| TryParse |  True | 4.143 ns | 0.0715 ns | 0.0669 ns | 4.119 ns | 4.062 ns | 4.262 ns |     - |     - |     - |         - |
| ToString |  True | 1.690 ns | 0.0468 ns | 0.0438 ns | 1.686 ns | 1.640 ns | 1.777 ns |     - |     - |     - |         - |
