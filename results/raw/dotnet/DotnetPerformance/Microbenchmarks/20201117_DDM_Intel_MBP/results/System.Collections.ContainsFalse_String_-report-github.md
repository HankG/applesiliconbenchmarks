``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|              Array |  512 |   952.80 μs | 11.783 μs | 11.022 μs |   947.00 μs |   940.58 μs |   973.48 μs |     - |     - |     - |       1 B |
|               Span |  512 |   789.33 μs | 11.590 μs | 10.841 μs |   786.38 μs |   776.04 μs |   814.40 μs |     - |     - |     - |       1 B |
|               List |  512 |   862.03 μs | 11.061 μs |  9.805 μs |   860.42 μs |   842.52 μs |   881.41 μs |     - |     - |     - |       1 B |
|        ICollection |  512 |   876.47 μs |  9.950 μs |  9.307 μs |   878.21 μs |   860.23 μs |   890.36 μs |     - |     - |     - |       1 B |
|         LinkedList |  512 | 1,188.93 μs | 12.389 μs | 11.589 μs | 1,185.06 μs | 1,172.48 μs | 1,211.69 μs |     - |     - |     - |       1 B |
|            HashSet |  512 |    10.17 μs |  0.102 μs |  0.091 μs |    10.16 μs |    10.03 μs |    10.32 μs |     - |     - |     - |         - |
|              Queue |  512 |   878.39 μs | 13.736 μs | 12.849 μs |   875.26 μs |   859.24 μs |   903.72 μs |     - |     - |     - |       1 B |
|              Stack |  512 |   969.67 μs | 11.827 μs | 10.485 μs |   971.65 μs |   954.25 μs |   987.59 μs |     - |     - |     - |       1 B |
|          SortedSet |  512 |   264.78 μs |  3.665 μs |  3.429 μs |   264.26 μs |   260.27 μs |   271.29 μs |     - |     - |     - |         - |
|     ImmutableArray |  512 |   867.91 μs |  5.387 μs |  4.498 μs |   867.97 μs |   857.28 μs |   875.68 μs |     - |     - |     - |       1 B |
|   ImmutableHashSet |  512 |    33.14 μs |  0.563 μs |  0.499 μs |    33.01 μs |    32.52 μs |    34.00 μs |     - |     - |     - |         - |
|      ImmutableList |  512 | 2,814.91 μs | 35.579 μs | 33.281 μs | 2,811.95 μs | 2,773.41 μs | 2,880.77 μs |     - |     - |     - |       4 B |
| ImmutableSortedSet |  512 |   271.07 μs |  4.518 μs |  4.005 μs |   270.17 μs |   266.74 μs |   281.77 μs |     - |     - |     - |         - |
