``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |                            arguments |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |------------------------------------- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|  **EncodeUtf8** |        **JavaScript,&amp;Hello+&lt;World&gt;!,16** |   **154.28 ns** |  **1.575 ns** |  **1.473 ns** |   **153.92 ns** |   **152.13 ns** |   **156.77 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |        JavaScript,&amp;Hello+&lt;World&gt;!,16 |   112.27 ns |  1.396 ns |  1.237 ns |   112.11 ns |   110.60 ns |   114.29 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |       **JavaScript,&amp;Hello+&lt;World&gt;!,512** | **1,865.32 ns** | **21.329 ns** | **19.951 ns** | **1,860.33 ns** | **1,838.85 ns** | **1,900.36 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |       JavaScript,&amp;Hello+&lt;World&gt;!,512 |   234.50 ns |  3.808 ns |  3.375 ns |   233.29 ns |   231.05 ns |   241.76 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |   **JavaScript,no escaping required,16** |   **138.64 ns** |  **1.538 ns** |  **1.439 ns** |   **138.40 ns** |   **136.66 ns** |   **141.06 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |   JavaScript,no escaping required,16 |    20.34 ns |  0.212 ns |  0.199 ns |    20.27 ns |    20.01 ns |    20.67 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |  **JavaScript,no escaping required,512** | **1,842.57 ns** | **17.661 ns** | **15.656 ns** | **1,841.89 ns** | **1,813.66 ns** | **1,875.59 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |  JavaScript,no escaping required,512 |   144.05 ns |  1.904 ns |  1.781 ns |   143.46 ns |   141.96 ns |   147.46 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |       **UnsafeRelaxed,hello &quot;there&quot;,16** |   **136.66 ns** |  **1.629 ns** |  **1.444 ns** |   **136.13 ns** |   **134.61 ns** |   **139.55 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |       UnsafeRelaxed,hello &quot;there&quot;,16 |    57.33 ns |  0.470 ns |  0.392 ns |    57.25 ns |    56.75 ns |    58.00 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |      **UnsafeRelaxed,hello &quot;there&quot;,512** | **1,838.10 ns** | **23.150 ns** | **21.654 ns** | **1,829.35 ns** | **1,813.66 ns** | **1,884.67 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |      UnsafeRelaxed,hello &quot;there&quot;,512 |   335.34 ns |  3.247 ns |  3.037 ns |   334.88 ns |   330.79 ns |   341.21 ns |     - |     - |     - |         - |
|  **EncodeUtf8** | **UnsafeRelaxed(...)&gt; required,16 [41]** |   **152.13 ns** |  **2.013 ns** |  **1.883 ns** |   **151.45 ns** |   **150.04 ns** |   **155.43 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 | UnsafeRelaxed(...)&gt; required,16 [41] |    31.91 ns |  0.466 ns |  0.436 ns |    31.78 ns |    31.39 ns |    32.57 ns |     - |     - |     - |         - |
|  **EncodeUtf8** | **UnsafeRelaxed(...) required,512 [42]** | **1,853.72 ns** | **27.612 ns** | **25.828 ns** | **1,846.13 ns** | **1,828.41 ns** | **1,902.31 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 | UnsafeRelaxed(...) required,512 [42] |   305.47 ns |  5.388 ns |  4.776 ns |   304.57 ns |   300.41 ns |   314.85 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |   **Url,&amp;lorem ipsum=dolor sit amet,16** |   **222.53 ns** |  **2.724 ns** |  **2.548 ns** |   **221.71 ns** |   **219.32 ns** |   **226.27 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |   Url,&amp;lorem ipsum=dolor sit amet,16 |   149.29 ns |  2.047 ns |  1.814 ns |   148.54 ns |   147.50 ns |   152.90 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |  **Url,&amp;lorem ipsum=dolor sit amet,512** | **1,930.20 ns** | **21.974 ns** | **20.554 ns** | **1,924.62 ns** | **1,903.85 ns** | **1,969.78 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |  Url,&amp;lorem ipsum=dolor sit amet,512 |   524.79 ns |  7.166 ns |  6.703 ns |   523.52 ns |   514.22 ns |   536.67 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |                         **Url,�2020,16** |   **114.63 ns** |  **1.188 ns** |  **1.053 ns** |   **114.46 ns** |   **113.26 ns** |   **116.96 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |                         Url,�2020,16 |    57.37 ns |  0.513 ns |  0.480 ns |    57.39 ns |    56.47 ns |    58.16 ns |     - |     - |     - |         - |
|  **EncodeUtf8** |                        **Url,�2020,512** | **1,827.22 ns** | **20.461 ns** | **18.138 ns** | **1,824.42 ns** | **1,804.24 ns** | **1,864.14 ns** |     **-** |     **-** |     **-** |         **-** |
| EncodeUtf16 |                        Url,�2020,512 |   428.75 ns |  4.769 ns |  4.461 ns |   427.14 ns |   424.28 ns |   437.80 ns |     - |     - |     - |         - |
