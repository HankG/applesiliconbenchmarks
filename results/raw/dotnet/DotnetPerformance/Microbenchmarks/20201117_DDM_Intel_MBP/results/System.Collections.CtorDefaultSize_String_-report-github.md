``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method |         Mean |       Error |      StdDev |       Median |        Min |          Max |  Gen 0 |  Gen 1 |  Gen 2 | Allocated |
|--------------------- |-------------:|------------:|------------:|-------------:|-----------:|-------------:|-------:|-------:|-------:|----------:|
|                 List |    11.156 ns |   0.2774 ns |   0.2595 ns |    11.141 ns |  10.770 ns |    11.675 ns | 0.0076 |      - |      - |      32 B |
|           LinkedList |     4.377 ns |   0.1477 ns |   0.1382 ns |     4.341 ns |   4.243 ns |     4.695 ns | 0.0095 |      - |      - |      40 B |
|              HashSet |    11.837 ns |   0.2395 ns |   0.2240 ns |    11.801 ns |  11.539 ns |    12.283 ns | 0.0172 |      - |      - |      72 B |
|           Dictionary |    12.279 ns |   0.3085 ns |   0.2735 ns |    12.143 ns |  11.991 ns |    12.889 ns | 0.0191 |      - |      - |      80 B |
|                Queue |    15.360 ns |   0.3468 ns |   0.3244 ns |    15.278 ns |  14.983 ns |    15.937 ns | 0.0096 |      - |      - |      40 B |
|                Stack |    15.037 ns |   0.3318 ns |   0.3103 ns |    15.003 ns |  14.614 ns |    15.515 ns | 0.0076 |      - |      - |      32 B |
|           SortedList |    25.300 ns |   0.4159 ns |   0.3890 ns |    25.228 ns |  24.775 ns |    25.916 ns | 0.0153 |      - |      - |      64 B |
|            SortedSet |    12.775 ns |   0.2972 ns |   0.2919 ns |    12.673 ns |  12.433 ns |    13.360 ns | 0.0115 |      - |      - |      48 B |
|     SortedDictionary |    30.606 ns |   0.5089 ns |   0.4511 ns |    30.626 ns |  29.970 ns |    31.548 ns | 0.0267 |      - |      - |     112 B |
| ConcurrentDictionary |    93.580 ns |   1.6402 ns |   1.5342 ns |    93.311 ns |  91.172 ns |    96.420 ns | 0.1625 |      - |      - |     680 B |
|      ConcurrentQueue |    72.885 ns |   1.3323 ns |   1.2462 ns |    72.470 ns |  71.486 ns |    75.033 ns | 0.1987 |      - |      - |     832 B |
|      ConcurrentStack |     3.254 ns |   0.1012 ns |   0.0897 ns |     3.237 ns |   3.148 ns |     3.449 ns | 0.0057 |      - |      - |      24 B |
|        ConcurrentBag | 1,000.715 ns | 139.3338 ns | 160.4570 ns | 1,013.280 ns | 641.631 ns | 1,318.379 ns | 0.0269 | 0.0077 | 0.0038 |     128 B |
