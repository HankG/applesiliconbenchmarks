``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|--------- |---------:|--------:|--------:|---------:|---------:|---------:|--------:|--------:|--------:|----------:|
|      Jil | 476.7 μs | 7.11 μs | 5.94 μs | 477.7 μs | 466.8 μs | 486.9 μs | 89.5833 | 43.7500 | 43.7500 | 424.38 KB |
| JSON.NET | 733.9 μs | 9.02 μs | 7.53 μs | 733.1 μs | 721.5 μs | 743.6 μs | 93.8416 | 46.9208 | 46.9208 | 452.64 KB |
| Utf8Json | 487.2 μs | 9.68 μs | 9.05 μs | 487.4 μs | 473.7 μs | 504.2 μs | 85.3890 | 85.3890 | 85.3890 | 394.66 KB |
