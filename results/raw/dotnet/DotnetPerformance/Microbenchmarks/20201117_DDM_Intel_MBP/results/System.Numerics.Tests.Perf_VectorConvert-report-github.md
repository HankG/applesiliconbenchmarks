``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|    Convert_float_int |    692.1 ns |   9.05 ns |   8.02 ns |    690.9 ns |    680.8 ns |    710.1 ns |     - |     - |     - |         - |
|   Convert_float_uint | 11,174.3 ns | 135.06 ns | 126.34 ns | 11,107.8 ns | 11,033.6 ns | 11,427.9 ns |     - |     - |     - |         - |
|  Convert_double_long |  2,317.0 ns |  25.70 ns |  24.04 ns |  2,308.8 ns |  2,274.8 ns |  2,353.4 ns |     - |     - |     - |         - |
| Convert_double_ulong | 13,623.9 ns | 167.15 ns | 156.35 ns | 13,580.1 ns | 13,460.5 ns | 13,911.5 ns |     - |     - |     - |         - |
|    Convert_int_float |    691.7 ns |   9.21 ns |   8.62 ns |    689.3 ns |    680.7 ns |    706.3 ns |     - |     - |     - |         - |
|   Convert_uint_float |    926.2 ns |  11.65 ns |  10.90 ns |    922.6 ns |    912.0 ns |    949.3 ns |     - |     - |     - |         - |
|  Convert_long_double |  2,291.2 ns |  25.86 ns |  24.19 ns |  2,290.5 ns |  2,255.1 ns |  2,340.9 ns |     - |     - |     - |         - |
| Convert_ulong_double |  1,375.2 ns |  14.97 ns |  14.00 ns |  1,369.9 ns |  1,360.3 ns |  1,405.5 ns |     - |     - |     - |         - |
|        Narrow_double |    807.9 ns |  12.08 ns |  10.71 ns |    804.7 ns |    790.8 ns |    826.7 ns |     - |     - |     - |         - |
|         Narrow_short |    999.9 ns |  13.15 ns |  11.66 ns |    998.0 ns |    982.6 ns |  1,026.6 ns |     - |     - |     - |         - |
|        Narrow_ushort |  1,146.8 ns |  14.63 ns |  13.68 ns |  1,144.9 ns |  1,124.0 ns |  1,170.1 ns |     - |     - |     - |         - |
|           Narrow_int |  1,031.7 ns |  13.86 ns |  12.97 ns |  1,031.3 ns |  1,014.0 ns |  1,052.0 ns |     - |     - |     - |         - |
|          Narrow_uint |  1,032.9 ns |  13.21 ns |  12.35 ns |  1,029.0 ns |  1,018.0 ns |  1,058.5 ns |     - |     - |     - |         - |
|          Narrow_long |  1,611.9 ns |  24.86 ns |  23.25 ns |  1,610.4 ns |  1,581.3 ns |  1,655.3 ns |     - |     - |     - |         - |
|         Narrow_ulong |  1,620.0 ns |  35.34 ns |  39.28 ns |  1,602.7 ns |  1,578.7 ns |  1,711.7 ns |     - |     - |     - |         - |
|          Widen_float |    750.7 ns |  10.55 ns |   9.35 ns |    752.5 ns |    732.6 ns |    763.7 ns |     - |     - |     - |         - |
|          Widen_sbyte |    940.2 ns |  20.65 ns |  22.10 ns |    932.9 ns |    916.8 ns |    986.9 ns |     - |     - |     - |         - |
|           Widen_byte |    920.3 ns |  11.48 ns |  10.74 ns |    916.2 ns |    908.9 ns |    939.8 ns |     - |     - |     - |         - |
|          Widen_short |    925.4 ns |  14.70 ns |  13.03 ns |    919.3 ns |    908.6 ns |    950.3 ns |     - |     - |     - |         - |
|         Widen_ushort |    923.0 ns |  16.14 ns |  14.31 ns |    920.8 ns |    904.8 ns |    955.8 ns |     - |     - |     - |         - |
|            Widen_int |    928.6 ns |   8.68 ns |   8.12 ns |    927.6 ns |    917.8 ns |    941.7 ns |     - |     - |     - |         - |
|           Widen_uint |    919.1 ns |   9.91 ns |   8.78 ns |    921.1 ns |    906.6 ns |    938.0 ns |     - |     - |     - |         - |
