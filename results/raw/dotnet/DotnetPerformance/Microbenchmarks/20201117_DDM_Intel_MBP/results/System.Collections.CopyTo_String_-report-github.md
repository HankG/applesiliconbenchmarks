``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | Size |     Mean |    Error |   StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |----- |---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|          Array | 2048 | 417.8 ns | 35.02 ns | 40.33 ns | 392.0 ns | 375.8 ns | 467.4 ns |     - |     - |     - |         - |
|           Span | 2048 | 516.5 ns |  5.62 ns |  4.98 ns | 516.2 ns | 510.9 ns | 525.8 ns |     - |     - |     - |         - |
|   ReadOnlySpan | 2048 | 519.0 ns |  5.47 ns |  5.12 ns | 518.8 ns | 511.2 ns | 527.5 ns |     - |     - |     - |         - |
|         Memory | 2048 | 531.0 ns |  5.24 ns |  4.64 ns | 530.4 ns | 525.1 ns | 543.3 ns |     - |     - |     - |         - |
| ReadOnlyMemory | 2048 | 524.2 ns |  4.14 ns |  3.87 ns | 523.9 ns | 517.6 ns | 529.6 ns |     - |     - |     - |         - |
|           List | 2048 | 496.3 ns |  5.04 ns |  4.72 ns | 494.6 ns | 488.4 ns | 505.4 ns |     - |     - |     - |         - |
| ImmutableArray | 2048 | 433.7 ns |  8.42 ns |  7.47 ns | 433.4 ns | 419.6 ns | 445.8 ns |     - |     - |     - |         - |
