``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |             Mean |         Error |        StdDev |           Median |              Min |              Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |-----------------:|--------------:|--------------:|-----------------:|-----------------:|-----------------:|------:|------:|------:|----------:|
|     TryWriteThenTryRead |         41.08 ns |      0.593 ns |      0.526 ns |         41.05 ns |         40.36 ns |         42.31 ns |     - |     - |     - |         - |
| WriteAsyncThenReadAsync |         67.44 ns |      0.900 ns |      0.842 ns |         67.05 ns |         66.48 ns |         68.74 ns |     - |     - |     - |         - |
| ReadAsyncThenWriteAsync |         91.57 ns |      1.021 ns |      0.905 ns |         91.26 ns |         90.42 ns |         93.40 ns |     - |     - |     - |         - |
|                PingPong | 13,398,614.65 ns | 85,756.910 ns | 80,217.067 ns | 13,434,434.69 ns | 13,251,857.69 ns | 13,514,613.19 ns |     - |     - |     - |   18262 B |
