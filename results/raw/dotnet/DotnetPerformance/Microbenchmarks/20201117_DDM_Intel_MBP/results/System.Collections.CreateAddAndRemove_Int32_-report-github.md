``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|----------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|-------:|------:|----------:|
|             List |  512 |  15.686 μs | 0.2019 μs | 0.1889 μs |  15.648 μs |  15.445 μs |  15.999 μs | 1.0132 |      - |     - |    4.2 KB |
|       LinkedList |  512 |  12.466 μs | 0.2424 μs | 0.2267 μs |  12.369 μs |  12.220 μs |  12.875 μs | 5.8460 | 0.0475 |     - |  24.04 KB |
|          HashSet |  512 |  11.951 μs | 0.2317 μs | 0.2168 μs |  11.865 μs |  11.707 μs |  12.439 μs | 6.6160 | 0.0469 |     - |  27.07 KB |
|       Dictionary |  512 |  13.404 μs | 0.1846 μs | 0.1541 μs |  13.442 μs |  13.041 μs |  13.612 μs | 8.2237 | 0.0531 |     - |  33.69 KB |
|       SortedList |  512 |  99.679 μs | 0.7562 μs | 0.7073 μs |  99.452 μs |  98.635 μs | 101.000 μs | 1.9904 |      - |     - |   8.41 KB |
|        SortedSet |  512 | 108.767 μs | 1.8181 μs | 1.7007 μs | 108.647 μs | 106.727 μs | 111.845 μs | 4.6769 |      - |     - |  20.05 KB |
| SortedDictionary |  512 | 141.337 μs | 2.3765 μs | 2.2230 μs | 140.424 μs | 138.893 μs | 145.027 μs | 5.6306 |      - |     - |  24.11 KB |
|            Stack |  512 |   2.157 μs | 0.0484 μs | 0.0538 μs |   2.134 μs |   2.087 μs |   2.269 μs | 1.0287 |      - |     - |    4.2 KB |
|            Queue |  512 |   2.711 μs | 0.0425 μs | 0.0398 μs |   2.714 μs |   2.647 μs |   2.768 μs | 1.0281 |      - |     - |   4.21 KB |
