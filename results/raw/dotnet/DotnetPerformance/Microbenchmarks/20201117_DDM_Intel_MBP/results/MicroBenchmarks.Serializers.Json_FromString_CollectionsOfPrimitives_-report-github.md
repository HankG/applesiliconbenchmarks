``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|--------:|------:|----------:|
|      Jil | 277.7 μs |  3.98 μs |  3.72 μs | 276.8 μs | 273.4 μs | 285.0 μs | 35.7143 | 11.1607 |     - | 167.61 KB |
| JSON.NET | 561.7 μs | 10.74 μs | 10.05 μs | 559.7 μs | 549.6 μs | 578.1 μs | 71.7489 | 22.4215 |     - | 299.89 KB |
| Utf8Json | 287.3 μs |  5.73 μs |  5.08 μs | 285.5 μs | 280.3 μs | 298.7 μs | 53.5714 | 16.7411 |     - | 222.73 KB |
