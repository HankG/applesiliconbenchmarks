``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |      value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |----------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **ToString** |          **0** |  **2.033 ns** | **0.0644 ns** | **0.0571 ns** |  **2.016 ns** |  **1.976 ns** |  **2.139 ns** |      **-** |     **-** |     **-** |         **-** |
|   TryFormat |          0 |  4.329 ns | 0.0591 ns | 0.0552 ns |  4.317 ns |  4.260 ns |  4.438 ns |      - |     - |     - |         - |
|   ParseSpan |          0 | 12.786 ns | 0.2042 ns | 0.1910 ns | 12.718 ns | 12.581 ns | 13.204 ns |      - |     - |     - |         - |
|       Parse |          0 | 11.557 ns | 0.1344 ns | 0.1191 ns | 11.548 ns | 11.388 ns | 11.789 ns |      - |     - |     - |         - |
|    TryParse |          0 | 10.564 ns | 0.1480 ns | 0.1384 ns | 10.533 ns | 10.395 ns | 10.838 ns |      - |     - |     - |         - |
| TryParseHex |          0 |  8.894 ns | 0.1248 ns | 0.1168 ns |  8.858 ns |  8.715 ns |  9.046 ns |      - |     - |     - |         - |
|    **ToString** |      **12345** | **11.601 ns** | **0.2609 ns** | **0.2440 ns** | **11.556 ns** | **11.308 ns** | **12.081 ns** | **0.0076** |     **-** |     **-** |      **32 B** |
|   TryFormat |      12345 |  8.297 ns | 0.1661 ns | 0.1554 ns |  8.287 ns |  8.075 ns |  8.651 ns |      - |     - |     - |         - |
|   ParseSpan |      12345 | 16.158 ns | 0.1869 ns | 0.1749 ns | 16.127 ns | 15.924 ns | 16.450 ns |      - |     - |     - |         - |
|       Parse |      12345 | 15.736 ns | 0.3144 ns | 0.2941 ns | 15.676 ns | 15.388 ns | 16.218 ns |      - |     - |     - |         - |
|    TryParse |      12345 | 14.947 ns | 0.1808 ns | 0.1691 ns | 14.876 ns | 14.721 ns | 15.283 ns |      - |     - |     - |         - |
| **TryParseHex** |       **3039** | **13.935 ns** | **0.2970 ns** | **0.2917 ns** | **13.835 ns** | **13.618 ns** | **14.720 ns** |      **-** |     **-** |     **-** |         **-** |
|    **ToString** | **4294967295** | **17.351 ns** | **0.3701 ns** | **0.3281 ns** | **17.261 ns** | **16.910 ns** | **18.063 ns** | **0.0115** |     **-** |     **-** |      **48 B** |
|   TryFormat | 4294967295 | 13.742 ns | 0.1410 ns | 0.1250 ns | 13.702 ns | 13.543 ns | 13.957 ns |      - |     - |     - |         - |
|   ParseSpan | 4294967295 | 22.609 ns | 0.2866 ns | 0.2681 ns | 22.538 ns | 22.258 ns | 23.212 ns |      - |     - |     - |         - |
|       Parse | 4294967295 | 21.934 ns | 0.3311 ns | 0.2935 ns | 21.932 ns | 21.562 ns | 22.654 ns |      - |     - |     - |         - |
|    TryParse | 4294967295 | 21.070 ns | 0.2768 ns | 0.2589 ns | 21.008 ns | 20.760 ns | 21.566 ns |      - |     - |     - |         - |
| **TryParseHex** |   **FFFFFFFF** | **19.470 ns** | **0.2087 ns** | **0.1850 ns** | **19.415 ns** | **19.150 ns** | **19.767 ns** |      **-** |     **-** |     **-** |         **-** |
