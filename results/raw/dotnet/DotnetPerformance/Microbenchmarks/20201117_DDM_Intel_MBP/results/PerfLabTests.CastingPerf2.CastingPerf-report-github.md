``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |    Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|----------:|
|               ObjFooIsObj | 455.7 μs |  5.51 μs |  5.16 μs | 454.0 μs | 448.9 μs | 465.5 μs |        - |     - |     - |       1 B |
|               FooObjIsFoo | 548.9 μs |  9.59 μs |  8.97 μs | 545.1 μs | 540.7 μs | 572.0 μs |        - |     - |     - |       1 B |
|              FooObjIsNull | 455.9 μs |  5.76 μs |  5.10 μs | 454.9 μs | 450.1 μs | 466.4 μs |        - |     - |     - |       1 B |
|        FooObjIsDescendant | 496.9 μs |  4.32 μs |  4.04 μs | 496.3 μs | 491.6 μs | 504.3 μs |        - |     - |     - |       1 B |
|             IFooFooIsIFoo | 454.7 μs |  4.02 μs |  3.56 μs | 454.4 μs | 449.2 μs | 462.3 μs |        - |     - |     - |       1 B |
|             IFooObjIsIFoo | 549.4 μs |  8.05 μs |  7.13 μs | 549.9 μs | 540.3 μs | 560.4 μs |        - |     - |     - |       1 B |
|    IFooObjIsIFooInterAlia | 416.1 μs |  4.69 μs |  4.38 μs | 415.3 μs | 409.8 μs | 422.9 μs |        - |     - |     - |         - |
| IFooObjIsDescendantOfIFoo | 545.7 μs |  6.14 μs |  5.44 μs | 543.9 μs | 538.6 μs | 556.9 μs |        - |     - |     - |       1 B |
|                    ObjInt | 507.2 μs |  8.32 μs |  7.78 μs | 505.2 μs | 498.4 μs | 522.4 μs | 572.5806 |     - |     - | 2400001 B |
|                    IntObj | 199.3 μs |  1.99 μs |  1.86 μs | 199.9 μs | 195.8 μs | 202.2 μs |        - |     - |     - |         - |
|        ObjScalarValueType | 538.5 μs |  9.23 μs |  8.64 μs | 538.3 μs | 525.9 μs | 550.2 μs | 572.9167 |     - |     - | 2400001 B |
|        ScalarValueTypeObj | 289.9 μs |  2.71 μs |  2.54 μs | 289.7 μs | 285.9 μs | 293.5 μs |        - |     - |     - |         - |
|        ObjObjrefValueType | 924.2 μs | 15.36 μs | 14.37 μs | 921.7 μs | 902.2 μs | 950.0 μs | 764.7059 |     - |     - | 3200001 B |
|        ObjrefValueTypeObj | 943.4 μs | 12.68 μs | 11.24 μs | 942.5 μs | 927.5 μs | 963.9 μs |        - |     - |     - |       1 B |
|           FooObjCastIfIsa | 386.0 μs |  3.76 μs |  3.52 μs | 385.2 μs | 381.6 μs | 391.4 μs |        - |     - |     - |         - |
