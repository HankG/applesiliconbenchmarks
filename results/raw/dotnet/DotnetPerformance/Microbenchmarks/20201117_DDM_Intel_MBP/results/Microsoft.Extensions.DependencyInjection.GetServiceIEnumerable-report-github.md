``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|    Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| Transient | 94.06 ns | 1.824 ns | 1.873 ns | 93.68 ns | 91.59 ns | 98.49 ns | 0.0822 |     - |     - |     344 B |
|    Scoped | 48.51 ns | 0.559 ns | 0.523 ns | 48.55 ns | 47.14 ns | 49.15 ns |      - |     - |     - |         - |
| Singleton | 22.68 ns | 0.276 ns | 0.245 ns | 22.65 ns | 22.38 ns | 23.19 ns |      - |     - |     - |         - |
