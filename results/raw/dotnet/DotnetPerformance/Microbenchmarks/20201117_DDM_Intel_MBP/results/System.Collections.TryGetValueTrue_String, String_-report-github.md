``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  11.48 μs | 0.139 μs | 0.130 μs |  11.45 μs |  11.27 μs |  11.68 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  12.52 μs | 0.137 μs | 0.128 μs |  12.52 μs |  12.36 μs |  12.81 μs |     - |     - |     - |         - |
|                SortedList |  512 | 210.45 μs | 2.248 μs | 2.103 μs | 210.59 μs | 206.92 μs | 214.26 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 242.33 μs | 3.239 μs | 3.030 μs | 241.95 μs | 238.05 μs | 248.82 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  15.76 μs | 0.186 μs | 0.174 μs |  15.78 μs |  15.48 μs |  16.04 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 |  33.91 μs | 0.440 μs | 0.411 μs |  33.77 μs |  33.46 μs |  34.77 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 212.51 μs | 2.367 μs | 2.214 μs | 211.98 μs | 209.71 μs | 216.92 μs |     - |     - |     - |         - |
