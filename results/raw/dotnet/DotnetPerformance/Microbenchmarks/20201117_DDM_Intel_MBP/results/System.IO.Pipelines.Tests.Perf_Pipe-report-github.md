``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                             Method |       Mean |    Error |   StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                      SyncReadAsync |   828.7 μs |  9.91 μs |  8.78 μs |   827.9 μs |   818.7 μs |   844.5 μs |     - |     - |     - |       1 B |
|                          ReadAsync | 2,459.0 μs | 48.00 μs | 44.90 μs | 2,443.3 μs | 2,408.4 μs | 2,577.7 μs |     - |     - |     - |       3 B |
| SyncReadAsyncWithCancellationToken |   836.0 μs | 10.05 μs |  9.40 μs |   834.1 μs |   823.2 μs |   854.3 μs |     - |     - |     - |       1 B |
|     ReadAsyncWithCancellationToken | 2,814.1 μs | 32.95 μs | 30.83 μs | 2,800.3 μs | 2,775.6 μs | 2,870.4 μs |     - |     - |     - |       3 B |
