``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|            CreateFromVector4Benchmark |  5.0024 ns | 0.1246 ns | 0.1165 ns |  4.9667 ns |  4.8794 ns |  5.2892 ns |     - |     - |     - |         - |
| CreateFromVector3WithScalarDBenchmark |  4.8162 ns | 0.0786 ns | 0.0697 ns |  4.7827 ns |  4.7482 ns |  4.9591 ns |     - |     - |     - |         - |
|         CreateFromScalarXYZDBenchmark |  4.6166 ns | 0.0917 ns | 0.0813 ns |  4.6317 ns |  4.4936 ns |  4.7322 ns |     - |     - |     - |         - |
|             EqualityOperatorBenchmark |  1.1680 ns | 0.0374 ns | 0.0332 ns |  1.1541 ns |  1.1295 ns |  1.2309 ns |     - |     - |     - |         - |
|           InequalityOperatorBenchmark |  1.1633 ns | 0.0491 ns | 0.0483 ns |  1.1439 ns |  1.1008 ns |  1.2367 ns |     - |     - |     - |         - |
|           CreateFromVerticesBenchmark |  8.1532 ns | 0.2032 ns | 0.2340 ns |  8.1002 ns |  7.8226 ns |  8.6194 ns |     - |     - |     - |         - |
|                          DotBenchmark |  1.5464 ns | 0.0449 ns | 0.0420 ns |  1.5490 ns |  1.4953 ns |  1.6420 ns |     - |     - |     - |         - |
|                DotCoordinateBenchmark |  0.4213 ns | 0.0336 ns | 0.0330 ns |  0.4163 ns |  0.3756 ns |  0.4852 ns |     - |     - |     - |         - |
|                    DotNormalBenchmark |  0.3788 ns | 0.0207 ns | 0.0193 ns |  0.3744 ns |  0.3525 ns |  0.4191 ns |     - |     - |     - |         - |
|                       EqualsBenchmark |  1.5648 ns | 0.0308 ns | 0.0257 ns |  1.5579 ns |  1.5257 ns |  1.6124 ns |     - |     - |     - |         - |
|                    NormalizeBenchmark |  5.7248 ns | 0.0949 ns | 0.0888 ns |  5.6940 ns |  5.6154 ns |  5.9056 ns |     - |     - |     - |         - |
|         TransformByMatrix4x4Benchmark | 28.4399 ns | 0.5800 ns | 0.6447 ns | 28.5797 ns | 27.5516 ns | 29.6963 ns |     - |     - |     - |         - |
|        TransformByQuaternionBenchmark |  8.4510 ns | 0.1393 ns | 0.1235 ns |  8.4069 ns |  8.3133 ns |  8.6894 ns |     - |     - |     - |         - |
