``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RTZWTM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=5000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|               Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|-------:|------:|----------:|
|                Array |  512 |  9.297 μs | 0.4087 μs | 0.4373 μs |  9.321 μs |  8.602 μs | 10.110 μs |      - |      - |     - |         - |
|  Array_ComparerClass |  512 | 30.921 μs | 0.3054 μs | 0.2857 μs | 30.867 μs | 30.415 μs | 31.375 μs |      - |      - |     - |      64 B |
| Array_ComparerStruct |  512 | 35.074 μs | 0.4476 μs | 0.4187 μs | 35.034 μs | 34.490 μs | 35.910 μs |      - |      - |     - |      88 B |
|     Array_Comparison |  512 | 31.363 μs | 0.6071 μs | 0.5679 μs | 31.206 μs | 30.760 μs | 32.611 μs |      - |      - |     - |         - |
|                 List |  512 |  9.064 μs | 0.1159 μs | 0.1084 μs |  9.078 μs |  8.830 μs |  9.222 μs |      - |      - |     - |         - |
|            LinqQuery |  512 | 57.784 μs | 0.7669 μs | 0.7174 μs | 57.605 μs | 56.722 μs | 59.220 μs | 8.2000 | 0.4000 |     - |   35152 B |
| LinqOrderByExtension |  512 | 58.102 μs | 0.7610 μs | 0.6746 μs | 57.883 μs | 57.181 μs | 59.178 μs | 8.2000 | 0.4000 |     - |   35152 B |
