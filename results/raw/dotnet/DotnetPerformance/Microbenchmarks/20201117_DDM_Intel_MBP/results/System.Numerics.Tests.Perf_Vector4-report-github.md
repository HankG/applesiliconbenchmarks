``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                    Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------ |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                          CreateFromScalar | 0.3438 ns | 0.0177 ns | 0.0166 ns | 0.3376 ns | 0.3258 ns | 0.3725 ns |     - |     - |     - |         - |
|      CreateFromVector3WithScalarBenchmark | 0.4154 ns | 0.0283 ns | 0.0265 ns | 0.4076 ns | 0.3799 ns | 0.4652 ns |     - |     - |     - |         - |
|      CreateFromVector2WithScalarBenchmark | 0.4540 ns | 0.0336 ns | 0.0330 ns | 0.4512 ns | 0.4069 ns | 0.5335 ns |     - |     - |     - |         - |
|             CreateFromScalarXYZWBenchmark | 0.3232 ns | 0.0175 ns | 0.0164 ns | 0.3137 ns | 0.3091 ns | 0.3529 ns |     - |     - |     - |         - |
|                              OneBenchmark | 0.1487 ns | 0.0300 ns | 0.0345 ns | 0.1337 ns | 0.1077 ns | 0.2244 ns |     - |     - |     - |         - |
|                            UnitXBenchmark | 0.0965 ns | 0.0174 ns | 0.0163 ns | 0.0888 ns | 0.0794 ns | 0.1240 ns |     - |     - |     - |         - |
|                            UnitYBenchmark | 0.1076 ns | 0.0287 ns | 0.0268 ns | 0.0936 ns | 0.0818 ns | 0.1713 ns |     - |     - |     - |         - |
|                            UnitZBenchmark | 0.0940 ns | 0.0170 ns | 0.0159 ns | 0.0909 ns | 0.0723 ns | 0.1337 ns |     - |     - |     - |         - |
|                            UnitWBenchmark | 0.0923 ns | 0.0160 ns | 0.0150 ns | 0.0852 ns | 0.0783 ns | 0.1224 ns |     - |     - |     - |         - |
|                             ZeroBenchmark | 0.1221 ns | 0.0169 ns | 0.0150 ns | 0.1212 ns | 0.1013 ns | 0.1472 ns |     - |     - |     - |         - |
|                      AddOperatorBenchmark | 0.4725 ns | 0.0205 ns | 0.0192 ns | 0.4706 ns | 0.4462 ns | 0.5126 ns |     - |     - |     - |         - |
|                   DivideOperatorBenchmark | 0.4739 ns | 0.0293 ns | 0.0260 ns | 0.4706 ns | 0.4399 ns | 0.5347 ns |     - |     - |     - |         - |
|           DivideByScalarOperatorBenchmark | 0.1738 ns | 0.0148 ns | 0.0139 ns | 0.1694 ns | 0.1495 ns | 0.1954 ns |     - |     - |     - |         - |
|                 EqualityOperatorBenchmark | 0.4767 ns | 0.0208 ns | 0.0195 ns | 0.4721 ns | 0.4506 ns | 0.5098 ns |     - |     - |     - |         - |
|               InequalityOperatorBenchmark | 0.4883 ns | 0.0259 ns | 0.0229 ns | 0.4851 ns | 0.4528 ns | 0.5307 ns |     - |     - |     - |         - |
|                 MultiplyOperatorBenchmark | 0.4516 ns | 0.0191 ns | 0.0169 ns | 0.4507 ns | 0.4254 ns | 0.4876 ns |     - |     - |     - |         - |
|         MultiplyByScalarOperatorBenchmark | 0.1759 ns | 0.0122 ns | 0.0108 ns | 0.1745 ns | 0.1569 ns | 0.1975 ns |     - |     - |     - |         - |
|                 SubtractOperatorBenchmark | 0.4517 ns | 0.0203 ns | 0.0190 ns | 0.4504 ns | 0.4211 ns | 0.4892 ns |     - |     - |     - |         - |
|                   NegateOperatorBenchmark | 0.1221 ns | 0.0189 ns | 0.0177 ns | 0.1174 ns | 0.1037 ns | 0.1564 ns |     - |     - |     - |         - |
|                              AbsBenchmark | 0.2173 ns | 0.0307 ns | 0.0328 ns | 0.2117 ns | 0.1745 ns | 0.2832 ns |     - |     - |     - |         - |
|                      AddFunctionBenchmark | 0.4517 ns | 0.0220 ns | 0.0205 ns | 0.4498 ns | 0.4160 ns | 0.4976 ns |     - |     - |     - |         - |
|                            ClampBenchmark | 0.5011 ns | 0.0159 ns | 0.0149 ns | 0.5068 ns | 0.4801 ns | 0.5209 ns |     - |     - |     - |         - |
|                         DistanceBenchmark | 0.5384 ns | 0.0331 ns | 0.0309 ns | 0.5354 ns | 0.4921 ns | 0.6141 ns |     - |     - |     - |         - |
|                  DistanceSquaredBenchmark | 0.6002 ns | 0.0252 ns | 0.0236 ns | 0.6072 ns | 0.5638 ns | 0.6326 ns |     - |     - |     - |         - |
| DistanceSquaredJitOptimizeCanaryBenchmark | 0.6037 ns | 0.0251 ns | 0.0235 ns | 0.6025 ns | 0.5652 ns | 0.6364 ns |     - |     - |     - |         - |
|                           DivideBenchmark | 0.4496 ns | 0.0186 ns | 0.0165 ns | 0.4479 ns | 0.4294 ns | 0.4887 ns |     - |     - |     - |         - |
|                   DivideByScalarBenchmark | 0.1904 ns | 0.0107 ns | 0.0095 ns | 0.1913 ns | 0.1731 ns | 0.2095 ns |     - |     - |     - |         - |
|                              DotBenchmark | 1.3576 ns | 0.0299 ns | 0.0280 ns | 1.3535 ns | 1.3257 ns | 1.4207 ns |     - |     - |     - |         - |
|                           EqualsBenchmark | 0.4733 ns | 0.0213 ns | 0.0189 ns | 0.4682 ns | 0.4456 ns | 0.5108 ns |     - |     - |     - |         - |
|                      GetHashCodeBenchmark | 4.9328 ns | 0.0871 ns | 0.0815 ns | 4.8912 ns | 4.8369 ns | 5.0595 ns |     - |     - |     - |         - |
|                           LengthBenchmark | 0.3815 ns | 0.0183 ns | 0.0162 ns | 0.3767 ns | 0.3638 ns | 0.4154 ns |     - |     - |     - |         - |
|                    LengthSquaredBenchmark | 0.2218 ns | 0.0284 ns | 0.0266 ns | 0.2112 ns | 0.1910 ns | 0.2886 ns |     - |     - |     - |         - |
|                             LerpBenchmark | 0.4917 ns | 0.0216 ns | 0.0202 ns | 0.4890 ns | 0.4622 ns | 0.5268 ns |     - |     - |     - |         - |
|                              MaxBenchmark | 0.4690 ns | 0.0148 ns | 0.0131 ns | 0.4679 ns | 0.4474 ns | 0.4915 ns |     - |     - |     - |         - |
|                              MinBenchmark | 0.4804 ns | 0.0330 ns | 0.0308 ns | 0.4771 ns | 0.4381 ns | 0.5437 ns |     - |     - |     - |         - |
|                 MultiplyFunctionBenchmark | 0.4613 ns | 0.0209 ns | 0.0195 ns | 0.4557 ns | 0.4398 ns | 0.4996 ns |     - |     - |     - |         - |
|                 MultiplyByScalarBenchmark | 0.1867 ns | 0.0251 ns | 0.0223 ns | 0.1755 ns | 0.1618 ns | 0.2252 ns |     - |     - |     - |         - |
|                           NegateBenchmark | 0.1502 ns | 0.0143 ns | 0.0127 ns | 0.1476 ns | 0.1348 ns | 0.1803 ns |     - |     - |     - |         - |
|                        NormalizeBenchmark | 0.6323 ns | 0.0244 ns | 0.0228 ns | 0.6274 ns | 0.6084 ns | 0.6807 ns |     - |     - |     - |         - |
|                       SquareRootBenchmark | 0.1093 ns | 0.0139 ns | 0.0130 ns | 0.1102 ns | 0.0887 ns | 0.1339 ns |     - |     - |     - |         - |
|                 SubtractFunctionBenchmark | 0.4505 ns | 0.0162 ns | 0.0151 ns | 0.4477 ns | 0.4246 ns | 0.4834 ns |     - |     - |     - |         - |
|            TransformByQuaternionBenchmark | 4.8737 ns | 0.0598 ns | 0.0559 ns | 4.8568 ns | 4.7900 ns | 4.9787 ns |     - |     - |     - |         - |
|             TransformByMatrix4x4Benchmark | 6.8016 ns | 0.1063 ns | 0.0942 ns | 6.7847 ns | 6.6092 ns | 6.9856 ns |     - |     - |     - |         - |
|     TransformVector3ByQuaternionBenchmark | 4.8604 ns | 0.0787 ns | 0.0698 ns | 4.8506 ns | 4.7579 ns | 4.9738 ns |     - |     - |     - |         - |
|      TransformVector3ByMatrix4x4Benchmark | 6.5719 ns | 0.0691 ns | 0.0577 ns | 6.5662 ns | 6.5081 ns | 6.7155 ns |     - |     - |     - |         - |
|     TransformVector2ByQuaternionBenchmark | 3.7274 ns | 0.0465 ns | 0.0435 ns | 3.7115 ns | 3.6755 ns | 3.8077 ns |     - |     - |     - |         - |
|      TransformVector2ByMatrix4x4Benchmark | 5.7343 ns | 0.1254 ns | 0.1288 ns | 5.7019 ns | 5.5646 ns | 5.9908 ns |     - |     - |     - |         - |
