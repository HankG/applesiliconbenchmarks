``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method | value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **Parse** |     **0** | **11.194 ns** | **0.1842 ns** | **0.1633 ns** | **11.110 ns** | **11.035 ns** | **11.492 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |     0 | 10.500 ns | 0.1157 ns | 0.1082 ns | 10.474 ns | 10.341 ns | 10.715 ns |      - |     - |     - |         - |
| ToString |     0 |  2.368 ns | 0.0597 ns | 0.0529 ns |  2.373 ns |  2.279 ns |  2.456 ns |      - |     - |     - |         - |
|    **Parse** |   **255** | **12.583 ns** | **0.1800 ns** | **0.1684 ns** | **12.549 ns** | **12.365 ns** | **12.940 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |   255 | 12.003 ns | 0.1806 ns | 0.1601 ns | 11.929 ns | 11.773 ns | 12.315 ns |      - |     - |     - |         - |
| ToString |   255 |  9.940 ns | 0.2349 ns | 0.2197 ns |  9.905 ns |  9.632 ns | 10.224 ns | 0.0076 |     - |     - |      32 B |
