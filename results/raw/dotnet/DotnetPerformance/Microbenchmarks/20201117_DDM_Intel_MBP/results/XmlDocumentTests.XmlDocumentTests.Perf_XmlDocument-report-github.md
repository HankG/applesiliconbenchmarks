``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|             Create |   315.374 ns |  5.5125 ns |  5.1564 ns |   315.104 ns |   306.996 ns |   324.944 ns | 0.4397 |     - |     - |    1840 B |
|            LoadXml | 2,598.933 ns | 41.3164 ns | 38.6474 ns | 2,595.631 ns | 2,537.229 ns | 2,650.901 ns | 2.7902 |     - |     - |   11678 B |
| GetDocumentElement |     4.629 ns |  0.0898 ns |  0.0840 ns |     4.587 ns |     4.505 ns |     4.793 ns |      - |     - |     - |         - |
