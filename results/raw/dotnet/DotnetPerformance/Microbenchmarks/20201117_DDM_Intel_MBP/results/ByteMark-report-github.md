``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |        Gen 0 |        Gen 1 |        Gen 2 |      Allocated |
|---------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------------:|-------------:|-------------:|---------------:|
|      BenchNumericSortJagged | 1,269.7 ms |  3.72 ms |  3.30 ms | 1,270.1 ms | 1,263.5 ms | 1,274.6 ms |   12000.0000 |    6000.0000 |    1000.0000 |    63438.34 KB |
| BenchNumericSortRectangular | 1,147.6 ms |  3.41 ms |  3.02 ms | 1,146.8 ms | 1,144.2 ms | 1,154.9 ms |    2000.0000 |    2000.0000 |    2000.0000 |    31684.63 KB |
|             BenchStringSort | 1,303.1 ms |  5.14 ms |  4.81 ms | 1,301.1 ms | 1,296.5 ms | 1,312.1 ms |   14000.0000 |    7000.0000 |    2000.0000 |    69638.39 KB |
|                 BenchBitOps |   718.4 ms | 16.25 ms | 18.71 ms |   714.4 ms |   693.4 ms |   759.7 ms | 4166000.0000 | 4166000.0000 | 4166000.0000 | 12805859.52 KB |
|                BenchEmFloat | 3,129.3 ms |  6.95 ms |  6.17 ms | 3,127.4 ms | 3,118.5 ms | 3,139.0 ms |   21000.0000 |    2000.0000 |            - |   101717.64 KB |
|           BenchEmFloatClass |   650.3 ms |  5.28 ms |  4.68 ms |   650.8 ms |   642.4 ms |   658.5 ms |    7000.0000 |    1000.0000 |            - |    35216.42 KB |
|                BenchFourier |   327.0 ms |  3.71 ms |  3.47 ms |   326.4 ms |   321.6 ms |   332.5 ms |            - |            - |            - |      483.13 KB |
|           BenchAssignJagged |   972.5 ms |  3.01 ms |  2.82 ms |   972.4 ms |   966.7 ms |   977.1 ms |            - |            - |            - |     5576.93 KB |
|      BenchAssignRectangular | 1,077.8 ms |  6.10 ms |  5.10 ms | 1,076.3 ms | 1,071.5 ms | 1,088.5 ms |    1000.0000 |            - |            - |     5214.11 KB |
|         BenchIDEAEncryption |   920.6 ms |  3.64 ms |  3.04 ms |   921.5 ms |   914.6 ms |   925.4 ms |            - |            - |            - |      610.48 KB |
|           BenchNeuralJagged |   704.3 ms |  5.53 ms |  5.17 ms |   703.7 ms |   696.6 ms |   714.9 ms |            - |            - |            - |       847.6 KB |
|                 BenchNeural |   633.8 ms |  3.44 ms |  3.22 ms |   634.5 ms |   628.1 ms |   640.3 ms |            - |            - |            - |     1434.55 KB |
|               BenchLUDecomp |   988.4 ms | 10.01 ms |  8.88 ms |   987.8 ms |   978.7 ms | 1,008.8 ms |   49000.0000 |   21000.0000 |    7000.0000 |   211168.14 KB |
