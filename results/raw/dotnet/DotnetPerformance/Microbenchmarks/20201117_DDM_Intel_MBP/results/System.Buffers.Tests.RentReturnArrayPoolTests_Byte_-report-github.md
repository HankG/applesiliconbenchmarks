``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | RentalSize | ManipulateArray | Async | UseSharedPool |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |----------- |---------------- |------ |-------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|     **SingleSerial** |       **4096** |           **False** | **False** |         **False** |     **30.32 ns** |   **0.391 ns** |   **0.366 ns** |     **30.18 ns** |     **29.92 ns** |     **31.16 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False | False |         False |    359.25 ns |   7.013 ns |   7.504 ns |    360.42 ns |    346.41 ns |    370.51 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False | False |         False |    252.93 ns |   3.010 ns |   2.513 ns |    252.22 ns |    250.01 ns |    259.17 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False | False |         False |  1,084.88 ns |  40.010 ns |  44.471 ns |  1,076.48 ns |  1,027.72 ns |  1,173.12 ns | 0.0200 |     - |     - |      84 B |
|     **SingleSerial** |       **4096** |           **False** | **False** |          **True** |     **23.96 ns** |   **0.419 ns** |   **0.392 ns** |     **23.81 ns** |     **23.50 ns** |     **24.79 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False | False |          True |     58.94 ns |   3.233 ns |   3.723 ns |     57.32 ns |     54.13 ns |     67.47 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False | False |          True |    526.98 ns |   6.608 ns |   5.858 ns |    526.77 ns |    517.76 ns |    535.99 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False | False |          True |  1,142.58 ns |  89.033 ns |  98.959 ns |  1,158.87 ns |    885.25 ns |  1,303.27 ns | 0.0150 |     - |     - |      81 B |
|     **SingleSerial** |       **4096** |           **False** |  **True** |         **False** |    **646.30 ns** |  **12.795 ns** |  **13.140 ns** |    **652.57 ns** |    **622.30 ns** |    **664.14 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False |  True |         False |  1,732.72 ns |  82.934 ns |  88.738 ns |  1,717.76 ns |  1,629.83 ns |  1,937.19 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False |  True |         False |    977.21 ns |  21.463 ns |  24.716 ns |    974.57 ns |    939.25 ns |  1,033.36 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False |  True |         False |    707.38 ns |  10.607 ns |   9.921 ns |    707.09 ns |    694.25 ns |    724.47 ns |      - |     - |     - |       1 B |
|     **SingleSerial** |       **4096** |           **False** |  **True** |          **True** |    **642.83 ns** |  **12.485 ns** |  **14.377 ns** |    **641.04 ns** |    **620.05 ns** |    **666.58 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False |  True |          True |    822.21 ns |  13.926 ns |  13.027 ns |    826.41 ns |    793.78 ns |    839.50 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False |  True |          True |  1,278.61 ns |  22.755 ns |  21.285 ns |  1,270.00 ns |  1,252.47 ns |  1,318.10 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False |  True |          True |    470.61 ns |  30.860 ns |  35.538 ns |    471.95 ns |    415.43 ns |    533.84 ns |      - |     - |     - |       1 B |
|     **SingleSerial** |       **4096** |            **True** | **False** |         **False** |  **1,401.86 ns** |  **23.823 ns** |  **19.893 ns** |  **1,402.69 ns** |  **1,352.69 ns** |  **1,432.93 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True | False |         False |  2,573.17 ns | 243.077 ns | 279.928 ns |  2,557.00 ns |  2,234.73 ns |  3,099.90 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True | False |         False | 11,403.61 ns | 221.610 ns | 255.207 ns | 11,361.18 ns | 10,750.45 ns | 11,800.00 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True | False |         False |  2,283.65 ns |  45.564 ns |  50.644 ns |  2,288.87 ns |  2,181.72 ns |  2,377.66 ns |      - |     - |     - |      33 B |
|     **SingleSerial** |       **4096** |            **True** | **False** |          **True** |  **1,545.89 ns** |  **18.312 ns** |  **17.129 ns** |  **1,539.37 ns** |  **1,527.46 ns** |  **1,582.58 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True | False |          True |  2,215.67 ns |  35.906 ns |  29.983 ns |  2,215.03 ns |  2,172.22 ns |  2,264.73 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True | False |          True | 13,728.65 ns | 125.048 ns | 116.970 ns | 13,721.74 ns | 13,567.78 ns | 13,952.45 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True | False |          True |  2,371.36 ns |  90.555 ns | 104.284 ns |  2,399.09 ns |  2,203.32 ns |  2,622.67 ns |      - |     - |     - |      29 B |
|     **SingleSerial** |       **4096** |            **True** |  **True** |         **False** |  **2,571.60 ns** |  **31.585 ns** |  **27.999 ns** |  **2,577.63 ns** |  **2,528.29 ns** |  **2,627.93 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True |  True |         False |  3,038.09 ns | 222.638 ns | 247.461 ns |  3,113.90 ns |  2,649.38 ns |  3,593.30 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True |  True |         False | 15,372.91 ns |  31.474 ns |  24.573 ns | 15,366.75 ns | 15,333.54 ns | 15,411.28 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True |  True |         False |  2,138.56 ns |  52.797 ns |  58.683 ns |  2,110.94 ns |  2,058.69 ns |  2,241.24 ns |      - |     - |     - |         - |
|     **SingleSerial** |       **4096** |            **True** |  **True** |          **True** |  **2,565.27 ns** |  **32.828 ns** |  **30.707 ns** |  **2,555.15 ns** |  **2,529.45 ns** |  **2,623.88 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True |  True |          True |  3,046.67 ns | 173.143 ns | 199.392 ns |  3,083.12 ns |  2,729.74 ns |  3,517.27 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True |  True |          True | 15,812.66 ns |  35.367 ns |  33.083 ns | 15,817.36 ns | 15,757.28 ns | 15,868.71 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True |  True |          True |  2,293.75 ns |  38.313 ns |  33.964 ns |  2,293.84 ns |  2,227.11 ns |  2,371.25 ns |      - |     - |     - |       2 B |
