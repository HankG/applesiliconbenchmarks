``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    DeserializeFromString | 630.1 ns | 12.36 ns | 11.56 ns | 625.1 ns | 618.3 ns | 652.1 ns | 0.2543 |     - |     - |   1.05 KB |
| DeserializeFromUtf8Bytes | 455.0 ns |  7.90 ns |  8.45 ns | 454.6 ns | 440.8 ns | 469.6 ns | 0.2549 |     - |     - |   1.05 KB |
|    DeserializeFromStream | 777.0 ns |  9.41 ns |  8.34 ns | 776.4 ns | 764.0 ns | 792.7 ns | 0.2723 |     - |     - |   1.12 KB |
