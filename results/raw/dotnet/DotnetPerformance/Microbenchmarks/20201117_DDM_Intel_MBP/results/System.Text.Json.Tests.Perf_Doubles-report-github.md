``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method | Formatted | SkipValidation |     Mean |    Error |   StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |---------- |--------------- |---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|------:|----------:|
| **WriteDoubles** |     **False** |          **False** | **23.39 ms** | **0.308 ms** | **0.273 ms** | **23.33 ms** | **23.08 ms** | **23.92 ms** |     **-** |     **-** |     **-** |     **152 B** |
| **WriteDoubles** |     **False** |           **True** | **23.28 ms** | **0.419 ms** | **0.392 ms** | **23.12 ms** | **22.91 ms** | **24.03 ms** |     **-** |     **-** |     **-** |     **152 B** |
| **WriteDoubles** |      **True** |          **False** | **23.88 ms** | **0.300 ms** | **0.266 ms** | **23.85 ms** | **23.52 ms** | **24.35 ms** |     **-** |     **-** |     **-** |     **149 B** |
| **WriteDoubles** |      **True** |           **True** | **23.82 ms** | **0.301 ms** | **0.282 ms** | **23.74 ms** | **23.50 ms** | **24.34 ms** |     **-** |     **-** |     **-** |     **149 B** |
