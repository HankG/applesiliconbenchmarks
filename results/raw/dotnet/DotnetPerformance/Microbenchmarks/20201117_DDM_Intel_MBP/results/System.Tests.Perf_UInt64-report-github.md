``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |                value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |--------------------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **ToString** |                    **0** |  **2.510 ns** | **0.0558 ns** | **0.0466 ns** |  **2.494 ns** |  **2.450 ns** |  **2.622 ns** |      **-** |     **-** |     **-** |         **-** |
|   TryFormat |                    0 |  7.658 ns | 0.0979 ns | 0.0868 ns |  7.640 ns |  7.543 ns |  7.883 ns |      - |     - |     - |         - |
|   ParseSpan |                    0 | 12.471 ns | 0.1759 ns | 0.1560 ns | 12.437 ns | 12.287 ns | 12.769 ns |      - |     - |     - |         - |
|       Parse |                    0 | 11.722 ns | 0.2554 ns | 0.2623 ns | 11.682 ns | 11.424 ns | 12.222 ns |      - |     - |     - |         - |
|    TryParse |                    0 | 11.883 ns | 0.2151 ns | 0.2012 ns | 11.834 ns | 11.641 ns | 12.330 ns |      - |     - |     - |         - |
| TryParseHex |                    0 |  9.160 ns | 0.1202 ns | 0.1124 ns |  9.138 ns |  8.958 ns |  9.385 ns |      - |     - |     - |         - |
|    **ToString** |                **12345** | **13.137 ns** | **0.2405 ns** | **0.2132 ns** | **13.093 ns** | **12.824 ns** | **13.618 ns** | **0.0076** |     **-** |     **-** |      **32 B** |
|   TryFormat |                12345 | 10.654 ns | 0.1137 ns | 0.1064 ns | 10.629 ns | 10.526 ns | 10.863 ns |      - |     - |     - |         - |
|   ParseSpan |                12345 | 15.916 ns | 0.1822 ns | 0.1704 ns | 15.870 ns | 15.696 ns | 16.254 ns |      - |     - |     - |         - |
|       Parse |                12345 | 14.842 ns | 0.1648 ns | 0.1461 ns | 14.841 ns | 14.609 ns | 15.103 ns |      - |     - |     - |         - |
|    TryParse |                12345 | 14.947 ns | 0.1813 ns | 0.1696 ns | 14.884 ns | 14.714 ns | 15.240 ns |      - |     - |     - |         - |
|    **ToString** | **18446744073709551615** | **37.216 ns** | **0.6554 ns** | **0.6131 ns** | **37.002 ns** | **36.403 ns** | **38.490 ns** | **0.0152** |     **-** |     **-** |      **64 B** |
|   TryFormat | 18446744073709551615 | 31.983 ns | 0.4092 ns | 0.3827 ns | 31.916 ns | 31.564 ns | 32.719 ns |      - |     - |     - |         - |
|   ParseSpan | 18446744073709551615 | 32.863 ns | 0.5613 ns | 0.4976 ns | 32.784 ns | 32.327 ns | 33.949 ns |      - |     - |     - |         - |
|       Parse | 18446744073709551615 | 32.123 ns | 0.3816 ns | 0.3569 ns | 32.023 ns | 31.650 ns | 32.887 ns |      - |     - |     - |         - |
|    TryParse | 18446744073709551615 | 32.214 ns | 0.4745 ns | 0.4207 ns | 32.065 ns | 31.851 ns | 33.230 ns |      - |     - |     - |         - |
| **TryParseHex** |                 **3039** | **14.047 ns** | **0.1219 ns** | **0.1081 ns** | **14.055 ns** | **13.825 ns** | **14.237 ns** |      **-** |     **-** |     **-** |         **-** |
| **TryParseHex** |     **FFFFFFFFFFFFFFFF** | **29.668 ns** | **0.4763 ns** | **0.4222 ns** | **29.503 ns** | **29.153 ns** | **30.429 ns** |      **-** |     **-** |     **-** |         **-** |
