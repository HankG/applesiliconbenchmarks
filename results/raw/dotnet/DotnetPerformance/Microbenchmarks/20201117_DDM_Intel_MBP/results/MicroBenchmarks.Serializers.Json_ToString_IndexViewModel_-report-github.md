``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|-------:|------:|----------:|
|      Jil | 34.81 μs | 0.640 μs | 0.599 μs | 34.60 μs | 34.01 μs | 35.90 μs | 13.8098 | 0.1424 |     - |  56.65 KB |
| JSON.NET | 40.46 μs | 0.630 μs | 0.589 μs | 40.26 μs | 39.58 μs | 41.61 μs | 14.4115 | 1.9215 |     - |  59.25 KB |
| Utf8Json | 27.60 μs | 0.500 μs | 0.468 μs | 27.44 μs | 27.01 μs | 28.40 μs |  5.8882 |      - |     - |  24.55 KB |
