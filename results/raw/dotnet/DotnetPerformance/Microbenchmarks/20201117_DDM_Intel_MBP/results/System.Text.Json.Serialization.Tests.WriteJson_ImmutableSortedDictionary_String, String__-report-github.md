``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 17.79 μs | 0.286 μs | 0.253 μs | 17.80 μs | 17.42 μs | 18.26 μs | 2.8792 |     - |     - |   12208 B |
|    SerializeToUtf8Bytes | 17.36 μs | 0.329 μs | 0.308 μs | 17.31 μs | 16.96 μs | 17.85 μs | 1.4391 |     - |     - |    6296 B |
|       SerializeToStream | 17.04 μs | 0.168 μs | 0.140 μs | 17.01 μs | 16.82 μs | 17.39 μs | 0.0679 |     - |     - |     352 B |
| SerializeObjectProperty | 18.42 μs | 0.261 μs | 0.244 μs | 18.39 μs | 18.04 μs | 18.80 μs | 2.9556 |     - |     - |   12536 B |
