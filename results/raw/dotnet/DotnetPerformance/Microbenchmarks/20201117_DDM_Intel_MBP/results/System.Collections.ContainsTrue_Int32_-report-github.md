``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|              Array |  512 |  35.059 μs | 0.4113 μs | 0.3646 μs |  34.920 μs |  34.590 μs |  35.805 μs |     - |     - |     - |         - |
|               Span |  512 |  40.384 μs | 0.4802 μs | 0.4256 μs |  40.245 μs |  39.798 μs |  41.355 μs |     - |     - |     - |         - |
|               List |  512 |  29.441 μs | 0.3006 μs | 0.2812 μs |  29.356 μs |  29.063 μs |  29.959 μs |     - |     - |     - |         - |
|        ICollection |  512 |  30.619 μs | 0.4999 μs | 0.4432 μs |  30.404 μs |  30.138 μs |  31.458 μs |     - |     - |     - |         - |
|         LinkedList |  512 | 185.664 μs | 1.7102 μs | 1.5160 μs | 185.581 μs | 183.771 μs | 189.262 μs |     - |     - |     - |         - |
|            HashSet |  512 |   3.150 μs | 0.0382 μs | 0.0339 μs |   3.150 μs |   3.089 μs |   3.216 μs |     - |     - |     - |         - |
|              Queue |  512 |  35.187 μs | 0.4526 μs | 0.4233 μs |  35.295 μs |  34.584 μs |  35.718 μs |     - |     - |     - |         - |
|              Stack |  512 |  61.122 μs | 0.6271 μs | 0.5866 μs |  60.995 μs |  60.329 μs |  62.500 μs |     - |     - |     - |         - |
|          SortedSet |  512 |  24.743 μs | 0.3935 μs | 0.3681 μs |  24.764 μs |  24.224 μs |  25.221 μs |     - |     - |     - |         - |
|     ImmutableArray |  512 |  31.658 μs | 0.3989 μs | 0.3732 μs |  31.576 μs |  31.141 μs |  32.131 μs |     - |     - |     - |         - |
|   ImmutableHashSet |  512 |  24.551 μs | 0.2828 μs | 0.2645 μs |  24.530 μs |  24.167 μs |  25.199 μs |     - |     - |     - |         - |
|      ImmutableList |  512 | 488.807 μs | 7.0994 μs | 6.6408 μs | 485.346 μs | 482.849 μs | 503.639 μs |     - |     - |     - |       1 B |
| ImmutableSortedSet |  512 |  26.944 μs | 0.3521 μs | 0.3293 μs |  26.857 μs |  26.608 μs |  27.628 μs |     - |     - |     - |         - |
