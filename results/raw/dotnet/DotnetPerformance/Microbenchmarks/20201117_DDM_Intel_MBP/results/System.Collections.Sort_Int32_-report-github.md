``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RTZWTM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=5000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|               Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                Array |  512 |  3.953 μs | 0.1482 μs | 0.1647 μs |  3.920 μs |  3.748 μs |  4.288 μs |      - |     - |     - |         - |
|  Array_ComparerClass |  512 | 22.039 μs | 0.2869 μs | 0.2684 μs | 21.977 μs | 21.682 μs | 22.527 μs |      - |     - |     - |      64 B |
| Array_ComparerStruct |  512 | 24.745 μs | 0.1889 μs | 0.1577 μs | 24.773 μs | 24.463 μs | 24.945 μs |      - |     - |     - |      88 B |
|     Array_Comparison |  512 | 22.334 μs | 0.4078 μs | 0.3405 μs | 22.299 μs | 21.779 μs | 22.923 μs |      - |     - |     - |         - |
|                 List |  512 |  3.815 μs | 0.1192 μs | 0.1373 μs |  3.826 μs |  3.595 μs |  4.123 μs |      - |     - |     - |         - |
|            LinqQuery |  512 | 40.984 μs | 0.4766 μs | 0.4225 μs | 40.968 μs | 40.410 μs | 41.892 μs | 1.4000 |     - |     - |    6456 B |
| LinqOrderByExtension |  512 | 40.588 μs | 0.5082 μs | 0.4753 μs | 40.386 μs | 39.992 μs | 41.328 μs | 1.4000 |     - |     - |    6456 B |
