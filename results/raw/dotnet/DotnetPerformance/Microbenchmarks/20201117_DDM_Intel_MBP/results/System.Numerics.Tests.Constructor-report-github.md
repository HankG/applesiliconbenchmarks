``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|      SpanCastBenchmark_Byte | 0.9334 ns | 0.0298 ns | 0.0279 ns | 0.9367 ns | 0.8841 ns | 0.9885 ns |     - |     - |     - |         - |
|     SpanCastBenchmark_SByte | 0.4944 ns | 0.0164 ns | 0.0145 ns | 0.4911 ns | 0.4739 ns | 0.5242 ns |     - |     - |     - |         - |
|    SpanCastBenchmark_UInt16 | 0.5602 ns | 0.0215 ns | 0.0201 ns | 0.5518 ns | 0.5369 ns | 0.6001 ns |     - |     - |     - |         - |
|     SpanCastBenchmark_Int16 | 0.5480 ns | 0.0166 ns | 0.0156 ns | 0.5417 ns | 0.5259 ns | 0.5826 ns |     - |     - |     - |         - |
|    SpanCastBenchmark_UInt32 | 0.5700 ns | 0.0298 ns | 0.0278 ns | 0.5596 ns | 0.5387 ns | 0.6129 ns |     - |     - |     - |         - |
|     SpanCastBenchmark_Int32 | 1.3368 ns | 0.0310 ns | 0.0290 ns | 1.3291 ns | 1.2972 ns | 1.3993 ns |     - |     - |     - |         - |
|    SpanCastBenchmark_UInt64 | 0.6937 ns | 0.0254 ns | 0.0237 ns | 0.6886 ns | 0.6662 ns | 0.7313 ns |     - |     - |     - |         - |
|     SpanCastBenchmark_Int64 | 0.7015 ns | 0.0219 ns | 0.0205 ns | 0.6950 ns | 0.6748 ns | 0.7308 ns |     - |     - |     - |         - |
|    SpanCastBenchmark_Single | 0.6990 ns | 0.0215 ns | 0.0191 ns | 0.6953 ns | 0.6700 ns | 0.7398 ns |     - |     - |     - |         - |
|    SpanCastBenchmark_Double | 0.5675 ns | 0.0310 ns | 0.0275 ns | 0.5674 ns | 0.5316 ns | 0.6283 ns |     - |     - |     - |         - |
|   ConstructorBenchmark_Byte | 0.8323 ns | 0.0258 ns | 0.0229 ns | 0.8270 ns | 0.8044 ns | 0.8800 ns |     - |     - |     - |         - |
|  ConstructorBenchmark_SByte | 0.4906 ns | 0.0237 ns | 0.0221 ns | 0.4824 ns | 0.4661 ns | 0.5308 ns |     - |     - |     - |         - |
| ConstructorBenchmark_UInt16 | 0.4867 ns | 0.0118 ns | 0.0092 ns | 0.4868 ns | 0.4738 ns | 0.5018 ns |     - |     - |     - |         - |
|  ConstructorBenchmark_Int16 | 0.4957 ns | 0.0253 ns | 0.0224 ns | 0.4897 ns | 0.4712 ns | 0.5345 ns |     - |     - |     - |         - |
| ConstructorBenchmark_UInt32 | 0.5021 ns | 0.0247 ns | 0.0219 ns | 0.4954 ns | 0.4788 ns | 0.5447 ns |     - |     - |     - |         - |
|  ConstructorBenchmark_Int32 | 0.9177 ns | 0.0287 ns | 0.0269 ns | 0.9052 ns | 0.8672 ns | 0.9643 ns |     - |     - |     - |         - |
| ConstructorBenchmark_UInt64 | 0.5234 ns | 0.0328 ns | 0.0291 ns | 0.5136 ns | 0.4915 ns | 0.5951 ns |     - |     - |     - |         - |
|  ConstructorBenchmark_Int64 | 0.4839 ns | 0.0216 ns | 0.0191 ns | 0.4766 ns | 0.4624 ns | 0.5207 ns |     - |     - |     - |         - |
| ConstructorBenchmark_Single | 0.5100 ns | 0.0167 ns | 0.0156 ns | 0.5052 ns | 0.4849 ns | 0.5404 ns |     - |     - |     - |         - |
| ConstructorBenchmark_Double | 0.4931 ns | 0.0222 ns | 0.0207 ns | 0.4858 ns | 0.4655 ns | 0.5275 ns |     - |     - |     - |         - |
