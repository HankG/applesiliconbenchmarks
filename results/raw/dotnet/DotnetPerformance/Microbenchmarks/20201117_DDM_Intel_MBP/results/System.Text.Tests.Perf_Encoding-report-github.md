``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method | size | encName |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----- |-------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|     **GetBytes** |   **16** |   **ascii** |  **17.452 ns** | **0.3075 ns** | **0.2726 ns** |  **17.413 ns** |  **17.132 ns** |  **18.000 ns** | **0.0095** |     **-** |     **-** |      **40 B** |
|    GetString |   16 |   ascii |  20.230 ns | 0.4653 ns | 0.4569 ns |  20.067 ns |  19.640 ns |  21.342 ns | 0.0134 |     - |     - |      56 B |
|     GetChars |   16 |   ascii |  20.858 ns | 0.4819 ns | 0.4507 ns |  20.719 ns |  20.272 ns |  21.696 ns | 0.0133 |     - |     - |      56 B |
|   GetEncoder |   16 |   ascii |   8.589 ns | 0.2450 ns | 0.2723 ns |   8.499 ns |   8.206 ns |   9.088 ns | 0.0115 |     - |     - |      48 B |
| GetByteCount |   16 |   ascii |   5.345 ns | 0.0838 ns | 0.0783 ns |   5.311 ns |   5.260 ns |   5.462 ns |      - |     - |     - |         - |
|     **GetBytes** |   **16** |   **utf-8** |  **18.107 ns** | **0.3875 ns** | **0.3625 ns** |  **18.090 ns** |  **17.631 ns** |  **18.830 ns** | **0.0095** |     **-** |     **-** |      **40 B** |
|    GetString |   16 |   utf-8 |  21.199 ns | 0.3072 ns | 0.2723 ns |  21.169 ns |  20.833 ns |  21.805 ns | 0.0133 |     - |     - |      56 B |
|     GetChars |   16 |   utf-8 |  28.142 ns | 0.5613 ns | 0.5250 ns |  28.054 ns |  27.390 ns |  29.133 ns | 0.0133 |     - |     - |      56 B |
|   GetEncoder |   16 |   utf-8 |   7.331 ns | 0.1479 ns | 0.1384 ns |   7.319 ns |   7.034 ns |   7.546 ns | 0.0114 |     - |     - |      48 B |
| GetByteCount |   16 |   utf-8 |   8.726 ns | 0.1219 ns | 0.1140 ns |   8.685 ns |   8.592 ns |   8.952 ns |      - |     - |     - |         - |
|     **GetBytes** |  **512** |   **ascii** |  **71.990 ns** | **1.2028 ns** | **1.1251 ns** |  **72.012 ns** |  **70.665 ns** |  **74.135 ns** | **0.1279** |     **-** |     **-** |     **536 B** |
|    GetString |  512 |   ascii |  86.402 ns | 1.6614 ns | 1.5541 ns |  85.665 ns |  84.792 ns |  89.628 ns | 0.2503 |     - |     - |    1048 B |
|     GetChars |  512 |   ascii |  85.896 ns | 1.6611 ns | 1.5538 ns |  85.665 ns |  83.972 ns |  89.105 ns | 0.2505 |     - |     - |    1048 B |
|   GetEncoder |  512 |   ascii |   8.302 ns | 0.2414 ns | 0.2683 ns |   8.312 ns |   7.477 ns |   8.663 ns | 0.0115 |     - |     - |      48 B |
| GetByteCount |  512 |   ascii |   5.342 ns | 0.0934 ns | 0.0874 ns |   5.307 ns |   5.257 ns |   5.520 ns |      - |     - |     - |         - |
|     **GetBytes** |  **512** |   **utf-8** |  **91.447 ns** | **1.3799 ns** | **1.2908 ns** |  **91.171 ns** |  **89.610 ns** |  **93.786 ns** | **0.1281** |     **-** |     **-** |     **536 B** |
|    GetString |  512 |   utf-8 | 102.855 ns | 1.5333 ns | 1.4343 ns | 102.985 ns | 100.425 ns | 105.275 ns | 0.2504 |     - |     - |    1048 B |
|     GetChars |  512 |   utf-8 | 105.204 ns | 1.8850 ns | 1.9358 ns | 104.554 ns | 102.419 ns | 108.948 ns | 0.2503 |     - |     - |    1048 B |
|   GetEncoder |  512 |   utf-8 |   7.332 ns | 0.2017 ns | 0.2158 ns |   7.270 ns |   7.068 ns |   7.777 ns | 0.0114 |     - |     - |      48 B |
| GetByteCount |  512 |   utf-8 |  23.922 ns | 0.2780 ns | 0.2464 ns |  23.870 ns |  23.579 ns |  24.397 ns |      - |     - |     - |         - |
