``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-BXZFXU : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MaxWarmupIterationCount=10  MinIterationCount=15  
MinWarmupIterationCount=2  WarmupCount=-1  

```
|                                                     Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|                                           Await_FromResult |    15.329 ns |  0.2313 ns |  0.2163 ns |    15.250 ns |    15.054 ns |    15.726 ns |      - |     - |     - |         - |
|                                    Await_FromCompletedTask |    24.149 ns |  0.3932 ns |  0.3486 ns |    24.150 ns |    23.563 ns |    24.886 ns | 0.0171 |     - |     - |      72 B |
|                         Await_FromCompletedValueTaskSource |    61.849 ns |  1.0017 ns |  0.8880 ns |    61.796 ns |    60.817 ns |    64.091 ns | 0.0170 |     - |     - |      72 B |
|                                  CreateAndAwait_FromResult |    16.283 ns |  0.2053 ns |  0.1820 ns |    16.256 ns |    16.028 ns |    16.713 ns |      - |     - |     - |         - |
|                   CreateAndAwait_FromResult_ConfigureAwait |    18.658 ns |  0.2255 ns |  0.2109 ns |    18.604 ns |    18.420 ns |    19.068 ns |      - |     - |     - |         - |
|                           CreateAndAwait_FromCompletedTask |    18.938 ns |  0.3567 ns |  0.3336 ns |    18.843 ns |    18.503 ns |    19.724 ns |      - |     - |     - |         - |
|            CreateAndAwait_FromCompletedTask_ConfigureAwait |    21.467 ns |  0.2875 ns |  0.2690 ns |    21.328 ns |    21.159 ns |    21.956 ns |      - |     - |     - |         - |
|                CreateAndAwait_FromCompletedValueTaskSource |    26.684 ns |  0.2775 ns |  0.2460 ns |    26.612 ns |    26.403 ns |    27.199 ns |      - |     - |     - |         - |
| CreateAndAwait_FromCompletedValueTaskSource_ConfigureAwait |    29.317 ns |  0.4112 ns |  0.3846 ns |    29.195 ns |    28.792 ns |    30.166 ns |      - |     - |     - |         - |
|                     CreateAndAwait_FromYieldingAsyncMethod | 1,243.980 ns | 56.8884 ns | 65.5127 ns | 1,264.772 ns | 1,142.561 ns | 1,313.407 ns | 0.0534 |     - |     - |     239 B |
|                              CreateAndAwait_FromDelayedTCS |   202.678 ns |  3.3940 ns |  3.0087 ns |   201.984 ns |   199.026 ns |   208.374 ns | 0.0550 |     - |     - |     232 B |
|                    Copy_PassAsArgumentAndReturn_FromResult |     8.908 ns |  0.1029 ns |  0.0962 ns |     8.890 ns |     8.774 ns |     9.126 ns |      - |     - |     - |         - |
|                      Copy_PassAsArgumentAndReturn_FromTask |    10.800 ns |  0.1803 ns |  0.1687 ns |    10.773 ns |    10.627 ns |    11.224 ns |      - |     - |     - |         - |
|           Copy_PassAsArgumentAndReturn_FromValueTaskSource |    14.708 ns |  0.1656 ns |  0.1549 ns |    14.679 ns |    14.487 ns |    14.966 ns |      - |     - |     - |         - |
