``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|                     Array |  512 |    238.1 ns |   2.60 ns |   2.43 ns |    237.7 ns |    235.3 ns |    242.2 ns |      - |     - |     - |         - |
|                      Span |  512 |    223.7 ns |   4.20 ns |   3.93 ns |    222.1 ns |    219.4 ns |    233.5 ns |      - |     - |     - |         - |
|              ReadOnlySpan |  512 |    217.0 ns |   1.96 ns |   1.74 ns |    216.6 ns |    214.7 ns |    220.3 ns |      - |     - |     - |         - |
|               IEnumerable |  512 |  2,238.7 ns |  26.67 ns |  24.94 ns |  2,232.0 ns |  2,208.8 ns |  2,275.4 ns |      - |     - |     - |      32 B |
|                      List |  512 |  2,069.8 ns |  32.65 ns |  27.27 ns |  2,080.3 ns |  2,011.8 ns |  2,102.0 ns |      - |     - |     - |         - |
|                LinkedList |  512 |  3,082.2 ns |  38.83 ns |  34.42 ns |  3,074.1 ns |  3,043.4 ns |  3,149.4 ns |      - |     - |     - |         - |
|                   HashSet |  512 |  2,066.1 ns |  28.76 ns |  25.50 ns |  2,066.0 ns |  2,028.3 ns |  2,108.5 ns |      - |     - |     - |         - |
|                Dictionary |  512 |  2,737.3 ns |  28.95 ns |  24.17 ns |  2,742.4 ns |  2,701.3 ns |  2,783.7 ns |      - |     - |     - |         - |
|                     Queue |  512 |  3,268.6 ns |  27.92 ns |  23.31 ns |  3,268.6 ns |  3,235.1 ns |  3,327.8 ns |      - |     - |     - |         - |
|                     Stack |  512 |  3,228.7 ns |  31.44 ns |  27.87 ns |  3,225.7 ns |  3,165.1 ns |  3,286.3 ns |      - |     - |     - |         - |
|                SortedList |  512 |  5,096.9 ns |  96.96 ns | 103.75 ns |  5,042.4 ns |  4,997.8 ns |  5,315.2 ns |      - |     - |     - |      56 B |
|                 SortedSet |  512 |  8,057.9 ns |  38.83 ns |  30.32 ns |  8,066.2 ns |  8,003.2 ns |  8,102.7 ns | 0.0322 |     - |     - |     216 B |
|          SortedDictionary |  512 | 10,521.3 ns | 131.15 ns | 122.67 ns | 10,488.1 ns | 10,371.3 ns | 10,798.3 ns | 0.0415 |     - |     - |     216 B |
|      ConcurrentDictionary |  512 | 12,343.3 ns | 159.96 ns | 149.62 ns | 12,274.3 ns | 12,188.1 ns | 12,613.8 ns |      - |     - |     - |      64 B |
|           ConcurrentQueue |  512 |  4,381.0 ns |  49.44 ns |  46.24 ns |  4,371.1 ns |  4,320.3 ns |  4,481.5 ns |      - |     - |     - |      72 B |
|           ConcurrentStack |  512 |  3,761.0 ns |  50.93 ns |  47.64 ns |  3,760.7 ns |  3,698.8 ns |  3,870.7 ns |      - |     - |     - |      48 B |
|             ConcurrentBag |  512 |  4,482.3 ns |  65.75 ns |  54.90 ns |  4,464.8 ns |  4,410.1 ns |  4,615.9 ns | 0.9926 |     - |     - |    4160 B |
|            ImmutableArray |  512 |    211.6 ns |   2.19 ns |   2.04 ns |    210.7 ns |    209.4 ns |    215.2 ns |      - |     - |     - |         - |
|       ImmutableDictionary |  512 | 49,348.0 ns | 698.40 ns | 653.28 ns | 49,127.5 ns | 48,540.6 ns | 50,399.5 ns |      - |     - |     - |         - |
|          ImmutableHashSet |  512 | 43,847.3 ns | 648.39 ns | 541.44 ns | 43,770.9 ns | 43,076.4 ns | 44,972.1 ns |      - |     - |     - |         - |
|             ImmutableList |  512 | 21,882.4 ns | 249.81 ns | 233.67 ns | 21,827.5 ns | 21,581.7 ns | 22,418.0 ns |      - |     - |     - |         - |
|            ImmutableQueue |  512 |  4,250.0 ns |  40.91 ns |  38.27 ns |  4,246.3 ns |  4,187.7 ns |  4,307.3 ns |      - |     - |     - |         - |
|            ImmutableStack |  512 |  3,910.9 ns |  77.03 ns |  64.33 ns |  3,921.6 ns |  3,723.8 ns |  3,994.7 ns |      - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 23,163.7 ns | 258.70 ns | 241.98 ns | 23,173.0 ns | 22,845.3 ns | 23,530.5 ns |      - |     - |     - |         - |
|        ImmutableSortedSet |  512 | 23,319.2 ns | 360.17 ns | 336.91 ns | 23,200.3 ns | 22,976.2 ns | 24,077.3 ns |      - |     - |     - |         - |
