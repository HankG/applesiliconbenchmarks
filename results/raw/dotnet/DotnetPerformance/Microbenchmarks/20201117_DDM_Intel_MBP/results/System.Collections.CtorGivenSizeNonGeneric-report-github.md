``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|  ArrayList |  512 | 155.5 ns | 3.64 ns | 4.19 ns | 155.2 ns | 150.0 ns | 165.9 ns | 0.9920 |     - |     - |   4.05 KB |
|  Hashtable |  512 | 661.4 ns | 5.97 ns | 4.66 ns | 661.5 ns | 654.0 ns | 667.2 ns | 4.3839 |     - |     - |  17.93 KB |
|      Queue |  512 | 154.3 ns | 3.72 ns | 4.29 ns | 152.7 ns | 149.2 ns | 164.9 ns | 0.9960 |     - |     - |   4.07 KB |
|      Stack |  512 | 157.7 ns | 3.31 ns | 3.81 ns | 156.4 ns | 152.8 ns | 167.5 ns | 0.9916 |     - |     - |   4.05 KB |
| SortedList |  512 | 332.5 ns | 7.97 ns | 8.85 ns | 330.7 ns | 319.2 ns | 349.1 ns | 1.9907 |     - |     - |   8.13 KB |
