``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
| Method |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|   Sha1 |  91.61 ms | 0.615 ms | 0.545 ms |  91.34 ms |  90.93 ms |  92.46 ms |     - |     - |     - |     676 B |
| Sha256 | 220.53 ms | 2.757 ms | 2.579 ms | 219.21 ms | 217.58 ms | 225.63 ms |     - |     - |     - |     400 B |
| Sha384 | 147.38 ms | 1.093 ms | 0.853 ms | 147.19 ms | 146.14 ms | 148.89 ms |     - |     - |     - |    1304 B |
| Sha512 | 147.79 ms | 1.492 ms | 1.395 ms | 147.46 ms | 146.20 ms | 151.10 ms |     - |     - |     - |    1960 B |
