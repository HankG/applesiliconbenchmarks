``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method |       Mean |       Error |      StdDev |     Median |        Min |          Max |  Gen 0 |  Gen 1 |  Gen 2 | Allocated |
|--------------------- |-----------:|------------:|------------:|-----------:|-----------:|-------------:|-------:|-------:|-------:|----------:|
|                 List |   4.793 ns |   0.1420 ns |   0.1259 ns |   4.771 ns |   4.658 ns |     5.032 ns | 0.0076 |      - |      - |      32 B |
|           LinkedList |   3.576 ns |   0.0636 ns |   0.0496 ns |   3.590 ns |   3.481 ns |     3.651 ns | 0.0096 |      - |      - |      40 B |
|              HashSet |   6.035 ns |   0.1794 ns |   0.1590 ns |   5.987 ns |   5.843 ns |     6.330 ns | 0.0172 |      - |      - |      72 B |
|           Dictionary |   6.417 ns |   0.1014 ns |   0.0846 ns |   6.414 ns |   6.249 ns |     6.558 ns | 0.0191 |      - |      - |      80 B |
|                Queue |   4.911 ns |   0.1619 ns |   0.1514 ns |   4.915 ns |   4.733 ns |     5.234 ns | 0.0096 |      - |      - |      40 B |
|                Stack |   4.546 ns |   0.1190 ns |   0.1113 ns |   4.534 ns |   4.402 ns |     4.742 ns | 0.0076 |      - |      - |      32 B |
|           SortedList |   9.983 ns |   0.3585 ns |   0.4128 ns |   9.935 ns |   9.417 ns |    10.757 ns | 0.0153 |      - |      - |      64 B |
|            SortedSet |   5.213 ns |   0.1255 ns |   0.1174 ns |   5.192 ns |   5.020 ns |     5.418 ns | 0.0115 |      - |      - |      48 B |
|     SortedDictionary |  17.714 ns |   0.3879 ns |   0.3984 ns |  17.587 ns |  17.235 ns |    18.502 ns | 0.0268 |      - |      - |     112 B |
| ConcurrentDictionary |  88.694 ns |   1.3900 ns |   1.2322 ns |  88.153 ns |  87.612 ns |    91.564 ns | 0.1625 |      - |      - |     680 B |
|      ConcurrentQueue |  56.384 ns |   1.2350 ns |   1.3726 ns |  55.809 ns |  54.771 ns |    58.806 ns | 0.1375 |      - |      - |     576 B |
|      ConcurrentStack |   3.109 ns |   0.1187 ns |   0.1219 ns |   3.084 ns |   2.959 ns |     3.337 ns | 0.0057 |      - |      - |      24 B |
|        ConcurrentBag | 959.525 ns | 144.3012 ns | 166.1775 ns | 986.991 ns | 623.385 ns | 1,222.261 ns | 0.0299 | 0.0100 | 0.0033 |     128 B |
