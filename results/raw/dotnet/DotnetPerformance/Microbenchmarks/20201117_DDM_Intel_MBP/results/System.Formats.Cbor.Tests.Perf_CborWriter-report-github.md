``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |  publicKey |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----------- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| **WriteCoseKey** | **ECDSA_P256** | **576.4 ns** | **5.59 ns** | **4.95 ns** | **575.5 ns** | **570.3 ns** | **587.1 ns** | **0.0094** |     **-** |     **-** |      **40 B** |
| **WriteCoseKey** | **ECDSA_P384** | **618.1 ns** | **8.74 ns** | **8.17 ns** | **615.6 ns** | **605.9 ns** | **635.2 ns** | **0.0171** |     **-** |     **-** |      **80 B** |
| **WriteCoseKey** | **ECDSA_P521** | **674.9 ns** | **8.71 ns** | **8.14 ns** | **673.7 ns** | **664.3 ns** | **691.8 ns** | **0.0268** |     **-** |     **-** |     **120 B** |
