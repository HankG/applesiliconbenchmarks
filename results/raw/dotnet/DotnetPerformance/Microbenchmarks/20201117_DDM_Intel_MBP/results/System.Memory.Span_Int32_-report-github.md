``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                 Clear |  512 |  23.912 ns | 0.3563 ns | 0.3333 ns |  23.806 ns |  23.529 ns |  24.546 ns |      - |     - |     - |         - |
|                  Fill |  512 | 118.307 ns | 2.2468 ns | 2.3073 ns | 117.889 ns | 115.666 ns | 124.219 ns |      - |     - |     - |         - |
|               Reverse |  512 | 124.047 ns | 1.6919 ns | 1.5826 ns | 123.616 ns | 122.166 ns | 127.418 ns |      - |     - |     - |         - |
|               ToArray |  512 | 121.190 ns | 3.0626 ns | 3.5269 ns | 121.455 ns | 116.236 ns | 128.291 ns | 0.4950 |     - |     - |    2072 B |
|         SequenceEqual |  512 |  29.771 ns | 0.3456 ns | 0.3064 ns |  29.803 ns |  29.273 ns |  30.416 ns |      - |     - |     - |         - |
|     SequenceCompareTo |  512 | 481.483 ns | 9.3859 ns | 8.3203 ns | 479.477 ns | 471.930 ns | 500.594 ns |      - |     - |     - |         - |
|            StartsWith |  512 |  18.316 ns | 0.2036 ns | 0.1805 ns |  18.225 ns |  18.136 ns |  18.709 ns |      - |     - |     - |         - |
|              EndsWith |  512 |  18.882 ns | 0.2193 ns | 0.2051 ns |  18.787 ns |  18.632 ns |  19.212 ns |      - |     - |     - |         - |
|          IndexOfValue |  512 |  50.317 ns | 1.0326 ns | 1.0141 ns |  50.509 ns |  49.131 ns |  52.863 ns |      - |     - |     - |         - |
|   IndexOfAnyTwoValues |  512 | 126.442 ns | 2.5405 ns | 2.3764 ns | 126.932 ns | 123.573 ns | 131.612 ns |      - |     - |     - |         - |
| IndexOfAnyThreeValues |  512 | 141.398 ns | 2.5196 ns | 2.3568 ns | 140.537 ns | 137.804 ns | 144.813 ns |      - |     - |     - |         - |
|  IndexOfAnyFourValues |  512 | 243.330 ns | 2.7330 ns | 2.5564 ns | 242.190 ns | 239.656 ns | 247.525 ns |      - |     - |     - |         - |
|      LastIndexOfValue |  512 | 109.519 ns | 1.0899 ns | 1.0195 ns | 109.116 ns | 108.257 ns | 111.339 ns |      - |     - |     - |         - |
|  LastIndexOfAnyValues |  512 | 112.289 ns | 1.7087 ns | 1.5983 ns | 111.771 ns | 110.157 ns | 114.826 ns |      - |     - |     - |         - |
|          BinarySearch |  512 |  16.246 ns | 0.2217 ns | 0.1851 ns |  16.205 ns |  15.964 ns |  16.552 ns |      - |     - |     - |         - |
|  GetPinnableReference |  512 |   1.333 ns | 0.0135 ns | 0.0126 ns |   1.331 ns |   1.318 ns |   1.358 ns |      - |     - |     - |         - |
