``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|   ValueTupleCompareNoOpt | 8.842 ns | 0.0948 ns | 0.0840 ns | 8.836 ns | 8.718 ns | 9.025 ns |     - |     - |     - |         - |
|        ValueTupleCompare | 2.795 ns | 0.0556 ns | 0.0520 ns | 2.778 ns | 2.726 ns | 2.880 ns |     - |     - |     - |         - |
|  ValueTupleCompareCached | 8.116 ns | 0.0818 ns | 0.0726 ns | 8.103 ns | 8.024 ns | 8.267 ns |     - |     - |     - |         - |
| ValueTupleCompareWrapped | 8.416 ns | 0.0967 ns | 0.0857 ns | 8.398 ns | 8.302 ns | 8.581 ns |     - |     - |     - |         - |
