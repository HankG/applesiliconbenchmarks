``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |                           Options |          Mean |       Error |      StdDev |        Median |           Min |           Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |---------------------------------- |--------------:|------------:|------------:|--------------:|--------------:|--------------:|------:|------:|------:|----------:|
|          **IsPrefix_FirstHalf** |             **(, IgnoreCase, False)** |    **246.034 ns** |   **3.0709 ns** |   **2.8725 ns** |    **244.676 ns** |    **242.915 ns** |    **250.874 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |             (, IgnoreCase, False) |     14.519 ns |   0.1708 ns |   0.1514 ns |     14.494 ns |     14.316 ns |     14.796 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |             (, IgnoreCase, False) |    272.239 ns |   2.7674 ns |   2.5886 ns |    271.727 ns |    268.493 ns |    276.573 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |             (, IgnoreCase, False) |     15.058 ns |   0.3032 ns |   0.2688 ns |     14.989 ns |     14.729 ns |     15.511 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |             (, IgnoreCase, False) |    481.693 ns |   5.7238 ns |   5.3541 ns |    480.788 ns |    474.059 ns |    490.817 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |             (, IgnoreCase, False) |    481.774 ns |   7.5111 ns |   7.0259 ns |    479.062 ns |    474.258 ns |    495.800 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |              **(, IgnoreCase, True)** |  **2,588.257 ns** |  **25.6393 ns** |  **23.9830 ns** |  **2,592.927 ns** |  **2,524.856 ns** |  **2,614.727 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |              (, IgnoreCase, True) |    774.256 ns |  13.8079 ns |  12.9160 ns |    771.638 ns |    758.304 ns |    804.642 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |              (, IgnoreCase, True) |  5,268.989 ns |  47.4878 ns |  42.0967 ns |  5,262.851 ns |  5,222.180 ns |  5,348.352 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |              (, IgnoreCase, True) |  1,163.505 ns |  13.9739 ns |  11.6688 ns |  1,164.612 ns |  1,133.927 ns |  1,177.666 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |              (, IgnoreCase, True) | 11,300.797 ns | 130.7966 ns | 122.3473 ns | 11,331.006 ns | 11,090.014 ns | 11,485.099 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |              (, IgnoreCase, True) | 17,107.209 ns | 230.0345 ns | 215.1744 ns | 16,994.675 ns | 16,868.329 ns | 17,490.898 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |                   **(, None, False)** |    **144.218 ns** |   **1.5884 ns** |   **1.4858 ns** |    **144.023 ns** |    **142.361 ns** |    **146.373 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |                   (, None, False) |     14.673 ns |   0.2338 ns |   0.2073 ns |     14.597 ns |     14.421 ns |     14.948 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |                   (, None, False) |    219.215 ns |   2.8774 ns |   2.6916 ns |    218.489 ns |    216.402 ns |    224.756 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |                   (, None, False) |     14.401 ns |   0.1618 ns |   0.1514 ns |     14.401 ns |     14.194 ns |     14.677 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |                   (, None, False) |    390.617 ns |   4.4364 ns |   4.1498 ns |    389.274 ns |    385.341 ns |    396.896 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |                   (, None, False) |    385.683 ns |   6.9245 ns |   6.1384 ns |    383.331 ns |    378.272 ns |    400.467 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |                    **(, None, True)** |  **2,592.250 ns** |  **28.6256 ns** |  **26.7764 ns** |  **2,584.730 ns** |  **2,557.154 ns** |  **2,632.847 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |                    (, None, True) |    767.504 ns |  10.9453 ns |  10.2383 ns |    766.916 ns |    753.450 ns |    788.967 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |                    (, None, True) |  5,214.985 ns | 102.9886 ns |  96.3356 ns |  5,172.335 ns |  5,077.605 ns |  5,355.570 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |                    (, None, True) |  1,199.088 ns |  23.4594 ns |  19.5897 ns |  1,189.518 ns |  1,173.363 ns |  1,227.109 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |                    (, None, True) | 11,367.965 ns | 329.5472 ns | 366.2908 ns | 11,308.364 ns | 10,859.116 ns | 12,010.619 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |                    (, None, True) | 16,894.730 ns | 200.3117 ns | 177.5711 ns | 16,894.599 ns | 16,609.282 ns | 17,230.592 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |        **(en-US, IgnoreCase, False)** |    **247.262 ns** |   **3.6124 ns** |   **3.3790 ns** |    **246.632 ns** |    **242.989 ns** |    **253.066 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |        (en-US, IgnoreCase, False) |     14.371 ns |   0.2074 ns |   0.1940 ns |     14.359 ns |     14.110 ns |     14.739 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |        (en-US, IgnoreCase, False) |    271.553 ns |   2.8326 ns |   2.5110 ns |    270.631 ns |    268.519 ns |    276.200 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |        (en-US, IgnoreCase, False) |     14.600 ns |   0.1535 ns |   0.1436 ns |     14.596 ns |     14.364 ns |     14.820 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |        (en-US, IgnoreCase, False) |    484.529 ns |   7.5910 ns |   6.7292 ns |    483.022 ns |    475.335 ns |    497.021 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |        (en-US, IgnoreCase, False) |    483.025 ns |   4.8323 ns |   4.5201 ns |    482.670 ns |    475.960 ns |    490.447 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |         **(en-US, IgnoreCase, True)** |  **2,595.201 ns** |  **31.7004 ns** |  **29.6525 ns** |  **2,601.468 ns** |  **2,546.499 ns** |  **2,637.821 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |         (en-US, IgnoreCase, True) |    763.505 ns |  13.0908 ns |  12.2452 ns |    764.815 ns |    733.430 ns |    780.323 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |         (en-US, IgnoreCase, True) |  5,196.768 ns |  70.5122 ns |  65.9572 ns |  5,191.933 ns |  5,071.347 ns |  5,313.313 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |         (en-US, IgnoreCase, True) |  1,166.062 ns |  22.6527 ns |  24.2382 ns |  1,168.799 ns |  1,127.013 ns |  1,209.175 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |         (en-US, IgnoreCase, True) | 11,154.790 ns | 114.7501 ns | 101.7230 ns | 11,134.542 ns | 11,012.831 ns | 11,365.310 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |         (en-US, IgnoreCase, True) | 17,174.879 ns | 215.7563 ns | 201.8185 ns | 17,109.092 ns | 16,911.007 ns | 17,536.095 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |    **(en-US, IgnoreNonSpace, False)** |    **143.607 ns** |   **1.7691 ns** |   **1.4773 ns** |    **143.415 ns** |    **141.643 ns** |    **146.456 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |    (en-US, IgnoreNonSpace, False) |     14.631 ns |   0.1739 ns |   0.1542 ns |     14.547 ns |     14.468 ns |     14.963 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |    (en-US, IgnoreNonSpace, False) |    219.565 ns |   3.1089 ns |   2.9081 ns |    218.463 ns |    216.153 ns |    225.223 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |    (en-US, IgnoreNonSpace, False) |     14.132 ns |   0.1753 ns |   0.1640 ns |     14.044 ns |     13.962 ns |     14.422 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |    (en-US, IgnoreNonSpace, False) |    391.190 ns |   7.1835 ns |   5.9986 ns |    388.748 ns |    384.888 ns |    401.574 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |    (en-US, IgnoreNonSpace, False) |    387.193 ns |   5.2761 ns |   4.4058 ns |    387.412 ns |    379.011 ns |    396.747 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |     **(en-US, IgnoreSymbols, False)** | **16,216.787 ns** | **321.3665 ns** | **300.6064 ns** | **16,179.490 ns** | **15,825.220 ns** | **16,622.804 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |     (en-US, IgnoreSymbols, False) | 26,784.830 ns | 370.0486 ns | 346.1437 ns | 26,777.907 ns | 26,299.516 ns | 27,473.214 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |     (en-US, IgnoreSymbols, False) | 18,288.607 ns | 249.2017 ns | 233.1034 ns | 18,228.232 ns | 17,879.681 ns | 18,682.586 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |     (en-US, IgnoreSymbols, False) | 31,534.040 ns | 509.9552 ns | 523.6864 ns | 31,344.433 ns | 31,029.308 ns | 32,719.445 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |     (en-US, IgnoreSymbols, False) | 11,218.428 ns | 180.6521 ns | 160.1433 ns | 11,173.845 ns | 11,062.145 ns | 11,641.539 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |     (en-US, IgnoreSymbols, False) | 16,340.282 ns | 214.8322 ns | 200.9542 ns | 16,244.340 ns | 16,140.660 ns | 16,731.761 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |              **(en-US, None, False)** |    **143.556 ns** |   **1.4711 ns** |   **1.3761 ns** |    **143.077 ns** |    **141.767 ns** |    **146.640 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |              (en-US, None, False) |     14.855 ns |   0.1887 ns |   0.1765 ns |     14.854 ns |     14.577 ns |     15.263 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |              (en-US, None, False) |    218.975 ns |   2.5820 ns |   2.2889 ns |    218.193 ns |    216.278 ns |    224.047 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |              (en-US, None, False) |     14.339 ns |   0.2231 ns |   0.1978 ns |     14.337 ns |     14.087 ns |     14.729 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |              (en-US, None, False) |    389.869 ns |   4.7810 ns |   4.2383 ns |    389.015 ns |    384.538 ns |    396.885 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |              (en-US, None, False) |    384.424 ns |   5.5805 ns |   5.2200 ns |    382.339 ns |    378.382 ns |    396.125 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |               **(en-US, None, True)** |  **2,564.235 ns** |  **30.7490 ns** |  **25.6768 ns** |  **2,560.350 ns** |  **2,527.482 ns** |  **2,620.342 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |               (en-US, None, True) |    756.063 ns |  14.1004 ns |  13.1895 ns |    754.848 ns |    724.945 ns |    776.063 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |               (en-US, None, True) |  5,173.423 ns |  65.3771 ns |  61.1538 ns |  5,150.739 ns |  5,099.057 ns |  5,290.891 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |               (en-US, None, True) |  1,167.798 ns |  19.0702 ns |  17.8383 ns |  1,164.246 ns |  1,144.472 ns |  1,210.580 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |               (en-US, None, True) | 11,375.718 ns | 175.3550 ns | 164.0272 ns | 11,323.122 ns | 11,171.850 ns | 11,682.027 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |               (en-US, None, True) | 17,153.186 ns | 267.9311 ns | 250.6229 ns | 17,042.004 ns | 16,882.028 ns | 17,601.362 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |           **(en-US, Ordinal, False)** |     **11.411 ns** |   **0.2244 ns** |   **0.1990 ns** |     **11.331 ns** |     **11.232 ns** |     **11.826 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |           (en-US, Ordinal, False) |      8.188 ns |   0.1083 ns |   0.1013 ns |      8.169 ns |      8.071 ns |      8.359 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |           (en-US, Ordinal, False) |     12.162 ns |   0.1526 ns |   0.1428 ns |     12.138 ns |     12.008 ns |     12.436 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |           (en-US, Ordinal, False) |     15.579 ns |   0.1735 ns |   0.1623 ns |     15.542 ns |     15.343 ns |     15.874 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |           (en-US, Ordinal, False) |     32.143 ns |   0.5438 ns |   0.5086 ns |     31.966 ns |     31.513 ns |     33.066 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |           (en-US, Ordinal, False) |     97.133 ns |   0.9123 ns |   0.8087 ns |     97.039 ns |     95.675 ns |     98.484 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** | **(en-US, OrdinalIgnoreCase, False)** |     **75.586 ns** |   **0.4842 ns** |   **0.4043 ns** |     **75.586 ns** |     **74.973 ns** |     **76.420 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar | (en-US, OrdinalIgnoreCase, False) |      8.557 ns |   0.1194 ns |   0.1117 ns |      8.517 ns |      8.424 ns |      8.717 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf | (en-US, OrdinalIgnoreCase, False) |     76.536 ns |   0.8224 ns |   0.6867 ns |     76.328 ns |     75.815 ns |     77.919 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar | (en-US, OrdinalIgnoreCase, False) |    146.715 ns |   1.8674 ns |   1.7468 ns |    146.302 ns |    144.291 ns |    149.699 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound | (en-US, OrdinalIgnoreCase, False) |    553.565 ns |   8.8960 ns |   7.8861 ns |    553.597 ns |    542.465 ns |    568.460 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound | (en-US, OrdinalIgnoreCase, False) |    628.398 ns |   6.5573 ns |   6.1337 ns |    626.922 ns |    620.821 ns |    638.539 ns |     - |     - |     - |         - |
|          **IsPrefix_FirstHalf** |              **(pl-PL, None, False)** |  **4,002.687 ns** |  **43.4608 ns** |  **40.6533 ns** |  **3,993.798 ns** |  **3,945.857 ns** |  **4,071.551 ns** |     **-** |     **-** |     **-** |         **-** |
| IsPrefix_DifferentFirstChar |              (pl-PL, None, False) |    755.317 ns |  12.0240 ns |  10.6590 ns |    753.267 ns |    741.265 ns |    777.162 ns |     - |     - |     - |         - |
|         IsSuffix_SecondHalf |              (pl-PL, None, False) |  5,900.738 ns |  77.9517 ns |  72.9160 ns |  5,872.047 ns |  5,811.270 ns |  6,054.514 ns |     - |     - |     - |         - |
|  IsSuffix_DifferentLastChar |              (pl-PL, None, False) |  1,142.422 ns |  18.5194 ns |  17.3230 ns |  1,144.094 ns |  1,116.680 ns |  1,167.267 ns |     - |     - |     - |         - |
|       IndexOf_Word_NotFound |              (pl-PL, None, False) | 13,311.732 ns | 242.1990 ns | 226.5531 ns | 13,260.881 ns | 12,983.834 ns | 13,707.099 ns |     - |     - |     - |         - |
|   LastIndexOf_Word_NotFound |              (pl-PL, None, False) | 17,837.093 ns | 316.0951 ns | 295.6756 ns | 17,726.736 ns | 17,533.925 ns | 18,424.894 ns |     - |     - |     - |         - |
