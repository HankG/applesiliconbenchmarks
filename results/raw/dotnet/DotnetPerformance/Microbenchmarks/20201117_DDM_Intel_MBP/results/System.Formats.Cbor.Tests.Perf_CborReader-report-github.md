``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |              encoding |  publicKey |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |---------------------- |----------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|   **SkipValue** |       **(Array, Strict)** |          **?** | **239.13 ns** |  **5.393 ns** |  **5.994 ns** | **237.88 ns** | **231.72 ns** | **251.95 ns** | **0.0987** |     **-** |     **-** |     **416 B** |
|   **SkipValue** | **(Byte String, Strict)** |          **?** |  **43.83 ns** |  **0.560 ns** |  **0.524 ns** |  **43.69 ns** |  **42.97 ns** |  **44.63 ns** | **0.0325** |     **-** |     **-** |     **136 B** |
|   **SkipValue** |     **(Integer, Strict)** |          **?** |  **43.97 ns** |  **0.894 ns** |  **0.836 ns** |  **43.85 ns** |  **42.77 ns** |  **45.27 ns** | **0.0324** |     **-** |     **-** |     **136 B** |
|   **SkipValue** |      **(Map, Canonical)** |          **?** | **653.16 ns** | **11.223 ns** | **10.498 ns** | **648.54 ns** | **640.02 ns** | **678.25 ns** | **0.0981** |     **-** |     **-** |     **416 B** |
|   **SkipValue** |            **(Map, Lax)** |          **?** | **560.83 ns** | **11.217 ns** | **10.492 ns** | **559.58 ns** | **546.24 ns** | **579.23 ns** | **0.0984** |     **-** |     **-** |     **416 B** |
|   **SkipValue** |         **(Map, Strict)** |          **?** | **867.56 ns** | **14.736 ns** | **13.784 ns** | **865.66 ns** | **843.40 ns** | **890.08 ns** | **0.2143** |     **-** |     **-** |     **904 B** |
|   **SkipValue** | **(Text String, Strict)** |          **?** |  **54.76 ns** |  **0.806 ns** |  **0.673 ns** |  **54.76 ns** |  **53.74 ns** |  **56.08 ns** | **0.0325** |     **-** |     **-** |     **136 B** |
| **ReadCoseKey** |                     **?** | **ECDSA_P256** | **528.54 ns** |  **8.143 ns** |  **7.617 ns** | **526.76 ns** | **519.40 ns** | **543.13 ns** | **0.1342** |     **-** |     **-** |     **568 B** |
| **ReadCoseKey** |                     **?** | **ECDSA_P384** | **527.02 ns** |  **5.504 ns** |  **4.596 ns** | **525.86 ns** | **519.30 ns** | **535.79 ns** | **0.1415** |     **-** |     **-** |     **600 B** |
| **ReadCoseKey** |                     **?** | **ECDSA_P521** | **539.10 ns** | **14.358 ns** | **16.535 ns** | **533.42 ns** | **521.87 ns** | **576.84 ns** | **0.1549** |     **-** |     **-** |     **648 B** |
