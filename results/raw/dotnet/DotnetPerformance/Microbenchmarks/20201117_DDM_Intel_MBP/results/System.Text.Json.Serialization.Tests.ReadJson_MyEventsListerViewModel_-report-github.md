``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|--------:|-------:|------:|----------:|
|    DeserializeFromString | 319.3 μs | 5.24 μs | 4.90 μs | 317.7 μs | 313.5 μs | 328.1 μs | 18.7500 | 5.0000 |     - |   76.9 KB |
| DeserializeFromUtf8Bytes | 314.4 μs | 3.72 μs | 3.48 μs | 314.1 μs | 309.3 μs | 320.5 μs | 18.7500 | 5.0000 |     - |   76.9 KB |
|    DeserializeFromStream | 375.9 μs | 5.90 μs | 5.23 μs | 375.9 μs | 368.8 μs | 386.3 μs | 17.8571 | 4.4643 |     - |  77.74 KB |
