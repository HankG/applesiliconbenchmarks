``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|  EmptyAsyncMethodInvocation |  12.66 ns |  0.252 ns |  0.290 ns |  12.61 ns |  12.32 ns |  13.23 ns |      - |     - |     - |         - |
| SingleYieldMethodInvocation | 768.48 ns |  7.998 ns |  7.481 ns | 765.57 ns | 759.70 ns | 783.99 ns | 0.0268 |     - |     - |     112 B |
|                       Yield | 618.68 ns | 16.115 ns | 18.559 ns | 619.39 ns | 586.08 ns | 652.82 ns |      - |     - |     - |         - |
