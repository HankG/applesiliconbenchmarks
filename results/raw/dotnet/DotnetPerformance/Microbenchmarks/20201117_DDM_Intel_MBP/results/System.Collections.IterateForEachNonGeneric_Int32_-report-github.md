``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|  ArrayList |  512 |  3.065 μs | 0.0380 μs | 0.0356 μs |  3.053 μs |  3.025 μs |  3.129 μs |      - |     - |     - |      48 B |
|  Hashtable |  512 | 10.294 μs | 0.1661 μs | 0.1554 μs | 10.245 μs | 10.082 μs | 10.535 μs | 3.9139 |     - |     - |   16440 B |
|      Queue |  512 |  3.902 μs | 0.0496 μs | 0.0464 μs |  3.886 μs |  3.852 μs |  3.989 μs |      - |     - |     - |      40 B |
|      Stack |  512 |  3.681 μs | 0.0504 μs | 0.0471 μs |  3.670 μs |  3.618 μs |  3.779 μs |      - |     - |     - |      40 B |
| SortedList |  512 |  7.293 μs | 0.1403 μs | 0.1243 μs |  7.296 μs |  7.123 μs |  7.476 μs | 3.9299 |     - |     - |   16448 B |
