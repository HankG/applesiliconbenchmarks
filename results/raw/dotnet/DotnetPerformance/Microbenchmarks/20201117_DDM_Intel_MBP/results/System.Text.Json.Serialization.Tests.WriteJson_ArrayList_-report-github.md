``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |      Mean |     Error |    StdDev |    Median |       Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |----------:|----------:|----------:|----------:|----------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 10.329 μs | 0.1236 μs | 0.1156 μs | 10.299 μs | 10.131 μs | 10.55 μs | 1.4436 |     - |     - |    6096 B |
|    SerializeToUtf8Bytes |  9.860 μs | 0.1400 μs | 0.1241 μs |  9.833 μs |  9.701 μs | 10.09 μs | 0.7353 |     - |     - |    3136 B |
|       SerializeToStream |  9.881 μs | 0.1182 μs | 0.1106 μs |  9.840 μs |  9.745 μs | 10.13 μs |      - |     - |     - |     152 B |
| SerializeObjectProperty | 10.577 μs | 0.1576 μs | 0.1475 μs | 10.552 μs | 10.369 μs | 10.85 μs | 1.5162 |     - |     - |    6424 B |
