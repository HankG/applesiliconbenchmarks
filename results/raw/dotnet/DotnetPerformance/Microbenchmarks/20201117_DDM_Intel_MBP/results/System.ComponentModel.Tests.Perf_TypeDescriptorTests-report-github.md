``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |        typeToConvert |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |--------------------- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| **GetConverter** |        **ClassIDerived** | **127.9 ns** | **2.07 ns** | **1.93 ns** | **127.2 ns** | **125.5 ns** | **131.8 ns** | **0.0094** |     **-** |     **-** |      **40 B** |
| **GetConverter** | **ClassWithNoConverter** | **120.6 ns** | **2.48 ns** | **2.32 ns** | **119.8 ns** | **117.5 ns** | **124.7 ns** | **0.0093** |     **-** |     **-** |      **40 B** |
| **GetConverter** |         **DerivedClass** | **168.2 ns** | **2.82 ns** | **2.64 ns** | **167.2 ns** | **165.5 ns** | **173.0 ns** | **0.0089** |     **-** |     **-** |      **40 B** |
| **GetConverter** |             **IDerived** | **104.2 ns** | **1.73 ns** | **1.62 ns** | **103.6 ns** | **102.0 ns** | **106.8 ns** | **0.0094** |     **-** |     **-** |      **40 B** |
| **GetConverter** |             **SomeEnum** | **250.5 ns** | **3.39 ns** | **3.00 ns** | **250.1 ns** | **246.6 ns** | **256.7 ns** | **0.0091** |     **-** |     **-** |      **40 B** |
| **GetConverter** |                 **Enum** | **182.9 ns** | **2.45 ns** | **2.29 ns** | **182.4 ns** | **180.3 ns** | **187.2 ns** | **0.0089** |     **-** |     **-** |      **40 B** |
| **GetConverter** |                 **Guid** | **185.3 ns** | **2.72 ns** | **2.41 ns** | **185.2 ns** | **181.7 ns** | **189.3 ns** | **0.0089** |     **-** |     **-** |      **40 B** |
| **GetConverter** |                **Int32** | **183.4 ns** | **2.80 ns** | **2.48 ns** | **182.7 ns** | **180.7 ns** | **189.4 ns** | **0.0089** |     **-** |     **-** |      **40 B** |
| **GetConverter** |       **SomeValueType?** | **173.2 ns** | **1.10 ns** | **0.86 ns** | **173.4 ns** | **171.3 ns** | **174.5 ns** | **0.0092** |     **-** |     **-** |      **40 B** |
| **GetConverter** |               **Int32?** | **199.9 ns** | **3.16 ns** | **2.96 ns** | **200.2 ns** | **196.2 ns** | **204.9 ns** | **0.0095** |     **-** |     **-** |      **40 B** |
| **GetConverter** |               **String** | **128.4 ns** | **3.35 ns** | **3.86 ns** | **126.8 ns** | **124.8 ns** | **138.0 ns** | **0.0093** |     **-** |     **-** |      **40 B** |
