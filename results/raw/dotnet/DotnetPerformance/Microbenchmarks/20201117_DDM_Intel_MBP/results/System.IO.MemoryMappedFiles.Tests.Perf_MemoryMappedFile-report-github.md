``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | capacity |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |--------- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|      **CreateNew** |    **10000** | **138.06 μs** | **3.360 μs** | **3.595 μs** | **137.56 μs** | **133.87 μs** | **146.71 μs** |     **-** |     **-** |     **-** |     **896 B** |
| CreateFromFile |    10000 |  36.28 μs | 0.715 μs | 0.734 μs |  35.98 μs |  35.51 μs |  37.55 μs |     - |     - |     - |     408 B |
|      **CreateNew** |   **100000** | **139.08 μs** | **2.607 μs** | **2.438 μs** | **138.64 μs** | **135.41 μs** | **144.42 μs** |     **-** |     **-** |     **-** |     **896 B** |
| CreateFromFile |   100000 |  36.26 μs | 0.648 μs | 0.606 μs |  36.07 μs |  35.34 μs |  37.39 μs |     - |     - |     - |     408 B |
|      **CreateNew** |  **1000000** | **140.24 μs** | **5.250 μs** | **5.617 μs** | **139.56 μs** | **132.48 μs** | **153.31 μs** |     **-** |     **-** |     **-** |     **896 B** |
| CreateFromFile |  1000000 |  37.26 μs | 0.729 μs | 0.749 μs |  37.03 μs |  36.24 μs |  38.83 μs |     - |     - |     - |     408 B |
|      **CreateNew** | **10000000** | **140.00 μs** | **2.478 μs** | **2.069 μs** | **140.23 μs** | **137.14 μs** | **144.34 μs** |     **-** |     **-** |     **-** |     **896 B** |
| CreateFromFile | 10000000 |  47.74 μs | 0.551 μs | 0.488 μs |  47.72 μs |  46.99 μs |  49.05 μs |     - |     - |     - |     408 B |
