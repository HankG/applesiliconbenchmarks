``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|           Ctor_String |  3.5323 ns | 0.0641 ns | 0.0569 ns |  3.5078 ns |  3.4614 ns |  3.6426 ns |      - |     - |     - |         - |
|              GetValue |  1.6069 ns | 0.0690 ns | 0.0611 ns |  1.6051 ns |  1.5204 ns |  1.7217 ns |      - |     - |     - |         - |
|               Indexer |  7.0898 ns | 0.0866 ns | 0.0810 ns |  7.0630 ns |  7.0053 ns |  7.2666 ns |      - |     - |     - |         - |
| Equals_Object_Invalid |  0.0033 ns | 0.0092 ns | 0.0082 ns |  0.0000 ns |  0.0000 ns |  0.0307 ns |      - |     - |     - |         - |
|   Equals_Object_Valid |  5.9217 ns | 0.0886 ns | 0.0829 ns |  5.8883 ns |  5.8320 ns |  6.0646 ns |      - |     - |     - |         - |
|          Equals_Valid |  3.7626 ns | 0.0710 ns | 0.0630 ns |  3.7428 ns |  3.6985 ns |  3.8931 ns |      - |     - |     - |         - |
|         Equals_String |  4.7594 ns | 0.0726 ns | 0.0680 ns |  4.7504 ns |  4.6613 ns |  4.8747 ns |      - |     - |     - |         - |
|    GetSegmentHashCode |  9.0772 ns | 0.1810 ns | 0.1693 ns |  8.9931 ns |  8.9313 ns |  9.5073 ns |      - |     - |     - |         - |
|            StartsWith |  8.4303 ns | 0.1624 ns | 0.1520 ns |  8.3863 ns |  8.2755 ns |  8.7347 ns |      - |     - |     - |         - |
|              EndsWith |  9.5162 ns | 0.1597 ns | 0.1494 ns |  9.4516 ns |  9.3471 ns |  9.7902 ns |      - |     - |     - |         - |
|             SubString | 10.0727 ns | 0.2168 ns | 0.2028 ns | 10.0250 ns |  9.8135 ns | 10.5269 ns | 0.0076 |     - |     - |      32 B |
|            SubSegment |  4.7120 ns | 0.0825 ns | 0.0772 ns |  4.6839 ns |  4.6051 ns |  4.9058 ns |      - |     - |     - |         - |
|               IndexOf |  6.4416 ns | 0.1122 ns | 0.1049 ns |  6.4007 ns |  6.3120 ns |  6.6705 ns |      - |     - |     - |         - |
|            IndexOfAny |  6.6104 ns | 0.0982 ns | 0.0918 ns |  6.5732 ns |  6.4943 ns |  6.7668 ns |      - |     - |     - |         - |
|           LastIndexOf |  7.8780 ns | 0.0882 ns | 0.0825 ns |  7.8862 ns |  7.7083 ns |  8.0386 ns |      - |     - |     - |         - |
|                  Trim | 26.7423 ns | 0.3917 ns | 0.3472 ns | 26.6808 ns | 26.3596 ns | 27.6154 ns |      - |     - |     - |         - |
|             TrimStart |  8.5295 ns | 0.1165 ns | 0.1033 ns |  8.4920 ns |  8.4350 ns |  8.7576 ns |      - |     - |     - |         - |
|               TrimEnd |  9.4933 ns | 0.2118 ns | 0.2175 ns |  9.4055 ns |  9.2674 ns | 10.0862 ns |      - |     - |     - |         - |
