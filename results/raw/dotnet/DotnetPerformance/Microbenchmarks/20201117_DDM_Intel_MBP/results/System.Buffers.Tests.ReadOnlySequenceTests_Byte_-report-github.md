``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                          Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|              IterateTryGetArray |  6.501 ns | 0.1067 ns | 0.0998 ns |  6.468 ns |  6.345 ns |  6.653 ns |     - |     - |     - |         - |
|             IterateForEachArray | 16.692 ns | 0.2167 ns | 0.1921 ns | 16.649 ns | 16.472 ns | 17.080 ns |     - |     - |     - |         - |
|         IterateGetPositionArray | 29.910 ns | 0.4000 ns | 0.3742 ns | 29.858 ns | 29.480 ns | 30.551 ns |     - |     - |     - |         - |
|                      FirstArray |  8.577 ns | 0.0722 ns | 0.0564 ns |  8.574 ns |  8.506 ns |  8.688 ns |     - |     - |     - |         - |
|                  FirstSpanArray | 12.894 ns | 0.1567 ns | 0.1466 ns | 12.849 ns | 12.693 ns | 13.212 ns |     - |     - |     - |         - |
|                 FirstSpanMemory | 13.347 ns | 0.1690 ns | 0.1581 ns | 13.304 ns | 13.169 ns | 13.665 ns |     - |     - |     - |         - |
|          FirstSpanSingleSegment | 18.626 ns | 0.2233 ns | 0.2089 ns | 18.580 ns | 18.394 ns | 19.058 ns |     - |     - |     - |         - |
|            FirstSpanTenSegments | 18.806 ns | 0.2618 ns | 0.2320 ns | 18.744 ns | 18.547 ns | 19.205 ns |     - |     - |     - |         - |
|                      SliceArray |  6.862 ns | 0.0673 ns | 0.0630 ns |  6.843 ns |  6.757 ns |  6.961 ns |     - |     - |     - |         - |
|             IterateTryGetMemory | 24.823 ns | 0.4204 ns | 0.3727 ns | 24.685 ns | 24.240 ns | 25.577 ns |     - |     - |     - |         - |
|            IterateForEachMemory | 32.702 ns | 0.5058 ns | 0.4731 ns | 32.521 ns | 32.208 ns | 33.587 ns |     - |     - |     - |         - |
|        IterateGetPositionMemory | 45.962 ns | 0.4770 ns | 0.4462 ns | 45.756 ns | 45.338 ns | 46.849 ns |     - |     - |     - |         - |
|                     FirstMemory |  9.631 ns | 0.1491 ns | 0.1394 ns |  9.553 ns |  9.470 ns |  9.903 ns |     - |     - |     - |         - |
|                     SliceMemory |  8.466 ns | 0.1105 ns | 0.1033 ns |  8.427 ns |  8.341 ns |  8.672 ns |     - |     - |     - |         - |
|      IterateTryGetSingleSegment | 13.127 ns | 0.2704 ns | 0.2529 ns | 13.139 ns | 12.765 ns | 13.636 ns |     - |     - |     - |         - |
|     IterateForEachSingleSegment | 23.976 ns | 0.2293 ns | 0.2032 ns | 23.949 ns | 23.731 ns | 24.355 ns |     - |     - |     - |         - |
| IterateGetPositionSingleSegment | 35.691 ns | 0.5443 ns | 0.4825 ns | 35.643 ns | 35.060 ns | 36.645 ns |     - |     - |     - |         - |
|              FirstSingleSegment |  3.814 ns | 0.0576 ns | 0.0539 ns |  3.799 ns |  3.743 ns |  3.913 ns |     - |     - |     - |         - |
|              SliceSingleSegment |  7.215 ns | 0.0915 ns | 0.0811 ns |  7.179 ns |  7.133 ns |  7.379 ns |     - |     - |     - |         - |
|        IterateTryGetTenSegments | 53.537 ns | 0.5027 ns | 0.4198 ns | 53.628 ns | 52.821 ns | 54.484 ns |     - |     - |     - |         - |
|       IterateForEachTenSegments | 67.708 ns | 1.0099 ns | 0.9447 ns | 67.589 ns | 66.158 ns | 69.579 ns |     - |     - |     - |         - |
|   IterateGetPositionTenSegments | 64.364 ns | 1.1028 ns | 1.0316 ns | 63.976 ns | 63.327 ns | 66.807 ns |     - |     - |     - |         - |
|                FirstTenSegments |  3.581 ns | 0.0520 ns | 0.0487 ns |  3.566 ns |  3.522 ns |  3.693 ns |     - |     - |     - |         - |
|                SliceTenSegments | 21.096 ns | 0.2502 ns | 0.2340 ns | 21.024 ns | 20.862 ns | 21.522 ns |     - |     - |     - |         - |
