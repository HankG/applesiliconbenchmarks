``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
| **Dictionary** |    **0** | **131.0081 ns** | **1.5163 ns** | **1.4183 ns** | **131.0500 ns** | **128.7394 ns** | **133.7877 ns** |     **-** |     **-** |     **-** |         **-** |
|      Queue |    0 |   5.5585 ns | 0.0912 ns | 0.0853 ns |   5.5403 ns |   5.4602 ns |   5.6927 ns |     - |     - |     - |         - |
|      Stack |    0 |   0.0000 ns | 0.0000 ns | 0.0000 ns |   0.0000 ns |   0.0000 ns |   0.0000 ns |     - |     - |     - |         - |
|        Bag |    0 |  12.6715 ns | 0.1500 ns | 0.1330 ns |  12.6678 ns |  12.4975 ns |  12.8678 ns |     - |     - |     - |         - |
| **Dictionary** |  **512** |   **2.4835 ns** | **0.0533 ns** | **0.0498 ns** |   **2.4615 ns** |   **2.4358 ns** |   **2.5929 ns** |     **-** |     **-** |     **-** |         **-** |
|      Queue |  512 |   5.9279 ns | 0.1056 ns | 0.0988 ns |   5.9034 ns |   5.7877 ns |   6.0985 ns |     - |     - |     - |         - |
|      Stack |  512 |   0.1059 ns | 0.0214 ns | 0.0190 ns |   0.1048 ns |   0.0794 ns |   0.1422 ns |     - |     - |     - |         - |
|        Bag |  512 |  10.8336 ns | 0.1470 ns | 0.1375 ns |  10.8369 ns |  10.6756 ns |  11.1026 ns |     - |     - |     - |         - |
