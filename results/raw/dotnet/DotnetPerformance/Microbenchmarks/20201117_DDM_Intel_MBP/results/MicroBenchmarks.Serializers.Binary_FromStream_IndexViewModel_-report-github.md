``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|---------------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|-------:|------:|----------:|
| BinaryFormatter | 25.722 μs | 0.5035 μs | 0.4945 μs | 25.714 μs | 25.092 μs | 26.607 μs | 5.1661 |      - |     - |  21.49 KB |
|    protobuf-net | 47.720 μs | 0.8211 μs | 0.8786 μs | 47.581 μs | 46.785 μs | 49.603 μs | 7.5758 |      - |     - |  31.04 KB |
|     MessagePack |  8.997 μs | 0.1487 μs | 0.1391 μs |  8.971 μs |  8.799 μs |  9.221 μs | 5.1879 | 0.1052 |     - |   21.2 KB |
