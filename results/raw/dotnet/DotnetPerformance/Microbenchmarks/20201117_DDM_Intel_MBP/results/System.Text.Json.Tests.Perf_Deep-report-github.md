``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | Formatted | SkipValidation |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |---------- |--------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|  **WriteDeepUtf8** |     **False** |          **False** |  **4.384 ms** | **0.0652 ms** | **0.0610 ms** |  **4.370 ms** |  **4.303 ms** |  **4.519 ms** |     **-** |     **-** |     **-** |     **341 B** |
| WriteDeepUtf16 |     False |          False |  4.270 ms | 0.0670 ms | 0.0626 ms |  4.265 ms |  4.181 ms |  4.389 ms |     - |     - |     - |     341 B |
|  **WriteDeepUtf8** |     **False** |           **True** |  **3.986 ms** | **0.0724 ms** | **0.0677 ms** |  **3.957 ms** |  **3.902 ms** |  **4.100 ms** |     **-** |     **-** |     **-** |     **124 B** |
| WriteDeepUtf16 |     False |           True |  4.083 ms | 0.0508 ms | 0.0475 ms |  4.072 ms |  4.007 ms |  4.169 ms |     - |     - |     - |     125 B |
|  **WriteDeepUtf8** |      **True** |          **False** | **27.705 ms** | **3.4107 ms** | **3.9277 ms** | **27.478 ms** | **22.185 ms** | **34.529 ms** |     **-** |     **-** |     **-** |     **624 B** |
| WriteDeepUtf16 |      True |          False | 28.196 ms | 3.1648 ms | 3.6446 ms | 28.779 ms | 22.375 ms | 33.749 ms |     - |     - |     - |     624 B |
|  **WriteDeepUtf8** |      **True** |           **True** | **28.370 ms** | **3.2815 ms** | **3.7789 ms** | **29.194 ms** | **21.987 ms** | **34.918 ms** |     **-** |     **-** |     **-** |     **408 B** |
| WriteDeepUtf16 |      True |           True | 27.143 ms | 2.7907 ms | 3.2137 ms | 27.175 ms | 22.403 ms | 32.012 ms |     - |     - |     - |     408 B |
