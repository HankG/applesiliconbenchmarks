``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                          Method | IsDataCompact |      TestCase |          Mean |        Error |       StdDev |        Median |          Min |           Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |-------------- |-------------- |--------------:|-------------:|-------------:|--------------:|-------------:|--------------:|------:|------:|------:|----------:|
|               **ReadSpanEmptyLoop** |         **False** |    **HelloWorld** |     **106.49 ns** |     **1.118 ns** |     **1.045 ns** |     **106.38 ns** |    **104.93 ns** |     **108.50 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |    HelloWorld |     119.85 ns |     1.490 ns |     1.394 ns |     119.43 ns |    117.93 ns |     122.53 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |    HelloWorld |     197.02 ns |     2.747 ns |     2.570 ns |     195.89 ns |    194.35 ns |     202.04 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |    HelloWorld |     121.66 ns |     1.211 ns |     1.133 ns |     121.30 ns |    120.28 ns |     123.60 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** |      **DeepTree** |   **8,984.35 ns** |    **98.323 ns** |    **87.161 ns** |   **8,970.00 ns** |  **8,880.02 ns** |   **9,143.30 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |      DeepTree |   8,857.26 ns |    74.200 ns |    65.776 ns |   8,845.95 ns |  8,765.26 ns |   8,971.40 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |      DeepTree |   9,596.46 ns |   144.893 ns |   128.444 ns |   9,541.92 ns |  9,454.77 ns |   9,792.48 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |      DeepTree |   9,455.90 ns |   111.947 ns |   104.715 ns |   9,424.09 ns |  9,335.36 ns |   9,648.40 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** |     **BroadTree** |  **12,356.93 ns** |   **151.916 ns** |   **142.102 ns** |  **12,306.81 ns** | **12,184.30 ns** |  **12,581.73 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |     BroadTree |  12,136.49 ns |   126.054 ns |   117.911 ns |  12,115.09 ns | 11,972.72 ns |  12,314.38 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |     BroadTree |  13,689.25 ns |   152.394 ns |   142.549 ns |  13,614.83 ns | 13,536.41 ns |  13,984.89 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |     BroadTree |  13,819.76 ns |   203.582 ns |   180.470 ns |  13,776.57 ns | 13,549.70 ns |  14,198.63 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** | **LotsOfNumbers** |   **3,179.44 ns** |    **38.284 ns** |    **35.811 ns** |   **3,163.81 ns** |  **3,135.39 ns** |   **3,243.78 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False | LotsOfNumbers |   3,199.01 ns |    34.581 ns |    30.655 ns |   3,190.09 ns |  3,156.93 ns |   3,263.11 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False | LotsOfNumbers |   3,826.80 ns |    55.755 ns |    52.153 ns |   3,806.02 ns |  3,768.52 ns |   3,926.51 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False | LotsOfNumbers |   3,568.30 ns |    38.834 ns |    36.325 ns |   3,553.71 ns |  3,522.70 ns |   3,626.47 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** | **LotsOfStrings** |   **2,003.66 ns** |    **25.862 ns** |    **24.191 ns** |   **2,000.64 ns** |  **1,970.87 ns** |   **2,045.03 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False | LotsOfStrings |   2,045.61 ns |    22.143 ns |    19.629 ns |   2,040.05 ns |  2,016.91 ns |   2,080.13 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False | LotsOfStrings |   2,238.40 ns |    27.265 ns |    24.170 ns |   2,232.39 ns |  2,209.46 ns |   2,286.54 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False | LotsOfStrings |   2,314.60 ns |    33.039 ns |    30.905 ns |   2,303.20 ns |  2,273.82 ns |   2,376.29 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** |      **Json400B** |     **923.24 ns** |     **9.004 ns** |     **7.519 ns** |     **924.67 ns** |    **911.95 ns** |     **934.49 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |      Json400B |     942.78 ns |    12.459 ns |    11.654 ns |     938.76 ns |    927.55 ns |     962.60 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |      Json400B |   1,121.10 ns |    11.738 ns |    10.406 ns |   1,117.93 ns |  1,102.50 ns |   1,136.61 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |      Json400B |   1,050.04 ns |    15.139 ns |    14.161 ns |   1,044.47 ns |  1,033.77 ns |   1,085.30 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** |       **Json4KB** |   **8,123.14 ns** |    **83.401 ns** |    **78.013 ns** |   **8,105.93 ns** |  **8,017.79 ns** |   **8,265.91 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |       Json4KB |   8,119.22 ns |   134.743 ns |   126.039 ns |   8,082.90 ns |  7,962.69 ns |   8,355.27 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |       Json4KB |   8,703.17 ns |   110.575 ns |   103.432 ns |   8,655.25 ns |  8,567.89 ns |   8,850.96 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |       Json4KB |   9,047.65 ns |   131.668 ns |   116.720 ns |   9,010.96 ns |  8,919.74 ns |   9,274.46 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |         **False** |      **Json40KB** |  **90,651.32 ns** |   **901.164 ns** |   **842.949 ns** |  **90,495.64 ns** | **89,399.62 ns** |  **92,430.61 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |         False |      Json40KB |  90,373.00 ns |   840.379 ns |   744.974 ns |  90,478.16 ns | 89,323.70 ns |  91,772.22 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |         False |      Json40KB |  96,904.78 ns | 1,482.314 ns | 1,386.557 ns |  96,478.33 ns | 95,440.35 ns | 100,162.07 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |         False |      Json40KB | 101,030.39 ns | 1,005.154 ns |   940.222 ns | 100,966.83 ns | 99,510.64 ns | 102,843.49 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |    **HelloWorld** |      **95.03 ns** |     **1.261 ns** |     **1.180 ns** |      **94.89 ns** |     **93.41 ns** |      **97.09 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |    HelloWorld |     101.90 ns |     0.899 ns |     0.841 ns |     101.68 ns |    100.92 ns |     103.50 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |    HelloWorld |     179.31 ns |     2.502 ns |     2.218 ns |     178.92 ns |    176.69 ns |     184.04 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |    HelloWorld |     108.90 ns |     1.468 ns |     1.373 ns |     109.37 ns |    107.04 ns |     110.90 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |      **DeepTree** |   **3,542.67 ns** |    **46.688 ns** |    **41.387 ns** |   **3,531.01 ns** |  **3,500.41 ns** |   **3,622.94 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |      DeepTree |   3,581.24 ns |    37.837 ns |    35.393 ns |   3,568.77 ns |  3,541.93 ns |   3,639.01 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |      DeepTree |   4,260.04 ns |    57.324 ns |    53.621 ns |   4,238.04 ns |  4,198.89 ns |   4,344.48 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |      DeepTree |   4,218.84 ns |    77.846 ns |    76.455 ns |   4,206.72 ns |  4,101.43 ns |   4,394.20 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |     **BroadTree** |   **7,854.52 ns** |   **109.735 ns** |   **102.646 ns** |   **7,826.21 ns** |  **7,734.47 ns** |   **8,052.63 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |     BroadTree |   7,729.38 ns |    87.348 ns |    72.939 ns |   7,728.76 ns |  7,638.82 ns |   7,900.20 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |     BroadTree |   9,586.02 ns |    77.931 ns |    65.076 ns |   9,582.95 ns |  9,498.78 ns |   9,705.83 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |     BroadTree |   9,493.90 ns |   105.978 ns |    99.132 ns |   9,496.27 ns |  9,327.57 ns |   9,688.24 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** | **LotsOfNumbers** |   **2,544.10 ns** |    **30.162 ns** |    **28.214 ns** |   **2,537.45 ns** |  **2,500.89 ns** |   **2,589.02 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True | LotsOfNumbers |   2,487.28 ns |    23.855 ns |    21.147 ns |   2,480.35 ns |  2,462.88 ns |   2,532.39 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True | LotsOfNumbers |   3,166.69 ns |    42.407 ns |    37.592 ns |   3,148.71 ns |  3,118.89 ns |   3,229.69 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True | LotsOfNumbers |   2,598.51 ns |    21.992 ns |    19.495 ns |   2,595.83 ns |  2,576.42 ns |   2,639.76 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** | **LotsOfStrings** |   **1,276.63 ns** |    **19.722 ns** |    **18.448 ns** |   **1,268.99 ns** |  **1,252.92 ns** |   **1,306.60 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True | LotsOfStrings |   1,341.87 ns |    12.915 ns |    12.080 ns |   1,343.97 ns |  1,325.55 ns |   1,364.63 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True | LotsOfStrings |   1,505.92 ns |    11.752 ns |    10.993 ns |   1,505.42 ns |  1,483.99 ns |   1,520.38 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True | LotsOfStrings |   1,702.97 ns |    20.328 ns |    19.015 ns |   1,701.81 ns |  1,675.44 ns |   1,743.26 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |      **Json400B** |     **708.22 ns** |     **8.708 ns** |     **8.145 ns** |     **706.60 ns** |    **696.49 ns** |     **722.29 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |      Json400B |     715.65 ns |     7.588 ns |     6.727 ns |     714.72 ns |    704.58 ns |     732.10 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |      Json400B |     859.95 ns |     9.128 ns |     8.092 ns |     856.99 ns |    850.30 ns |     873.92 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |      Json400B |     825.22 ns |    10.539 ns |     9.343 ns |     822.92 ns |    811.66 ns |     844.90 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |       **Json4KB** |   **5,471.17 ns** |    **48.143 ns** |    **45.033 ns** |   **5,480.38 ns** |  **5,402.55 ns** |   **5,547.93 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |       Json4KB |   5,380.87 ns |    59.530 ns |    55.684 ns |   5,376.48 ns |  5,303.50 ns |   5,471.92 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |       Json4KB |   5,803.09 ns |    53.660 ns |    47.568 ns |   5,810.96 ns |  5,734.42 ns |   5,880.50 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |       Json4KB |   6,353.35 ns |    70.850 ns |    66.273 ns |   6,323.24 ns |  6,271.59 ns |   6,481.56 ns |     - |     - |     - |         - |
|               **ReadSpanEmptyLoop** |          **True** |      **Json40KB** |  **67,358.79 ns** |   **977.075 ns** |   **866.151 ns** |  **67,311.69 ns** | **66,273.75 ns** |  **68,699.34 ns** |     **-** |     **-** |     **-** |         **-** |
| ReadSingleSpanSequenceEmptyLoop |          True |      Json40KB |  66,458.75 ns |   670.982 ns |   560.301 ns |  66,443.18 ns | 65,669.79 ns |  67,841.39 ns |     - |     - |     - |         - |
|  ReadMultiSpanSequenceEmptyLoop |          True |      Json40KB |  70,943.38 ns | 1,034.062 ns |   967.263 ns |  70,593.64 ns | 69,649.76 ns |  73,199.05 ns |     - |     - |     - |         - |
|                 ReadReturnBytes |          True |      Json40KB |  77,302.11 ns |   885.355 ns |   828.161 ns |  77,353.99 ns | 76,316.88 ns |  78,985.06 ns |     - |     - |     - |         - |
