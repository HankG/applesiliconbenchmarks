``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QZKIVK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MaxWarmupIterationCount=10  
MinIterationCount=15  MinWarmupIterationCount=6  UnrollFactor=1  
WarmupCount=-1  

```
|          Method |    Size |      Mean |    Error |   StdDev |    Median |       Min |       Max |      Gen 0 |     Gen 1 |     Gen 2 | Allocated |
|---------------- |-------- |----------:|---------:|---------:|----------:|----------:|----------:|-----------:|----------:|----------:|----------:|
|   ConcurrentBag | 2000000 | 223.89 ms | 7.716 ms | 8.885 ms | 222.93 ms | 210.90 ms | 243.83 ms |  1000.0000 | 1000.0000 | 1000.0000 |   8.02 MB |
| ConcurrentStack | 2000000 | 102.60 ms | 8.680 ms | 9.996 ms | 101.45 ms |  85.76 ms | 120.00 ms | 14000.0000 | 1000.0000 |         - |  61.65 MB |
| ConcurrentQueue | 2000000 |  24.19 ms | 1.918 ms | 2.209 ms |  23.32 ms |  21.89 ms |  28.13 ms |          - |         - |         - |      1 MB |
