``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|               CreateElement |  45.38 ns | 0.977 ns | 0.914 ns |  45.19 ns |  44.20 ns |  47.44 ns | 0.0153 |     - |     - |      64 B |
|  CreateElementWithNamespace |  74.70 ns | 1.290 ns | 1.206 ns |  74.69 ns |  73.01 ns |  76.82 ns | 0.0151 |     - |     - |      64 B |
|          CreateWithElements |  75.37 ns | 0.998 ns | 0.885 ns |  75.04 ns |  74.03 ns |  76.89 ns | 0.0197 |     - |     - |      83 B |
| CreateElementsWithNamespace | 113.64 ns | 2.186 ns | 2.045 ns | 113.44 ns | 110.95 ns | 117.23 ns | 0.0195 |     - |     - |      83 B |
|                  GetElement |  36.22 ns | 0.361 ns | 0.320 ns |  36.14 ns |  35.82 ns |  36.96 ns |      - |     - |     - |         - |
|     GetElementWithNamespace |  71.19 ns | 0.897 ns | 0.839 ns |  71.06 ns |  70.05 ns |  72.92 ns |      - |     - |     - |         - |
|                GetAttribute |  25.19 ns | 0.332 ns | 0.310 ns |  25.08 ns |  24.72 ns |  25.85 ns |      - |     - |     - |         - |
|                    GetValue |  63.20 ns | 0.698 ns | 0.619 ns |  63.27 ns |  62.27 ns |  64.50 ns | 0.0095 |     - |     - |      40 B |
