``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  3.075 μs | 0.0393 μs | 0.0328 μs |  3.078 μs |  2.994 μs |  3.115 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  4.690 μs | 0.0657 μs | 0.0614 μs |  4.670 μs |  4.609 μs |  4.820 μs |     - |     - |     - |         - |
|                SortedList |  512 | 26.495 μs | 0.4290 μs | 0.4013 μs | 26.313 μs | 26.114 μs | 27.292 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 45.859 μs | 0.8706 μs | 0.8550 μs | 45.583 μs | 44.952 μs | 48.127 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  3.035 μs | 0.0376 μs | 0.0351 μs |  3.039 μs |  2.978 μs |  3.091 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 | 19.798 μs | 0.3048 μs | 0.2702 μs | 19.705 μs | 19.512 μs | 20.372 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 26.598 μs | 0.3433 μs | 0.3212 μs | 26.505 μs | 26.275 μs | 27.232 μs |     - |     - |     - |         - |
