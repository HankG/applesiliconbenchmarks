``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | culturestring |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |-------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| **ToStringHebrewIsrael** |             **?** |   **411.8 ns** |   **7.05 ns** |   **5.89 ns** |   **411.2 ns** |   **401.2 ns** |   **420.2 ns** | **0.0611** |     **-** |     **-** |     **256 B** |
|             **ToString** |              **** |   **226.3 ns** |   **3.27 ns** |   **3.06 ns** |   **226.1 ns** |   **222.3 ns** |   **232.2 ns** | **0.0146** |     **-** |     **-** |      **64 B** |
|                Parse |               |   346.5 ns |   4.19 ns |   3.92 ns |   344.7 ns |   341.3 ns |   354.6 ns |      - |     - |     - |         - |
|             **ToString** |            **da** |   **214.0 ns** |   **3.18 ns** |   **2.82 ns** |   **212.8 ns** |   **211.1 ns** |   **218.9 ns** | **0.0146** |     **-** |     **-** |      **64 B** |
|                Parse |            da |   370.3 ns |   6.76 ns |   5.99 ns |   368.2 ns |   364.5 ns |   381.9 ns |      - |     - |     - |         - |
|             **ToString** |            **fr** |   **226.8 ns** |   **3.71 ns** |   **3.47 ns** |   **227.1 ns** |   **222.7 ns** |   **233.2 ns** | **0.0147** |     **-** |     **-** |      **64 B** |
|                Parse |            fr |   345.7 ns |   4.66 ns |   4.36 ns |   343.8 ns |   341.0 ns |   353.4 ns |      - |     - |     - |         - |
|             **ToString** |            **ja** |   **226.5 ns** |   **2.97 ns** |   **2.78 ns** |   **226.2 ns** |   **223.0 ns** |   **231.8 ns** | **0.0148** |     **-** |     **-** |      **64 B** |
|                Parse |            ja | 4,209.1 ns | 616.97 ns | 685.76 ns | 4,066.0 ns | 3,296.0 ns | 5,584.0 ns |      - |     - |     - |     288 B |
