``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|---------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|-------:|------:|----------:|
|                Create |     4.112 ns |  0.1019 ns |  0.0904 ns |     4.093 ns |     3.999 ns |     4.303 ns | 0.0134 |      - |     - |      56 B |
|                 Parse | 2,125.313 ns | 25.3856 ns | 19.8194 ns | 2,121.072 ns | 2,085.691 ns | 2,149.314 ns | 2.8711 | 0.0084 |     - |   12016 B |
| CreateWithRootlEement |    52.882 ns |  0.7615 ns |  0.6750 ns |    52.866 ns |    51.721 ns |    54.096 ns | 0.0362 |      - |     - |     152 B |
|        GetRootElement |     6.068 ns |  0.1253 ns |  0.1111 ns |     6.024 ns |     5.928 ns |     6.299 ns |      - |      - |     - |         - |
|            GetElement |    35.302 ns |  0.3211 ns |  0.2846 ns |    35.288 ns |    34.876 ns |    35.843 ns |      - |      - |     - |         - |
