``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                                     Method |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------------------------- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|               &#39;GetCustomAttributes - Class: Hit (inherit)&#39; | 1,154.82 ns | 21.156 ns | 19.789 ns | 1,151.19 ns | 1,129.80 ns | 1,200.28 ns | 0.0502 |     - |     - |     224 B |
|              &#39;GetCustomAttributes - Class: Miss (inherit)&#39; |   297.57 ns |  3.652 ns |  3.416 ns |   296.52 ns |   292.81 ns |   303.98 ns |      - |     - |     - |         - |
|            &#39;GetCustomAttributes - Class: Hit (no inherit)&#39; |   975.39 ns | 12.304 ns | 11.509 ns |   972.99 ns |   952.15 ns |   991.39 ns | 0.0506 |     - |     - |     224 B |
|           &#39;GetCustomAttributes - Class: Miss (no inherit)&#39; |   171.08 ns |  2.509 ns |  2.347 ns |   170.15 ns |   168.53 ns |   175.35 ns |      - |     - |     - |         - |
|     &#39;GetCustomAttributes - Method Override: Hit (inherit)&#39; | 1,778.02 ns | 13.157 ns | 10.272 ns | 1,777.30 ns | 1,754.83 ns | 1,791.25 ns | 0.0851 |     - |     - |     360 B |
|    &#39;GetCustomAttributes - Method Override: Miss (inherit)&#39; | 1,017.04 ns | 19.029 ns | 16.869 ns | 1,018.91 ns |   991.55 ns | 1,044.59 ns | 0.0528 |     - |     - |     224 B |
|  &#39;GetCustomAttributes - Method Override: Hit (no inherit)&#39; | 1,010.94 ns | 13.675 ns | 12.792 ns | 1,007.21 ns |   994.76 ns | 1,037.27 ns | 0.0564 |     - |     - |     240 B |
| &#39;GetCustomAttributes - Method Override: Miss (no inherit)&#39; | 1,005.09 ns | 17.091 ns | 15.150 ns | 1,000.34 ns |   987.14 ns | 1,033.21 ns | 0.0528 |     - |     - |     224 B |
|         &#39;GetCustomAttributes - Method Base: Hit (inherit)&#39; | 1,024.88 ns | 15.084 ns | 14.109 ns | 1,024.87 ns | 1,005.70 ns | 1,043.08 ns | 0.0527 |     - |     - |     224 B |
|        &#39;GetCustomAttributes - Method Base: Miss (inherit)&#39; |   191.31 ns |  2.782 ns |  2.466 ns |   191.67 ns |   188.18 ns |   196.52 ns |      - |     - |     - |         - |
|      &#39;GetCustomAttributes - Method Base: Hit (no inherit)&#39; | 1,005.12 ns | 16.081 ns | 15.042 ns | 1,004.68 ns |   981.00 ns | 1,028.67 ns | 0.0532 |     - |     - |     224 B |
|     &#39;GetCustomAttributes - Method Base: Miss (no inherit)&#39; |   166.63 ns |  1.956 ns |  1.734 ns |   166.66 ns |   164.30 ns |   169.49 ns |      - |     - |     - |         - |
|                         &#39;IsDefined - Class: Hit (inherit)&#39; |   420.40 ns |  5.904 ns |  5.523 ns |   418.79 ns |   412.73 ns |   429.66 ns | 0.0320 |     - |     - |     136 B |
|                        &#39;IsDefined - Class: Miss (inherit)&#39; |   778.78 ns |  8.003 ns |  7.094 ns |   777.20 ns |   769.06 ns |   794.85 ns | 0.0278 |     - |     - |     120 B |
|                      &#39;IsDefined - Class: Hit (no inherit)&#39; |   418.83 ns |  6.119 ns |  5.724 ns |   417.96 ns |   411.46 ns |   430.29 ns | 0.0315 |     - |     - |     136 B |
|                     &#39;IsDefined - Class: Miss (no inherit)&#39; |    88.55 ns |  1.626 ns |  1.521 ns |    88.11 ns |    86.72 ns |    91.64 ns |      - |     - |     - |         - |
|               &#39;IsDefined - Method Override: Hit (inherit)&#39; |   429.69 ns |  4.729 ns |  3.949 ns |   429.00 ns |   423.02 ns |   436.13 ns | 0.0324 |     - |     - |     136 B |
|              &#39;IsDefined - Method Override: Miss (inherit)&#39; |   489.07 ns |  7.689 ns |  7.193 ns |   487.86 ns |   480.65 ns |   503.91 ns | 0.0314 |     - |     - |     136 B |
|            &#39;IsDefined - Method Override: Hit (no inherit)&#39; |   434.53 ns |  8.323 ns |  8.174 ns |   431.68 ns |   424.94 ns |   451.09 ns | 0.0309 |     - |     - |     136 B |
|           &#39;IsDefined - Method Override: Miss (no inherit)&#39; |   432.40 ns |  7.438 ns |  6.958 ns |   432.16 ns |   423.54 ns |   444.30 ns | 0.0324 |     - |     - |     136 B |
|                   &#39;IsDefined - Method Base: Hit (inherit)&#39; |   433.37 ns |  6.605 ns |  6.178 ns |   431.21 ns |   424.83 ns |   443.48 ns | 0.0324 |     - |     - |     136 B |
|                  &#39;IsDefined - Method Base: Miss (inherit)&#39; |   116.63 ns |  1.480 ns |  1.312 ns |   116.45 ns |   115.12 ns |   119.23 ns |      - |     - |     - |         - |
|                &#39;IsDefined - Method Base: Hit (no inherit)&#39; |   429.53 ns |  3.863 ns |  3.225 ns |   428.61 ns |   424.48 ns |   436.94 ns | 0.0311 |     - |     - |     136 B |
|               &#39;IsDefined - Method Base: Miss (no inherit)&#39; |    83.56 ns |  1.213 ns |  1.135 ns |    83.29 ns |    82.14 ns |    85.81 ns |      - |     - |     - |         - |
