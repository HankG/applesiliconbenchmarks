``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|                Array |  512 |    151.0 ns |   1.96 ns |   1.83 ns |    150.5 ns |    148.8 ns |    154.7 ns |     - |     - |     - |         - |
|                 Span |  512 |    150.5 ns |   1.98 ns |   1.85 ns |    150.3 ns |    148.2 ns |    153.5 ns |     - |     - |     - |         - |
|                 List |  512 |    586.9 ns |   6.67 ns |   6.24 ns |    584.5 ns |    580.0 ns |    597.7 ns |     - |     - |     - |         - |
|                IList |  512 |  1,999.3 ns |  27.67 ns |  25.88 ns |  1,988.4 ns |  1,963.4 ns |  2,044.6 ns |     - |     - |     - |         - |
|           Dictionary |  512 |  4,374.1 ns | 161.61 ns | 186.11 ns |  4,368.5 ns |  4,088.5 ns |  4,652.7 ns |     - |     - |     - |         - |
|           SortedList |  512 | 26,408.0 ns | 309.75 ns | 289.74 ns | 26,363.0 ns | 25,986.3 ns | 26,898.4 ns |     - |     - |     - |         - |
|     SortedDictionary |  512 | 46,660.4 ns | 617.84 ns | 577.93 ns | 46,406.8 ns | 45,888.7 ns | 47,736.6 ns |     - |     - |     - |         - |
| ConcurrentDictionary |  512 | 15,824.7 ns | 203.94 ns | 190.77 ns | 15,750.6 ns | 15,597.4 ns | 16,167.0 ns |     - |     - |     - |         - |
