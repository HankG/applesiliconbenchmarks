``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 144.9 ns | 2.17 ns | 2.03 ns | 144.5 ns | 142.1 ns | 149.0 ns | 0.0436 |     - |     - |     184 B |
|    SerializeToUtf8Bytes | 123.3 ns | 1.71 ns | 1.52 ns | 123.3 ns | 120.6 ns | 125.4 ns | 0.0439 |     - |     - |     184 B |
|       SerializeToStream | 194.4 ns | 2.67 ns | 2.50 ns | 194.0 ns | 191.3 ns | 199.2 ns | 0.0362 |     - |     - |     152 B |
| SerializeObjectProperty | 245.9 ns | 4.11 ns | 3.64 ns | 245.8 ns | 240.8 ns | 252.7 ns | 0.0472 |     - |     - |     200 B |
