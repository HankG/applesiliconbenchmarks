``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|              Array |  512 |    62.255 μs |  0.6708 μs |  0.6275 μs |    62.109 μs |    61.562 μs |    63.580 μs |     - |     - |     - |         - |
|               Span |  512 |    51.042 μs |  0.5306 μs |  0.4963 μs |    50.826 μs |    50.386 μs |    51.924 μs |     - |     - |     - |         - |
|               List |  512 |    52.065 μs |  0.5732 μs |  0.4786 μs |    52.093 μs |    51.357 μs |    52.954 μs |     - |     - |     - |         - |
|        ICollection |  512 |    61.956 μs |  0.7582 μs |  0.6721 μs |    61.892 μs |    61.154 μs |    63.314 μs |     - |     - |     - |         - |
|         LinkedList |  512 |   370.990 μs |  4.2685 μs |  3.7840 μs |   370.136 μs |   365.920 μs |   378.811 μs |     - |     - |     - |         - |
|            HashSet |  512 |     2.785 μs |  0.0463 μs |  0.0433 μs |     2.797 μs |     2.714 μs |     2.846 μs |     - |     - |     - |         - |
|              Queue |  512 |    65.464 μs |  1.1054 μs |  1.0340 μs |    65.311 μs |    64.190 μs |    67.542 μs |     - |     - |     - |         - |
|              Stack |  512 |   118.195 μs |  1.2424 μs |  1.1014 μs |   118.278 μs |   116.454 μs |   120.006 μs |     - |     - |     - |         - |
|          SortedSet |  512 |    27.654 μs |  0.3488 μs |  0.3263 μs |    27.596 μs |    27.240 μs |    28.202 μs |     - |     - |     - |         - |
|     ImmutableArray |  512 |    53.222 μs |  0.4974 μs |  0.4409 μs |    53.164 μs |    52.641 μs |    54.199 μs |     - |     - |     - |         - |
|   ImmutableHashSet |  512 |    21.648 μs |  0.2513 μs |  0.2228 μs |    21.591 μs |    21.375 μs |    22.133 μs |     - |     - |     - |         - |
|      ImmutableList |  512 | 1,027.582 μs | 11.2146 μs | 10.4902 μs | 1,024.307 μs | 1,014.137 μs | 1,044.989 μs |     - |     - |     - |       1 B |
| ImmutableSortedSet |  512 |    30.379 μs |  0.5765 μs |  0.5393 μs |    30.192 μs |    29.695 μs |    31.697 μs |     - |     - |     - |         - |
