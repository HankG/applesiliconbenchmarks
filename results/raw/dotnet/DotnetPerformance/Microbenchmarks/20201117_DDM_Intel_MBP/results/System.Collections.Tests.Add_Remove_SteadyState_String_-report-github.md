``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method | Count |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|   ConcurrentBag |   512 | 41.158 ns | 0.6104 ns | 0.5411 ns | 41.150 ns | 40.510 ns | 42.434 ns |      - |     - |     - |         - |
| ConcurrentQueue |   512 | 16.519 ns | 0.2998 ns | 0.2658 ns | 16.425 ns | 16.264 ns | 17.095 ns |      - |     - |     - |         - |
| ConcurrentStack |   512 | 24.078 ns | 0.3776 ns | 0.3348 ns | 23.990 ns | 23.682 ns | 24.809 ns | 0.0076 |     - |     - |      32 B |
|           Queue |   512 |  7.656 ns | 0.1159 ns | 0.1084 ns |  7.611 ns |  7.534 ns |  7.827 ns |      - |     - |     - |         - |
|           Stack |   512 |  6.042 ns | 0.1060 ns | 0.0992 ns |  6.001 ns |  5.940 ns |  6.287 ns |      - |     - |     - |         - |
