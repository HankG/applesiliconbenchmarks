``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method | BytesCount |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |----------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
| **GetStringHashCode** |         **10** |     **3.624 ns** |  **0.0457 ns** |  **0.0381 ns** |     **3.620 ns** |     **3.577 ns** |     **3.700 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |        **100** |    **31.025 ns** |  **0.4550 ns** |  **0.4256 ns** |    **30.975 ns** |    **30.523 ns** |    **31.932 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |       **1000** |   **315.551 ns** |  **3.8625 ns** |  **3.6130 ns** |   **314.473 ns** |   **310.360 ns** |   **322.709 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |      **10000** | **3,138.674 ns** | **51.9219 ns** | **46.0274 ns** | **3,118.734 ns** | **3,093.468 ns** | **3,238.419 ns** |     **-** |     **-** |     **-** |         **-** |
