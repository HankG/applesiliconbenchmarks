``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method | value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|    **Parse** |     **0** | **10.707 ns** | **0.1855 ns** | **0.1645 ns** | **10.653 ns** | **10.542 ns** | **11.046 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |     0 | 10.519 ns | 0.1509 ns | 0.1411 ns | 10.461 ns | 10.321 ns | 10.798 ns |      - |     - |     - |         - |
| ToString |     0 |  2.323 ns | 0.0719 ns | 0.0637 ns |  2.308 ns |  2.231 ns |  2.434 ns |      - |     - |     - |         - |
|    **Parse** | **12345** | **14.353 ns** | **0.1864 ns** | **0.1743 ns** | **14.284 ns** | **14.188 ns** | **14.721 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse | 12345 | 14.508 ns | 0.1769 ns | 0.1477 ns | 14.519 ns | 14.289 ns | 14.814 ns |      - |     - |     - |         - |
| ToString | 12345 | 12.385 ns | 0.2606 ns | 0.2438 ns | 12.352 ns | 12.029 ns | 12.827 ns | 0.0076 |     - |     - |      32 B |
|    **Parse** | **65535** | **14.408 ns** | **0.1748 ns** | **0.1549 ns** | **14.378 ns** | **14.217 ns** | **14.739 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse | 65535 | 14.263 ns | 0.1639 ns | 0.1453 ns | 14.229 ns | 14.109 ns | 14.562 ns |      - |     - |     - |         - |
| ToString | 65535 | 13.190 ns | 0.3196 ns | 0.3139 ns | 13.171 ns | 12.671 ns | 13.618 ns | 0.0076 |     - |     - |      32 B |
