``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |                             text |  value |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |--------------------------------- |------- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
|         **HasFlag** |                                **?** |      **?** |   **0.0013 ns** | **0.0030 ns** | **0.0028 ns** |   **0.0000 ns** |   **0.0000 ns** |   **0.0096 ns** |      **-** |     **-** |     **-** |         **-** |
|         Compare |                                ? |      ? |   0.9040 ns | 0.0268 ns | 0.0251 ns |   0.8988 ns |   0.8748 ns |   0.9488 ns |      - |     - |     - |         - |
|           **Parse** |                              **Red** |      **?** | **126.7850 ns** | **1.9672 ns** | **1.9320 ns** | **126.2336 ns** | **124.3006 ns** | **130.4725 ns** | **0.0056** |     **-** |     **-** |      **24 B** |
| TryParseGeneric |                              Red |      ? |  53.6027 ns | 0.7855 ns | 0.7348 ns |  53.3905 ns |  52.7739 ns |  54.9594 ns |      - |     - |     - |         - |
|           **Parse** | **Red, Orange, Yellow, Green, Blue** |      **?** | **214.8572 ns** | **2.9956 ns** | **2.8021 ns** | **214.7118 ns** | **210.8473 ns** | **219.7919 ns** | **0.0052** |     **-** |     **-** |      **24 B** |
| TryParseGeneric | Red, Orange, Yellow, Green, Blue |      ? | 138.6424 ns | 1.5636 ns | 1.3861 ns | 138.2488 ns | 136.9346 ns | 141.1092 ns |      - |     - |     - |         - |
|    **EnumToString** |                                **?** | **Yellow** |  **22.1897 ns** | **0.4495 ns** | **0.4205 ns** |  **22.1065 ns** |  **21.6681 ns** |  **23.0083 ns** | **0.0057** |     **-** |     **-** |      **24 B** |
