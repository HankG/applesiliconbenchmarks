``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |          Mean |      Error |     StdDev |        Median |           Min |           Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |--------------:|-----------:|-----------:|--------------:|--------------:|--------------:|-------:|------:|------:|----------:|
| CreateInstanceGeneric |     0.0172 ns |  0.0160 ns |  0.0142 ns |     0.0158 ns |     0.0014 ns |     0.0586 ns |      - |     - |     - |         - |
|    CreateInstanceType |    27.7909 ns |  0.3513 ns |  0.2933 ns |    27.7435 ns |    27.3270 ns |    28.4515 ns | 0.0057 |     - |     - |      24 B |
|   CreateInstanceNames | 4,461.5585 ns | 77.5847 ns | 68.7768 ns | 4,440.2709 ns | 4,362.7103 ns | 4,603.0207 ns | 0.0696 |     - |     - |     360 B |
