``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 369.0 ns |  6.54 ns |  6.11 ns | 367.3 ns | 361.6 ns | 380.4 ns | 0.0812 |     - |     - |     344 B |
|    SerializeToUtf8Bytes | 317.5 ns |  6.58 ns |  7.58 ns | 314.6 ns | 307.7 ns | 332.6 ns | 0.0620 |     - |     - |     264 B |
|       SerializeToStream | 393.5 ns |  6.84 ns |  6.40 ns | 392.6 ns | 385.8 ns | 405.8 ns | 0.0356 |     - |     - |     152 B |
| SerializeObjectProperty | 519.6 ns | 11.36 ns | 13.08 ns | 514.6 ns | 503.4 ns | 551.7 ns | 0.1594 |     - |     - |     672 B |
