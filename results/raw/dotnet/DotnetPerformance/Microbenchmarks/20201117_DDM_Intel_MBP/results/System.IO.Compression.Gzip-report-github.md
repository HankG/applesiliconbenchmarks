``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method |   level |             file |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |-------- |----------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|   **Compress** | **Optimal** | **TestDocument.pdf** | **2,076.3 μs** | **37.29 μs** | **34.88 μs** | **2,078.5 μs** | **2,027.3 μs** | **2,156.0 μs** |      **-** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Optimal | TestDocument.pdf |   312.9 μs |  5.22 μs |  4.88 μs |   312.5 μs |   305.8 μs |   320.8 μs | 1.2255 |     - |     - |   8.26 KB |
|   **Compress** | **Optimal** |      **alice29.txt** | **6,385.9 μs** | **72.20 μs** | **67.53 μs** | **6,368.4 μs** | **6,303.3 μs** | **6,543.1 μs** |      **-** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Optimal |      alice29.txt |   493.9 μs |  5.92 μs |  5.54 μs |   492.9 μs |   486.6 μs |   502.1 μs | 1.9531 |     - |     - |   8.26 KB |
|   **Compress** | **Optimal** |              **sum** | **1,295.9 μs** | **13.30 μs** | **12.44 μs** | **1,296.3 μs** | **1,277.6 μs** | **1,316.8 μs** |      **-** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Optimal |              sum |   116.7 μs |  1.56 μs |  1.46 μs |   117.0 μs |   114.5 μs |   118.7 μs | 1.8797 |     - |     - |   8.26 KB |
|   **Compress** | **Fastest** | **TestDocument.pdf** | **2,018.2 μs** | **31.46 μs** | **29.43 μs** | **2,013.0 μs** | **1,977.3 μs** | **2,084.3 μs** |      **-** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Fastest | TestDocument.pdf |   315.4 μs |  4.94 μs |  4.62 μs |   315.0 μs |   309.4 μs |   323.8 μs | 1.2500 |     - |     - |   8.26 KB |
|   **Compress** | **Fastest** |      **alice29.txt** | **1,625.8 μs** | **12.93 μs** | **12.09 μs** | **1,626.9 μs** | **1,606.0 μs** | **1,648.9 μs** |      **-** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Fastest |      alice29.txt |   529.0 μs |  5.70 μs |  5.33 μs |   529.3 μs |   520.7 μs |   538.8 μs |      - |     - |     - |   8.26 KB |
|   **Compress** | **Fastest** |              **sum** |   **369.1 μs** |  **5.00 μs** |  **4.68 μs** |   **367.7 μs** |   **362.8 μs** |   **377.7 μs** | **1.4535** |     **-** |     **-** |   **8.26 KB** |
| Decompress | Fastest |              sum |   128.9 μs |  1.62 μs |  1.36 μs |   128.9 μs |   125.8 μs |   130.9 μs | 1.5244 |     - |     - |   8.26 KB |
