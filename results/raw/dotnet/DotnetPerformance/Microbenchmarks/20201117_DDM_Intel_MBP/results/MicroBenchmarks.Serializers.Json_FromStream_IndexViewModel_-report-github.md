``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------------- |----------:|---------:|---------:|----------:|----------:|----------:|--------:|-------:|------:|----------:|
|                        Jil |  60.84 μs | 0.735 μs | 0.687 μs |  61.02 μs |  59.58 μs |  61.65 μs |  6.5154 | 0.7239 |     - |   26.7 KB |
|                   JSON.NET |  65.38 μs | 1.029 μs | 0.963 μs |  65.08 μs |  64.13 μs |  67.39 μs |  8.2347 | 0.2573 |     - |  34.27 KB |
|                   Utf8Json |  35.05 μs | 0.593 μs | 0.526 μs |  34.99 μs |  34.46 μs |  36.20 μs |  5.2083 | 0.2815 |     - |  21.58 KB |
| DataContractJsonSerializer | 292.20 μs | 6.833 μs | 7.869 μs | 288.97 μs | 284.19 μs | 311.74 μs | 21.2054 | 1.1161 |     - |  90.16 KB |
