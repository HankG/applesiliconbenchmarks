``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|                Dictionary |  512 | 4,049.750 ns | 40.1063 ns | 37.5154 ns | 4,041.029 ns | 4,004.821 ns | 4,110.604 ns |     - |     - |     - |         - |
|                     Queue |  512 |     6.387 ns |  0.1166 ns |  0.1090 ns |     6.393 ns |     6.222 ns |     6.555 ns |     - |     - |     - |         - |
| Queue_EnqueueCountDequeue |  512 |    21.976 ns |  0.3165 ns |  0.2961 ns |    21.888 ns |    21.676 ns |    22.516 ns |     - |     - |     - |         - |
|                     Stack |  512 |   475.495 ns |  5.8098 ns |  5.4345 ns |   473.900 ns |   469.646 ns |   484.584 ns |     - |     - |     - |         - |
|                       Bag |  512 |    36.268 ns |  0.4336 ns |  0.4056 ns |    36.085 ns |    35.856 ns |    37.039 ns |     - |     - |     - |         - |
