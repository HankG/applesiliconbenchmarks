``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | IsDataIndented |      TestCase |           Mean |        Error |       StdDev |         Median |            Min |            Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |--------------- |-------------- |---------------:|-------------:|-------------:|---------------:|---------------:|---------------:|-------:|------:|------:|----------:|
| **ParseThenWrite** |          **False** |    **HelloWorld** |       **482.7 ns** |      **7.07 ns** |      **6.27 ns** |       **481.2 ns** |       **474.5 ns** |       **493.2 ns** | **0.0176** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** |      **DeepTree** |    **14,489.2 ns** |    **184.42 ns** |    **172.51 ns** |    **14,471.8 ns** |    **14,286.7 ns** |    **14,806.6 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** |     **BroadTree** |    **25,817.2 ns** |    **265.12 ns** |    **247.99 ns** |    **25,794.3 ns** |    **25,446.9 ns** |    **26,242.9 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** | **LotsOfNumbers** |     **5,817.2 ns** |     **68.18 ns** |     **63.77 ns** |     **5,802.2 ns** |     **5,735.8 ns** |     **5,926.8 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** | **LotsOfStrings** |     **4,551.9 ns** |     **59.72 ns** |     **55.87 ns** |     **4,519.0 ns** |     **4,488.2 ns** |     **4,642.8 ns** | **0.0180** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** |      **Json400B** |     **2,427.8 ns** |     **33.92 ns** |     **30.07 ns** |     **2,421.2 ns** |     **2,394.6 ns** |     **2,495.8 ns** | **0.0097** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** |       **Json4KB** |    **17,460.7 ns** |    **226.16 ns** |    **211.55 ns** |    **17,436.0 ns** |    **17,195.0 ns** |    **17,868.1 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |          **False** |     **Json400KB** | **1,829,039.3 ns** | **34,895.40 ns** | **37,337.69 ns** | **1,826,791.8 ns** | **1,708,069.6 ns** | **1,886,962.8 ns** |      **-** |     **-** |     **-** |      **84 B** |
| **ParseThenWrite** |           **True** |    **HelloWorld** |       **504.8 ns** |      **8.38 ns** |      **7.84 ns** |       **502.5 ns** |       **494.6 ns** |       **516.5 ns** | **0.0181** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** |      **DeepTree** |    **20,594.8 ns** |    **172.84 ns** |    **161.68 ns** |    **20,601.9 ns** |    **20,361.5 ns** |    **20,887.8 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** |     **BroadTree** |    **31,176.4 ns** |    **392.56 ns** |    **347.99 ns** |    **31,137.8 ns** |    **30,745.7 ns** |    **31,760.2 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** | **LotsOfNumbers** |     **6,683.3 ns** |     **86.81 ns** |     **81.20 ns** |     **6,658.9 ns** |     **6,579.7 ns** |     **6,820.9 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** | **LotsOfStrings** |     **5,113.0 ns** |     **54.29 ns** |     **48.13 ns** |     **5,102.5 ns** |     **5,054.7 ns** |     **5,203.1 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** |      **Json400B** |     **2,722.7 ns** |     **46.12 ns** |     **38.51 ns** |     **2,708.2 ns** |     **2,674.3 ns** |     **2,776.4 ns** | **0.0113** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** |       **Json4KB** |    **20,245.3 ns** |    **303.19 ns** |    **283.60 ns** |    **20,110.3 ns** |    **19,919.0 ns** |    **20,846.3 ns** |      **-** |     **-** |     **-** |      **80 B** |
| **ParseThenWrite** |           **True** |     **Json400KB** | **2,091,410.5 ns** | **39,938.84 ns** | **35,404.74 ns** | **2,072,448.8 ns** | **2,052,059.4 ns** | **2,154,314.7 ns** |      **-** |     **-** |     **-** |     **114 B** |
