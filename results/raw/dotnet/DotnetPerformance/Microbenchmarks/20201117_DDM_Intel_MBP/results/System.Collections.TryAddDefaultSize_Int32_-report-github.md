``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Count |      Mean |     Error |    StdDev |    Median |       Min |       Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|--------:|-------:|------:|----------:|
|           Dictionary |   512 |  8.098 μs | 0.1277 μs | 0.1195 μs |  8.073 μs |  7.880 μs |  8.341 μs |  8.2297 | 0.0324 |     - |  33.69 KB |
| ConcurrentDictionary |   512 | 32.941 μs | 0.5341 μs | 0.4735 μs | 32.928 μs | 32.200 μs | 33.836 μs | 15.4948 | 0.1302 |     - |  63.58 KB |
