``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method | folder | option |       Mean |     Error |    StdDev |     Median |        Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |------- |------- |-----------:|----------:|----------:|-----------:|-----------:|------------:|-------:|------:|------:|----------:|
|     **GetEnvironmentVariable** |      **?** |      **?** |   **437.3 ns** |   **8.56 ns** |   **8.01 ns** |   **438.6 ns** |   **417.1 ns** |    **449.4 ns** | **0.0069** |     **-** |     **-** |      **32 B** |
| ExpandEnvironmentVariables |      ? |      ? |   493.2 ns |   8.38 ns |   7.43 ns |   491.8 ns |   481.2 ns |    508.5 ns | 0.0380 |     - |     - |     160 B |
|    GetEnvironmentVariables |      ? |      ? | 9,909.0 ns | 146.69 ns | 137.21 ns | 9,942.1 ns | 9,662.9 ns | 10,098.4 ns | 2.0651 |     - |     - |    8712 B |
|           GetLogicalDrives |      ? |      ? | 3,368.4 ns |  91.77 ns | 105.68 ns | 3,342.0 ns | 3,202.1 ns |  3,609.0 ns | 0.1302 |     - |     - |     552 B |
|     SetEnvironmentVariable |      ? |      ? |   951.7 ns |  12.26 ns |  11.47 ns |   952.3 ns |   927.6 ns |    966.1 ns |      - |     - |     - |         - |
|              **GetFolderPath** | **System** |   **None** | **4,839.0 ns** |  **95.62 ns** |  **89.45 ns** | **4,815.4 ns** | **4,725.3 ns** |  **4,971.0 ns** |      **-** |     **-** |     **-** |      **24 B** |
