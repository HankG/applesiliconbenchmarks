``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |       Mean |    Error |   StdDev |     Median |      Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |-----------:|---------:|---------:|-----------:|---------:|-----------:|-------:|------:|------:|----------:|
|          XmlSerializer | 1,005.9 ns | 19.95 ns | 22.17 ns | 1,003.9 ns | 975.2 ns | 1,045.4 ns | 1.7038 |     - |     - |   6.97 KB |
| DataContractSerializer |   333.2 ns |  5.41 ns |  5.06 ns |   332.2 ns | 325.2 ns |   344.3 ns | 0.2733 |     - |     - |   1.12 KB |
