``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|        Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|     EnterExit | 11.536 ns | 0.1540 ns | 0.1286 ns | 11.509 ns | 11.387 ns | 11.855 ns |     - |     - |     - |         - |
|  TryEnterExit | 11.588 ns | 0.1428 ns | 0.1336 ns | 11.538 ns | 11.425 ns | 11.819 ns |     - |     - |     - |         - |
| TryEnter_Fail |  3.293 ns | 0.0508 ns | 0.0450 ns |  3.282 ns |  3.238 ns |  3.388 ns |     - |     - |     - |         - |
