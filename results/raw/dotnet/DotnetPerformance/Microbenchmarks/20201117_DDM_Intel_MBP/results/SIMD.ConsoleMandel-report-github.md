``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|  ScalarFloatSinglethreadRaw | 786.3 ms | 5.43 ms | 5.08 ms | 786.0 ms | 778.6 ms | 798.4 ms |     - |     - |     - |    5744 B |
|  ScalarFloatSinglethreadADT | 777.2 ms | 3.28 ms | 2.74 ms | 777.1 ms | 772.5 ms | 781.9 ms |     - |     - |     - |     512 B |
| ScalarDoubleSinglethreadRaw | 787.3 ms | 8.24 ms | 6.88 ms | 790.0 ms | 776.6 ms | 797.4 ms |     - |     - |     - |     512 B |
| ScalarDoubleSinglethreadADT | 782.6 ms | 7.11 ms | 6.65 ms | 780.8 ms | 772.4 ms | 796.5 ms |     - |     - |     - |     512 B |
|  VectorFloatSinglethreadRaw | 142.1 ms | 1.70 ms | 1.59 ms | 141.7 ms | 140.2 ms | 144.5 ms |     - |     - |     - |  211008 B |
|  VectorFloatSinglethreadADT | 139.9 ms | 1.92 ms | 1.80 ms | 139.6 ms | 137.8 ms | 144.3 ms |     - |     - |     - |  211008 B |
| VectorDoubleSinglethreadRaw | 254.4 ms | 3.25 ms | 3.04 ms | 253.1 ms | 249.8 ms | 259.0 ms |     - |     - |     - |  210192 B |
| VectorDoubleSinglethreadADT | 251.0 ms | 3.59 ms | 3.36 ms | 250.0 ms | 247.2 ms | 257.7 ms |     - |     - |     - |  210192 B |
