``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                        Jil |   229.5 ns |  3.55 ns |  3.32 ns |   228.6 ns |   226.1 ns |   236.7 ns |      - |     - |     - |         - |
|                   JSON.NET |   538.5 ns | 11.35 ns | 12.61 ns |   540.2 ns |   517.3 ns |   565.5 ns | 0.1070 |     - |     - |     448 B |
|                   Utf8Json |   146.6 ns |  1.67 ns |  1.48 ns |   146.6 ns |   144.5 ns |   149.6 ns |      - |     - |     - |         - |
| DataContractJsonSerializer | 1,046.4 ns | 19.70 ns | 18.42 ns | 1,041.2 ns | 1,024.3 ns | 1,085.5 ns | 0.2360 |     - |     - |    1000 B |
