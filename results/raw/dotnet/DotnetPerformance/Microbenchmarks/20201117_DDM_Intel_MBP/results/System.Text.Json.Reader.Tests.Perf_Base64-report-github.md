``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                   Method | NumberOfBytes |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------- |-------------- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    **ReadBase64EncodedByteArray_NoEscaping** |           **100** | **114.7 ns** | **1.91 ns** | **1.79 ns** | **114.4 ns** | **112.6 ns** | **118.0 ns** | **0.0304** |     **-** |     **-** |     **128 B** |
| ReadBase64EncodedByteArray_HeavyEscaping |           100 | 113.5 ns | 3.20 ns | 3.43 ns | 113.8 ns | 101.9 ns | 117.9 ns | 0.0304 |     - |     - |     128 B |
|    **ReadBase64EncodedByteArray_NoEscaping** |          **1000** | **293.3 ns** | **5.37 ns** | **5.02 ns** | **291.9 ns** | **287.0 ns** | **302.7 ns** | **0.2441** |     **-** |     **-** |    **1024 B** |
| ReadBase64EncodedByteArray_HeavyEscaping |          1000 | 315.5 ns | 7.88 ns | 9.07 ns | 313.0 ns | 303.9 ns | 333.9 ns | 0.2440 |     - |     - |    1024 B |
