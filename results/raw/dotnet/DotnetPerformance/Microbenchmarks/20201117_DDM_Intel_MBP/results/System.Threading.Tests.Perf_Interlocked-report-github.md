``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                         Method |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------- |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|                  Increment_int | 4.329 ns | 0.0563 ns | 0.0527 ns | 4.314 ns | 4.258 ns | 4.426 ns |     - |     - |     - |         - |
|                  Decrement_int | 4.327 ns | 0.0657 ns | 0.0583 ns | 4.332 ns | 4.237 ns | 4.425 ns |     - |     - |     - |         - |
|                 Increment_long | 4.265 ns | 0.0578 ns | 0.0513 ns | 4.245 ns | 4.205 ns | 4.362 ns |     - |     - |     - |         - |
|                 Decrement_long | 4.305 ns | 0.0795 ns | 0.0704 ns | 4.276 ns | 4.211 ns | 4.443 ns |     - |     - |     - |         - |
|                        Add_int | 4.345 ns | 0.0605 ns | 0.0566 ns | 4.327 ns | 4.278 ns | 4.444 ns |     - |     - |     - |         - |
|                       Add_long | 4.269 ns | 0.0773 ns | 0.0646 ns | 4.253 ns | 4.185 ns | 4.426 ns |     - |     - |     - |         - |
|                   Exchange_int | 4.407 ns | 0.1020 ns | 0.0904 ns | 4.390 ns | 4.282 ns | 4.565 ns |     - |     - |     - |         - |
|                  Exchange_long | 4.454 ns | 0.1583 ns | 0.1823 ns | 4.501 ns | 4.204 ns | 4.730 ns |     - |     - |     - |         - |
|            CompareExchange_int | 4.406 ns | 0.1067 ns | 0.0998 ns | 4.385 ns | 4.291 ns | 4.617 ns |     - |     - |     - |         - |
|           CompareExchange_long | 4.313 ns | 0.1148 ns | 0.1074 ns | 4.276 ns | 4.165 ns | 4.574 ns |     - |     - |     - |         - |
|   CompareExchange_object_Match | 4.469 ns | 0.0882 ns | 0.0825 ns | 4.447 ns | 4.351 ns | 4.619 ns |     - |     - |     - |         - |
| CompareExchange_object_NoMatch | 4.500 ns | 0.1471 ns | 0.1445 ns | 4.443 ns | 4.344 ns | 4.813 ns |     - |     - |     - |         - |
