``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method | width |                             checksum |    Mean |    Error |   StdDev |  Median |     Min |     Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |------ |------------------------------------- |--------:|---------:|---------:|--------:|--------:|--------:|------:|------:|------:|----------:|
| Mandelbrot_2 |  4000 | C7-E6-66-43-6(...)7-2F-FC-A1-D3 [47] | 1.168 s | 0.0038 s | 0.0032 s | 1.167 s | 1.165 s | 1.175 s |     - |     - |     - |   1.91 MB |
