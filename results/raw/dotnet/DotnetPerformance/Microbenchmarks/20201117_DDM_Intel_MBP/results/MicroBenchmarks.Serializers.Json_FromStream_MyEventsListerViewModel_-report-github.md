``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|---------:|--------:|------:|----------:|
|                        Jil |   446.4 μs |  7.64 μs |  6.77 μs |   444.3 μs |   439.2 μs |   461.5 μs |  24.3056 |  6.9444 |     - |  106.2 KB |
|                   JSON.NET |   694.1 μs | 10.84 μs | 10.14 μs |   691.0 μs |   681.1 μs |   718.1 μs |  38.8889 | 11.1111 |     - | 159.63 KB |
|                   Utf8Json |   332.6 μs |  5.21 μs |  4.88 μs |   332.4 μs |   325.9 μs |   341.7 μs |  26.0417 |  2.6042 |     - | 107.97 KB |
| DataContractJsonSerializer | 2,824.4 μs | 21.55 μs | 16.82 μs | 2,828.5 μs | 2,797.9 μs | 2,858.2 μs | 134.8315 | 33.7079 |     - | 568.18 KB |
