``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| Interpol_Scalar | 8.966 μs | 0.1572 μs | 0.1614 μs | 8.927 μs | 8.766 μs | 9.366 μs | 1.9394 |     - |     - |   8.02 KB |
| Interpol_Vector | 9.179 μs | 0.1265 μs | 0.1121 μs | 9.152 μs | 9.026 μs | 9.373 μs | 1.9693 |     - |     - |   8.08 KB |
|    Interpol_AVX | 2.343 μs | 0.0313 μs | 0.0293 μs | 2.348 μs | 2.289 μs | 2.397 μs | 1.9520 |     - |     - |   8.02 KB |
