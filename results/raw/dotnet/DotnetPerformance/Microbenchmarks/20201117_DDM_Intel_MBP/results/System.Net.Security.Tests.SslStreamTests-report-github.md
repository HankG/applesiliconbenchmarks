``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                          Method |         Mean |        Error |       StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |-------------:|-------------:|-------------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|       DefaultHandshakeIPv4Async | 66,784.93 μs | 1,090.680 μs |   910.767 μs | 66,714.35 μs | 65,406.50 μs | 68,405.29 μs |      - |     - |     - |   63032 B |
|       DefaultHandshakeIPv6Async | 66,379.18 μs |   969.896 μs |   859.787 μs | 66,231.57 μs | 64,894.59 μs | 67,852.99 μs |      - |     - |     - |   63032 B |
|       DefaultHandshakePipeAsync | 67,036.08 μs | 1,155.769 μs | 1,081.107 μs | 66,523.53 μs | 65,700.64 μs | 69,106.38 μs |      - |     - |     - |   63512 B |
| TLS12HandshakeECDSA256CertAsync | 14,258.20 μs |   219.225 μs |   205.063 μs | 14,222.89 μs | 13,996.51 μs | 14,752.57 μs |      - |     - |     - |   58932 B |
|  TLS12HandshakeRSA1024CertAsync |  8,871.52 μs |   154.150 μs |   144.192 μs |  8,880.35 μs |  8,667.73 μs |  9,205.19 μs |      - |     - |     - |   60113 B |
|  TLS12HandshakeRSA2048CertAsync | 17,943.36 μs |   208.316 μs |   194.859 μs | 17,942.02 μs | 17,613.49 μs | 18,336.43 μs |      - |     - |     - |   61785 B |
|  TLS12HandshakeRSA4096CertAsync | 68,072.34 μs | 1,155.613 μs |   964.989 μs | 67,776.90 μs | 67,034.37 μs | 70,340.83 μs |      - |     - |     - |   65320 B |
|                  WriteReadAsync |     26.06 μs |     0.556 μs |     0.641 μs |     26.22 μs |     25.04 μs |     26.91 μs | 0.0400 |     - |     - |     172 B |
|                  ReadWriteAsync |     30.61 μs |     0.540 μs |     0.479 μs |     30.55 μs |     29.90 μs |     31.43 μs | 0.0400 |     - |     - |     200 B |
|             ConcurrentReadWrite |     29.74 μs |     0.473 μs |     0.419 μs |     29.87 μs |     28.94 μs |     30.20 μs | 0.0800 |     - |     - |     366 B |
|  ConcurrentReadWriteLargeBuffer |     35.14 μs |     1.194 μs |     1.327 μs |     35.48 μs |     32.31 μs |     36.56 μs |      - |     - |     - |     376 B |
