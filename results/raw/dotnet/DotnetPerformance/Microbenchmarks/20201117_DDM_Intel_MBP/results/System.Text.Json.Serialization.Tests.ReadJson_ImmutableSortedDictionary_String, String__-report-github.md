``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|    DeserializeFromString | 78.20 μs | 1.209 μs | 1.131 μs | 77.90 μs | 76.57 μs | 80.63 μs | 9.2822 | 1.2376 |     - |  38.29 KB |
| DeserializeFromUtf8Bytes | 77.65 μs | 1.535 μs | 1.706 μs | 77.25 μs | 75.02 μs | 81.42 μs | 9.1019 | 1.2136 |     - |  38.29 KB |
|    DeserializeFromStream | 79.70 μs | 1.042 μs | 0.975 μs | 79.86 μs | 78.20 μs | 81.07 μs | 9.3750 | 0.9375 |     - |  38.36 KB |
