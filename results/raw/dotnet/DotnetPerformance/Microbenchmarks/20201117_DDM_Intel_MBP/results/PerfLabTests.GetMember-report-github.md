``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|    GetField |    63.18 μs |  1.202 μs |  1.066 μs |    63.10 μs |    61.85 μs |    65.23 μs |     - |     - |     - |         - |
|  GetMethod1 |   107.93 μs |  1.486 μs |  1.390 μs |   107.38 μs |   106.41 μs |   110.30 μs |     - |     - |     - |         - |
|  GetMethod2 |   215.28 μs |  2.302 μs |  2.041 μs |   214.81 μs |   212.56 μs |   219.92 μs |     - |     - |     - |       2 B |
|  GetMethod3 |   327.51 μs |  5.133 μs |  4.801 μs |   328.36 μs |   321.13 μs |   338.03 μs |     - |     - |     - |       4 B |
|  GetMethod4 |   431.12 μs |  5.386 μs |  5.038 μs |   430.54 μs |   423.26 μs |   438.58 μs |     - |     - |     - |       6 B |
|  GetMethod5 |   539.71 μs |  6.761 μs |  5.646 μs |   538.21 μs |   533.05 μs |   554.19 μs |     - |     - |     - |      10 B |
| GetMethod10 | 1,081.61 μs | 13.356 μs | 12.493 μs | 1,077.70 μs | 1,067.88 μs | 1,107.77 μs |     - |     - |     - |      38 B |
| GetMethod12 | 1,297.10 μs | 16.377 μs | 14.518 μs | 1,293.04 μs | 1,281.33 μs | 1,327.39 μs |     - |     - |     - |      61 B |
| GetMethod15 | 1,623.37 μs | 11.462 μs |  9.571 μs | 1,619.25 μs | 1,611.15 μs | 1,648.13 μs |     - |     - |     - |      84 B |
| GetMethod20 | 2,168.42 μs | 25.369 μs | 23.730 μs | 2,162.72 μs | 2,137.22 μs | 2,208.19 μs |     - |     - |     - |     158 B |
