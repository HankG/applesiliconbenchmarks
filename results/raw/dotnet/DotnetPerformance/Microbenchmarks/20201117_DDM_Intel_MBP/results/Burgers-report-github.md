``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|    Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |       Gen 0 | Gen 1 | Gen 2 |    Allocated |
|---------- |---------:|--------:|--------:|---------:|---------:|---------:|------------:|------:|------:|-------------:|
| Burgers_0 | 302.6 ms | 4.00 ms | 3.74 ms | 302.1 ms | 297.2 ms | 309.2 ms | 188000.0000 |     - |     - | 781640.94 KB |
| Burgers_1 | 186.8 ms | 1.57 ms | 1.47 ms | 186.7 ms | 184.7 ms | 189.5 ms |           - |     - |     - |    156.59 KB |
| Burgers_2 | 253.7 ms | 2.79 ms | 2.61 ms | 253.4 ms | 249.5 ms | 258.0 ms |           - |     - |     - |    156.59 KB |
| Burgers_3 | 149.7 ms | 1.53 ms | 1.43 ms | 149.4 ms | 147.7 ms | 152.3 ms |           - |     - |     - |     156.5 KB |
