``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                     Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                              CreateElement | 24.5995 ns | 0.3855 ns | 0.3417 ns | 24.5548 ns | 24.1812 ns | 25.1779 ns |      - |     - |     - |         - |
|                 CreateElementWithNamespace | 60.2295 ns | 0.8663 ns | 0.8104 ns | 59.9726 ns | 59.1595 ns | 62.0658 ns |      - |     - |     - |         - |
| CreateElementWithNamespaceImplicitOperator | 59.2842 ns | 0.7928 ns | 0.7028 ns | 59.1602 ns | 58.3095 ns | 60.4134 ns |      - |     - |     - |         - |
|                     EmptyNameSpaceToString |  0.8001 ns | 0.0440 ns | 0.0411 ns |  0.7843 ns |  0.7553 ns |  0.8592 ns |      - |     - |     - |         - |
|                  NonEmptyNameSpaceToString | 27.9913 ns | 0.6339 ns | 0.6509 ns | 27.8102 ns | 27.2483 ns | 29.1182 ns | 0.0191 |     - |     - |      80 B |
