``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method |                                input |   value |             comparisonType |          Mean |      Error |     StdDev |        Median |           Min |           Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |------------------------------------- |-------- |--------------------------- |--------------:|-----------:|-----------:|--------------:|--------------:|--------------:|------:|------:|------:|----------:|
|         **StringAsSpan** |                                    **?** |       **?** |                          **?** |     **0.2064 ns** |  **0.0169 ns** |  **0.0158 ns** |     **0.2056 ns** |     **0.1847 ns** |     **0.2419 ns** |     **-** |     **-** |     **-** |         **-** |
| GetPinnableReference |                                    ? |       ? |                          ? |     0.2387 ns |  0.0055 ns |  0.0057 ns |     0.2396 ns |     0.2310 ns |     0.2502 ns |     - |     - |     - |         - |
|                 **Trim** |                                     **** |       **?** |                          **?** |     **3.0310 ns** |  **0.0828 ns** |  **0.0850 ns** |     **2.9934 ns** |     **2.9378 ns** |     **3.2368 ns** |     **-** |     **-** |     **-** |         **-** |
|                 **Trim** |                            ** abcdefg ** |       **?** |                          **?** |     **7.1365 ns** |  **0.1304 ns** |  **0.1220 ns** |     **7.0938 ns** |     **7.0084 ns** |     **7.4073 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                           **AAAAA5AAAA** |       **5** |           **InvariantCulture** |    **32.1308 ns** |  **0.3766 ns** |  **0.3522 ns** |    **32.0484 ns** |    **31.7205 ns** |    **32.9915 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** | **AAAAAAAAAAAA(...)AAAAAAAAAAAA [1000]** |       **X** |                    **Ordinal** |    **28.2016 ns** |  **0.8077 ns** |  **0.9301 ns** |    **28.0969 ns** |    **26.8043 ns** |    **29.5506 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |  **AAAAAAAAAAAA(...)AAAAAAAAAAAA [100]** |       **x** | **InvariantCultureIgnoreCase** |   **172.3886 ns** |  **2.0951 ns** |  **1.8573 ns** |   **172.2312 ns** |   **169.8347 ns** |   **176.1945 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |  **AAAAAAAAAAAA(...)AAAAAAAAAAAA [100]** |       **x** |          **OrdinalIgnoreCase** |   **178.4223 ns** |  **2.2736 ns** |  **2.1267 ns** |   **178.0049 ns** |   **175.2710 ns** |   **182.2536 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                                **ABCDE** |       **c** | **InvariantCultureIgnoreCase** |    **28.5849 ns** |  **0.2668 ns** |  **0.2496 ns** |    **28.4570 ns** |    **28.2356 ns** |    **29.0964 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |  **Hello Worldb(...)allylong!xyz [186]** |       **w** |          **OrdinalIgnoreCase** |    **24.1319 ns** |  **0.1602 ns** |  **0.1338 ns** |    **24.1038 ns** |    **23.9881 ns** |    **24.4725 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |  **Hello Worldb(...)allylong!xyz [187]** |       **~** |                    **Ordinal** |    **18.6575 ns** |  **0.2216 ns** |  **0.2073 ns** |    **18.5634 ns** |    **18.3919 ns** |    **18.9656 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** | **Hello Worldbb(...)bbbbbbbbbbba! [47]** |       **y** |                    **Ordinal** |    **13.2442 ns** |  **0.2509 ns** |  **0.2095 ns** |    **13.1191 ns** |    **13.0054 ns** |    **13.6794 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                          **More Test&#39;s** |   **Tests** |          **OrdinalIgnoreCase** |    **30.6680 ns** |  **0.3829 ns** |  **0.3581 ns** |    **30.6862 ns** |    **30.2326 ns** |    **31.4494 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                               **StrIng** |  **string** |          **OrdinalIgnoreCase** |    **19.7069 ns** |  **0.1908 ns** |  **0.1785 ns** |    **19.6464 ns** |    **19.4785 ns** |    **20.0482 ns** |     **-** |     **-** |     **-** |         **-** |
|                 **Trim** |                              **abcdefg** |       **?** |                          **?** |     **4.1611 ns** |  **0.0764 ns** |  **0.0715 ns** |     **4.1331 ns** |     **4.0779 ns** |     **4.3059 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                         **foobardzsdzs** |   **rddzs** |           **InvariantCulture** |    **39.8812 ns** |  **0.4338 ns** |  **0.4058 ns** |    **39.8357 ns** |    **39.4094 ns** |    **40.6475 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                              **string1** | **string2** |           **InvariantCulture** |    **34.1040 ns** |  **0.5307 ns** |  **0.4705 ns** |    **33.9211 ns** |    **33.5850 ns** |    **35.0951 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |                                    **だ** |       **た** |           **InvariantCulture** | **5,209.1571 ns** | **72.3810 ns** | **64.1639 ns** | **5,204.2624 ns** | **5,145.1565 ns** | **5,342.1719 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** |  **だだだだだだだだだだだだ(...)だだだだだだだだだだだだ [100]** |       **す** |                    **Ordinal** |    **13.2706 ns** |  **0.1782 ns** |  **0.1667 ns** |    **13.2285 ns** |    **13.0588 ns** |    **13.6027 ns** |     **-** |     **-** |     **-** |         **-** |
|        **IndexOfString** | **だだだだだだだだだだだだ(...)だだだだだだだだだだだだ [1000]** |       **x** |                    **Ordinal** |    **26.9402 ns** |  **0.3269 ns** |  **0.3058 ns** |    **26.8434 ns** |    **26.6228 ns** |    **27.5985 ns** |     **-** |     **-** |     **-** |         **-** |
