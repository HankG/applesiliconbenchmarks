``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 4,787.8 ns | 82.93 ns | 77.57 ns | 4,800.4 ns | 4,641.5 ns | 4,890.0 ns | 1.1860 |     - |     - |    5008 B |
|    protobuf-net |   702.6 ns | 10.91 ns |  9.67 ns |   703.7 ns |   685.4 ns |   715.6 ns | 0.1026 |     - |     - |     440 B |
|     MessagePack |   136.8 ns |  2.78 ns |  2.98 ns |   135.4 ns |   133.9 ns |   143.9 ns | 0.0401 |     - |     - |     168 B |
