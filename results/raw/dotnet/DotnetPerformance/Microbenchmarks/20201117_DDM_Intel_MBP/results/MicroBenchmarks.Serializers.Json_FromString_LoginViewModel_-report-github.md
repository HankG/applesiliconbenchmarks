``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|      Jil |   228.8 ns |  2.29 ns |  2.03 ns |   228.9 ns |   225.6 ns |   233.0 ns | 0.0625 |     - |     - |     264 B |
| JSON.NET | 1,093.7 ns | 21.24 ns | 24.46 ns | 1,090.2 ns | 1,053.4 ns | 1,140.0 ns | 0.6602 |     - |     - |    2776 B |
| Utf8Json |   382.9 ns |  7.71 ns |  7.57 ns |   380.0 ns |   373.4 ns |   394.7 ns | 0.0654 |     - |     - |     280 B |
