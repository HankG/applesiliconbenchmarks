``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    DeserializeFromString | 53.85 μs | 0.783 μs | 0.732 μs | 53.70 μs | 52.76 μs | 54.99 μs | 7.2623 |     - |     - |  30.29 KB |
| DeserializeFromUtf8Bytes | 52.12 μs | 0.706 μs | 0.626 μs | 52.17 μs | 51.05 μs | 53.48 μs | 7.3161 |     - |     - |  30.29 KB |
|    DeserializeFromStream | 53.97 μs | 0.850 μs | 0.753 μs | 54.02 μs | 52.88 μs | 55.39 μs | 7.3024 |     - |     - |  30.36 KB |
