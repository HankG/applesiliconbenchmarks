``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 707.2 ns | 13.19 ns | 12.95 ns | 703.7 ns | 691.3 ns | 731.5 ns | 0.1156 |     - |     - |     488 B |
|    SerializeToUtf8Bytes | 670.0 ns |  9.85 ns |  9.21 ns | 668.0 ns | 657.6 ns | 687.7 ns | 0.0897 |     - |     - |     376 B |
|       SerializeToStream | 774.9 ns | 11.78 ns | 11.02 ns | 771.2 ns | 760.9 ns | 793.9 ns | 0.0550 |     - |     - |     232 B |
| SerializeObjectProperty | 902.3 ns |  9.35 ns |  7.80 ns | 905.0 ns | 889.1 ns | 912.5 ns | 0.1930 |     - |     - |     816 B |
