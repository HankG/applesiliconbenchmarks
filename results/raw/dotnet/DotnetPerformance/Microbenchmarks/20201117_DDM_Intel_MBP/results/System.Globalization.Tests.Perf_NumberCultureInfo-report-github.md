``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method | culturestring |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-------------- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| **ToString** |              **** | **129.0 ns** | **2.51 ns** | **2.57 ns** | **128.2 ns** | **125.6 ns** | **134.2 ns** | **0.0115** |     **-** |     **-** |      **48 B** |
| **ToString** |            **da** | **127.7 ns** | **1.30 ns** | **1.22 ns** | **127.3 ns** | **125.6 ns** | **130.1 ns** | **0.0110** |     **-** |     **-** |      **48 B** |
| **ToString** |            **fr** | **128.0 ns** | **1.94 ns** | **1.82 ns** | **127.7 ns** | **125.5 ns** | **130.8 ns** | **0.0110** |     **-** |     **-** |      **48 B** |
| **ToString** |            **ja** | **127.8 ns** | **1.71 ns** | **1.60 ns** | **127.5 ns** | **125.8 ns** | **131.0 ns** | **0.0115** |     **-** |     **-** |      **48 B** |
