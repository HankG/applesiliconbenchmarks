``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |      Gen 0 |     Gen 1 | Gen 2 | Allocated |
|------------ |---------:|--------:|--------:|---------:|---------:|---------:|-----------:|----------:|------:|----------:|
| CompileTest | 269.4 ms | 5.32 ms | 4.98 ms | 268.6 ms | 262.1 ms | 279.8 ms | 50000.0000 | 1000.0000 |     - |  200.5 MB |
| DatflowTest | 308.7 ms | 5.63 ms | 5.26 ms | 308.9 ms | 301.5 ms | 316.3 ms | 26000.0000 |         - |     - | 105.08 MB |
