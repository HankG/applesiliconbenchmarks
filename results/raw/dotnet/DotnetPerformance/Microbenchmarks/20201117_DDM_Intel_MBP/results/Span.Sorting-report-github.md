``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-XOSJGT : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|          Method | Size |       Mean |     Error |    StdDev |     Median |        Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|----------:|------:|------:|------:|----------:|
|   QuickSortSpan |  512 |   9.450 μs | 0.3331 μs | 0.3836 μs |   9.382 μs |   8.896 μs |  10.34 μs |     - |     - |     - |         - |
|  BubbleSortSpan |  512 | 314.669 μs | 3.7672 μs | 3.5238 μs | 313.372 μs | 311.025 μs | 322.38 μs |     - |     - |     - |         - |
|  QuickSortArray |  512 |   9.680 μs | 0.3386 μs | 0.3900 μs |   9.630 μs |   9.036 μs |  10.50 μs |     - |     - |     - |         - |
| BubbleSortArray |  512 | 198.315 μs | 2.2431 μs | 2.0982 μs | 197.648 μs | 195.692 μs | 202.78 μs |     - |     - |     - |         - |
