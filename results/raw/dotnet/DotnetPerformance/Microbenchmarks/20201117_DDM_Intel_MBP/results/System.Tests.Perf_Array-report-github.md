``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-VUUYSD : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
WarmupCount=1  

```
|          Method |        Job | UnrollFactor |             Mean |          Error |         StdDev |           Median |              Min |              Max |    Gen 0 |  Gen 1 |  Gen 2 | Allocated |
|---------------- |----------- |------------- |-----------------:|---------------:|---------------:|-----------------:|-----------------:|-----------------:|---------:|-------:|-------:|----------:|
|     ArrayResize | Job-VUUYSD |            1 |       126.603 ns |      4.2367 ns |      4.7090 ns |       125.661 ns |       119.999 ns |       135.730 ns |   0.0115 | 0.0040 | 0.0005 |      72 B |
|   ArrayCreate1D | Job-NXBZBK |           16 |       620.364 ns |     17.6094 ns |     19.5728 ns |       620.939 ns |       574.507 ns |       644.946 ns |   3.9063 | 0.0022 |      - |   16408 B |
|   ArrayCreate2D | Job-NXBZBK |           16 |       643.537 ns |     22.8853 ns |     26.3547 ns |       646.939 ns |       591.993 ns |       691.873 ns |   3.9058 | 0.0026 |      - |   16424 B |
|   ArrayCreate3D | Job-NXBZBK |           16 |       594.779 ns |     11.8697 ns |     13.1931 ns |       594.180 ns |       569.516 ns |       622.256 ns |   3.9202 | 0.0024 |      - |   16432 B |
|   ArrayAssign1D | Job-NXBZBK |           16 | 1,335,314.292 ns | 20,459.4453 ns | 18,136.7654 ns | 1,331,386.904 ns | 1,311,689.099 ns | 1,376,096.073 ns | 234.3750 |      - |      - |  983042 B |
|   ArrayAssign2D | Job-NXBZBK |           16 | 1,479,613.153 ns | 24,299.9461 ns | 22,730.1846 ns | 1,476,120.182 ns | 1,449,118.119 ns | 1,523,087.972 ns | 232.9545 |      - |      - |  983042 B |
|   ArrayAssign3D | Job-NXBZBK |           16 | 1,526,454.664 ns | 20,187.0644 ns | 15,760.7321 ns | 1,525,351.429 ns | 1,492,815.668 ns | 1,547,399.435 ns | 232.9545 |      - |      - |  983042 B |
| ArrayRetrieve1D | Job-NXBZBK |           16 | 2,561,592.960 ns | 31,754.6574 ns | 29,703.3263 ns | 2,555,969.795 ns | 2,516,926.768 ns | 2,608,437.080 ns | 232.1429 |      - |      - |  983043 B |
| ArrayRetrieve2D | Job-NXBZBK |           16 | 2,602,335.566 ns | 50,086.5641 ns | 46,851.0031 ns | 2,588,479.286 ns | 2,546,181.255 ns | 2,709,365.432 ns | 229.1667 |      - |      - |  983043 B |
| ArrayRetrieve3D | Job-NXBZBK |           16 | 2,919,510.628 ns | 35,887.4359 ns | 33,569.1298 ns | 2,908,508.656 ns | 2,853,105.531 ns | 2,974,095.198 ns | 250.0000 |      - |      - | 1081347 B |
|     ArrayCopy2D | Job-NXBZBK |           16 |       259.194 ns |      3.5232 ns |      3.2956 ns |       258.708 ns |       254.962 ns |       265.287 ns |        - |      - |      - |         - |
|     ArrayCopy3D | Job-NXBZBK |           16 |       180.251 ns |      3.7078 ns |      4.1212 ns |       181.455 ns |       171.716 ns |       184.953 ns |        - |      - |      - |         - |
|         Reverse | Job-NXBZBK |           16 |        61.711 ns |      1.1823 ns |      1.1059 ns |        61.588 ns |        60.225 ns |        64.015 ns |        - |      - |      - |         - |
|  ClearUnaligned | Job-NXBZBK |           16 |        73.298 ns |      0.9483 ns |      0.8406 ns |        72.943 ns |        72.246 ns |        74.487 ns |        - |      - |      - |         - |
|     IndexOfChar | Job-NXBZBK |           16 |         9.800 ns |      0.1136 ns |      0.1007 ns |         9.766 ns |         9.687 ns |        10.008 ns |        - |      - |      - |         - |
|    IndexOfShort | Job-NXBZBK |           16 |         9.188 ns |      0.1595 ns |      0.1414 ns |         9.178 ns |         8.979 ns |         9.431 ns |        - |      - |      - |         - |
