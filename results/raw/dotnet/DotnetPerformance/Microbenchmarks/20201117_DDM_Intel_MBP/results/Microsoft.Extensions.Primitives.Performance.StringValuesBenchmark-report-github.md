``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                 Ctor_String |  0.0042 ns | 0.0097 ns | 0.0081 ns |  0.0000 ns |  0.0000 ns |  0.0213 ns |     - |     - |     - |         - |
|                  Ctor_Array |  0.0230 ns | 0.0218 ns | 0.0193 ns |  0.0170 ns |  0.0002 ns |  0.0674 ns |     - |     - |     - |         - |
| Indexer_FirstElement_String |  1.1496 ns | 0.0447 ns | 0.0396 ns |  1.1324 ns |  1.0975 ns |  1.2162 ns |     - |     - |     - |         - |
|  Indexer_FirstElement_Array |  0.5571 ns | 0.0471 ns | 0.0440 ns |  0.5431 ns |  0.5080 ns |  0.6413 ns |     - |     - |     - |         - |
|                Count_String |  0.5944 ns | 0.0266 ns | 0.0249 ns |  0.5851 ns |  0.5645 ns |  0.6397 ns |     - |     - |     - |         - |
|                 Count_Array |  0.8676 ns | 0.0339 ns | 0.0283 ns |  0.8599 ns |  0.8317 ns |  0.9280 ns |     - |     - |     - |         - |
|              ForEach_String |  4.2945 ns | 0.0686 ns | 0.0641 ns |  4.2731 ns |  4.2186 ns |  4.4222 ns |     - |     - |     - |         - |
|               ForEach_Array | 13.4046 ns | 0.1587 ns | 0.1407 ns | 13.3589 ns | 13.2724 ns | 13.6971 ns |     - |     - |     - |         - |
