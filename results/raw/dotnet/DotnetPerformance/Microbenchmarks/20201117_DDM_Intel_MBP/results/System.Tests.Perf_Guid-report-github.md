``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|           NewGuid | 219.817 ns | 2.3756 ns | 2.2221 ns | 219.338 ns | 216.908 ns | 223.823 ns |      - |     - |     - |         - |
|          ctor_str |  81.837 ns | 0.7859 ns | 0.6563 ns |  81.728 ns |  80.413 ns |  83.008 ns |      - |     - |     - |         - |
|        ctor_bytes |   2.939 ns | 0.0602 ns | 0.0534 ns |   2.929 ns |   2.869 ns |   3.062 ns |      - |     - |     - |         - |
|        EqualsSame |   2.220 ns | 0.0476 ns | 0.0445 ns |   2.208 ns |   2.169 ns |   2.284 ns |      - |     - |     - |         - |
|    EqualsOperator |   2.594 ns | 0.0492 ns | 0.0460 ns |   2.587 ns |   2.544 ns |   2.665 ns |      - |     - |     - |         - |
| NotEqualsOperator |   2.619 ns | 0.0570 ns | 0.0505 ns |   2.601 ns |   2.568 ns |   2.723 ns |      - |     - |     - |         - |
|             Parse |  82.107 ns | 0.9373 ns | 0.8768 ns |  81.888 ns |  81.047 ns |  83.830 ns |      - |     - |     - |         - |
|       ParseExactD |  76.610 ns | 0.8067 ns | 0.7546 ns |  76.240 ns |  75.652 ns |  77.903 ns |      - |     - |     - |         - |
|      GuidToString |  37.057 ns | 0.7782 ns | 0.8326 ns |  36.751 ns |  35.955 ns |  38.708 ns | 0.0229 |     - |     - |      96 B |
|     TryWriteBytes |   2.512 ns | 0.0508 ns | 0.0450 ns |   2.498 ns |   2.467 ns |   2.617 ns |      - |     - |     - |         - |
