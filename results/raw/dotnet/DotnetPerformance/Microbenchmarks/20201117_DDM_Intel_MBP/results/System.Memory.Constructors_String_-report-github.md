``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                   Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                            SpanFromArray |  7.6759 ns | 0.1116 ns | 0.0990 ns |  7.6472 ns |  7.5454 ns |  7.8629 ns |     - |     - |     - |         - |
|                    ReadOnlySpanFromArray |  4.6873 ns | 0.1247 ns | 0.1436 ns |  4.6491 ns |  4.5205 ns |  4.9780 ns |     - |     - |     - |         - |
|                 SpanFromArrayStartLength |  8.2588 ns | 0.1448 ns | 0.1209 ns |  8.2572 ns |  8.1124 ns |  8.5641 ns |     - |     - |     - |         - |
|         ReadOnlySpanFromArrayStartLength |  5.0569 ns | 0.0802 ns | 0.0750 ns |  5.0291 ns |  4.9845 ns |  5.2318 ns |     - |     - |     - |         - |
|                           SpanFromMemory |  2.0978 ns | 0.0540 ns | 0.0478 ns |  2.0923 ns |  2.0379 ns |  2.1807 ns |     - |     - |     - |         - |
|                   ReadOnlySpanFromMemory |  2.0927 ns | 0.0379 ns | 0.0354 ns |  2.0811 ns |  2.0438 ns |  2.1461 ns |     - |     - |     - |         - |
|                SpanImplicitCastFromArray |  5.6925 ns | 0.0902 ns | 0.0844 ns |  5.6880 ns |  5.5588 ns |  5.8146 ns |     - |     - |     - |         - |
|        ReadOnlySpanImplicitCastFromArray |  1.6032 ns | 0.0322 ns | 0.0301 ns |  1.5924 ns |  1.5619 ns |  1.6602 ns |     - |     - |     - |         - |
|         SpanImplicitCastFromArraySegment |  8.7913 ns | 0.1056 ns | 0.0882 ns |  8.7599 ns |  8.6880 ns |  8.9569 ns |     - |     - |     - |         - |
| ReadOnlySpanImplicitCastFromArraySegment |  5.5019 ns | 0.0792 ns | 0.0741 ns |  5.4999 ns |  5.4020 ns |  5.6321 ns |     - |     - |     - |         - |
|         ReadOnlySpanImplicitCastFromSpan |  2.6136 ns | 0.0510 ns | 0.0452 ns |  2.5988 ns |  2.5607 ns |  2.7105 ns |     - |     - |     - |         - |
|                          MemoryFromArray |  8.8833 ns | 0.1262 ns | 0.1180 ns |  8.8787 ns |  8.7215 ns |  9.1484 ns |     - |     - |     - |         - |
|                  ReadOnlyMemoryFromArray |  3.6012 ns | 0.0959 ns | 0.0850 ns |  3.5745 ns |  3.4827 ns |  3.7450 ns |     - |     - |     - |         - |
|               MemoryFromArrayStartLength |  9.6821 ns | 0.1900 ns | 0.1685 ns |  9.7164 ns |  9.3882 ns |  9.9910 ns |     - |     - |     - |         - |
|       ReadOnlyMemoryFromArrayStartLength |  3.7586 ns | 0.0657 ns | 0.0615 ns |  3.7448 ns |  3.6912 ns |  3.8622 ns |     - |     - |     - |         - |
|                              ArrayAsSpan | 10.2472 ns | 0.2233 ns | 0.2089 ns | 10.2651 ns |  9.9773 ns | 10.6971 ns |     - |     - |     - |         - |
|                   ArrayAsSpanStartLength |  9.7598 ns | 0.1456 ns | 0.1291 ns |  9.7324 ns |  9.6165 ns | 10.0149 ns |     - |     - |     - |         - |
|                            ArrayAsMemory | 10.7885 ns | 0.1413 ns | 0.1321 ns | 10.7656 ns | 10.5846 ns | 11.0458 ns |     - |     - |     - |         - |
|                 ArrayAsMemoryStartLength | 12.3333 ns | 0.2641 ns | 0.2826 ns | 12.3395 ns | 11.3161 ns | 12.6636 ns |     - |     - |     - |         - |
|                  MemoryMarshalCreateSpan |  0.8665 ns | 0.0284 ns | 0.0251 ns |  0.8578 ns |  0.8400 ns |  0.9232 ns |     - |     - |     - |         - |
|          MemoryMarshalCreateReadOnlySpan |  1.1475 ns | 0.0402 ns | 0.0357 ns |  1.1426 ns |  1.0996 ns |  1.2247 ns |     - |     - |     - |         - |
