``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QZKIVK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MaxWarmupIterationCount=10  
MinIterationCount=15  MinWarmupIterationCount=6  UnrollFactor=1  
WarmupCount=-1  

```
|          Method |    Size |     Mean |   Error |  StdDev |   Median |      Min |      Max |      Gen 0 | Gen 1 | Gen 2 |    Allocated |
|---------------- |-------- |---------:|--------:|--------:|---------:|---------:|---------:|-----------:|------:|------:|-------------:|
|   ConcurrentBag | 2000000 | 298.7 ms | 7.52 ms | 8.66 ms | 299.0 ms | 283.2 ms | 314.9 ms |          - |     - |     - |      6.89 KB |
| ConcurrentStack | 2000000 | 124.3 ms | 2.25 ms | 2.21 ms | 124.0 ms | 121.6 ms | 129.5 ms | 30000.0000 |     - |     - | 125797.76 KB |
| ConcurrentQueue | 2000000 | 136.9 ms | 3.55 ms | 3.94 ms | 136.8 ms | 128.1 ms | 143.2 ms |          - |     - |     - |      8.45 KB |
