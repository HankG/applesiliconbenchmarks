``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  3.603 μs | 0.0407 μs | 0.0361 μs |  3.591 μs |  3.538 μs |  3.667 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  4.894 μs | 0.0655 μs | 0.0580 μs |  4.867 μs |  4.809 μs |  5.008 μs |     - |     - |     - |         - |
|                SortedList |  512 | 27.329 μs | 0.2517 μs | 0.2231 μs | 27.299 μs | 26.975 μs | 27.722 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 44.436 μs | 0.4747 μs | 0.4208 μs | 44.422 μs | 43.825 μs | 45.285 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  3.206 μs | 0.0361 μs | 0.0320 μs |  3.201 μs |  3.171 μs |  3.274 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 | 20.728 μs | 0.3012 μs | 0.2817 μs | 20.587 μs | 20.442 μs | 21.312 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 25.885 μs | 0.3886 μs | 0.3445 μs | 25.857 μs | 25.343 μs | 26.435 μs |     - |     - |     - |         - |
