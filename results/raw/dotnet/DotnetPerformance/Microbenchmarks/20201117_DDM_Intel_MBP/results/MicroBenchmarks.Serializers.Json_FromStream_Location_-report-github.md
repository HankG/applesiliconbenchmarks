``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |     Error |    StdDev |     Median |        Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|----------:|----------:|-----------:|-----------:|------------:|-------:|------:|------:|----------:|
|                        Jil | 1,151.3 ns |  22.75 ns |  21.28 ns | 1,145.9 ns | 1,125.3 ns |  1,198.2 ns | 0.9138 |     - |     - |    3824 B |
|                   JSON.NET | 2,648.6 ns |  49.86 ns |  51.21 ns | 2,630.6 ns | 2,582.7 ns |  2,770.2 ns | 1.4515 |     - |     - |    6080 B |
|                   Utf8Json |   865.4 ns |  15.39 ns |  14.40 ns |   862.4 ns |   847.5 ns |    887.1 ns | 0.1070 |     - |     - |     448 B |
| DataContractJsonSerializer | 9,949.9 ns | 192.56 ns | 221.75 ns | 9,863.4 ns | 9,687.6 ns | 10,447.0 ns | 3.3546 |     - |     - |   14104 B |
