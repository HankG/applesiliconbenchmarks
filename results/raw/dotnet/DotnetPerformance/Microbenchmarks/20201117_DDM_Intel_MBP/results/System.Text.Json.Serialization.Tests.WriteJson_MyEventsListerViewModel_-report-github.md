``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|------------------------ |---------:|--------:|--------:|---------:|---------:|---------:|--------:|--------:|--------:|----------:|
|       SerializeToString | 509.7 μs | 9.86 μs | 9.68 μs | 507.6 μs | 498.6 μs | 526.9 μs | 46.8750 | 46.8750 | 46.8750 | 283.67 KB |
|    SerializeToUtf8Bytes | 466.5 μs | 7.97 μs | 7.46 μs | 465.1 μs | 458.0 μs | 480.3 μs | 49.6324 |  9.1912 |       - | 209.56 KB |
|       SerializeToStream | 454.6 μs | 5.03 μs | 4.46 μs | 454.0 μs | 448.2 μs | 465.3 μs | 32.1429 |       - |       - | 135.98 KB |
| SerializeObjectProperty | 502.2 μs | 5.47 μs | 5.12 μs | 502.3 μs | 494.2 μs | 511.6 μs | 46.3710 | 46.3710 | 46.3710 |  284.2 KB |
