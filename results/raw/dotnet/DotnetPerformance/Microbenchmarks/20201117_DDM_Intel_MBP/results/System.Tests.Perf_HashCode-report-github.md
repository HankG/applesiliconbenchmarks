``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|    Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|       Add |   2.909 μs | 0.0440 μs | 0.0412 μs |   2.892 μs |   2.861 μs |   2.983 μs |     - |     - |     - |         - |
| Combine_1 |  47.830 μs | 0.6125 μs | 0.5729 μs |  47.578 μs |  47.097 μs |  48.780 μs |     - |     - |     - |         - |
| Combine_2 |  59.472 μs | 0.9058 μs | 0.8473 μs |  59.271 μs |  58.347 μs |  61.239 μs |     - |     - |     - |         - |
| Combine_3 |  72.836 μs | 0.9278 μs | 0.8679 μs |  72.571 μs |  71.806 μs |  74.189 μs |     - |     - |     - |         - |
| Combine_4 |  66.772 μs | 0.4153 μs | 0.3468 μs |  66.801 μs |  66.247 μs |  67.535 μs |     - |     - |     - |         - |
| Combine_5 |  77.645 μs | 0.7548 μs | 0.6303 μs |  77.570 μs |  76.736 μs |  79.155 μs |     - |     - |     - |         - |
| Combine_6 |  90.719 μs | 1.8005 μs | 2.0734 μs |  90.736 μs |  87.628 μs |  95.104 μs |     - |     - |     - |         - |
| Combine_7 | 108.532 μs | 1.7337 μs | 1.6217 μs | 108.679 μs | 104.268 μs | 111.047 μs |     - |     - |     - |         - |
| Combine_8 |  93.264 μs | 1.2237 μs | 1.0848 μs |  92.829 μs |  91.952 μs |  95.392 μs |     - |     - |     - |         - |
