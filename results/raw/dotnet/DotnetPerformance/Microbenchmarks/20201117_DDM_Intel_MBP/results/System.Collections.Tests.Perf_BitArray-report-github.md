``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method | Size |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |----- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|      **BitArrayLengthCtor** |    **4** |    **10.573 ns** |  **0.2152 ns** |  **0.2013 ns** |    **10.534 ns** |    **10.287 ns** |    **10.980 ns** | **0.0153** |     **-** |     **-** |      **64 B** |
| BitArrayLengthValueCtor |    4 |    13.353 ns |  0.2296 ns |  0.2035 ns |    13.333 ns |    13.057 ns |    13.740 ns | 0.0153 |     - |     - |      64 B |
|    BitArrayBitArrayCtor |    4 |    15.901 ns |  0.2785 ns |  0.2605 ns |    15.930 ns |    15.490 ns |    16.278 ns | 0.0152 |     - |     - |      64 B |
|   BitArrayBoolArrayCtor |    4 |    15.558 ns |  0.3095 ns |  0.2417 ns |    15.520 ns |    15.101 ns |    15.959 ns | 0.0153 |     - |     - |      64 B |
|   BitArrayByteArrayCtor |    4 |    11.223 ns |  0.2784 ns |  0.2604 ns |    11.109 ns |    10.921 ns |    11.757 ns | 0.0153 |     - |     - |      64 B |
|    BitArrayIntArrayCtor |    4 |    16.175 ns |  0.3361 ns |  0.2807 ns |    16.094 ns |    15.740 ns |    16.715 ns | 0.0172 |     - |     - |      72 B |
|          BitArraySetAll |    4 |     4.342 ns |  0.0734 ns |  0.0687 ns |     4.321 ns |     4.251 ns |     4.483 ns |      - |     - |     - |         - |
|             BitArrayNot |    4 |     2.086 ns |  0.0423 ns |  0.0354 ns |     2.081 ns |     2.035 ns |     2.147 ns |      - |     - |     - |         - |
|             BitArrayGet |    4 |    26.376 ns |  0.3099 ns |  0.2747 ns |    26.290 ns |    26.081 ns |    26.946 ns |      - |     - |     - |         - |
|      BitArrayRightShift |    4 |     4.538 ns |  0.0982 ns |  0.0871 ns |     4.511 ns |     4.440 ns |     4.708 ns |      - |     - |     - |         - |
|       BitArrayLeftShift |    4 |     3.826 ns |  0.0441 ns |  0.0412 ns |     3.824 ns |     3.754 ns |     3.897 ns |      - |     - |     - |         - |
|             BitArrayAnd |    4 |     2.634 ns |  0.0573 ns |  0.0536 ns |     2.618 ns |     2.556 ns |     2.757 ns |      - |     - |     - |         - |
|              BitArrayOr |    4 |     2.541 ns |  0.1100 ns |  0.0975 ns |     2.507 ns |     2.432 ns |     2.753 ns |      - |     - |     - |         - |
|             BitArrayXor |    4 |     2.889 ns |  0.0657 ns |  0.0615 ns |     2.880 ns |     2.803 ns |     3.015 ns |      - |     - |     - |         - |
|             BitArraySet |    4 |     6.068 ns |  0.0573 ns |  0.0479 ns |     6.059 ns |     6.003 ns |     6.163 ns |      - |     - |     - |         - |
|   BitArraySetLengthGrow |    4 |    30.869 ns |  0.3009 ns |  0.2668 ns |    30.932 ns |    30.421 ns |    31.201 ns | 0.0229 |     - |     - |      96 B |
| BitArraySetLengthShrink |    4 |    13.759 ns |  0.3784 ns |  0.4048 ns |    13.609 ns |    13.300 ns |    14.735 ns | 0.0152 |     - |     - |      64 B |
|  BitArrayCopyToIntArray |    4 |    10.223 ns |  0.1246 ns |  0.1165 ns |    10.189 ns |    10.086 ns |    10.455 ns |      - |     - |     - |         - |
| BitArrayCopyToByteArray |    4 |    11.381 ns |  0.1393 ns |  0.1303 ns |    11.329 ns |    11.214 ns |    11.660 ns |      - |     - |     - |         - |
| BitArrayCopyToBoolArray |    4 |    15.358 ns |  0.1750 ns |  0.1637 ns |    15.274 ns |    15.171 ns |    15.632 ns |      - |     - |     - |         - |
|      **BitArrayLengthCtor** |  **512** |    **12.847 ns** |  **0.3119 ns** |  **0.3203 ns** |    **12.727 ns** |    **12.407 ns** |    **13.467 ns** | **0.0287** |     **-** |     **-** |     **120 B** |
| BitArrayLengthValueCtor |  512 |    16.667 ns |  0.3957 ns |  0.3886 ns |    16.528 ns |    16.249 ns |    17.457 ns | 0.0287 |     - |     - |     120 B |
|    BitArrayBitArrayCtor |  512 |    18.789 ns |  0.8902 ns |  0.9894 ns |    18.914 ns |    17.362 ns |    20.690 ns | 0.0287 |     - |     - |     120 B |
|   BitArrayBoolArrayCtor |  512 |    26.638 ns |  0.4402 ns |  0.3902 ns |    26.563 ns |    26.150 ns |    27.458 ns | 0.0286 |     - |     - |     120 B |
|   BitArrayByteArrayCtor |  512 |   156.828 ns |  4.4979 ns |  5.1798 ns |   154.899 ns |   151.036 ns |   167.745 ns | 0.1356 |     - |     - |     568 B |
|    BitArrayIntArrayCtor |  512 |   125.698 ns |  1.8674 ns |  1.6554 ns |   125.340 ns |   123.445 ns |   129.023 ns | 0.5029 |     - |     - |    2104 B |
|          BitArraySetAll |  512 |    30.797 ns |  0.4408 ns |  0.4124 ns |    30.679 ns |    30.301 ns |    31.708 ns |      - |     - |     - |         - |
|             BitArrayNot |  512 |     8.666 ns |  0.1768 ns |  0.1567 ns |     8.660 ns |     8.460 ns |     8.921 ns |      - |     - |     - |         - |
|             BitArrayGet |  512 | 3,316.677 ns | 60.9902 ns | 57.0503 ns | 3,301.983 ns | 3,245.556 ns | 3,422.165 ns |      - |     - |     - |         - |
|      BitArrayRightShift |  512 |   212.891 ns |  3.0247 ns |  2.8293 ns |   211.513 ns |   209.920 ns |   219.356 ns |      - |     - |     - |         - |
|       BitArrayLeftShift |  512 |   204.283 ns |  2.1416 ns |  2.0033 ns |   203.275 ns |   201.693 ns |   208.073 ns |      - |     - |     - |         - |
|             BitArrayAnd |  512 |    10.860 ns |  0.1444 ns |  0.1280 ns |    10.848 ns |    10.692 ns |    11.107 ns |      - |     - |     - |         - |
|              BitArrayOr |  512 |    11.290 ns |  0.1670 ns |  0.1562 ns |    11.301 ns |    11.089 ns |    11.640 ns |      - |     - |     - |         - |
|             BitArrayXor |  512 |    13.710 ns |  0.1781 ns |  0.1666 ns |    13.693 ns |    13.463 ns |    14.066 ns |      - |     - |     - |         - |
|             BitArraySet |  512 |   819.704 ns |  8.8703 ns |  8.2972 ns |   818.717 ns |   808.140 ns |   833.293 ns |      - |     - |     - |         - |
|   BitArraySetLengthGrow |  512 |   249.883 ns |  4.3202 ns |  4.0411 ns |   248.282 ns |   244.343 ns |   258.326 ns | 0.3861 |     - |     - |    1616 B |
| BitArraySetLengthShrink |  512 |   158.425 ns |  3.1885 ns |  3.2743 ns |   157.128 ns |   154.560 ns |   164.103 ns | 0.1353 |     - |     - |     568 B |
|  BitArrayCopyToIntArray |  512 |    17.335 ns |  0.3259 ns |  0.2889 ns |    17.227 ns |    17.021 ns |    17.910 ns |      - |     - |     - |         - |
| BitArrayCopyToByteArray |  512 |   162.432 ns |  1.8160 ns |  1.6987 ns |   161.829 ns |   160.399 ns |   166.040 ns |      - |     - |     - |         - |
| BitArrayCopyToBoolArray |  512 |   166.681 ns |  2.1128 ns |  1.9763 ns |   166.491 ns |   164.098 ns |   169.711 ns |      - |     - |     - |         - |
