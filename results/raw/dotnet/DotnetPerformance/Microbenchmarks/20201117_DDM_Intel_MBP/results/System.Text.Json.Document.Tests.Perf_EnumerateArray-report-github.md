``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |       TestCase |         Mean |      Error |    StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |--------------- |-------------:|-----------:|----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|                 **Parse** |      **Json400KB** | **1,073.914 μs** | **10.3530 μs** | **9.6842 μs** | **1,071.066 μs** | **1,057.045 μs** | **1,089.598 μs** |     **-** |     **-** |     **-** |      **81 B** |
|        EnumerateArray |      Json400KB |     2.251 μs |  0.0410 μs | 0.0364 μs |     2.258 μs |     2.170 μs |     2.305 μs |     - |     - |     - |         - |
| EnumerateUsingIndexer |      Json400KB |   122.551 μs |  1.3395 μs | 1.2529 μs |   122.425 μs |   120.941 μs |   124.953 μs |     - |     - |     - |         - |
|                 **Parse** | **ArrayOfNumbers** |    **14.227 μs** |  **0.2104 μs** | **0.1968 μs** |    **14.154 μs** |    **14.012 μs** |    **14.583 μs** |     **-** |     **-** |     **-** |      **80 B** |
|        EnumerateArray | ArrayOfNumbers |     2.125 μs |  0.0360 μs | 0.0337 μs |     2.125 μs |     2.086 μs |     2.199 μs |     - |     - |     - |         - |
| EnumerateUsingIndexer | ArrayOfNumbers |     1.306 μs |  0.0143 μs | 0.0133 μs |     1.306 μs |     1.286 μs |     1.333 μs |     - |     - |     - |         - |
|                 **Parse** | **ArrayOfStrings** |    **12.784 μs** |  **0.1503 μs** | **0.1255 μs** |    **12.747 μs** |    **12.624 μs** |    **13.033 μs** |     **-** |     **-** |     **-** |      **80 B** |
|        EnumerateArray | ArrayOfStrings |     2.220 μs |  0.0416 μs | 0.0409 μs |     2.220 μs |     2.132 μs |     2.277 μs |     - |     - |     - |         - |
| EnumerateUsingIndexer | ArrayOfStrings |     1.304 μs |  0.0151 μs | 0.0141 μs |     1.300 μs |     1.286 μs |     1.331 μs |     - |     - |     - |         - |
