``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                        Method |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------ |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|                     SpanStart | 1.520 ns | 0.0302 ns | 0.0283 ns | 1.516 ns | 1.479 ns | 1.581 ns |     - |     - |     - |         - |
|               SpanStartLength | 1.544 ns | 0.0182 ns | 0.0170 ns | 1.539 ns | 1.522 ns | 1.572 ns |     - |     - |     - |         - |
|             ReadOnlySpanStart | 1.488 ns | 0.0195 ns | 0.0182 ns | 1.480 ns | 1.467 ns | 1.525 ns |     - |     - |     - |         - |
|       ReadOnlySpanStartLength | 1.511 ns | 0.0173 ns | 0.0162 ns | 1.505 ns | 1.492 ns | 1.537 ns |     - |     - |     - |         - |
|                   MemoryStart | 1.556 ns | 0.0327 ns | 0.0321 ns | 1.550 ns | 1.520 ns | 1.625 ns |     - |     - |     - |         - |
|               MemoryStartSpan | 2.185 ns | 0.0188 ns | 0.0176 ns | 2.182 ns | 2.159 ns | 2.215 ns |     - |     - |     - |         - |
|             MemoryStartLength | 1.482 ns | 0.0102 ns | 0.0085 ns | 1.482 ns | 1.468 ns | 1.501 ns |     - |     - |     - |         - |
|         MemoryStartLengthSpan | 1.962 ns | 0.0258 ns | 0.0241 ns | 1.960 ns | 1.927 ns | 2.019 ns |     - |     - |     - |         - |
|           ReadOnlyMemoryStart | 1.438 ns | 0.0120 ns | 0.0100 ns | 1.438 ns | 1.426 ns | 1.461 ns |     - |     - |     - |         - |
|       ReadOnlyMemoryStartSpan | 2.222 ns | 0.0222 ns | 0.0185 ns | 2.223 ns | 2.196 ns | 2.261 ns |     - |     - |     - |         - |
|     ReadOnlyMemoryStartLength | 1.367 ns | 0.0134 ns | 0.0119 ns | 1.365 ns | 1.353 ns | 1.395 ns |     - |     - |     - |         - |
| ReadOnlyMemoryStartLengthSpan | 2.002 ns | 0.0226 ns | 0.0211 ns | 1.996 ns | 1.976 ns | 2.040 ns |     - |     - |     - |         - |
|               MemorySpanStart | 2.503 ns | 0.0336 ns | 0.0297 ns | 2.493 ns | 2.462 ns | 2.555 ns |     - |     - |     - |         - |
|         MemorySpanStartLength | 2.422 ns | 0.0472 ns | 0.0418 ns | 2.407 ns | 2.374 ns | 2.511 ns |     - |     - |     - |         - |
|       ReadOnlyMemorySpanStart | 2.520 ns | 0.0276 ns | 0.0245 ns | 2.511 ns | 2.489 ns | 2.573 ns |     - |     - |     - |         - |
| ReadOnlyMemorySpanStartLength | 2.446 ns | 0.0311 ns | 0.0291 ns | 2.438 ns | 2.400 ns | 2.493 ns |     - |     - |     - |         - |
