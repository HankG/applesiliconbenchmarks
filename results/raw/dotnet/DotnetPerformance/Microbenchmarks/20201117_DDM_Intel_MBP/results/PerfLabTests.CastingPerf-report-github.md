``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                        Method |            Mean |         Error |        StdDev |          Median |             Min |             Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------------------------- |----------------:|--------------:|--------------:|----------------:|----------------:|----------------:|------:|------:|------:|----------:|
|                                   ObjFooIsObj | 228,769.1867 ns | 4,492.3980 ns | 4,202.1918 ns | 226,536.4049 ns | 224,485.6839 ns | 237,475.5145 ns |     - |     - |     - |         - |
|                                  ObjFooIsObj2 | 228,245.7775 ns | 2,710.5197 ns | 2,535.4218 ns | 227,544.9375 ns | 224,448.1938 ns | 232,433.5426 ns |     - |     - |     - |         - |
|                                   ObjObjIsFoo | 520,090.2344 ns | 4,598.0068 ns | 4,300.9784 ns | 519,121.5375 ns | 515,726.8979 ns | 529,368.2604 ns |     - |     - |     - |       1 B |
|                                   FooObjIsFoo | 363,864.0524 ns | 4,612.2338 ns | 4,088.6251 ns | 362,924.0799 ns | 358,798.5712 ns | 370,175.6875 ns |     - |     - |     - |         - |
|                                  FooObjIsFoo2 | 341,072.6001 ns | 3,737.4833 ns | 3,496.0442 ns | 340,513.8302 ns | 336,135.8111 ns | 348,275.6128 ns |     - |     - |     - |         - |
|                                  FooObjIsNull | 227,637.1346 ns | 2,458.8186 ns | 2,299.9804 ns | 227,015.8054 ns | 224,936.0670 ns | 231,964.4107 ns |     - |     - |     - |         - |
|                            FooObjIsDescendant | 522,667.3582 ns | 7,113.2764 ns | 5,939.9079 ns | 521,363.8979 ns | 516,051.4354 ns | 538,483.7188 ns |     - |     - |     - |       1 B |
|                                 IFooFooIsIFoo | 227,746.6487 ns | 2,399.8006 ns | 2,244.7749 ns | 227,276.3234 ns | 224,662.1033 ns | 231,821.2047 ns |     - |     - |     - |         - |
|                                 IFooObjIsIFoo | 746,201.4838 ns | 6,896.1056 ns | 6,113.2180 ns | 746,062.7060 ns | 736,798.9858 ns | 757,787.5682 ns |     - |     - |     - |       1 B |
|                        IFooObjIsIFooInterAlia | 520,103.4333 ns | 4,210.6449 ns | 3,938.6398 ns | 519,662.5250 ns | 513,306.4083 ns | 526,373.6562 ns |     - |     - |     - |       1 B |
|                     IFooObjIsDescendantOfIFoo | 521,760.2653 ns | 5,927.5899 ns | 5,544.6713 ns | 519,315.5504 ns | 516,139.5948 ns | 531,690.9274 ns |     - |     - |     - |       1 B |
|                                        ObjInt | 228,095.0980 ns | 3,724.2536 ns | 3,301.4538 ns | 227,041.6780 ns | 224,673.6060 ns | 236,450.9266 ns |     - |     - |     - |         - |
|                                        IntObj | 364,107.7477 ns | 4,190.5505 ns | 3,919.8435 ns | 362,998.9759 ns | 359,367.4048 ns | 370,623.9872 ns |     - |     - |     - |         - |
|                            ObjScalarValueType | 227,885.9356 ns | 2,607.2990 ns | 2,438.8691 ns | 227,372.2989 ns | 224,836.2708 ns | 232,881.8777 ns |     - |     - |     - |         - |
|                            ScalarValueTypeObj | 363,824.9525 ns | 4,459.2444 ns | 3,953.0040 ns | 362,532.8321 ns | 359,207.3350 ns | 371,719.5603 ns |     - |     - |     - |         - |
|                            ObjObjrefValueType | 227,769.7474 ns | 2,889.6518 ns | 2,702.9821 ns | 226,816.8986 ns | 224,607.4212 ns | 233,310.3696 ns |     - |     - |     - |         - |
|                            ObjrefValueTypeObj | 363,720.0710 ns | 3,589.6001 ns | 3,182.0870 ns | 362,735.3807 ns | 358,785.8857 ns | 369,438.2649 ns |     - |     - |     - |         - |
|                               FooObjCastIfIsa | 518,879.4851 ns | 6,259.4849 ns | 5,855.1260 ns | 517,416.1794 ns | 511,936.1371 ns | 532,114.8589 ns |     - |     - |     - |       1 B |
|                        CheckObjIsInterfaceYes | 228,393.8760 ns | 2,303.7674 ns | 2,154.9455 ns | 228,223.8786 ns | 225,196.3089 ns | 232,335.9312 ns |     - |     - |     - |         - |
|                         CheckObjIsInterfaceNo | 251,154.0726 ns | 2,961.9512 ns | 2,770.6110 ns | 250,473.6746 ns | 247,275.0050 ns | 257,112.3214 ns |     - |     - |     - |         - |
|                  CheckIsInstAnyIsInterfaceYes | 227,951.9650 ns | 2,722.8944 ns | 2,546.9971 ns | 227,695.3379 ns | 224,666.3379 ns | 233,058.5888 ns |     - |     - |     - |         - |
|                   CheckIsInstAnyIsInterfaceNo | 249,879.0852 ns | 2,075.7010 ns | 1,733.3044 ns | 249,080.9835 ns | 248,089.7535 ns | 252,409.1241 ns |     - |     - |     - |         - |
|                      CheckArrayIsInterfaceYes | 733,011.0436 ns | 6,088.6244 ns | 5,397.4070 ns | 731,901.2401 ns | 724,292.8040 ns | 742,108.3011 ns |     - |     - |     - |       1 B |
|                       CheckArrayIsInterfaceNo | 341,521.2564 ns | 4,178.1317 ns | 3,908.2270 ns | 339,797.5272 ns | 336,548.6780 ns | 347,902.1753 ns |     - |     - |     - |         - |
|        CheckArrayIsNonvariantGenericInterface |       6.5088 ns |     0.1309 ns |     0.1225 ns |       6.5135 ns |       6.3670 ns |       6.7365 ns |     - |     - |     - |         - |
|      CheckArrayIsNonvariantGenericInterfaceNo |       7.0130 ns |     0.1065 ns |     0.0996 ns |       6.9910 ns |       6.8702 ns |       7.1807 ns |     - |     - |     - |         - |
|                   CheckArrayIsArrayByVariance |       6.8215 ns |     0.0931 ns |     0.0871 ns |       6.8049 ns |       6.7092 ns |       6.9470 ns |     - |     - |     - |         - |
|            CheckListIsVariantGenericInterface |       2.9394 ns |     0.0387 ns |     0.0362 ns |       2.9279 ns |       2.8850 ns |       3.0166 ns |     - |     - |     - |         - |
|         CheckArrayIsVariantGenericInterfaceNo |       2.8215 ns |     0.0541 ns |     0.0507 ns |       2.8029 ns |       2.7622 ns |       2.9246 ns |     - |     - |     - |         - |
|                  AssignArrayElementByVariance |       8.2165 ns |     0.1197 ns |     0.1061 ns |       8.2237 ns |       8.0057 ns |       8.4169 ns |     - |     - |     - |         - |
| CheckArrayIsVariantGenericInterfaceReflection |       0.0272 ns |     0.0120 ns |     0.0113 ns |       0.0258 ns |       0.0161 ns |       0.0505 ns |     - |     - |     - |         - |
