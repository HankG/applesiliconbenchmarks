``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | RentalSize | ManipulateArray | Async | UseSharedPool |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |----------- |---------------- |------ |-------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|     **SingleSerial** |       **4096** |           **False** | **False** |         **False** |     **31.73 ns** |   **0.420 ns** |   **0.351 ns** |     **31.62 ns** |     **31.22 ns** |     **32.27 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False | False |         False |    400.25 ns |   7.643 ns |   8.177 ns |    400.47 ns |    386.92 ns |    413.57 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False | False |         False |    287.46 ns |  11.064 ns |  12.741 ns |    290.13 ns |    251.49 ns |    298.75 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False | False |         False |  1,077.88 ns |  28.711 ns |  31.912 ns |  1,078.20 ns |  1,027.51 ns |  1,135.82 ns | 0.0200 |     - |     - |      84 B |
|     **SingleSerial** |       **4096** |           **False** | **False** |          **True** |     **25.62 ns** |   **0.367 ns** |   **0.326 ns** |     **25.59 ns** |     **25.17 ns** |     **26.31 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False | False |          True |     66.45 ns |   2.662 ns |   2.959 ns |     65.45 ns |     62.89 ns |     72.72 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False | False |          True |    601.62 ns |  22.697 ns |  26.138 ns |    595.91 ns |    572.67 ns |    664.04 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False | False |          True |  1,206.31 ns |  88.262 ns |  98.103 ns |  1,169.34 ns |    988.34 ns |  1,389.33 ns | 0.0100 |     - |     - |      53 B |
|     **SingleSerial** |       **4096** |           **False** |  **True** |         **False** |    **675.89 ns** |  **16.689 ns** |  **19.219 ns** |    **681.29 ns** |    **625.83 ns** |    **698.37 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False |  True |         False |  1,806.25 ns |  50.592 ns |  58.262 ns |  1,810.20 ns |  1,696.60 ns |  1,884.69 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False |  True |         False |    975.65 ns |  17.380 ns |  16.258 ns |    978.27 ns |    952.37 ns |  1,002.71 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False |  True |         False |    744.02 ns |  21.136 ns |  24.341 ns |    745.64 ns |    682.32 ns |    777.75 ns |      - |     - |     - |       1 B |
|     **SingleSerial** |       **4096** |           **False** |  **True** |          **True** |    **650.92 ns** |   **7.145 ns** |   **6.334 ns** |    **652.64 ns** |    **633.44 ns** |    **660.65 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |           False |  True |          True |    843.19 ns |  31.724 ns |  36.533 ns |    857.50 ns |    780.00 ns |    884.72 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |           False |  True |          True |  1,258.95 ns |  41.055 ns |  45.633 ns |  1,255.44 ns |  1,186.13 ns |  1,350.20 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |           False |  True |          True |    491.51 ns |  19.739 ns |  21.940 ns |    496.05 ns |    434.30 ns |    522.28 ns |      - |     - |     - |       1 B |
|     **SingleSerial** |       **4096** |            **True** | **False** |         **False** |  **2,911.37 ns** |  **31.189 ns** |  **27.648 ns** |  **2,896.49 ns** |  **2,877.86 ns** |  **2,966.39 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True | False |         False |  6,198.79 ns |  32.519 ns |  30.418 ns |  6,197.42 ns |  6,144.73 ns |  6,237.65 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True | False |         False | 18,246.40 ns |  36.370 ns |  30.371 ns | 18,247.74 ns | 18,193.52 ns | 18,298.75 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True | False |         False |  2,047.84 ns |  91.985 ns | 105.930 ns |  2,023.94 ns |  1,863.67 ns |  2,298.14 ns |      - |     - |     - |      35 B |
|     **SingleSerial** |       **4096** |            **True** | **False** |          **True** |  **2,069.09 ns** |  **40.771 ns** |  **38.138 ns** |  **2,055.42 ns** |  **2,022.56 ns** |  **2,142.49 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True | False |          True |  4,306.17 ns |  57.863 ns |  51.294 ns |  4,302.33 ns |  4,233.97 ns |  4,388.39 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True | False |          True | 24,736.15 ns | 120.199 ns | 106.553 ns | 24,702.27 ns | 24,634.58 ns | 25,013.48 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True | False |          True |  2,449.12 ns | 112.180 ns | 129.186 ns |  2,487.51 ns |  2,220.88 ns |  2,662.18 ns |      - |     - |     - |      25 B |
|     **SingleSerial** |       **4096** |            **True** |  **True** |         **False** |  **3,620.86 ns** |  **23.820 ns** |  **19.891 ns** |  **3,611.02 ns** |  **3,600.89 ns** |  **3,666.06 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True |  True |         False |  4,530.44 ns | 218.747 ns | 224.637 ns |  4,580.74 ns |  4,176.59 ns |  4,864.15 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True |  True |         False | 20,849.74 ns | 162.734 ns | 135.891 ns | 20,826.17 ns | 20,651.55 ns | 21,131.86 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True |  True |         False |  2,017.28 ns |  50.776 ns |  58.474 ns |  1,998.89 ns |  1,930.53 ns |  2,159.52 ns |      - |     - |     - |       1 B |
|     **SingleSerial** |       **4096** |            **True** |  **True** |          **True** |  **3,391.17 ns** |  **20.750 ns** |  **18.394 ns** |  **3,385.49 ns** |  **3,371.34 ns** |  **3,429.85 ns** |      **-** |     **-** |     **-** |         **-** |
|   SingleParallel |       4096 |            True |  True |          True |  4,656.14 ns | 325.298 ns | 361.568 ns |  4,716.44 ns |  4,219.42 ns |  5,639.96 ns |      - |     - |     - |         - |
|   MultipleSerial |       4096 |            True |  True |          True | 25,812.12 ns | 152.421 ns | 127.278 ns | 25,800.65 ns | 25,591.00 ns | 26,080.89 ns |      - |     - |     - |         - |
| ProducerConsumer |       4096 |            True |  True |          True |  2,296.90 ns |  56.438 ns |  64.994 ns |  2,276.87 ns |  2,222.75 ns |  2,427.98 ns |      - |     - |     - |       1 B |
