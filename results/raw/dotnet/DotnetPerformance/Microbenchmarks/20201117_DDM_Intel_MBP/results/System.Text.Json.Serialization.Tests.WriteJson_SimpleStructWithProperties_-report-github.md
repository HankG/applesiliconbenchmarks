``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 256.8 ns | 4.22 ns | 4.51 ns | 255.8 ns | 250.6 ns | 265.6 ns | 0.0610 |     - |     - |     256 B |
|    SerializeToUtf8Bytes | 230.5 ns | 3.65 ns | 3.41 ns | 230.8 ns | 225.0 ns | 236.7 ns | 0.0551 |     - |     - |     232 B |
|       SerializeToStream | 306.2 ns | 5.16 ns | 4.58 ns | 304.7 ns | 300.1 ns | 314.4 ns | 0.0430 |     - |     - |     184 B |
| SerializeObjectProperty | 441.2 ns | 7.43 ns | 6.95 ns | 440.8 ns | 432.5 ns | 454.6 ns | 0.1391 |     - |     - |     584 B |
