``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|    DeserializeFromString | 29.83 μs | 0.367 μs | 0.343 μs | 29.88 μs | 29.21 μs | 30.51 μs | 5.3738 | 0.1168 |     - |  22.01 KB |
| DeserializeFromUtf8Bytes | 28.70 μs | 0.391 μs | 0.366 μs | 28.59 μs | 28.19 μs | 29.35 μs | 5.3800 | 0.1145 |     - |  22.01 KB |
|    DeserializeFromStream | 28.51 μs | 0.428 μs | 0.379 μs | 28.46 μs | 27.89 μs | 29.28 μs | 5.3023 |      - |     - |  22.08 KB |
