``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| ParseAbsoluteUri | 425.4 ns | 5.75 ns | 4.80 ns | 425.5 ns | 418.0 ns | 435.5 ns | 0.1514 |     - |     - |     640 B |
|      DnsSafeHost | 289.1 ns | 5.76 ns | 5.39 ns | 287.7 ns | 283.1 ns | 301.5 ns | 0.0516 |     - |     - |     216 B |
