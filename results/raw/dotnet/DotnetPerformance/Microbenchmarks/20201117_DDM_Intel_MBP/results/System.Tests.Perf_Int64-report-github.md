``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |                value |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |--------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|     **ToString** | **-9223372036854775808** | **42.84 ns** | **0.854 ns** | **0.757 ns** | **42.53 ns** | **41.98 ns** | **44.73 ns** | **0.0151** |     **-** |     **-** |      **64 B** |
|        Parse | -9223372036854775808 | 31.42 ns | 0.422 ns | 0.395 ns | 31.37 ns | 30.91 ns | 32.13 ns |      - |     - |     - |         - |
|     TryParse | -9223372036854775808 | 32.09 ns | 0.549 ns | 0.513 ns | 31.81 ns | 31.56 ns | 33.38 ns |      - |     - |     - |         - |
|    TryFormat | -9223372036854775808 | 39.60 ns | 0.470 ns | 0.440 ns | 39.44 ns | 39.06 ns | 40.48 ns |      - |     - |     - |         - |
|    ParseSpan | -9223372036854775808 | 32.94 ns | 0.439 ns | 0.410 ns | 32.87 ns | 32.48 ns | 33.67 ns |      - |     - |     - |         - |
| TryParseSpan | -9223372036854775808 | 30.63 ns | 0.358 ns | 0.299 ns | 30.70 ns | 30.19 ns | 31.22 ns |      - |     - |     - |         - |
|     **ToString** |                **12345** | **14.58 ns** | **0.251 ns** | **0.222 ns** | **14.55 ns** | **14.22 ns** | **15.04 ns** | **0.0076** |     **-** |     **-** |      **32 B** |
|        Parse |                12345 | 15.02 ns | 0.261 ns | 0.232 ns | 14.96 ns | 14.71 ns | 15.42 ns |      - |     - |     - |         - |
|     TryParse |                12345 | 15.55 ns | 0.175 ns | 0.164 ns | 15.52 ns | 15.24 ns | 15.82 ns |      - |     - |     - |         - |
|    TryFormat |                12345 | 12.11 ns | 0.164 ns | 0.153 ns | 12.09 ns | 11.91 ns | 12.37 ns |      - |     - |     - |         - |
|    ParseSpan |                12345 | 16.91 ns | 0.113 ns | 0.095 ns | 16.91 ns | 16.79 ns | 17.15 ns |      - |     - |     - |         - |
| TryParseSpan |                12345 | 14.29 ns | 0.221 ns | 0.196 ns | 14.27 ns | 14.07 ns | 14.62 ns |      - |     - |     - |         - |
|     **ToString** |  **9223372036854775807** | **36.21 ns** | **0.726 ns** | **0.679 ns** | **35.92 ns** | **35.38 ns** | **37.54 ns** | **0.0152** |     **-** |     **-** |      **64 B** |
|        Parse |  9223372036854775807 | 31.34 ns | 0.618 ns | 0.578 ns | 31.01 ns | 30.74 ns | 32.50 ns |      - |     - |     - |         - |
|     TryParse |  9223372036854775807 | 31.84 ns | 0.418 ns | 0.391 ns | 31.69 ns | 31.37 ns | 32.66 ns |      - |     - |     - |         - |
|    TryFormat |  9223372036854775807 | 33.12 ns | 0.380 ns | 0.337 ns | 33.12 ns | 32.64 ns | 33.83 ns |      - |     - |     - |         - |
|    ParseSpan |  9223372036854775807 | 32.32 ns | 0.339 ns | 0.300 ns | 32.23 ns | 31.94 ns | 32.88 ns |      - |     - |     - |         - |
| TryParseSpan |  9223372036854775807 | 30.12 ns | 0.457 ns | 0.428 ns | 30.02 ns | 29.64 ns | 31.05 ns |      - |     - |     - |         - |
