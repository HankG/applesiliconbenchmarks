``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method | Size |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------- |----- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|          Array | 2048 |  76.35 ns | 0.761 ns | 0.711 ns |  76.31 ns |  75.22 ns |  77.88 ns |     - |     - |     - |         - |
|           Span | 2048 |  75.57 ns | 1.415 ns | 1.254 ns |  75.08 ns |  74.22 ns |  78.70 ns |     - |     - |     - |         - |
|   ReadOnlySpan | 2048 |  77.44 ns | 1.055 ns | 0.987 ns |  77.09 ns |  76.11 ns |  79.35 ns |     - |     - |     - |         - |
|         Memory | 2048 |  80.82 ns | 1.075 ns | 1.006 ns |  80.32 ns |  79.56 ns |  82.62 ns |     - |     - |     - |         - |
| ReadOnlyMemory | 2048 |  79.64 ns | 0.808 ns | 0.756 ns |  79.63 ns |  78.53 ns |  80.79 ns |     - |     - |     - |         - |
|           List | 2048 |  81.09 ns | 0.992 ns | 0.928 ns |  80.68 ns |  80.04 ns |  82.74 ns |     - |     - |     - |         - |
| ImmutableArray | 2048 | 181.00 ns | 2.404 ns | 2.248 ns | 180.02 ns | 178.54 ns | 184.76 ns |     - |     - |     - |         - |
