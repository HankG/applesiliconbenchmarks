``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|       SerializeToString | 18.36 μs | 0.302 μs | 0.282 μs | 18.30 μs | 17.94 μs | 18.91 μs | 6.0250 | 0.4355 |     - |   25520 B |
|    SerializeToUtf8Bytes | 17.09 μs | 0.293 μs | 0.288 μs | 16.99 μs | 16.74 μs | 17.64 μs | 3.0438 |      - |     - |   13008 B |
|       SerializeToStream | 16.92 μs | 0.177 μs | 0.166 μs | 16.93 μs | 16.62 μs | 17.21 μs | 0.0674 |      - |     - |     464 B |
| SerializeObjectProperty | 18.81 μs | 0.321 μs | 0.300 μs | 18.76 μs | 18.43 μs | 19.49 μs | 6.0484 | 0.4480 |     - |   25536 B |
