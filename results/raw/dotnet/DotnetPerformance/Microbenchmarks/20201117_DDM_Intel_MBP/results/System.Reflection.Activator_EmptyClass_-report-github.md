``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |        Mean |     Error |    StdDev |      Median |         Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |------------:|----------:|----------:|------------:|------------:|------------:|-------:|------:|------:|----------:|
| CreateInstanceGeneric |    34.13 ns |  0.302 ns |  0.252 ns |    34.19 ns |    33.75 ns |    34.49 ns | 0.0057 |     - |     - |      24 B |
|    CreateInstanceType |    31.42 ns |  0.458 ns |  0.406 ns |    31.41 ns |    30.78 ns |    32.23 ns | 0.0056 |     - |     - |      24 B |
|   CreateInstanceNames | 5,255.90 ns | 97.515 ns | 91.216 ns | 5,232.60 ns | 5,121.69 ns | 5,438.14 ns | 0.1467 |     - |     - |     656 B |
