``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | length |       Mean |    Error |   StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|              Ref |   1024 |   469.8 ns |  7.00 ns |  6.55 ns |   468.7 ns |   461.7 ns |   484.4 ns |     - |     - |     - |         - |
|           Fixed1 |   1024 |   944.8 ns | 13.31 ns | 12.45 ns |   943.8 ns |   927.1 ns |   966.1 ns |     - |     - |     - |         - |
|           Fixed2 |   1024 |   941.9 ns | 11.23 ns | 10.50 ns |   938.9 ns |   930.2 ns |   962.5 ns |     - |     - |     - |         - |
|         Indexer1 |   1024 |   468.9 ns |  4.14 ns |  3.67 ns |   468.3 ns |   463.6 ns |   474.9 ns |     - |     - |     - |         - |
|         Indexer2 |   1024 |   468.6 ns |  4.33 ns |  4.05 ns |   467.9 ns |   461.9 ns |   474.7 ns |     - |     - |     - |         - |
|         Indexer3 |   1024 |   470.2 ns |  4.71 ns |  4.41 ns |   471.3 ns |   463.6 ns |   476.4 ns |     - |     - |     - |         - |
|         Indexer4 |   1024 | 4,692.4 ns | 56.61 ns | 52.96 ns | 4,670.0 ns | 4,618.9 ns | 4,791.7 ns |     - |     - |     - |         - |
|         Indexer5 |   1024 |   475.2 ns |  5.21 ns |  4.87 ns |   475.4 ns |   468.0 ns |   485.5 ns |     - |     - |     - |         - |
|         Indexer6 |   1024 |   478.6 ns |  5.77 ns |  5.11 ns |   476.8 ns |   471.5 ns |   488.3 ns |     - |     - |     - |         - |
| ReadOnlyIndexer1 |   1024 |   467.6 ns |  4.69 ns |  4.16 ns |   467.2 ns |   463.3 ns |   476.9 ns |     - |     - |     - |         - |
| ReadOnlyIndexer2 |   1024 |   470.7 ns |  6.88 ns |  6.10 ns |   468.8 ns |   462.7 ns |   481.8 ns |     - |     - |     - |         - |
| WriteViaIndexer1 |   1024 | 1,320.2 ns | 14.43 ns | 13.50 ns | 1,318.0 ns | 1,301.4 ns | 1,346.0 ns |     - |     - |     - |         - |
| WriteViaIndexer2 |   1024 |   938.0 ns |  9.72 ns |  8.12 ns |   937.1 ns |   928.2 ns |   954.9 ns |     - |     - |     - |         - |
|   KnownSizeArray |   1024 |   468.9 ns |  5.69 ns |  5.32 ns |   467.3 ns |   463.3 ns |   479.3 ns |     - |     - |     - |         - |
|    KnownSizeCtor |   1024 |   470.3 ns |  2.63 ns |  2.06 ns |   471.2 ns |   466.7 ns |   473.1 ns |     - |     - |     - |         - |
|   KnownSizeCtor2 |   1024 |   539.2 ns |  8.66 ns |  7.67 ns |   536.6 ns |   529.8 ns |   552.1 ns |     - |     - |     - |         - |
|       SameIndex1 |   1024 |   942.4 ns | 12.23 ns | 11.44 ns |   939.0 ns |   929.3 ns |   963.0 ns |     - |     - |     - |         - |
|       SameIndex2 |   1024 |   657.1 ns |  7.72 ns |  6.84 ns |   654.7 ns |   650.0 ns |   668.1 ns |     - |     - |     - |         - |
|    CoveredIndex1 |   1024 |   821.3 ns | 10.57 ns |  9.88 ns |   817.6 ns |   810.7 ns |   841.9 ns |     - |     - |     - |         - |
|    CoveredIndex2 |   1024 |   653.4 ns |  6.14 ns |  5.75 ns |   652.4 ns |   645.5 ns |   663.0 ns |     - |     - |     - |         - |
|    CoveredIndex3 |   1024 | 1,415.7 ns | 20.88 ns | 19.54 ns | 1,404.2 ns | 1,391.7 ns | 1,450.5 ns |     - |     - |     - |         - |
