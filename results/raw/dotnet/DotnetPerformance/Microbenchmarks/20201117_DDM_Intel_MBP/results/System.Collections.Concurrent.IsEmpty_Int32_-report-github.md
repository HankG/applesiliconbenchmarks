``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
| **Dictionary** |    **0** | **130.3926 ns** | **1.6770 ns** | **1.5687 ns** | **129.8705 ns** | **128.5252 ns** | **133.0550 ns** |     **-** |     **-** |     **-** |         **-** |
|      Queue |    0 |   5.0334 ns | 0.0953 ns | 0.0891 ns |   4.9909 ns |   4.9270 ns |   5.1641 ns |     - |     - |     - |         - |
|      Stack |    0 |   0.1117 ns | 0.0135 ns | 0.0119 ns |   0.1090 ns |   0.0990 ns |   0.1403 ns |     - |     - |     - |         - |
|        Bag |    0 |  11.5405 ns | 0.1732 ns | 0.1620 ns |  11.4573 ns |  11.3409 ns |  11.8398 ns |     - |     - |     - |         - |
| **Dictionary** |  **512** |   **2.4975 ns** | **0.0518 ns** | **0.0459 ns** |   **2.4817 ns** |   **2.4450 ns** |   **2.6057 ns** |     **-** |     **-** |     **-** |         **-** |
|      Queue |  512 |   5.1552 ns | 0.0745 ns | 0.0660 ns |   5.1543 ns |   5.0730 ns |   5.3061 ns |     - |     - |     - |         - |
|      Stack |  512 |   0.1080 ns | 0.0154 ns | 0.0144 ns |   0.1034 ns |   0.0852 ns |   0.1357 ns |     - |     - |     - |         - |
|        Bag |  512 |  10.5920 ns | 0.2018 ns | 0.1888 ns |  10.5444 ns |  10.3513 ns |  10.9788 ns |     - |     - |     - |         - |
