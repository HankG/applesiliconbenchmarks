``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | c | cultureName |                      input |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |-- |------------ |--------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|    **GetUnicodeCategory** | **.** |           **?** |                          **?** |  **0.4858 ns** | **0.0193 ns** | **0.0181 ns** |  **0.4791 ns** |  **0.4688 ns** |  **0.5208 ns** |     **-** |     **-** |     **-** |         **-** |
|          **Char_ToLower** | **A** |       **en-US** |                          **?** |  **3.0841 ns** | **0.0387 ns** | **0.0362 ns** |  **3.0786 ns** |  **3.0353 ns** |  **3.1489 ns** |     **-** |     **-** |     **-** |         **-** |
|          Char_ToUpper | A |       en-US |                          ? |  2.9697 ns | 0.0559 ns | 0.0523 ns |  2.9504 ns |  2.8993 ns |  3.0555 ns |     - |     - |     - |         - |
|          **Char_IsLower** | **?** |           **?** | **Good afternoon, Constable!** | **43.0543 ns** | **0.5982 ns** | **0.5303 ns** | **42.8908 ns** | **42.3974 ns** | **43.9972 ns** |     **-** |     **-** |     **-** |         **-** |
|          Char_IsUpper | ? |           ? | Good afternoon, Constable! | 47.3767 ns | 0.5109 ns | 0.4529 ns | 47.2902 ns | 46.7442 ns | 48.0780 ns |     - |     - |     - |         - |
| **Char_ToUpperInvariant** | **?** |           **?** |               **Hello World!** | **18.8291 ns** | **0.2348 ns** | **0.2196 ns** | **18.7551 ns** | **18.5459 ns** | **19.2322 ns** |     **-** |     **-** |     **-** |         **-** |
| Char_ToLowerInvariant | ? |           ? |               Hello World! | 19.5529 ns | 0.2438 ns | 0.2281 ns | 19.4494 ns | 19.2812 ns | 20.0659 ns |     - |     - |     - |         - |
|          **Char_ToLower** | **a** |       **en-US** |                          **?** |  **2.9214 ns** | **0.0581 ns** | **0.0515 ns** |  **2.9052 ns** |  **2.8629 ns** |  **3.0094 ns** |     **-** |     **-** |     **-** |         **-** |
|          Char_ToUpper | a |       en-US |                          ? |  3.0956 ns | 0.0480 ns | 0.0449 ns |  3.0881 ns |  3.0411 ns |  3.1773 ns |     - |     - |     - |         - |
|    **GetUnicodeCategory** | **a** |           **?** |                          **?** |  **0.4899 ns** | **0.0247 ns** | **0.0231 ns** |  **0.4822 ns** |  **0.4608 ns** |  **0.5298 ns** |     **-** |     **-** |     **-** |         **-** |
|          **Char_ToLower** | **İ** |       **en-US** |                          **?** | **18.4263 ns** | **0.2304 ns** | **0.2155 ns** | **18.3549 ns** | **18.1416 ns** | **18.8178 ns** |     **-** |     **-** |     **-** |         **-** |
|          Char_ToUpper | İ |       en-US |                          ? | 18.7404 ns | 0.3112 ns | 0.2911 ns | 18.6615 ns | 18.3693 ns | 19.2994 ns |     - |     - |     - |         - |
|    **GetUnicodeCategory** | **א** |           **?** |                          **?** |  **3.9417 ns** | **0.0625 ns** | **0.0585 ns** |  **3.9493 ns** |  **3.8654 ns** |  **4.0692 ns** |     **-** |     **-** |     **-** |         **-** |
|          **Char_ToLower** | **你** |     **zh-Hans** |                          **?** | **20.4324 ns** | **0.2843 ns** | **0.2520 ns** | **20.4074 ns** | **20.1185 ns** | **20.8197 ns** |     **-** |     **-** |     **-** |         **-** |
|          Char_ToUpper | 你 |     zh-Hans |                          ? | 20.3371 ns | 0.2459 ns | 0.2300 ns | 20.2808 ns | 20.0744 ns | 20.7531 ns |     - |     - |     - |         - |
