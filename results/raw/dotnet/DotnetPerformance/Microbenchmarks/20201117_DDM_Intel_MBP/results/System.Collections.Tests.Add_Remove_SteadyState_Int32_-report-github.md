``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method | Count |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |------ |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|   ConcurrentBag |   512 | 38.411 ns | 0.3728 ns | 0.3305 ns | 38.326 ns | 37.976 ns | 39.107 ns |      - |     - |     - |         - |
| ConcurrentQueue |   512 | 15.908 ns | 0.1895 ns | 0.1773 ns | 15.879 ns | 15.653 ns | 16.192 ns |      - |     - |     - |         - |
| ConcurrentStack |   512 | 22.598 ns | 0.4399 ns | 0.4518 ns | 22.480 ns | 21.988 ns | 23.348 ns | 0.0076 |     - |     - |      32 B |
|           Queue |   512 |  5.505 ns | 0.0837 ns | 0.0783 ns |  5.474 ns |  5.412 ns |  5.656 ns |      - |     - |     - |         - |
|           Stack |   512 |  2.614 ns | 0.0412 ns | 0.0385 ns |  2.597 ns |  2.572 ns |  2.689 ns |      - |     - |     - |         - |
