``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|              Array |  512 |    216.7 ns |   3.15 ns |   2.95 ns |    215.8 ns |    213.0 ns |    221.7 ns |     - |     - |     - |         - |
|               Span |  512 |    364.3 ns |   5.92 ns |   5.24 ns |    363.2 ns |    356.6 ns |    373.6 ns |     - |     - |     - |         - |
|       ReadOnlySpan |  512 |    357.2 ns |   2.81 ns |   2.35 ns |    357.1 ns |    353.5 ns |    361.4 ns |     - |     - |     - |         - |
|               List |  512 |    239.0 ns |   2.66 ns |   2.49 ns |    239.0 ns |    235.4 ns |    242.3 ns |     - |     - |     - |         - |
|              IList |  512 |  1,874.3 ns |  23.16 ns |  21.67 ns |  1,864.6 ns |  1,851.7 ns |  1,920.6 ns |     - |     - |     - |         - |
|     ImmutableArray |  512 |    214.5 ns |   2.99 ns |   2.65 ns |    213.1 ns |    211.8 ns |    219.3 ns |     - |     - |     - |         - |
|      ImmutableList |  512 | 11,122.3 ns | 135.77 ns | 127.00 ns | 11,118.6 ns | 10,930.6 ns | 11,339.9 ns |     - |     - |     - |         - |
| ImmutableSortedSet |  512 | 11,278.5 ns | 115.25 ns | 107.80 ns | 11,265.6 ns | 11,128.3 ns | 11,506.5 ns |     - |     - |     - |         - |
