``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|--------- |---------:|--------:|--------:|---------:|---------:|---------:|--------:|--------:|--------:|----------:|
|      Jil | 406.3 μs | 5.10 μs | 4.77 μs | 406.5 μs | 399.7 μs | 414.7 μs | 32.0513 | 32.0513 | 32.0513 | 211.77 KB |
| JSON.NET | 434.8 μs | 8.66 μs | 8.10 μs | 432.8 μs | 422.8 μs | 449.3 μs | 59.6491 | 29.8246 | 29.8246 | 293.12 KB |
| Utf8Json | 214.0 μs | 4.17 μs | 4.28 μs | 212.2 μs | 208.9 μs | 222.7 μs | 29.6108 | 29.6108 | 29.6108 |  98.02 KB |
