``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |  options |      Mean |     Error |    StdDev |    Median |       Min |       Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|------------- |--------- |----------:|----------:|----------:|----------:|----------:|----------:|--------:|--------:|--------:|----------:|
| **RegexRedux_5** |     **None** | **35.999 ms** | **1.0054 ms** | **1.1578 ms** | **35.721 ms** | **34.249 ms** | **37.868 ms** |       **-** |       **-** |       **-** |   **2.67 MB** |
| **RegexRedux_5** | **Compiled** |  **7.877 ms** | **0.0980 ms** | **0.0868 ms** |  **7.862 ms** |  **7.721 ms** |  **8.022 ms** | **34.4828** | **34.4828** | **34.4828** |   **2.67 MB** |
