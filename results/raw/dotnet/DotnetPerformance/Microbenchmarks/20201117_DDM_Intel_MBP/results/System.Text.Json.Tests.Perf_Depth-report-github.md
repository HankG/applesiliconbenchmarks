``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method | Depth |        Mean |     Error |    StdDev |       Median |          Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |------ |------------:|----------:|----------:|-------------:|-------------:|------------:|------:|------:|------:|----------:|
| **ReadSpanEmptyLoop** |     **1** |    **100.2 ns** |   **1.73 ns** |   **1.61 ns** |     **99.48 ns** |     **98.31 ns** |    **103.3 ns** |     **-** |     **-** |     **-** |         **-** |
| **ReadSpanEmptyLoop** |    **64** |  **3,193.0 ns** |  **38.08 ns** |  **35.62 ns** |  **3,182.21 ns** |  **3,154.00 ns** |  **3,268.0 ns** |     **-** |     **-** |     **-** |         **-** |
| **ReadSpanEmptyLoop** |    **65** |  **3,322.6 ns** |  **61.75 ns** |  **57.76 ns** |  **3,317.02 ns** |  **3,256.88 ns** |  **3,449.6 ns** |     **-** |     **-** |     **-** |      **32 B** |
| **ReadSpanEmptyLoop** |   **512** | **27,534.5 ns** | **358.20 ns** | **335.06 ns** | **27,418.20 ns** | **27,147.66 ns** | **28,160.1 ns** |     **-** |     **-** |     **-** |     **216 B** |
