``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-EGDQLZ : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
UnrollFactor=1  WarmupCount=30  

```
|              Method | NumberOfBytes |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |-------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
| Base64EncodeInPlace |     200000000 | 198.3 ms | 3.84 ms | 3.94 ms | 197.1 ms | 193.7 ms | 208.8 ms |     - |     - |     - |     360 B |
| Base64DecodeInPlace |     200000000 | 146.6 ms | 1.42 ms | 1.18 ms | 146.3 ms | 144.4 ms | 148.9 ms |     - |     - |     - |     288 B |
