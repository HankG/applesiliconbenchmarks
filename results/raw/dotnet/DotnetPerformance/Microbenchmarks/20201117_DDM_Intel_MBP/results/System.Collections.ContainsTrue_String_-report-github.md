``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |         Mean |      Error |     StdDev |       Median |          Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |-------------:|-----------:|-----------:|-------------:|-------------:|------------:|------:|------:|------:|----------:|
|              Array |  512 |   456.622 μs |  5.6327 μs |  4.9933 μs |   455.798 μs |   448.580 μs |   464.73 μs |     - |     - |     - |       1 B |
|               Span |  512 |   395.602 μs |  4.9966 μs |  4.6739 μs |   393.956 μs |   388.839 μs |   404.27 μs |     - |     - |     - |         - |
|               List |  512 |   434.405 μs |  4.0158 μs |  3.7564 μs |   433.937 μs |   428.952 μs |   439.71 μs |     - |     - |     - |         - |
|        ICollection |  512 |   439.730 μs |  8.5256 μs |  8.7552 μs |   436.161 μs |   427.782 μs |   456.41 μs |     - |     - |     - |         - |
|         LinkedList |  512 |   594.203 μs |  5.8693 μs |  5.2029 μs |   592.898 μs |   584.213 μs |   602.38 μs |     - |     - |     - |       1 B |
|            HashSet |  512 |     9.775 μs |  0.1763 μs |  0.1649 μs |     9.767 μs |     9.484 μs |    10.09 μs |     - |     - |     - |         - |
|              Queue |  512 |   435.934 μs |  4.6296 μs |  4.3305 μs |   435.228 μs |   430.083 μs |   443.28 μs |     - |     - |     - |         - |
|              Stack |  512 |   488.584 μs |  9.2313 μs |  9.8774 μs |   488.931 μs |   473.808 μs |   509.26 μs |     - |     - |     - |       1 B |
|          SortedSet |  512 |   206.404 μs |  2.4578 μs |  2.2991 μs |   205.655 μs |   203.158 μs |   210.47 μs |     - |     - |     - |         - |
|     ImmutableArray |  512 |   448.105 μs |  5.9934 μs |  5.3130 μs |   446.982 μs |   442.659 μs |   459.94 μs |     - |     - |     - |       1 B |
|   ImmutableHashSet |  512 |    36.308 μs |  0.5935 μs |  0.5552 μs |    36.389 μs |    35.574 μs |    37.26 μs |     - |     - |     - |         - |
|      ImmutableList |  512 | 1,385.858 μs | 25.4449 μs | 23.8012 μs | 1,381.689 μs | 1,354.287 μs | 1,423.44 μs |     - |     - |     - |       2 B |
| ImmutableSortedSet |  512 |   209.860 μs |  2.7751 μs |  2.5959 μs |   208.564 μs |   206.693 μs |   215.17 μs |     - |     - |     - |         - |
