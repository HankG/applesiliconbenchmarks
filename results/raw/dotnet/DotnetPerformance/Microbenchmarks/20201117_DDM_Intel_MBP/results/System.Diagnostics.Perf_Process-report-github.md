``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-VUUYSD : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
WarmupCount=1  

```
|              Method |        Job | UnrollFactor |            Mean |         Error |         StdDev |          Median |             Min |             Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|-------------------- |----------- |------------- |----------------:|--------------:|---------------:|----------------:|----------------:|----------------:|---------:|--------:|------:|----------:|
|               Start | Job-VUUYSD |            1 | 1,407,198.47 ns | 96,313.682 ns | 107,052.402 ns | 1,372,344.00 ns | 1,255,814.00 ns | 1,635,988.00 ns |        - |       - |     - |   24144 B |
|   GetCurrentProcess | Job-NXBZBK |           16 |        89.11 ns |      1.616 ns |       1.511 ns |        88.99 ns |        87.05 ns |        91.55 ns |   0.0630 |       - |     - |     264 B |
|      GetProcessById | Job-NXBZBK |           16 |       550.25 ns |     12.727 ns |      13.618 ns |       545.62 ns |       536.23 ns |       581.12 ns |   0.0624 |       - |     - |     264 B |
|        GetProcesses | Job-NXBZBK |           16 | 3,205,990.35 ns | 63,054.016 ns |  72,613.099 ns | 3,182,630.71 ns | 3,115,914.84 ns | 3,345,839.60 ns | 112.5000 | 25.0000 |     - |  537144 B |
|  GetProcessesByName | Job-NXBZBK |           16 | 3,251,217.29 ns | 61,745.102 ns |  63,407.674 ns | 3,249,352.52 ns | 3,143,386.77 ns | 3,360,943.48 ns | 112.5000 | 25.0000 |     - |  536805 B |
| StartAndWaitForExit | Job-NXBZBK |           16 | 3,830,417.36 ns | 54,614.416 ns |  51,086.358 ns | 3,806,989.83 ns | 3,769,670.02 ns | 3,915,602.08 ns |        - |       - |     - |   23577 B |
