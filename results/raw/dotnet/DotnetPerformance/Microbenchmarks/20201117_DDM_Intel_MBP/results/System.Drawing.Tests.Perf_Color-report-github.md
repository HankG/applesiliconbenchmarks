``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|              Method |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|   FromArgb_Channels |     1.693 ns |  0.0382 ns |  0.0357 ns |     1.689 ns |     1.633 ns |     1.760 ns |     - |     - |     - |         - |
| FromArgb_AlphaColor |     6.801 ns |  0.0957 ns |  0.0848 ns |     6.784 ns |     6.683 ns |     6.969 ns |     - |     - |     - |         - |
|       GetBrightness | 1,293.988 ns | 17.5119 ns | 16.3807 ns | 1,289.902 ns | 1,271.454 ns | 1,325.330 ns |     - |     - |     - |         - |
|              GetHue | 1,119.447 ns | 15.0615 ns | 14.0885 ns | 1,118.123 ns | 1,096.283 ns | 1,139.013 ns |     - |     - |     - |         - |
|       GetSaturation |   980.449 ns | 15.0081 ns | 14.0386 ns |   977.467 ns |   961.995 ns | 1,009.917 ns |     - |     - |     - |         - |
