``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 14.017 μs | 0.2670 μs | 0.2968 μs | 13.907 μs | 13.720 μs | 14.717 μs | 2.5131 |     - |     - |   10682 B |
|    protobuf-net | 21.813 μs | 0.4348 μs | 0.4067 μs | 21.748 μs | 21.369 μs | 22.778 μs | 1.1934 |     - |     - |    5009 B |
|     MessagePack |  5.709 μs | 0.0712 μs | 0.0631 μs |  5.690 μs |  5.627 μs |  5.817 μs |      - |     - |     - |         - |
