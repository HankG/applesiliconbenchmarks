``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                        Jil |   519.0 ns |  6.20 ns |  5.50 ns |   516.9 ns |   513.3 ns |   530.2 ns | 0.0211 |     - |     - |      96 B |
|                   JSON.NET | 1,263.7 ns | 19.01 ns | 17.78 ns | 1,259.8 ns | 1,238.7 ns | 1,297.7 ns | 0.1063 |     - |     - |     448 B |
|                   Utf8Json |   335.5 ns |  5.09 ns |  4.76 ns |   335.9 ns |   327.8 ns |   342.9 ns |      - |     - |     - |         - |
| DataContractJsonSerializer | 2,328.5 ns | 27.15 ns | 21.20 ns | 2,328.6 ns | 2,290.7 ns | 2,353.3 ns | 0.2307 |     - |     - |    1000 B |
