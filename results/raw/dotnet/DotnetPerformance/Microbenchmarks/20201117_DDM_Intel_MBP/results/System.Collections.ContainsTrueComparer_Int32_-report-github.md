``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|             Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|            HashSet |  512 |  5.987 μs | 0.0537 μs | 0.0502 μs |  6.001 μs |  5.909 μs |  6.075 μs |     - |     - |     - |         - |
|          SortedSet |  512 | 27.439 μs | 0.2953 μs | 0.2762 μs | 27.375 μs | 27.074 μs | 27.982 μs |     - |     - |     - |         - |
|   ImmutableHashSet |  512 | 25.341 μs | 0.4583 μs | 0.4287 μs | 25.220 μs | 24.935 μs | 26.322 μs |     - |     - |     - |         - |
| ImmutableSortedSet |  512 | 28.418 μs | 0.2959 μs | 0.2623 μs | 28.379 μs | 28.010 μs | 28.882 μs |     - |     - |     - |         - |
