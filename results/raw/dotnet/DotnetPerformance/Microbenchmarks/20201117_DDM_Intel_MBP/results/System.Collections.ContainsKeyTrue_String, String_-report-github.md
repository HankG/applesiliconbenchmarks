``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |    Error |   StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|---------:|---------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  11.62 μs | 0.160 μs | 0.142 μs |  11.60 μs |  11.39 μs |  11.84 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  12.43 μs | 0.139 μs | 0.123 μs |  12.40 μs |  12.24 μs |  12.67 μs |     - |     - |     - |         - |
|                SortedList |  512 | 212.67 μs | 2.767 μs | 2.588 μs | 211.70 μs | 209.49 μs | 217.56 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 227.48 μs | 2.508 μs | 2.095 μs | 227.05 μs | 224.74 μs | 230.34 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  16.56 μs | 0.146 μs | 0.122 μs |  16.60 μs |  16.28 μs |  16.74 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 |  33.04 μs | 0.509 μs | 0.451 μs |  32.91 μs |  32.52 μs |  33.85 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 212.45 μs | 2.975 μs | 2.783 μs | 211.67 μs | 209.24 μs | 219.19 μs |     - |     - |     - |         - |
