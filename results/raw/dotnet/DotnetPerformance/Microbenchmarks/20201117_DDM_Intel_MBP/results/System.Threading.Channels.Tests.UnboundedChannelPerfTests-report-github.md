``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |             Mean |         Error |        StdDev |           Median |              Min |              Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |-----------------:|--------------:|--------------:|-----------------:|-----------------:|-----------------:|------:|------:|------:|----------:|
|     TryWriteThenTryRead |         35.39 ns |      0.437 ns |      0.409 ns |         35.28 ns |         34.84 ns |         36.27 ns |     - |     - |     - |         - |
| WriteAsyncThenReadAsync |         65.19 ns |      0.873 ns |      0.816 ns |         64.88 ns |         64.21 ns |         66.50 ns |     - |     - |     - |         - |
| ReadAsyncThenWriteAsync |        107.69 ns |      0.936 ns |      0.876 ns |        107.53 ns |        106.66 ns |        109.61 ns |     - |     - |     - |         - |
|                PingPong | 13,544,991.68 ns | 71,406.235 ns | 63,299.767 ns | 13,534,684.19 ns | 13,466,659.31 ns | 13,691,311.44 ns |     - |     - |     - |   30398 B |
