``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|    DeserializeFromString | 12.94 μs | 0.161 μs | 0.134 μs | 12.98 μs | 12.65 μs | 13.13 μs | 3.5979 | 0.2570 |     - |  14.87 KB |
| DeserializeFromUtf8Bytes | 12.86 μs | 0.214 μs | 0.189 μs | 12.82 μs | 12.66 μs | 13.29 μs | 3.6349 | 0.2524 |     - |  14.87 KB |
|    DeserializeFromStream | 14.49 μs | 0.242 μs | 0.227 μs | 14.50 μs | 14.15 μs | 14.94 μs | 3.6004 | 0.2250 |     - |  14.94 KB |
