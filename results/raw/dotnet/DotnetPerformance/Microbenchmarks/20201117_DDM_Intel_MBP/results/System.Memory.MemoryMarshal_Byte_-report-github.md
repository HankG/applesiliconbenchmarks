``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| GetReference | 0.2872 ns | 0.0262 ns | 0.0232 ns | 0.2837 ns | 0.2571 ns | 0.3344 ns |     - |     - |     - |         - |
|      AsBytes | 0.2298 ns | 0.0156 ns | 0.0131 ns | 0.2275 ns | 0.2137 ns | 0.2634 ns |     - |     - |     - |         - |
|   CastToByte | 0.2221 ns | 0.0146 ns | 0.0137 ns | 0.2234 ns | 0.2030 ns | 0.2463 ns |     - |     - |     - |         - |
|    CastToInt | 0.2469 ns | 0.0177 ns | 0.0166 ns | 0.2435 ns | 0.2251 ns | 0.2837 ns |     - |     - |     - |         - |
|  TryGetArray | 3.9895 ns | 0.1030 ns | 0.0964 ns | 4.0008 ns | 3.7138 ns | 4.1278 ns |     - |     - |     - |         - |
|         Read | 1.4253 ns | 0.0271 ns | 0.0240 ns | 1.4174 ns | 1.4006 ns | 1.4827 ns |     - |     - |     - |         - |
