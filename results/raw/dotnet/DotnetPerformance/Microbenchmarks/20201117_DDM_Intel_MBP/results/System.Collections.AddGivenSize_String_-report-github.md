``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|--------:|-------:|------:|----------:|
|                 List |  512 |   2.090 μs | 0.0423 μs | 0.0488 μs |   2.085 μs |   2.026 μs |   2.174 μs |  0.9887 |      - |     - |   4.05 KB |
|          ICollection |  512 |   2.752 μs | 0.0541 μs | 0.0506 μs |   2.729 μs |   2.683 μs |   2.844 μs |  0.9852 |      - |     - |   4.05 KB |
|              HashSet |  512 |  11.467 μs | 0.2289 μs | 0.2248 μs |  11.450 μs |  11.159 μs |  11.970 μs |  2.4889 |      - |     - |   10.3 KB |
|           Dictionary |  512 |  13.300 μs | 0.2279 μs | 0.2020 μs |  13.274 μs |  13.010 μs |  13.655 μs |  3.4869 | 0.1585 |     - |  14.38 KB |
|          IDictionary |  512 |  14.460 μs | 0.2737 μs | 0.2426 μs |  14.421 μs |  14.136 μs |  14.994 μs |  3.5068 | 0.1697 |     - |  14.38 KB |
|           SortedList |  512 | 272.992 μs | 3.1067 μs | 2.9060 μs | 272.944 μs | 269.405 μs | 278.563 μs |  1.0776 |      - |     - |   8.11 KB |
|                Queue |  512 |   2.782 μs | 0.0507 μs | 0.0475 μs |   2.769 μs |   2.709 μs |   2.853 μs |  0.9928 |      - |     - |   4.06 KB |
|                Stack |  512 |   2.287 μs | 0.0456 μs | 0.0448 μs |   2.279 μs |   2.222 μs |   2.366 μs |  0.9903 |      - |     - |   4.05 KB |
| ConcurrentDictionary |  512 |  43.701 μs | 0.6353 μs | 0.5943 μs |  43.685 μs |  42.699 μs |  44.647 μs | 14.5150 | 1.0246 |     - |  59.48 KB |
