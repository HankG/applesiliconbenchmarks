``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |                        value |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |----------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|         **TryParseDouble** |     **-1.7976931348623157e+308** | **297.800 ns** | **3.7958 ns** | **3.5506 ns** | **297.414 ns** | **293.119 ns** | **304.602 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseSByte** |                         **-128** |   **5.506 ns** | **0.0993 ns** | **0.0929 ns** |   **5.485 ns** |   **5.371 ns** |   **5.685 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt32** |                  **-2147483648** |  **10.218 ns** | **0.1687 ns** | **0.1578 ns** |  **10.189 ns** |  **10.030 ns** |  **10.513 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseSingle** |               **-3.4028235E+38** | **129.748 ns** | **1.9576 ns** | **1.8312 ns** | **128.739 ns** | **127.788 ns** | **133.598 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt16** |                       **-32768** |   **6.455 ns** | **0.0828 ns** | **0.0734 ns** |   **6.429 ns** |   **6.349 ns** |   **6.580 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt64** |         **-9223372036854775808** |  **24.897 ns** | **0.3348 ns** | **0.2968 ns** |  **24.888 ns** |  **24.545 ns** |  **25.492 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseUInt64** |                            **0** |   **3.984 ns** | **0.0570 ns** | **0.0505 ns** |   **3.970 ns** |   **3.925 ns** |   **4.087 ns** |     **-** |     **-** |     **-** |         **-** |
|      TryParseUInt64Hex |                            0 |   4.176 ns | 0.0908 ns | 0.0849 ns |   4.172 ns |   4.052 ns |   4.312 ns |     - |     - |     - |         - |
|         TryParseUInt32 |                            0 |   3.186 ns | 0.0665 ns | 0.0622 ns |   3.158 ns |   3.108 ns |   3.312 ns |     - |     - |     - |         - |
|      TryParseUInt32Hex |                            0 |   4.581 ns | 0.0660 ns | 0.0585 ns |   4.577 ns |   4.471 ns |   4.714 ns |     - |     - |     - |         - |
|          TryParseInt16 |                            0 |   3.990 ns | 0.0642 ns | 0.0569 ns |   3.966 ns |   3.931 ns |   4.101 ns |     - |     - |     - |         - |
|         TryParseUInt16 |                            0 |   3.292 ns | 0.0445 ns | 0.0416 ns |   3.276 ns |   3.233 ns |   3.363 ns |     - |     - |     - |         - |
|           TryParseByte |                            0 |   3.357 ns | 0.0596 ns | 0.0558 ns |   3.337 ns |   3.280 ns |   3.455 ns |     - |     - |     - |         - |
|         **TryParseDouble** |      **1.7976931348623157e+308** | **296.691 ns** | **3.2062 ns** | **2.9991 ns** | **296.357 ns** | **292.605 ns** | **302.536 ns** |     **-** |     **-** |     **-** |         **-** |
| **TryParseDateTimeOffset** | **12/30/2017 3:45:22 AM -08:00** |  **13.338 ns** | **0.1895 ns** | **0.1772 ns** |  **13.270 ns** |  **13.156 ns** |  **13.682 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt64** |                        **12345** |   **7.391 ns** | **0.1101 ns** | **0.1030 ns** |   **7.375 ns** |   **7.273 ns** |   **7.604 ns** |     **-** |     **-** |     **-** |         **-** |
|         TryParseUInt64 |                        12345 |   6.414 ns | 0.0776 ns | 0.0726 ns |   6.389 ns |   6.313 ns |   6.550 ns |     - |     - |     - |         - |
|          TryParseInt32 |                        12345 |   6.114 ns | 0.0776 ns | 0.0687 ns |   6.090 ns |   6.025 ns |   6.223 ns |     - |     - |     - |         - |
|         TryParseUInt32 |                        12345 |   5.244 ns | 0.0579 ns | 0.0483 ns |   5.227 ns |   5.186 ns |   5.348 ns |     - |     - |     - |         - |
|         TryParseUInt16 |                        12345 |   5.593 ns | 0.0924 ns | 0.0865 ns |   5.552 ns |   5.488 ns |   5.793 ns |     - |     - |     - |         - |
|         TryParseDouble |                        12345 |  31.436 ns | 0.4562 ns | 0.4267 ns |  31.272 ns |  30.937 ns |  32.266 ns |     - |     - |     - |         - |
|         TryParseSingle |                        12345 |  32.097 ns | 0.5231 ns | 0.4893 ns |  31.977 ns |  31.540 ns |  33.013 ns |     - |     - |     - |         - |
|        **TryParseDecimal** |                   **123456.789** |  **48.128 ns** | **0.5412 ns** | **0.4798 ns** |  **48.081 ns** |  **47.429 ns** |  **49.036 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseSByte** |                          **127** |   **5.532 ns** | **0.0724 ns** | **0.0642 ns** |   **5.520 ns** |   **5.446 ns** |   **5.636 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseUInt64** |         **18446744073709551615** |  **24.448 ns** | **0.4223 ns** | **0.3950 ns** |  **24.351 ns** |  **23.975 ns** |  **25.255 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt32** |                   **2147483647** |  **10.248 ns** | **0.1852 ns** | **0.1642 ns** |  **10.205 ns** |  **10.092 ns** |  **10.611 ns** |     **-** |     **-** |     **-** |         **-** |
|           **TryParseByte** |                          **255** |   **4.487 ns** | **0.0804 ns** | **0.0752 ns** |   **4.456 ns** |   **4.384 ns** |   **4.636 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseSingle** |                **3.4028235E+38** | **128.646 ns** | **1.6014 ns** | **1.4196 ns** | **128.282 ns** | **127.049 ns** | **131.296 ns** |     **-** |     **-** |     **-** |         **-** |
|      **TryParseUInt64Hex** |                         **3039** |   **5.766 ns** | **0.0839 ns** | **0.0743 ns** |   **5.757 ns** |   **5.634 ns** |   **5.896 ns** |     **-** |     **-** |     **-** |         **-** |
|      TryParseUInt32Hex |                         3039 |   7.351 ns | 0.1427 ns | 0.1265 ns |   7.331 ns |   7.188 ns |   7.600 ns |     - |     - |     - |         - |
|          **TryParseInt16** |                        **32767** |   **6.249 ns** | **0.1103 ns** | **0.1032 ns** |   **6.240 ns** |   **6.127 ns** |   **6.462 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt32** |                            **4** |   **3.856 ns** | **0.0608 ns** | **0.0539 ns** |   **3.839 ns** |   **3.783 ns** |   **3.958 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseUInt32** |                   **4294967295** |   **9.158 ns** | **0.0768 ns** | **0.0641 ns** |   **9.164 ns** |   **9.062 ns** |   **9.299 ns** |     **-** |     **-** |     **-** |         **-** |
|         **TryParseUInt16** |                        **65535** |   **5.638 ns** | **0.1360 ns** | **0.1205 ns** |   **5.619 ns** |   **5.502 ns** |   **5.923 ns** |     **-** |     **-** |     **-** |         **-** |
|          **TryParseInt64** |          **9223372036854775807** |  **24.839 ns** | **0.3404 ns** | **0.3184 ns** |  **24.713 ns** |  **24.434 ns** |  **25.451 ns** |     **-** |     **-** |     **-** |         **-** |
|      **TryParseUInt64Hex** |             **FFFFFFFFFFFFFFFF** |  **14.220 ns** | **0.1788 ns** | **0.1585 ns** |  **14.222 ns** |  **13.878 ns** |  **14.469 ns** |     **-** |     **-** |     **-** |         **-** |
|      TryParseUInt32Hex |             FFFFFFFFFFFFFFFF |  12.850 ns | 0.1400 ns | 0.1241 ns |  12.842 ns |  12.671 ns |  13.094 ns |     - |     - |     - |         - |
|           **TryParseBool** |                        **False** |   **3.003 ns** | **0.0777 ns** | **0.0727 ns** |   **2.999 ns** |   **2.897 ns** |   **3.187 ns** |     **-** |     **-** |     **-** |         **-** |
|           **TryParseBool** |                         **True** |   **2.089 ns** | **0.0336 ns** | **0.0315 ns** |   **2.079 ns** |   **2.037 ns** |   **2.146 ns** |     **-** |     **-** |     **-** |         **-** |
