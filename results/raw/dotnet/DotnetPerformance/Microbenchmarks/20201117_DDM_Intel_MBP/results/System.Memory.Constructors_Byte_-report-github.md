``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                   Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                            SpanFromArray | 0.2145 ns | 0.0140 ns | 0.0117 ns | 0.2127 ns | 0.2010 ns | 0.2419 ns |     - |     - |     - |         - |
|                    ReadOnlySpanFromArray | 0.2004 ns | 0.0196 ns | 0.0174 ns | 0.2000 ns | 0.1758 ns | 0.2311 ns |     - |     - |     - |         - |
|                 SpanFromArrayStartLength | 0.1841 ns | 0.0195 ns | 0.0182 ns | 0.1758 ns | 0.1595 ns | 0.2117 ns |     - |     - |     - |         - |
|         ReadOnlySpanFromArrayStartLength | 0.2206 ns | 0.0239 ns | 0.0212 ns | 0.2173 ns | 0.1922 ns | 0.2599 ns |     - |     - |     - |         - |
|                           SpanFromMemory | 0.7572 ns | 0.0295 ns | 0.0262 ns | 0.7549 ns | 0.7187 ns | 0.8188 ns |     - |     - |     - |         - |
|                   ReadOnlySpanFromMemory | 0.7448 ns | 0.0265 ns | 0.0248 ns | 0.7353 ns | 0.7159 ns | 0.7819 ns |     - |     - |     - |         - |
|                SpanImplicitCastFromArray | 0.3666 ns | 0.0168 ns | 0.0157 ns | 0.3602 ns | 0.3456 ns | 0.3935 ns |     - |     - |     - |         - |
|        ReadOnlySpanImplicitCastFromArray | 0.2215 ns | 0.0177 ns | 0.0166 ns | 0.2174 ns | 0.2003 ns | 0.2541 ns |     - |     - |     - |         - |
|         SpanImplicitCastFromArraySegment | 1.4627 ns | 0.0366 ns | 0.0343 ns | 1.4524 ns | 1.4251 ns | 1.5388 ns |     - |     - |     - |         - |
| ReadOnlySpanImplicitCastFromArraySegment | 1.2421 ns | 0.0492 ns | 0.0483 ns | 1.2285 ns | 1.1786 ns | 1.3334 ns |     - |     - |     - |         - |
|         ReadOnlySpanImplicitCastFromSpan | 0.0000 ns | 0.0000 ns | 0.0000 ns | 0.0000 ns | 0.0000 ns | 0.0000 ns |     - |     - |     - |         - |
|                          MemoryFromArray | 3.5307 ns | 0.0933 ns | 0.0958 ns | 3.5039 ns | 3.4256 ns | 3.7495 ns |     - |     - |     - |         - |
|                  ReadOnlyMemoryFromArray | 3.6133 ns | 0.0989 ns | 0.1058 ns | 3.6111 ns | 3.4848 ns | 3.8513 ns |     - |     - |     - |         - |
|               MemoryFromArrayStartLength | 3.5680 ns | 0.0750 ns | 0.0701 ns | 3.5435 ns | 3.4657 ns | 3.7028 ns |     - |     - |     - |         - |
|       ReadOnlyMemoryFromArrayStartLength | 3.5950 ns | 0.0842 ns | 0.0788 ns | 3.5847 ns | 3.4821 ns | 3.7331 ns |     - |     - |     - |         - |
|                              ArrayAsSpan | 0.2873 ns | 0.0291 ns | 0.0272 ns | 0.2941 ns | 0.2395 ns | 0.3221 ns |     - |     - |     - |         - |
|                   ArrayAsSpanStartLength | 0.2341 ns | 0.0246 ns | 0.0231 ns | 0.2378 ns | 0.2061 ns | 0.2684 ns |     - |     - |     - |         - |
|                            ArrayAsMemory | 3.5727 ns | 0.0757 ns | 0.0708 ns | 3.5769 ns | 3.4869 ns | 3.7066 ns |     - |     - |     - |         - |
|                 ArrayAsMemoryStartLength | 3.4895 ns | 0.0621 ns | 0.0581 ns | 3.4694 ns | 3.4112 ns | 3.5778 ns |     - |     - |     - |         - |
|                  MemoryMarshalCreateSpan | 0.0194 ns | 0.0218 ns | 0.0242 ns | 0.0000 ns | 0.0000 ns | 0.0645 ns |     - |     - |     - |         - |
|          MemoryMarshalCreateReadOnlySpan | 0.0024 ns | 0.0099 ns | 0.0097 ns | 0.0000 ns | 0.0000 ns | 0.0387 ns |     - |     - |     - |         - |
