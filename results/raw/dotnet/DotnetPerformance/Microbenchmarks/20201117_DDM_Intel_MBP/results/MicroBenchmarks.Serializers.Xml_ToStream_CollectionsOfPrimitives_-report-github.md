``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |    Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---------:|--------:|--------:|---------:|---------:|---------:|---------:|------:|------:|----------:|
|          XmlSerializer | 443.6 μs | 7.80 μs | 7.29 μs | 442.8 μs | 433.4 μs | 459.1 μs | 109.3750 |     - |     - | 449.35 KB |
| DataContractSerializer | 569.1 μs | 6.79 μs | 5.67 μs | 567.6 μs | 563.2 μs | 581.8 μs |   6.8182 |     - |     - |  33.74 KB |
