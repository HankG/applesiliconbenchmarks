``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                     Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                          CreateFromScalars |  5.3080 ns | 0.1143 ns | 0.1069 ns |  5.2776 ns |  5.1892 ns |  5.4861 ns |     - |     - |     - |         - |
|                          IdentityBenchmark |  0.8302 ns | 0.0189 ns | 0.0167 ns |  0.8275 ns |  0.8076 ns |  0.8687 ns |     - |     - |     - |         - |
|                        IsIdentityBenchmark |  4.0642 ns | 0.0702 ns | 0.0622 ns |  4.0595 ns |  3.9758 ns |  4.2112 ns |     - |     - |     - |         - |
|                       AddOperatorBenchmark |  8.6613 ns | 0.0950 ns | 0.0842 ns |  8.6550 ns |  8.5451 ns |  8.8679 ns |     - |     - |     - |         - |
|                  EqualityOperatorBenchmark |  7.2530 ns | 0.1741 ns | 0.2005 ns |  7.2441 ns |  6.9541 ns |  7.6219 ns |     - |     - |     - |         - |
|                InequalityOperatorBenchmark |  7.1322 ns | 0.1155 ns | 0.1081 ns |  7.1087 ns |  6.9678 ns |  7.3435 ns |     - |     - |     - |         - |
|          MultiplyByMatrixOperatorBenchmark |  9.3868 ns | 0.2032 ns | 0.1901 ns |  9.3387 ns |  9.1921 ns |  9.8814 ns |     - |     - |     - |         - |
|          MultiplyByScalarOperatorBenchmark |  7.1199 ns | 0.0937 ns | 0.0831 ns |  7.1021 ns |  7.0296 ns |  7.3002 ns |     - |     - |     - |         - |
|                  SubtractOperatorBenchmark |  9.0719 ns | 0.1017 ns | 0.0902 ns |  9.0328 ns |  8.9554 ns |  9.2540 ns |     - |     - |     - |         - |
|                  NegationOperatorBenchmark |  7.0844 ns | 0.1033 ns | 0.0966 ns |  7.0492 ns |  6.9800 ns |  7.2757 ns |     - |     - |     - |         - |
|                               AddBenchmark |  8.7242 ns | 0.1280 ns | 0.1197 ns |  8.7433 ns |  8.5407 ns |  8.9552 ns |     - |     - |     - |         - |
|                    CreateRotationBenchmark | 16.7577 ns | 0.1958 ns | 0.1736 ns | 16.6914 ns | 16.5471 ns | 17.0938 ns |     - |     - |     - |         - |
|          CreateRotationWithCenterBenchmark | 16.5076 ns | 0.1869 ns | 0.1657 ns | 16.4717 ns | 16.2966 ns | 16.8794 ns |     - |     - |     - |         - |
|           CreateScaleFromScalarXYBenchmark |  5.5236 ns | 0.0980 ns | 0.0917 ns |  5.4868 ns |  5.4208 ns |  5.6785 ns |     - |     - |     - |         - |
|   CreateScaleFromScalarWithCenterBenchmark |  5.9400 ns | 0.0872 ns | 0.0816 ns |  5.9275 ns |  5.8388 ns |  6.0819 ns |     - |     - |     - |         - |
| CreateScaleFromScalarXYWithCenterBenchmark |  5.4768 ns | 0.0828 ns | 0.0734 ns |  5.4530 ns |  5.3958 ns |  5.6334 ns |     - |     - |     - |         - |
|             CreateScaleFromScalarBenchmark |  5.2967 ns | 0.0777 ns | 0.0726 ns |  5.2696 ns |  5.2015 ns |  5.4122 ns |     - |     - |     - |         - |
|             CreateScaleFromVectorBenchmark |  5.7056 ns | 0.0830 ns | 0.0736 ns |  5.6786 ns |  5.6192 ns |  5.8630 ns |     - |     - |     - |         - |
|   CreateScaleFromVectorWithCenterBenchmark |  5.7390 ns | 0.0981 ns | 0.0870 ns |  5.7148 ns |  5.6268 ns |  5.8636 ns |     - |     - |     - |         - |
|            CreateSkewFromScalarXYBenchmark | 13.4324 ns | 0.1609 ns | 0.1505 ns | 13.4853 ns | 13.2102 ns | 13.6953 ns |     - |     - |     - |         - |
|  CreateSkewFromScalarXYWithCenterBenchmark | 21.5693 ns | 0.4372 ns | 0.4294 ns | 21.3663 ns | 21.1096 ns | 22.4831 ns |     - |     - |     - |         - |
|       CreateTranslationFromVectorBenchmark |  1.1409 ns | 0.0488 ns | 0.0456 ns |  1.1297 ns |  1.0746 ns |  1.2244 ns |     - |     - |     - |         - |
|              CreateTranslationFromScalarXY |  4.8443 ns | 0.1229 ns | 0.1149 ns |  4.8052 ns |  4.7210 ns |  5.1189 ns |     - |     - |     - |         - |
|                            EqualsBenchmark |  6.6799 ns | 0.1553 ns | 0.1525 ns |  6.6655 ns |  6.4483 ns |  7.0214 ns |     - |     - |     - |         - |
|                    GetDeterminantBenchmark |  1.8219 ns | 0.0324 ns | 0.0303 ns |  1.8220 ns |  1.7771 ns |  1.8643 ns |     - |     - |     - |         - |
|                            InvertBenchmark |  4.9773 ns | 0.0915 ns | 0.0812 ns |  4.9557 ns |  4.8736 ns |  5.1283 ns |     - |     - |     - |         - |
|                              LerpBenchmark |  8.5440 ns | 0.1964 ns | 0.1837 ns |  8.4910 ns |  8.3520 ns |  8.9516 ns |     - |     - |     - |         - |
|                  MultiplyByMatrixBenchmark |  9.3657 ns | 0.0911 ns | 0.0808 ns |  9.3629 ns |  9.2463 ns |  9.4990 ns |     - |     - |     - |         - |
|                  MultiplyByScalarBenchmark |  7.3260 ns | 0.1142 ns | 0.1069 ns |  7.3261 ns |  7.0901 ns |  7.4948 ns |     - |     - |     - |         - |
|                            NegateBenchmark |  7.2300 ns | 0.1951 ns | 0.2247 ns |  7.2303 ns |  6.9611 ns |  7.7770 ns |     - |     - |     - |         - |
|                          SubtractBenchmark |  8.6789 ns | 0.1522 ns | 0.1424 ns |  8.6463 ns |  8.5131 ns |  8.9900 ns |     - |     - |     - |         - |
