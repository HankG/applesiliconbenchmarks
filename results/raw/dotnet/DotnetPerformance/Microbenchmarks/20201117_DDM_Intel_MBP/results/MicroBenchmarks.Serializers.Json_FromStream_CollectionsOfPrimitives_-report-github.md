``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|---------:|--------:|------:|----------:|
|                        Jil |   409.3 μs |  7.20 μs |  6.38 μs |   408.6 μs |   398.0 μs |   418.4 μs |  40.0641 | 12.8205 |     - | 170.81 KB |
|                   JSON.NET |   560.6 μs | 10.83 μs |  9.60 μs |   557.3 μs |   550.4 μs |   579.2 μs |  69.1964 | 22.3214 |     - | 302.85 KB |
|                   Utf8Json |   275.7 μs |  4.04 μs |  3.58 μs |   275.2 μs |   270.7 μs |   282.4 μs |  40.9483 | 12.9310 |     - | 175.05 KB |
| DataContractJsonSerializer | 3,202.3 μs | 52.30 μs | 48.92 μs | 3,189.9 μs | 3,145.4 μs | 3,317.8 μs | 175.0000 | 50.0000 |     - | 784.64 KB |
