``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method | HasSubscribers |  Json |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |--------------- |------ |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| **NestedScopes_TwoMessages** |          **False** | **False** |   **103.6 ns** |  **1.31 ns** |  **1.10 ns** |   **103.5 ns** |   **102.0 ns** |   **105.7 ns** | **0.0054** |     **-** |     **-** |      **24 B** |
| **NestedScopes_TwoMessages** |          **False** |  **True** |   **106.1 ns** |  **1.72 ns** |  **1.61 ns** |   **105.7 ns** |   **104.2 ns** |   **109.4 ns** | **0.0054** |     **-** |     **-** |      **24 B** |
| **NestedScopes_TwoMessages** |           **True** | **False** | **2,009.4 ns** | **25.49 ns** | **22.59 ns** | **2,007.6 ns** | **1,976.8 ns** | **2,059.7 ns** | **0.4677** |     **-** |     **-** |    **1960 B** |
| **NestedScopes_TwoMessages** |           **True** |  **True** | **3,761.8 ns** | **55.02 ns** | **51.47 ns** | **3,744.5 ns** | **3,698.3 ns** | **3,853.5 ns** | **1.1651** |     **-** |     **-** |    **4888 B** |
