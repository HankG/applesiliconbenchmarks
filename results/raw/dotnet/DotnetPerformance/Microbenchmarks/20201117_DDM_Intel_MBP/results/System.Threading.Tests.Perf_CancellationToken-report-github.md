``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                         Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|   RegisterAndUnregister_Serial |  42.99 ns | 0.448 ns | 0.397 ns |  42.92 ns |  42.53 ns |  43.89 ns |      - |     - |     - |         - |
| RegisterAndUnregister_Parallel |  15.68 ns | 0.793 ns | 0.848 ns |  15.66 ns |  14.46 ns |  16.98 ns |      - |     - |     - |         - |
|                         Cancel | 134.65 ns | 1.955 ns | 1.829 ns | 134.15 ns | 132.10 ns | 138.04 ns | 0.0689 |     - |     - |     288 B |
|       CreateLinkedTokenSource1 |  55.38 ns | 0.913 ns | 0.854 ns |  55.52 ns |  54.01 ns |  57.10 ns | 0.0190 |     - |     - |      80 B |
|       CreateLinkedTokenSource2 |  94.44 ns | 1.418 ns | 1.326 ns |  94.11 ns |  92.88 ns |  97.53 ns | 0.0228 |     - |     - |      96 B |
|       CreateLinkedTokenSource3 | 149.80 ns | 2.429 ns | 2.153 ns | 149.56 ns | 146.81 ns | 154.00 ns | 0.0340 |     - |     - |     144 B |
|                    CancelAfter | 133.67 ns | 2.527 ns | 2.704 ns | 132.89 ns | 130.01 ns | 138.68 ns | 0.0379 |     - |     - |     160 B |
