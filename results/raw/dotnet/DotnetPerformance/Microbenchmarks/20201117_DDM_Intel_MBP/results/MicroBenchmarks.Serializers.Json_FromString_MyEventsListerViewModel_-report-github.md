``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------- |---------:|--------:|--------:|---------:|---------:|---------:|--------:|--------:|------:|----------:|
|      Jil | 292.9 μs | 4.06 μs | 3.60 μs | 292.8 μs | 287.8 μs | 298.4 μs | 24.3056 |  6.9444 |     - |    103 KB |
| JSON.NET | 681.2 μs | 9.30 μs | 8.70 μs | 678.5 μs | 669.8 μs | 694.0 μs | 38.1471 |  2.7248 |     - | 156.68 KB |
| Utf8Json | 319.7 μs | 4.13 μs | 3.66 μs | 318.6 μs | 314.8 μs | 327.0 μs | 43.7500 | 10.0000 |     - | 182.79 KB |
