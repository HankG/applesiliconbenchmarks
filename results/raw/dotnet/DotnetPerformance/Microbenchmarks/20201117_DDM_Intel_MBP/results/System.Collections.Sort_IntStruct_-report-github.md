``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RTZWTM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=5000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|               Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                Array |  512 |  4.286 μs | 0.1274 μs | 0.1416 μs |  4.229 μs |  4.138 μs |  4.595 μs |      - |     - |     - |         - |
|  Array_ComparerClass |  512 | 23.556 μs | 0.2174 μs | 0.1815 μs | 23.608 μs | 23.290 μs | 23.910 μs |      - |     - |     - |      64 B |
| Array_ComparerStruct |  512 | 24.281 μs | 0.2348 μs | 0.2082 μs | 24.257 μs | 23.977 μs | 24.738 μs |      - |     - |     - |      88 B |
|     Array_Comparison |  512 | 21.701 μs | 0.3337 μs | 0.2958 μs | 21.600 μs | 21.391 μs | 22.302 μs |      - |     - |     - |         - |
|                 List |  512 |  4.411 μs | 0.1300 μs | 0.1445 μs |  4.379 μs |  4.217 μs |  4.672 μs |      - |     - |     - |         - |
|            LinqQuery |  512 | 40.839 μs | 0.6739 μs | 0.6303 μs | 40.660 μs | 40.093 μs | 41.969 μs | 1.4000 |     - |     - |    6456 B |
| LinqOrderByExtension |  512 | 40.318 μs | 0.2887 μs | 0.2254 μs | 40.334 μs | 40.010 μs | 40.616 μs | 1.4000 |     - |     - |    6456 B |
