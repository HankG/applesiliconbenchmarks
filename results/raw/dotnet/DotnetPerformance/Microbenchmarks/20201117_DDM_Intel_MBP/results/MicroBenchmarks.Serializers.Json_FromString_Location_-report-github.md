``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|      Jil |   570.2 ns | 10.21 ns | 10.03 ns |   567.4 ns |   558.1 ns |   591.1 ns | 0.1285 |     - |     - |     544 B |
| JSON.NET | 2,464.6 ns | 46.76 ns | 43.74 ns | 2,453.9 ns | 2,402.2 ns | 2,556.9 ns | 0.7243 |     - |     - |    3056 B |
| Utf8Json |   935.0 ns | 17.81 ns | 19.79 ns |   933.9 ns |   910.3 ns |   984.3 ns | 0.1603 |     - |     - |     672 B |
