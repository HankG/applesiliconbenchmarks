``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |        Mean |     Error |    StdDev |      Median |         Min |         Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|---------------- |------------:|----------:|----------:|------------:|------------:|------------:|---------:|--------:|------:|----------:|
| BinaryFormatter | 1,118.98 μs | 20.618 μs | 19.286 μs | 1,117.23 μs | 1,091.35 μs | 1,149.73 μs | 141.6667 | 58.3333 |     - | 639.83 KB |
|    protobuf-net |   231.80 μs |  4.376 μs |  4.093 μs |   230.86 μs |   226.55 μs |   240.88 μs |  55.2536 | 18.1159 |     - | 265.15 KB |
|     MessagePack |    72.24 μs |  1.385 μs |  1.295 μs |    71.78 μs |    70.49 μs |    74.60 μs |  18.2648 |  5.9932 |     - |  75.36 KB |
