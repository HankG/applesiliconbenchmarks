``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                         Method | length |         Mean |       Error |      StdDev |       Median |          Min |          Max |      Gen 0 |     Gen 1 | Gen 2 |   Allocated |
|------------------------------- |------- |-------------:|------------:|------------:|-------------:|-------------:|-------------:|-----------:|----------:|------:|------------:|
|                 **DelegateInvoke** |      **?** |     **272.9 μs** |     **2.34 μs** |     **1.83 μs** |     **273.3 μs** |     **269.7 μs** |     **276.5 μs** |          **-** |         **-** |     **-** |           **-** |
| MulticastDelegateCombineInvoke |      ? | 165,792.5 μs | 1,558.26 μs | 1,301.22 μs | 165,797.5 μs | 164,150.9 μs | 168,730.4 μs | 95000.0000 | 3000.0000 |     - | 400920192 B |
|        **MulticastDelegateInvoke** |    **100** |   **2,868.9 μs** |    **32.64 μs** |    **30.53 μs** |   **2,856.2 μs** |   **2,826.0 μs** |   **2,917.1 μs** |          **-** |         **-** |     **-** |         **3 B** |
|        **MulticastDelegateInvoke** |   **1000** |  **29,725.4 μs** |   **836.44 μs** |   **963.25 μs** |  **29,366.3 μs** |  **28,800.8 μs** |  **32,028.6 μs** |          **-** |         **-** |     **-** |        **41 B** |
