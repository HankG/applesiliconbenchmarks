``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QZKIVK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MaxWarmupIterationCount=10  
MinIterationCount=15  MinWarmupIterationCount=6  UnrollFactor=1  
WarmupCount=-1  

```
|          Method |    Size |      Mean |    Error |   StdDev |    Median |       Min |       Max |      Gen 0 |     Gen 1 |     Gen 2 | Allocated |
|---------------- |-------- |----------:|---------:|---------:|----------:|----------:|----------:|-----------:|----------:|----------:|----------:|
|   ConcurrentBag | 2000000 | 220.26 ms | 6.165 ms | 7.100 ms | 221.81 ms | 209.17 ms | 233.85 ms |  1000.0000 | 1000.0000 | 1000.0000 |      8 MB |
| ConcurrentStack | 2000000 |  77.05 ms | 2.950 ms | 3.279 ms |  77.27 ms |  69.32 ms |  83.52 ms | 15000.0000 |         - |         - |  61.66 MB |
| ConcurrentQueue | 2000000 |  24.03 ms | 2.129 ms | 2.366 ms |  23.41 ms |  20.50 ms |  28.02 ms |          - |         - |         - |      1 MB |
