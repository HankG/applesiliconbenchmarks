``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |         Mean |      Error |     StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
|                Dictionary |  512 | 3,988.618 ns | 32.9091 ns | 27.4806 ns | 3,987.625 ns | 3,954.488 ns | 4,054.531 ns |     - |     - |     - |         - |
|                     Queue |  512 |     4.366 ns |  0.0813 ns |  0.0761 ns |     4.347 ns |     4.285 ns |     4.538 ns |     - |     - |     - |         - |
| Queue_EnqueueCountDequeue |  512 |    19.887 ns |  0.1588 ns |  0.1240 ns |    19.875 ns |    19.708 ns |    20.096 ns |     - |     - |     - |         - |
|                     Stack |  512 |   474.353 ns |  4.1794 ns |  3.9094 ns |   472.966 ns |   468.781 ns |   480.869 ns |     - |     - |     - |         - |
|                       Bag |  512 |    32.658 ns |  0.3353 ns |  0.3136 ns |    32.635 ns |    32.227 ns |    33.258 ns |     - |     - |     - |         - |
