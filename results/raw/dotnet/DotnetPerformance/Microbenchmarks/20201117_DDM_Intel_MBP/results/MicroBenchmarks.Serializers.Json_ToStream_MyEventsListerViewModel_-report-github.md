``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|--------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|--------:|--------:|----------:|
|                        Jil | 432.0 μs |  4.09 μs |  3.62 μs | 431.5 μs | 427.6 μs | 439.3 μs | 30.2419 |       - |       - | 123.84 KB |
|                   JSON.NET | 689.2 μs | 12.30 μs | 10.90 μs | 686.6 μs | 674.1 μs | 707.6 μs | 35.3261 |       - |       - | 146.45 KB |
|                   Utf8Json | 503.1 μs |  9.17 μs |  8.58 μs | 502.0 μs | 489.6 μs | 519.5 μs | 41.5842 | 41.5842 | 41.5842 | 245.04 KB |
| DataContractJsonSerializer | 753.7 μs |  8.24 μs |  7.71 μs | 751.3 μs | 743.4 μs | 770.4 μs |  2.9940 |       - |       - |  23.62 KB |
