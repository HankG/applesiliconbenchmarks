``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-VUUYSD : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
WarmupCount=1  

```
|                         Method |        Job | UnrollFactor | depth |          Mean |       Error |      StdDev |        Median |           Min |           Max |    Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------- |----------- |------------- |------ |--------------:|------------:|------------:|--------------:|--------------:|--------------:|---------:|------:|------:|----------:|
|                **CreateDirectory** | **Job-VUUYSD** |            **1** |     **?** |     **76.987 μs** |   **5.2124 μs** |   **5.7936 μs** |     **74.585 μs** |     **72.250 μs** |     **92.281 μs** |        **-** |     **-** |     **-** |     **362 B** |
|            GetCurrentDirectory | Job-NXBZBK |           16 |     ? |     11.641 μs |   0.2059 μs |   0.1825 μs |     11.583 μs |     11.410 μs |     11.973 μs |   0.0459 |     - |     - |     312 B |
|                         Exists | Job-NXBZBK |           16 |     ? |      3.591 μs |   0.0696 μs |   0.0745 μs |      3.564 μs |      3.504 μs |      3.799 μs |        - |     - |     - |         - |
|                 EnumerateFiles | Job-NXBZBK |           16 |     ? |  7,673.713 μs | 117.0200 μs | 109.4606 μs |  7,633.205 μs |  7,531.675 μs |  7,844.632 μs | 416.6667 |     - |     - | 1752324 B |
| **RecursiveCreateDeleteDirectory** | **Job-NXBZBK** |           **16** |    **10** |  **1,371.436 μs** |  **21.5862 μs** |  **19.1356 μs** |  **1,371.207 μs** |  **1,337.052 μs** |  **1,403.876 μs** |        **-** |     **-** |     **-** |   **10051 B** |
| **RecursiveCreateDeleteDirectory** | **Job-NXBZBK** |           **16** |   **100** | **14,867.008 μs** | **208.4547 μs** | **184.7896 μs** | **14,918.006 μs** | **14,544.125 μs** | **15,103.065 μs** |  **58.8235** |     **-** |     **-** |  **642162 B** |
