``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|           GetByte | 1,318.8 ns | 15.84 ns | 14.82 ns | 1,314.6 ns | 1,300.1 ns | 1,346.5 ns |      - |     - |     - |         - |
|          GetSByte | 1,411.7 ns | 17.24 ns | 16.12 ns | 1,404.9 ns | 1,389.2 ns | 1,436.6 ns |      - |     - |     - |         - |
|          GetInt16 | 1,371.2 ns | 18.17 ns | 17.00 ns | 1,372.9 ns | 1,348.2 ns | 1,402.3 ns |      - |     - |     - |         - |
|          GetInt32 |   971.5 ns | 12.47 ns | 11.66 ns |   969.6 ns |   957.0 ns |   989.9 ns |      - |     - |     - |         - |
|          GetInt64 | 1,081.7 ns | 13.06 ns | 11.57 ns | 1,080.0 ns | 1,067.3 ns | 1,100.7 ns |      - |     - |     - |         - |
|         GetUInt16 | 1,344.7 ns | 18.63 ns | 17.43 ns | 1,338.1 ns | 1,324.8 ns | 1,385.5 ns |      - |     - |     - |         - |
|         GetUInt32 |   903.4 ns | 17.91 ns | 15.87 ns |   895.6 ns |   887.9 ns |   936.5 ns |      - |     - |     - |         - |
|         GetUInt64 | 1,014.9 ns | 10.66 ns |  9.97 ns | 1,010.9 ns | 1,002.3 ns | 1,034.2 ns |      - |     - |     - |         - |
|         GetSingle | 4,697.1 ns | 58.47 ns | 54.70 ns | 4,675.3 ns | 4,621.8 ns | 4,798.5 ns |      - |     - |     - |         - |
|         GetDouble | 4,888.2 ns | 55.07 ns | 48.82 ns | 4,886.4 ns | 4,815.6 ns | 4,996.9 ns |      - |     - |     - |         - |
|        GetDecimal | 5,228.8 ns | 55.83 ns | 52.23 ns | 5,224.4 ns | 5,168.2 ns | 5,321.1 ns |      - |     - |     - |         - |
|       GetDateTime | 4,203.3 ns | 63.38 ns | 56.19 ns | 4,202.3 ns | 4,122.6 ns | 4,307.1 ns |      - |     - |     - |         - |
| GetDateTimeOffset | 6,199.5 ns | 68.60 ns | 64.17 ns | 6,184.1 ns | 6,108.9 ns | 6,311.3 ns |      - |     - |     - |         - |
|           GetGuid | 6,489.4 ns | 83.32 ns | 73.87 ns | 6,467.0 ns | 6,402.2 ns | 6,668.2 ns |      - |     - |     - |         - |
|         GetString | 3,925.4 ns | 71.50 ns | 66.88 ns | 3,914.5 ns | 3,825.4 ns | 4,040.0 ns | 2.6715 |     - |     - |   11200 B |
