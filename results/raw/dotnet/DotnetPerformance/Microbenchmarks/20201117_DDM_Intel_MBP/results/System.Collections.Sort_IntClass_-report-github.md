``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RTZWTM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=5000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|               Method | Size |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|                Array |  512 | 35.10 μs | 0.383 μs | 0.359 μs | 35.00 μs | 34.71 μs | 35.98 μs |      - |     - |     - |         - |
|  Array_ComparerClass |  512 | 41.44 μs | 0.446 μs | 0.418 μs | 41.36 μs | 40.87 μs | 42.26 μs |      - |     - |     - |      64 B |
| Array_ComparerStruct |  512 | 46.32 μs | 0.615 μs | 0.575 μs | 46.04 μs | 45.78 μs | 47.62 μs |      - |     - |     - |      88 B |
|     Array_Comparison |  512 | 41.72 μs | 0.290 μs | 0.242 μs | 41.68 μs | 41.37 μs | 42.15 μs |      - |     - |     - |       1 B |
|                 List |  512 | 35.29 μs | 0.158 μs | 0.132 μs | 35.31 μs | 35.08 μs | 35.50 μs |      - |     - |     - |         - |
|            LinqQuery |  512 | 62.08 μs | 0.793 μs | 0.742 μs | 61.88 μs | 60.99 μs | 63.22 μs | 2.4000 |     - |     - |   10552 B |
| LinqOrderByExtension |  512 | 61.91 μs | 0.806 μs | 0.754 μs | 61.64 μs | 60.83 μs | 63.13 μs | 2.4000 |     - |     - |   10552 B |
