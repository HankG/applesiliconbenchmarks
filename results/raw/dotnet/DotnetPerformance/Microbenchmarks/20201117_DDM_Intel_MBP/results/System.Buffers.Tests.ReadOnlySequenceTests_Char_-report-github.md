``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                          Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|              IterateTryGetArray |  6.948 ns | 0.0924 ns | 0.0819 ns |  6.926 ns |  6.843 ns |  7.097 ns |     - |     - |     - |         - |
|             IterateForEachArray | 16.863 ns | 0.2146 ns | 0.2007 ns | 16.829 ns | 16.577 ns | 17.253 ns |     - |     - |     - |         - |
|         IterateGetPositionArray | 27.555 ns | 0.3053 ns | 0.2706 ns | 27.544 ns | 27.200 ns | 28.094 ns |     - |     - |     - |         - |
|                      FirstArray |  8.907 ns | 0.1346 ns | 0.1124 ns |  8.923 ns |  8.722 ns |  9.048 ns |     - |     - |     - |         - |
|                  FirstSpanArray | 11.232 ns | 0.1046 ns | 0.0979 ns | 11.231 ns | 11.117 ns | 11.419 ns |     - |     - |     - |         - |
|                 FirstSpanMemory | 12.192 ns | 0.1604 ns | 0.1422 ns | 12.142 ns | 12.020 ns | 12.480 ns |     - |     - |     - |         - |
|          FirstSpanSingleSegment | 16.762 ns | 0.2174 ns | 0.2034 ns | 16.716 ns | 16.522 ns | 17.091 ns |     - |     - |     - |         - |
|            FirstSpanTenSegments | 17.053 ns | 0.1695 ns | 0.1324 ns | 17.059 ns | 16.891 ns | 17.283 ns |     - |     - |     - |         - |
|                      SliceArray |  6.612 ns | 0.0765 ns | 0.0716 ns |  6.577 ns |  6.518 ns |  6.743 ns |     - |     - |     - |         - |
|             IterateTryGetMemory | 22.716 ns | 0.3063 ns | 0.2865 ns | 22.684 ns | 22.328 ns | 23.155 ns |     - |     - |     - |         - |
|            IterateForEachMemory | 33.106 ns | 0.4880 ns | 0.4565 ns | 32.940 ns | 32.606 ns | 33.921 ns |     - |     - |     - |         - |
|        IterateGetPositionMemory | 45.650 ns | 0.5657 ns | 0.5291 ns | 45.467 ns | 45.028 ns | 46.630 ns |     - |     - |     - |         - |
|                     FirstMemory |  9.865 ns | 0.1302 ns | 0.1218 ns |  9.817 ns |  9.727 ns | 10.031 ns |     - |     - |     - |         - |
|                     SliceMemory |  8.882 ns | 0.1170 ns | 0.1094 ns |  8.857 ns |  8.745 ns |  9.104 ns |     - |     - |     - |         - |
|      IterateTryGetSingleSegment | 13.111 ns | 0.2212 ns | 0.2069 ns | 13.053 ns | 12.833 ns | 13.461 ns |     - |     - |     - |         - |
|     IterateForEachSingleSegment | 24.283 ns | 0.3963 ns | 0.3707 ns | 24.132 ns | 23.884 ns | 25.188 ns |     - |     - |     - |         - |
| IterateGetPositionSingleSegment | 27.249 ns | 0.3547 ns | 0.3318 ns | 27.130 ns | 26.786 ns | 27.930 ns |     - |     - |     - |         - |
|              FirstSingleSegment |  3.922 ns | 0.0481 ns | 0.0450 ns |  3.911 ns |  3.864 ns |  4.005 ns |     - |     - |     - |         - |
|              SliceSingleSegment |  7.343 ns | 0.1391 ns | 0.1301 ns |  7.291 ns |  7.212 ns |  7.602 ns |     - |     - |     - |         - |
|        IterateTryGetTenSegments | 56.679 ns | 1.1492 ns | 1.0749 ns | 56.196 ns | 55.621 ns | 58.992 ns |     - |     - |     - |         - |
|       IterateForEachTenSegments | 62.127 ns | 0.9356 ns | 0.8752 ns | 61.791 ns | 61.228 ns | 63.957 ns |     - |     - |     - |         - |
|   IterateGetPositionTenSegments | 64.924 ns | 0.8129 ns | 0.7604 ns | 64.718 ns | 64.007 ns | 66.139 ns |     - |     - |     - |         - |
|                FirstTenSegments |  3.545 ns | 0.0452 ns | 0.0423 ns |  3.532 ns |  3.494 ns |  3.628 ns |     - |     - |     - |         - |
|                SliceTenSegments | 21.001 ns | 0.2953 ns | 0.2618 ns | 20.980 ns | 20.582 ns | 21.407 ns |     - |     - |     - |         - |
