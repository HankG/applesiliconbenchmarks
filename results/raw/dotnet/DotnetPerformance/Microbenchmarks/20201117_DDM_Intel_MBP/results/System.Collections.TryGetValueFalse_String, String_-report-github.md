``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |   8.875 μs | 0.0902 μs | 0.0844 μs |   8.859 μs |   8.742 μs |   9.035 μs |     - |     - |     - |         - |
|               IDictionary |  512 |   9.872 μs | 0.1091 μs | 0.1020 μs |   9.891 μs |   9.676 μs |  10.049 μs |     - |     - |     - |         - |
|                SortedList |  512 | 267.330 μs | 3.7625 μs | 3.5195 μs | 265.842 μs | 263.530 μs | 273.657 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 299.313 μs | 7.6716 μs | 8.8346 μs | 295.403 μs | 289.925 μs | 318.319 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  12.867 μs | 0.2020 μs | 0.1890 μs |  12.851 μs |  12.617 μs |  13.190 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 |  26.649 μs | 0.4921 μs | 0.4603 μs |  26.501 μs |  25.957 μs |  27.318 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 273.200 μs | 3.3385 μs | 3.1229 μs | 272.062 μs | 268.448 μs | 278.622 μs |     - |     - |     - |         - |
