``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |     Mean |    Error |   StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|    EnterExit | 14.75 ns | 0.281 ns | 0.263 ns | 14.67 ns | 14.47 ns | 15.32 ns |     - |     - |     - |         - |
| TryEnterExit | 13.82 ns | 0.283 ns | 0.265 ns | 13.72 ns | 13.48 ns | 14.35 ns |     - |     - |     - |         - |
