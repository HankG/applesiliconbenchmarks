``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                        Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------ |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|-------:|------:|------:|----------:|
|                       Combine |     3.636 ns |  0.0784 ns |  0.0734 ns |     3.622 ns |     3.552 ns |     3.791 ns |      - |     - |     - |         - |
|                   GetFileName |    26.035 ns |  0.4512 ns |  0.4220 ns |    25.936 ns |    25.311 ns |    26.675 ns | 0.0114 |     - |     - |      48 B |
|              GetDirectoryName |    63.080 ns |  1.2483 ns |  1.1066 ns |    63.127 ns |    61.369 ns |    65.040 ns | 0.0285 |     - |     - |     120 B |
|               ChangeExtension |    23.902 ns |  0.5312 ns |  0.5217 ns |    23.800 ns |    22.999 ns |    24.931 ns | 0.0344 |     - |     - |     144 B |
|                  GetExtension |    15.987 ns |  0.2850 ns |  0.2666 ns |    15.865 ns |    15.656 ns |    16.476 ns | 0.0076 |     - |     - |      32 B |
|   GetFileNameWithoutExtension |    30.982 ns |  0.5026 ns |  0.4702 ns |    30.820 ns |    30.227 ns |    31.880 ns | 0.0095 |     - |     - |      40 B |
|    GetFullPathForLegacyLength |   340.202 ns |  4.8030 ns |  4.2577 ns |   340.428 ns |   332.395 ns |   348.979 ns |      - |     - |     - |         - |
| GetFullPathForTypicalLongPath |   864.251 ns |  8.3096 ns |  7.7728 ns |   861.081 ns |   856.100 ns |   882.166 ns |      - |     - |     - |         - |
|  GetFullPathForReallyLongPath | 1,655.150 ns | 21.5815 ns | 20.1874 ns | 1,652.454 ns | 1,620.926 ns | 1,686.073 ns |      - |     - |     - |         - |
|                   GetPathRoot |     3.247 ns |  0.2211 ns |  0.2458 ns |     3.184 ns |     3.032 ns |     3.759 ns |      - |     - |     - |         - |
|             GetRandomFileName |   249.366 ns |  5.0321 ns |  5.5932 ns |   249.719 ns |   241.802 ns |   262.444 ns | 0.0110 |     - |     - |      48 B |
|                   GetTempPath |   409.524 ns |  6.5612 ns |  5.8163 ns |   410.367 ns |   396.505 ns |   418.014 ns | 0.0280 |     - |     - |     120 B |
|                  HasExtension |     4.194 ns |  0.0684 ns |  0.0640 ns |     4.183 ns |     4.115 ns |     4.316 ns |      - |     - |     - |         - |
|                  IsPathRooted |     2.081 ns |  0.0362 ns |  0.0302 ns |     2.076 ns |     2.043 ns |     2.153 ns |      - |     - |     - |         - |
