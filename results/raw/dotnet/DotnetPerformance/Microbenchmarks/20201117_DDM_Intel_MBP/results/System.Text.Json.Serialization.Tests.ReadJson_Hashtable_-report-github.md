``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|-------:|------:|----------:|
|    DeserializeFromString | 69.81 μs | 1.250 μs | 1.170 μs | 69.60 μs | 68.47 μs | 72.18 μs | 10.7854 | 0.2765 |     - |  44.22 KB |
| DeserializeFromUtf8Bytes | 69.17 μs | 0.907 μs | 0.804 μs | 69.10 μs | 68.28 μs | 70.76 μs | 10.5978 | 0.2717 |     - |  44.22 KB |
|    DeserializeFromStream | 71.28 μs | 1.266 μs | 1.184 μs | 70.74 μs | 69.82 μs | 73.13 μs | 10.6502 |      - |     - |  44.29 KB |
