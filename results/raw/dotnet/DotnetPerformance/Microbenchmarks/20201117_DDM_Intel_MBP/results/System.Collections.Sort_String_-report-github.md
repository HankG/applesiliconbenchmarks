``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RTZWTM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=5000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|               Method | Size |     Mean |   Error |  StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |---------:|--------:|--------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|                Array |  512 | 250.6 μs | 0.77 μs | 0.64 μs | 250.6 μs | 249.7 μs | 251.8 μs |      - |     - |     - |       1 B |
|  Array_ComparerClass |  512 | 283.5 μs | 1.10 μs | 0.98 μs | 283.3 μs | 282.2 μs | 285.7 μs |      - |     - |     - |      65 B |
| Array_ComparerStruct |  512 | 263.6 μs | 1.05 μs | 0.93 μs | 263.4 μs | 262.2 μs | 265.8 μs |      - |     - |     - |      88 B |
|     Array_Comparison |  512 | 264.0 μs | 0.78 μs | 0.65 μs | 263.8 μs | 263.1 μs | 265.1 μs |      - |     - |     - |         - |
|                 List |  512 | 254.9 μs | 2.19 μs | 1.94 μs | 254.6 μs | 252.7 μs | 259.1 μs |      - |     - |     - |         - |
|            LinqQuery |  512 | 224.3 μs | 0.63 μs | 0.59 μs | 224.3 μs | 223.4 μs | 225.6 μs | 2.4000 |     - |     - |   10584 B |
| LinqOrderByExtension |  512 | 228.8 μs | 0.54 μs | 0.45 μs | 228.7 μs | 228.2 μs | 229.6 μs | 2.4000 |     - |     - |   10584 B |
