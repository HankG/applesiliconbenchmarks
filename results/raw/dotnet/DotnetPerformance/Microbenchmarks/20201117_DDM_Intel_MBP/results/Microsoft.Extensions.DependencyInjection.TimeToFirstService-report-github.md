``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|        Method |        Mode |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------- |------------ |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| **BuildProvider** |     **Dynamic** |   **794.0 ns** | **14.65 ns** | **12.99 ns** |   **790.4 ns** |   **779.5 ns** |   **822.0 ns** | **0.7625** |     **-** |     **-** |   **3.12 KB** |
|     Transient |     Dynamic | 2,410.3 ns | 37.56 ns | 31.36 ns | 2,418.1 ns | 2,340.9 ns | 2,448.8 ns | 1.1178 |     - |     - |   4.58 KB |
|        Scoped |     Dynamic | 2,702.2 ns | 52.92 ns | 51.97 ns | 2,690.4 ns | 2,633.9 ns | 2,797.0 ns | 1.2723 |     - |     - |   5.22 KB |
|     Singleton |     Dynamic | 2,431.3 ns | 42.33 ns | 37.52 ns | 2,430.6 ns | 2,372.3 ns | 2,490.4 ns | 1.1520 |     - |     - |   4.73 KB |
| **BuildProvider** | **Expressions** |   **795.4 ns** | **11.95 ns** | **10.59 ns** |   **795.8 ns** |   **779.2 ns** |   **812.0 ns** | **0.7622** |     **-** |     **-** |   **3.12 KB** |
|     Transient | Expressions | 2,346.9 ns | 45.71 ns | 42.76 ns | 2,339.1 ns | 2,290.6 ns | 2,428.1 ns | 1.1192 |     - |     - |   4.58 KB |
|        Scoped | Expressions | 2,700.5 ns | 44.02 ns | 41.18 ns | 2,696.3 ns | 2,626.0 ns | 2,777.8 ns | 1.2715 |     - |     - |   5.22 KB |
|     Singleton | Expressions | 2,443.0 ns | 37.38 ns | 34.96 ns | 2,430.5 ns | 2,397.1 ns | 2,504.9 ns | 1.1516 |     - |     - |   4.73 KB |
| **BuildProvider** |      **ILEmit** |   **797.4 ns** | **14.09 ns** | **13.18 ns** |   **794.1 ns** |   **773.9 ns** |   **825.3 ns** | **0.7608** |     **-** |     **-** |   **3.12 KB** |
|     Transient |      ILEmit | 2,518.5 ns | 49.55 ns | 53.01 ns | 2,506.2 ns | 2,448.1 ns | 2,623.9 ns | 1.1202 |     - |     - |   4.58 KB |
|        Scoped |      ILEmit | 2,743.3 ns | 53.20 ns | 49.76 ns | 2,724.9 ns | 2,692.3 ns | 2,833.8 ns | 1.2675 |     - |     - |   5.22 KB |
|     Singleton |      ILEmit | 2,443.0 ns | 44.94 ns | 42.04 ns | 2,426.2 ns | 2,395.5 ns | 2,529.4 ns | 1.1555 |     - |     - |   4.73 KB |
| **BuildProvider** |     **Runtime** |   **801.0 ns** | **15.85 ns** | **14.05 ns** |   **799.3 ns** |   **779.7 ns** |   **830.9 ns** | **0.7617** |     **-** |     **-** |   **3.12 KB** |
|     Transient |     Runtime | 2,408.5 ns | 36.70 ns | 32.53 ns | 2,398.8 ns | 2,352.3 ns | 2,465.9 ns | 1.1205 |     - |     - |   4.58 KB |
|        Scoped |     Runtime | 2,725.9 ns | 51.80 ns | 48.46 ns | 2,713.4 ns | 2,653.3 ns | 2,795.9 ns | 1.2711 |     - |     - |   5.22 KB |
|     Singleton |     Runtime | 2,490.5 ns | 47.03 ns | 44.00 ns | 2,487.2 ns | 2,438.1 ns | 2,569.0 ns | 1.1569 |     - |     - |   4.73 KB |
