``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    DeserializeFromString | 1.221 μs | 0.0206 μs | 0.0182 μs | 1.222 μs | 1.198 μs | 1.255 μs | 0.1023 |     - |     - |     448 B |
| DeserializeFromUtf8Bytes | 1.137 μs | 0.0162 μs | 0.0151 μs | 1.136 μs | 1.115 μs | 1.164 μs | 0.1047 |     - |     - |     448 B |
|    DeserializeFromStream | 1.501 μs | 0.0224 μs | 0.0209 μs | 1.496 μs | 1.475 μs | 1.548 μs | 0.1239 |     - |     - |     520 B |
