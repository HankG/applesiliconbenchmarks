``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|         Method |      name |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------- |---------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
| **CreateFromName** |       **RSA** | **469.6 ns** |  **5.28 ns** |  **4.41 ns** | **470.0 ns** | **462.7 ns** | **475.6 ns** | **0.1512** |      **-** |     **-** |     **640 B** |
| **CreateFromName** |    **SHA512** | **697.1 ns** | **15.55 ns** | **16.64 ns** | **695.3 ns** | **668.0 ns** | **731.2 ns** | **0.0961** | **0.0481** |     **-** |     **408 B** |
| **CreateFromName** | **X509Chain** | **391.1 ns** |  **6.96 ns** |  **6.83 ns** | **391.6 ns** | **381.7 ns** | **407.3 ns** | **0.1108** |      **-** |     **-** |     **464 B** |
