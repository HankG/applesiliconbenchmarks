``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|       SerializeToString |   882.6 ns | 14.58 ns | 13.64 ns |   881.5 ns |   864.4 ns |   906.3 ns | 0.1385 |     - |     - |     584 B |
|    SerializeToUtf8Bytes |   813.4 ns | 14.45 ns | 12.81 ns |   808.9 ns |   797.9 ns |   837.3 ns | 0.0916 |     - |     - |     384 B |
|       SerializeToStream |   881.2 ns | 10.60 ns |  9.91 ns |   878.0 ns |   867.7 ns |   902.2 ns | 0.0347 |     - |     - |     152 B |
| SerializeObjectProperty | 1,052.3 ns | 20.01 ns | 17.74 ns | 1,050.7 ns | 1,028.5 ns | 1,091.1 ns | 0.2177 |     - |     - |     912 B |
