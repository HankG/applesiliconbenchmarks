``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |  4.006 μs | 0.0543 μs | 0.0508 μs |  4.003 μs |  3.914 μs |  4.093 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  5.885 μs | 0.0980 μs | 0.1089 μs |  5.863 μs |  5.743 μs |  6.169 μs |     - |     - |     - |         - |
|                SortedList |  512 | 24.803 μs | 0.2443 μs | 0.2285 μs | 24.779 μs | 24.458 μs | 25.179 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 50.438 μs | 0.4735 μs | 0.4429 μs | 50.291 μs | 49.769 μs | 51.146 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  2.774 μs | 0.0282 μs | 0.0264 μs |  2.768 μs |  2.742 μs |  2.824 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 | 18.501 μs | 0.2773 μs | 0.2593 μs | 18.558 μs | 18.049 μs | 18.883 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 28.887 μs | 0.2985 μs | 0.2792 μs | 28.834 μs | 28.488 μs | 29.343 μs |     - |     - |     - |         - |
