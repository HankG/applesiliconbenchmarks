``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method | value |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------ |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|    **Parse** |  **-128** | **11.91 ns** | **0.152 ns** | **0.142 ns** | **11.88 ns** | **11.71 ns** | **12.16 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |  -128 | 12.36 ns | 0.179 ns | 0.168 ns | 12.28 ns | 12.16 ns | 12.68 ns |      - |     - |     - |         - |
| ToString |  -128 | 18.25 ns | 0.365 ns | 0.341 ns | 18.16 ns | 17.76 ns | 18.73 ns | 0.0076 |     - |     - |      32 B |
|    **Parse** |   **127** | **11.77 ns** | **0.180 ns** | **0.168 ns** | **11.72 ns** | **11.59 ns** | **12.12 ns** |      **-** |     **-** |     **-** |         **-** |
| TryParse |   127 | 12.47 ns | 0.115 ns | 0.108 ns | 12.47 ns | 12.26 ns | 12.65 ns |      - |     - |     - |         - |
| ToString |   127 | 11.03 ns | 0.214 ns | 0.190 ns | 11.02 ns | 10.72 ns | 11.42 ns | 0.0076 |     - |     - |      32 B |
