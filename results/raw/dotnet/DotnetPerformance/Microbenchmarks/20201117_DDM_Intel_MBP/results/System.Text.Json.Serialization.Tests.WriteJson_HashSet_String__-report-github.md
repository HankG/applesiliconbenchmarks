``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 6.825 μs | 0.0675 μs | 0.0632 μs | 6.836 μs | 6.691 μs | 6.915 μs | 1.4585 |     - |     - |    6136 B |
|    SerializeToUtf8Bytes | 6.441 μs | 0.1107 μs | 0.0981 μs | 6.423 μs | 6.318 μs | 6.665 μs | 0.7401 |     - |     - |    3176 B |
|       SerializeToStream | 6.368 μs | 0.0616 μs | 0.0576 μs | 6.353 μs | 6.295 μs | 6.487 μs | 0.0254 |     - |     - |     192 B |
| SerializeObjectProperty | 6.922 μs | 0.1356 μs | 0.1393 μs | 6.879 μs | 6.765 μs | 7.200 μs | 1.5415 |     - |     - |    6464 B |
