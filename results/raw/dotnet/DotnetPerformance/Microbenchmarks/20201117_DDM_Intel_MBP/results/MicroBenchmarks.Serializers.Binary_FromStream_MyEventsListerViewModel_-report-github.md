``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |   Gen 0 |   Gen 1 | Gen 2 | Allocated |
|---------------- |----------:|---------:|---------:|----------:|----------:|----------:|--------:|--------:|------:|----------:|
| BinaryFormatter |  69.99 μs | 0.801 μs | 0.669 μs |  70.10 μs |  69.07 μs |  71.45 μs | 13.3333 |  0.5556 |     - |  55.16 KB |
|    protobuf-net | 684.25 μs | 9.116 μs | 7.612 μs | 683.72 μs | 672.42 μs | 699.64 μs | 54.3478 | 10.8696 |     - | 227.03 KB |
|     MessagePack |  82.05 μs | 1.307 μs | 1.222 μs |  81.95 μs |  80.32 μs |  84.38 μs | 18.3634 |  1.2887 |     - |  75.67 KB |
