``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|  ArrayList |  4.730 ns | 0.1281 ns | 0.1198 ns |  4.663 ns |  4.598 ns |  4.934 ns | 0.0076 |     - |     - |      32 B |
|  Hashtable | 17.161 ns | 0.4069 ns | 0.3996 ns | 17.042 ns | 16.578 ns | 17.871 ns | 0.0401 |     - |     - |     168 B |
|      Queue | 20.903 ns | 0.4788 ns | 0.5322 ns | 20.881 ns | 20.292 ns | 22.004 ns | 0.0784 |     - |     - |     328 B |
|      Stack | 11.441 ns | 0.2379 ns | 0.2225 ns | 11.369 ns | 11.193 ns | 11.866 ns | 0.0325 |     - |     - |     136 B |
| SortedList | 24.395 ns | 0.5309 ns | 0.5215 ns | 24.233 ns | 23.743 ns | 25.407 ns | 0.0210 |     - |     - |      88 B |
