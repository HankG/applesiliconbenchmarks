``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                  Method |               Mean |            Error |           StdDev |             Median |                Min |                Max |       Gen 0 |     Gen 1 | Gen 2 |    Allocated |
|---------------------------------------- |-------------------:|-----------------:|-----------------:|-------------------:|-------------------:|-------------------:|------------:|----------:|------:|-------------:|
|                 ShortScheduleAndDispose |           185.1 ns |          5.07 ns |          5.84 ns |           184.7 ns |           175.0 ns |           199.5 ns |      0.0340 |         - |     - |        144 B |
|                  LongScheduleAndDispose |           178.5 ns |          3.49 ns |          3.59 ns |           177.4 ns |           173.7 ns |           184.9 ns |      0.0344 |         - |     - |        144 B |
|             ScheduleManyThenDisposeMany |   518,471,699.6 ns | 10,276,109.44 ns | 10,092,513.51 ns |   519,408,048.0 ns |   487,976,429.0 ns |   530,919,571.0 ns |  25000.0000 | 8000.0000 |     - |  144000288 B |
|                   SynchronousContention | 2,009,894,170.3 ns | 48,576,025.60 ns | 53,992,123.75 ns | 2,028,218,174.0 ns | 1,873,836,162.0 ns | 2,100,059,283.0 ns | 277000.0000 |         - |     - | 1152000984 B |
|                  AsynchronousContention | 2,263,717,246.8 ns | 73,866,332.44 ns | 85,064,578.28 ns | 2,270,326,495.0 ns | 2,142,595,073.0 ns | 2,413,676,625.0 ns | 276000.0000 |         - |     - | 1152002584 B |
| ShortScheduleAndDisposeWithFiringTimers |           241.1 ns |          7.86 ns |          9.05 ns |           241.1 ns |           226.7 ns |           260.1 ns |      0.0397 |         - |     - |        168 B |
