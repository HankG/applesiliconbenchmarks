``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                        Method |     Mean |     Error |    StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------ |---------:|----------:|----------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|                     SpanStart | 4.459 ns | 0.0407 ns | 0.0380 ns | 4.443 ns | 4.416 ns | 4.532 ns |     - |     - |     - |         - |
|               SpanStartLength | 4.114 ns | 0.0436 ns | 0.0387 ns | 4.115 ns | 4.042 ns | 4.178 ns |     - |     - |     - |         - |
|             ReadOnlySpanStart | 3.803 ns | 0.0620 ns | 0.0580 ns | 3.785 ns | 3.740 ns | 3.905 ns |     - |     - |     - |         - |
|       ReadOnlySpanStartLength | 3.828 ns | 0.0457 ns | 0.0428 ns | 3.811 ns | 3.772 ns | 3.917 ns |     - |     - |     - |         - |
|                   MemoryStart | 6.795 ns | 0.0937 ns | 0.0831 ns | 6.768 ns | 6.709 ns | 6.978 ns |     - |     - |     - |         - |
|               MemoryStartSpan | 9.226 ns | 0.1017 ns | 0.0951 ns | 9.211 ns | 9.102 ns | 9.454 ns |     - |     - |     - |         - |
|             MemoryStartLength | 6.941 ns | 0.0754 ns | 0.0669 ns | 6.915 ns | 6.843 ns | 7.059 ns |     - |     - |     - |         - |
|         MemoryStartLengthSpan | 9.247 ns | 0.1266 ns | 0.1184 ns | 9.218 ns | 9.104 ns | 9.511 ns |     - |     - |     - |         - |
|           ReadOnlyMemoryStart | 6.526 ns | 0.0777 ns | 0.0727 ns | 6.506 ns | 6.430 ns | 6.676 ns |     - |     - |     - |         - |
|       ReadOnlyMemoryStartSpan | 8.938 ns | 0.1135 ns | 0.1062 ns | 8.906 ns | 8.819 ns | 9.154 ns |     - |     - |     - |         - |
|     ReadOnlyMemoryStartLength | 6.584 ns | 0.1289 ns | 0.1266 ns | 6.593 ns | 6.338 ns | 6.751 ns |     - |     - |     - |         - |
| ReadOnlyMemoryStartLengthSpan | 9.052 ns | 0.1289 ns | 0.1143 ns | 9.046 ns | 8.883 ns | 9.219 ns |     - |     - |     - |         - |
|               MemorySpanStart | 6.602 ns | 0.0770 ns | 0.0720 ns | 6.582 ns | 6.516 ns | 6.737 ns |     - |     - |     - |         - |
|         MemorySpanStartLength | 7.106 ns | 0.0706 ns | 0.0625 ns | 7.099 ns | 7.027 ns | 7.250 ns |     - |     - |     - |         - |
|       ReadOnlyMemorySpanStart | 6.808 ns | 0.1339 ns | 0.1315 ns | 6.802 ns | 6.636 ns | 7.074 ns |     - |     - |     - |         - |
| ReadOnlyMemorySpanStartLength | 5.891 ns | 0.0772 ns | 0.0684 ns | 5.863 ns | 5.817 ns | 6.024 ns |     - |     - |     - |         - |
