``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |             Mean |          Error |         StdDev |           Median |              Min |              Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |-----------------:|---------------:|---------------:|-----------------:|-----------------:|-----------------:|------:|------:|------:|----------:|
|     TryWriteThenTryRead |         28.58 ns |       0.301 ns |       0.281 ns |         28.56 ns |         28.17 ns |         29.15 ns |     - |     - |     - |         - |
| WriteAsyncThenReadAsync |         58.67 ns |       0.648 ns |       0.606 ns |         58.43 ns |         57.78 ns |         59.55 ns |     - |     - |     - |         - |
| ReadAsyncThenWriteAsync |        103.33 ns |       1.158 ns |       1.083 ns |        103.20 ns |        101.77 ns |        105.07 ns |     - |     - |     - |         - |
|                PingPong | 13,079,740.99 ns | 172,554.834 ns | 161,407.899 ns | 13,087,282.17 ns | 12,825,229.06 ns | 13,356,892.00 ns |     - |     - |     - |   24886 B |
