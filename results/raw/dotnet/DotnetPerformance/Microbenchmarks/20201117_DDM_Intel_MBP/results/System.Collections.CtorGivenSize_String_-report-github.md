``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method | Size |     Mean |    Error |  StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------- |----- |---------:|---------:|--------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|                Array |  512 | 148.4 ns |  5.87 ns | 6.76 ns | 145.9 ns | 139.3 ns | 165.7 ns | 0.9837 |      - |     - |   4.02 KB |
|                 List |  512 | 166.9 ns |  3.82 ns | 4.40 ns | 165.7 ns | 160.2 ns | 175.5 ns | 0.9919 |      - |     - |   4.05 KB |
|              HashSet |  512 | 422.8 ns |  8.38 ns | 7.83 ns | 423.7 ns | 404.8 ns | 432.3 ns | 2.5173 |      - |     - |   10.3 KB |
|           Dictionary |  512 | 567.1 ns | 10.15 ns | 9.50 ns | 565.1 ns | 547.3 ns | 585.8 ns | 3.5120 | 0.0023 |     - |  14.38 KB |
|                Queue |  512 | 164.5 ns |  3.65 ns | 3.91 ns | 163.8 ns | 158.3 ns | 173.6 ns | 0.9936 |      - |     - |   4.06 KB |
|                Stack |  512 | 160.7 ns |  2.97 ns | 3.30 ns | 160.0 ns | 156.0 ns | 166.7 ns | 0.9918 |      - |     - |   4.05 KB |
|           SortedList |  512 | 314.8 ns |  5.48 ns | 5.12 ns | 314.3 ns | 307.0 ns | 323.3 ns | 1.9837 |      - |     - |   8.11 KB |
| ConcurrentDictionary |  512 | 211.5 ns |  4.10 ns | 3.84 ns | 212.2 ns | 205.1 ns | 219.0 ns | 1.0475 |      - |     - |   4.28 KB |
