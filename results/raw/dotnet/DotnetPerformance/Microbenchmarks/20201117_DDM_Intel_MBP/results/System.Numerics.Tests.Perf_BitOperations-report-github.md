``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|   LeadingZeroCount_uint | 490.6 ns |  8.99 ns |  8.41 ns | 489.9 ns | 478.2 ns | 507.6 ns |     - |     - |     - |         - |
|  LeadingZeroCount_ulong | 481.7 ns |  4.06 ns |  3.60 ns | 481.5 ns | 476.0 ns | 489.4 ns |     - |     - |     - |         - |
|               Log2_uint | 555.2 ns |  9.79 ns |  9.16 ns | 552.2 ns | 544.5 ns | 575.1 ns |     - |     - |     - |         - |
|              Log2_ulong | 691.2 ns |  9.12 ns |  8.54 ns | 688.6 ns | 680.3 ns | 703.6 ns |     - |     - |     - |         - |
|  TrailingZeroCount_uint | 486.4 ns |  8.82 ns |  8.67 ns | 482.2 ns | 476.9 ns | 504.8 ns |     - |     - |     - |         - |
| TrailingZeroCount_ulong | 485.2 ns |  7.49 ns |  7.00 ns | 482.3 ns | 476.5 ns | 500.1 ns |     - |     - |     - |         - |
|           PopCount_uint | 491.8 ns | 10.13 ns | 11.66 ns | 489.4 ns | 478.6 ns | 516.0 ns |     - |     - |     - |         - |
|          PopCount_ulong | 483.5 ns |  5.92 ns |  5.54 ns | 481.3 ns | 476.5 ns | 492.6 ns |     - |     - |     - |         - |
