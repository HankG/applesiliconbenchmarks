``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method | HasISupportLoggingScopeLogger | CaptureScopes |       Mean |     Error |    StdDev |     Median |        Min |        Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |------------------------------ |-------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|--------:|-------:|------:|------:|----------:|
|             **FilteredByLevel** |                         **False** |         **False** |   **7.841 ns** | **0.1288 ns** | **0.1142 ns** |   **7.851 ns** |   **7.681 ns** |   **8.088 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                         False |         False |  17.337 ns | 0.1832 ns | 0.1624 ns |  17.325 ns |  17.070 ns |  17.569 ns |  2.21 |    0.04 |      - |     - |     - |         - |
|                 NotFiltered |                         False |         False |  42.800 ns | 0.7419 ns | 0.6939 ns |  42.630 ns |  42.070 ns |  44.289 ns |  5.45 |    0.11 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                         False |         False |  51.168 ns | 1.0198 ns | 1.0473 ns |  51.291 ns |  48.671 ns |  52.502 ns |  6.52 |    0.16 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                         **False** |          **True** |   **7.323 ns** | **0.1410 ns** | **0.1319 ns** |   **7.264 ns** |   **7.148 ns** |   **7.568 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                         False |          True |  22.687 ns | 0.3665 ns | 0.3249 ns |  22.557 ns |  22.371 ns |  23.353 ns |  3.10 |    0.07 |      - |     - |     - |         - |
|                 NotFiltered |                         False |          True |  42.675 ns | 0.3715 ns | 0.3102 ns |  42.595 ns |  42.248 ns |  43.365 ns |  5.82 |    0.13 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                         False |          True |  60.310 ns | 0.6776 ns | 0.6339 ns |  60.242 ns |  59.663 ns |  61.691 ns |  8.24 |    0.17 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                          **True** |         **False** |   **7.621 ns** | **0.1733 ns** | **0.1702 ns** |   **7.555 ns** |   **7.375 ns** |   **8.031 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                          True |         False |  16.859 ns | 0.1713 ns | 0.1430 ns |  16.799 ns |  16.640 ns |  17.162 ns |  2.21 |    0.05 |      - |     - |     - |         - |
|                 NotFiltered |                          True |         False |  42.976 ns | 0.3681 ns | 0.3444 ns |  42.949 ns |  42.382 ns |  43.428 ns |  5.65 |    0.13 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                          True |         False |  54.404 ns | 1.5528 ns | 1.7882 ns |  54.836 ns |  49.048 ns |  55.571 ns |  7.13 |    0.35 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                          **True** |          **True** |   **7.549 ns** | **0.1308 ns** | **0.1159 ns** |   **7.508 ns** |   **7.415 ns** |   **7.838 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                          True |          True |  86.151 ns | 1.0447 ns | 0.9772 ns |  86.173 ns |  84.542 ns |  87.891 ns | 11.41 |    0.18 | 0.0285 |     - |     - |     120 B |
|                 NotFiltered |                          True |          True |  43.190 ns | 0.5794 ns | 0.5419 ns |  42.956 ns |  42.546 ns |  43.991 ns |  5.73 |    0.08 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                          True |          True | 122.426 ns | 2.1780 ns | 1.9308 ns | 122.472 ns | 119.073 ns | 125.580 ns | 16.22 |    0.37 | 0.0284 |     - |     - |     120 B |
