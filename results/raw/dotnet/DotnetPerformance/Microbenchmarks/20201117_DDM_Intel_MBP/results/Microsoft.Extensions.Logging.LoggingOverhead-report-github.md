``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                     Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|--------:|-------:|------:|------:|----------:|
|                NoArguments_FilteredByLevel | 16.679 ns | 0.1685 ns | 0.1407 ns | 16.644 ns | 16.512 ns | 16.979 ns |  2.31 |    0.02 |      - |     - |     - |         - |
|  NoArguments_DefineMessage_FilteredByLevel |  7.225 ns | 0.0579 ns | 0.0483 ns |  7.228 ns |  7.131 ns |  7.296 ns |  1.00 |    0.00 |      - |     - |     - |         - |
|                                NoArguments | 22.939 ns | 0.3438 ns | 0.3216 ns | 22.790 ns | 22.582 ns | 23.585 ns |  3.18 |    0.05 |      - |     - |     - |         - |
|                  NoArguments_DefineMessage | 25.849 ns | 0.3411 ns | 0.3024 ns | 25.741 ns | 25.510 ns | 26.460 ns |  3.58 |    0.05 |      - |     - |     - |         - |
|                               TwoArguments | 73.888 ns | 1.1218 ns | 0.9367 ns | 73.955 ns | 72.681 ns | 75.935 ns | 10.23 |    0.14 | 0.0152 |     - |     - |      64 B |
|               TwoArguments_FilteredByLevel | 62.844 ns | 1.0528 ns | 0.9847 ns | 62.763 ns | 61.487 ns | 64.423 ns |  8.69 |    0.17 | 0.0151 |     - |     - |      64 B |
|                 TwoArguments_DefineMessage | 43.083 ns | 0.3183 ns | 0.2658 ns | 43.163 ns | 42.590 ns | 43.493 ns |  5.96 |    0.06 |      - |     - |     - |         - |
| TwoArguments_DefineMessage_FilteredByLevel |  7.502 ns | 0.0956 ns | 0.0894 ns |  7.478 ns |  7.362 ns |  7.656 ns |  1.04 |    0.01 |      - |     - |     - |         - |
