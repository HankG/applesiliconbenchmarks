``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|----------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|--------:|-------:|------:|----------:|
|             List |  512 |  38.371 μs | 0.4889 μs | 0.4082 μs |  38.298 μs |  37.917 μs |  39.341 μs |  1.9721 |      - |     - |   8.19 KB |
|       LinkedList |  512 |  16.714 μs | 0.3314 μs | 0.3100 μs |  16.686 μs |  16.323 μs |  17.314 μs |  5.8738 | 0.0660 |     - |  24.04 KB |
|          HashSet |  512 |  28.847 μs | 0.5190 μs | 0.4855 μs |  28.763 μs |  28.183 μs |  29.747 μs |  8.2024 |      - |     - |  33.68 KB |
|       Dictionary |  512 |  28.362 μs | 0.5839 μs | 0.6248 μs |  28.206 μs |  27.397 μs |  29.845 μs | 11.3944 |      - |     - |  46.97 KB |
|       SortedList |  512 | 500.670 μs | 5.9004 μs | 5.5192 μs | 497.905 μs | 494.427 μs | 513.201 μs |  3.9063 |      - |     - |  16.38 KB |
|        SortedSet |  512 | 478.446 μs | 4.7241 μs | 3.9449 μs | 479.066 μs | 471.990 μs | 486.330 μs |  5.6818 |      - |     - |  24.05 KB |
| SortedDictionary |  512 | 526.837 μs | 6.8764 μs | 6.4322 μs | 525.487 μs | 518.981 μs | 537.931 μs |  6.2500 |      - |     - |  28.11 KB |
|            Stack |  512 |   3.970 μs | 0.0631 μs | 0.0620 μs |   3.960 μs |   3.900 μs |   4.102 μs |  1.9897 |      - |     - |   8.19 KB |
|            Queue |  512 |   4.832 μs | 0.0910 μs | 0.0851 μs |   4.846 μs |   4.699 μs |   5.012 μs |  1.9974 |      - |     - |    8.2 KB |
