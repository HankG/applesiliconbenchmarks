``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|        Method | numElements |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------- |------------ |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| **CallBlockCopy** |          **10** |  **6.323 ns** | **0.0975 ns** | **0.0912 ns** |  **6.275 ns** |  **6.236 ns** |  **6.510 ns** |     **-** |     **-** |     **-** |         **-** |
| **CallBlockCopy** |         **100** |  **7.734 ns** | **0.1007 ns** | **0.0892 ns** |  **7.708 ns** |  **7.601 ns** |  **7.871 ns** |     **-** |     **-** |     **-** |         **-** |
| **CallBlockCopy** |        **1000** | **20.855 ns** | **1.2421 ns** | **1.4305 ns** | **21.722 ns** | **19.064 ns** | **22.486 ns** |     **-** |     **-** |     **-** |         **-** |
