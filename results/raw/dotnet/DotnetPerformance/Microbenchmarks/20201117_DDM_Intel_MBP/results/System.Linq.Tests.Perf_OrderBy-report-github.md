``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | NumberOfPeople |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |--------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|         OrderByString |            512 | 236.51 μs | 2.338 μs | 2.073 μs | 236.11 μs | 233.27 μs | 239.74 μs | 1.8657 |     - |     - |  10.34 KB |
|      OrderByValueType |            512 |  46.18 μs | 1.150 μs | 1.325 μs |  46.06 μs |  44.30 μs |  48.35 μs | 2.5144 |     - |     - |   10.3 KB |
| OrderByCustomComparer |            512 | 157.60 μs | 3.116 μs | 3.200 μs | 157.82 μs | 153.81 μs | 162.90 μs | 4.3750 |     - |     - |   18.3 KB |
