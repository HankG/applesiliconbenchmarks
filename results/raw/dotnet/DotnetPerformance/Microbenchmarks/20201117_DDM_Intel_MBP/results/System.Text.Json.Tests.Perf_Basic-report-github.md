``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method | Formatted | SkipValidation | DataSize |           Mean |        Error |       StdDev |         Median |            Min |            Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------- |--------------- |--------- |---------------:|-------------:|-------------:|---------------:|---------------:|---------------:|-------:|------:|------:|----------:|
|  **WriteBasicUtf8** |     **False** |          **False** |       **10** |       **683.2 ns** |      **7.46 ns** |      **6.61 ns** |       **681.8 ns** |       **672.4 ns** |       **698.4 ns** | **0.0270** |     **-** |     **-** |     **120 B** |
| WriteBasicUtf16 |     False |          False |       10 |       757.8 ns |      9.71 ns |      9.08 ns |       755.2 ns |       745.2 ns |       776.7 ns | 0.0269 |     - |     - |     120 B |
|  **WriteBasicUtf8** |     **False** |          **False** |   **100000** | **1,683,605.3 ns** | **30,387.83 ns** | **28,424.79 ns** | **1,692,704.7 ns** | **1,646,280.2 ns** | **1,749,558.0 ns** |      **-** |     **-** |     **-** |     **122 B** |
| WriteBasicUtf16 |     False |          False |   100000 | 1,633,733.7 ns | 20,055.26 ns | 18,759.70 ns | 1,633,889.0 ns | 1,610,012.4 ns | 1,662,754.6 ns |      - |     - |     - |     122 B |
|  **WriteBasicUtf8** |     **False** |           **True** |       **10** |       **632.4 ns** |     **10.01 ns** |      **9.36 ns** |       **632.3 ns** |       **617.7 ns** |       **645.3 ns** | **0.0279** |     **-** |     **-** |     **120 B** |
| WriteBasicUtf16 |     False |           True |       10 |       684.0 ns |     13.53 ns |     12.66 ns |       680.3 ns |       669.1 ns |       712.4 ns | 0.0269 |     - |     - |     120 B |
|  **WriteBasicUtf8** |     **False** |           **True** |   **100000** | **1,550,120.1 ns** | **17,007.19 ns** | **15,908.54 ns** | **1,548,974.7 ns** | **1,528,567.3 ns** | **1,576,449.9 ns** |      **-** |     **-** |     **-** |     **122 B** |
| WriteBasicUtf16 |     False |           True |   100000 | 1,527,229.9 ns | 17,774.28 ns | 15,756.44 ns | 1,525,354.7 ns | 1,502,200.7 ns | 1,556,370.8 ns |      - |     - |     - |     122 B |
|  **WriteBasicUtf8** |      **True** |          **False** |       **10** |       **819.7 ns** |      **8.81 ns** |      **8.24 ns** |       **817.5 ns** |       **807.3 ns** |       **835.2 ns** | **0.0258** |     **-** |     **-** |     **120 B** |
| WriteBasicUtf16 |      True |          False |       10 |       878.5 ns |     16.87 ns |     14.96 ns |       874.3 ns |       863.6 ns |       919.0 ns | 0.0277 |     - |     - |     120 B |
|  **WriteBasicUtf8** |      **True** |          **False** |   **100000** | **2,095,922.2 ns** | **26,737.53 ns** | **25,010.30 ns** | **2,090,776.5 ns** | **2,063,161.3 ns** | **2,134,934.3 ns** |      **-** |     **-** |     **-** |     **122 B** |
| WriteBasicUtf16 |      True |          False |   100000 | 2,084,986.2 ns | 27,412.38 ns | 25,641.56 ns | 2,082,801.9 ns | 2,055,135.2 ns | 2,132,014.0 ns |      - |     - |     - |     138 B |
|  **WriteBasicUtf8** |      **True** |           **True** |       **10** |       **775.5 ns** |     **12.98 ns** |     **12.14 ns** |       **772.6 ns** |       **759.9 ns** |       **798.8 ns** | **0.0276** |     **-** |     **-** |     **120 B** |
| WriteBasicUtf16 |      True |           True |       10 |       854.3 ns |     13.17 ns |     12.32 ns |       850.4 ns |       837.6 ns |       874.4 ns | 0.0271 |     - |     - |     120 B |
|  **WriteBasicUtf8** |      **True** |           **True** |   **100000** | **1,941,880.7 ns** | **26,101.72 ns** | **23,138.49 ns** | **1,933,931.4 ns** | **1,915,894.4 ns** | **1,990,507.3 ns** |      **-** |     **-** |     **-** |     **122 B** |
| WriteBasicUtf16 |      True |           True |   100000 | 1,959,198.9 ns | 18,257.60 ns | 16,184.89 ns | 1,957,466.9 ns | 1,937,316.0 ns | 1,990,404.3 ns |      - |     - |     - |     138 B |
