``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|  Method | Size |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------- |----- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|     Pin |  512 | 40.67 ns | 0.504 ns | 0.446 ns | 40.53 ns | 40.13 ns | 41.44 ns |      - |     - |     - |         - |
| ToArray |  512 | 33.61 ns | 0.640 ns | 0.598 ns | 33.41 ns | 32.94 ns | 34.80 ns | 0.1280 |     - |     - |     536 B |
