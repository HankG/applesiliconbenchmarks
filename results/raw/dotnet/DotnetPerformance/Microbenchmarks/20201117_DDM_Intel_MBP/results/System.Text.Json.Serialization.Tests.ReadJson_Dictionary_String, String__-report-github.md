``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 |  Gen 1 | Gen 2 | Allocated |
|------------------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|-------:|------:|----------:|
|    DeserializeFromString | 22.88 μs | 0.427 μs | 0.379 μs | 22.80 μs | 22.40 μs | 23.72 μs | 6.1239 | 0.7205 |     - |  25.27 KB |
| DeserializeFromUtf8Bytes | 22.05 μs | 0.434 μs | 0.406 μs | 21.94 μs | 21.48 μs | 22.81 μs | 6.1461 | 0.6925 |     - |  25.27 KB |
|    DeserializeFromStream | 23.55 μs | 0.442 μs | 0.413 μs | 23.46 μs | 23.02 μs | 24.40 μs | 6.1400 | 0.1833 |     - |  25.34 KB |
