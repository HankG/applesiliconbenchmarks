``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|      Jil |   784.7 ns | 15.34 ns | 16.41 ns |   778.9 ns |   766.6 ns |   819.0 ns | 0.3307 |     - |     - |    1392 B |
| JSON.NET | 1,484.3 ns | 22.14 ns | 19.62 ns | 1,485.0 ns | 1,454.6 ns | 1,513.3 ns | 0.4143 |     - |     - |    1736 B |
| Utf8Json |   385.5 ns |  6.27 ns |  5.87 ns |   384.4 ns |   378.4 ns |   396.8 ns | 0.1008 |     - |     - |     424 B |
