``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                    Method |       Mean |      Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------ |-----------:|-----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                    ReadStructAndReverseBE |  10.456 ns |  0.1519 ns | 0.1421 ns |  10.403 ns |  10.316 ns |  10.775 ns |     - |     - |     - |         - |
|                    ReadStructAndReverseLE |   2.603 ns |  0.0495 ns | 0.0463 ns |   2.594 ns |   2.547 ns |   2.700 ns |     - |     - |     - |         - |
|                  ReadStructFieldByFieldBE |   9.035 ns |  0.1019 ns | 0.0851 ns |   9.018 ns |   8.931 ns |   9.251 ns |     - |     - |     - |         - |
|                  ReadStructFieldByFieldLE |   8.233 ns |  0.1671 ns | 0.1563 ns |   8.183 ns |   7.920 ns |   8.478 ns |     - |     - |     - |         - |
| ReadStructFieldByFieldUsingBitConverterLE |  17.977 ns |  0.2525 ns | 0.2362 ns |  17.872 ns |  17.745 ns |  18.435 ns |     - |     - |     - |         - |
| ReadStructFieldByFieldUsingBitConverterBE |  25.041 ns |  0.5117 ns | 0.4787 ns |  24.916 ns |  24.518 ns |  26.254 ns |     - |     - |     - |         - |
|                  MeasureReverseEndianness | 917.353 ns | 10.1688 ns | 9.5119 ns | 914.939 ns | 903.469 ns | 935.288 ns |     - |     - |     - |         - |
|                   MeasureReverseUsingNtoH | 913.732 ns |  9.2271 ns | 7.7051 ns | 913.776 ns | 905.637 ns | 934.171 ns |     - |     - |     - |         - |
