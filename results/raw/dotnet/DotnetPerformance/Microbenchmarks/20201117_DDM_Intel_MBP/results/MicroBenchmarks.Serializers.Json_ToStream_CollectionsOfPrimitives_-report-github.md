``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|--------:|------:|------:|----------:|
|                        Jil |   302.3 μs |  3.20 μs |  2.84 μs |   301.8 μs |   298.6 μs |   307.8 μs |       - |     - |     - |      97 B |
|                   JSON.NET |   380.3 μs |  3.44 μs |  2.68 μs |   381.0 μs |   376.3 μs |   384.0 μs | 24.2057 |     - |     - |  107136 B |
|                   Utf8Json |   195.2 μs |  3.20 μs |  3.14 μs |   194.2 μs |   191.9 μs |   202.9 μs |       - |     - |     - |    2761 B |
| DataContractJsonSerializer | 1,264.1 μs | 17.94 μs | 16.78 μs | 1,261.9 μs | 1,245.3 μs | 1,295.2 μs | 15.0754 |     - |     - |   74921 B |
