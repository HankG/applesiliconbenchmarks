``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                              Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------ |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
|                    CreateFromScalar | 0.0705 ns | 0.0263 ns | 0.0246 ns | 0.0610 ns | 0.0397 ns | 0.1210 ns |     - |     - |     - |         - |
|         CreateFromScalarXYBenchmark | 0.1165 ns | 0.0187 ns | 0.0166 ns | 0.1126 ns | 0.0926 ns | 0.1438 ns |     - |     - |     - |         - |
|                        OneBenchmark | 0.1288 ns | 0.0136 ns | 0.0120 ns | 0.1256 ns | 0.1151 ns | 0.1597 ns |     - |     - |     - |         - |
|                      UnitXBenchmark | 0.1167 ns | 0.0155 ns | 0.0145 ns | 0.1149 ns | 0.0968 ns | 0.1470 ns |     - |     - |     - |         - |
|                      UnitYBenchmark | 0.1118 ns | 0.0116 ns | 0.0103 ns | 0.1110 ns | 0.0912 ns | 0.1307 ns |     - |     - |     - |         - |
|                       ZeroBenchmark | 0.1108 ns | 0.0177 ns | 0.0166 ns | 0.1042 ns | 0.0865 ns | 0.1414 ns |     - |     - |     - |         - |
|                AddOperatorBenchmark | 0.2740 ns | 0.0199 ns | 0.0166 ns | 0.2711 ns | 0.2534 ns | 0.3091 ns |     - |     - |     - |         - |
|    DivideByVector2OperatorBenchmark | 0.4039 ns | 0.0171 ns | 0.0160 ns | 0.3994 ns | 0.3832 ns | 0.4373 ns |     - |     - |     - |         - |
|     DivideByScalarOperatorBenchmark | 0.3394 ns | 0.0250 ns | 0.0222 ns | 0.3331 ns | 0.3099 ns | 0.3776 ns |     - |     - |     - |         - |
|           EqualityOperatorBenchmark | 0.4435 ns | 0.0210 ns | 0.0197 ns | 0.4341 ns | 0.4222 ns | 0.4847 ns |     - |     - |     - |         - |
|         InequalityOperatorBenchmark | 0.4494 ns | 0.0171 ns | 0.0160 ns | 0.4473 ns | 0.4222 ns | 0.4741 ns |     - |     - |     - |         - |
|           MultiplyOperatorBenchmark | 0.2693 ns | 0.0178 ns | 0.0158 ns | 0.2636 ns | 0.2494 ns | 0.3024 ns |     - |     - |     - |         - |
|   MultiplyByScalarOperatorBenchmark | 0.3281 ns | 0.0209 ns | 0.0195 ns | 0.3205 ns | 0.3070 ns | 0.3695 ns |     - |     - |     - |         - |
|           SubtractOperatorBenchmark | 0.2865 ns | 0.0183 ns | 0.0153 ns | 0.2819 ns | 0.2644 ns | 0.3130 ns |     - |     - |     - |         - |
|             NegateOperatorBenchmark | 0.1381 ns | 0.0232 ns | 0.0206 ns | 0.1288 ns | 0.1163 ns | 0.1854 ns |     - |     - |     - |         - |
|                        AbsBenchmark | 0.3350 ns | 0.0200 ns | 0.0187 ns | 0.3251 ns | 0.3144 ns | 0.3762 ns |     - |     - |     - |         - |
|                AddFunctionBenchmark | 0.2440 ns | 0.0216 ns | 0.0202 ns | 0.2470 ns | 0.2186 ns | 0.2871 ns |     - |     - |     - |         - |
|                      ClampBenchmark | 0.3128 ns | 0.0184 ns | 0.0173 ns | 0.3093 ns | 0.2910 ns | 0.3427 ns |     - |     - |     - |         - |
|                   DistanceBenchmark | 0.5315 ns | 0.0223 ns | 0.0208 ns | 0.5334 ns | 0.5003 ns | 0.5693 ns |     - |     - |     - |         - |
|            DistanceSquaredBenchmark | 0.6009 ns | 0.0242 ns | 0.0226 ns | 0.5956 ns | 0.5705 ns | 0.6446 ns |     - |     - |     - |         - |
|            DivideByVector2Benchmark | 0.3985 ns | 0.0226 ns | 0.0211 ns | 0.3914 ns | 0.3709 ns | 0.4433 ns |     - |     - |     - |         - |
|             DivideByScalarBenchmark | 7.4087 ns | 0.0901 ns | 0.0843 ns | 7.3866 ns | 7.3078 ns | 7.5526 ns |     - |     - |     - |         - |
|                        DotBenchmark | 0.4115 ns | 0.0155 ns | 0.0130 ns | 0.4097 ns | 0.3898 ns | 0.4334 ns |     - |     - |     - |         - |
|                     EqualsBenchmark | 0.1130 ns | 0.0164 ns | 0.0153 ns | 0.1101 ns | 0.0915 ns | 0.1404 ns |     - |     - |     - |         - |
|                GetHashCodeBenchmark | 2.5634 ns | 0.0457 ns | 0.0427 ns | 2.5531 ns | 2.5183 ns | 2.6507 ns |     - |     - |     - |         - |
|                     LengthBenchmark | 0.2813 ns | 0.0223 ns | 0.0209 ns | 0.2837 ns | 0.2424 ns | 0.3119 ns |     - |     - |     - |         - |
|              LengthSquaredBenchmark | 0.1843 ns | 0.0164 ns | 0.0153 ns | 0.1835 ns | 0.1623 ns | 0.2133 ns |     - |     - |     - |         - |
|                       LerpBenchmark | 6.6166 ns | 0.0817 ns | 0.0764 ns | 6.5734 ns | 6.5184 ns | 6.7464 ns |     - |     - |     - |         - |
|                        MaxBenchmark | 0.2729 ns | 0.0139 ns | 0.0130 ns | 0.2708 ns | 0.2574 ns | 0.2985 ns |     - |     - |     - |         - |
|                        MinBenchmark | 0.2830 ns | 0.0221 ns | 0.0207 ns | 0.2747 ns | 0.2634 ns | 0.3221 ns |     - |     - |     - |         - |
|           MultiplyFunctionBenchmark | 0.2447 ns | 0.0231 ns | 0.0216 ns | 0.2426 ns | 0.2166 ns | 0.2810 ns |     - |     - |     - |         - |
|                     NegateBenchmark | 5.2712 ns | 0.0877 ns | 0.0820 ns | 5.2386 ns | 5.1886 ns | 5.4601 ns |     - |     - |     - |         - |
|                  NormalizeBenchmark | 0.8272 ns | 0.0195 ns | 0.0173 ns | 0.8244 ns | 0.8058 ns | 0.8595 ns |     - |     - |     - |         - |
|                    ReflectBenchmark | 0.7431 ns | 0.0216 ns | 0.0202 ns | 0.7353 ns | 0.7161 ns | 0.7782 ns |     - |     - |     - |         - |
|                 SquareRootBenchmark | 0.1122 ns | 0.0191 ns | 0.0159 ns | 0.1069 ns | 0.0969 ns | 0.1530 ns |     - |     - |     - |         - |
|           SubtractFunctionBenchmark | 0.2404 ns | 0.0167 ns | 0.0156 ns | 0.2383 ns | 0.2139 ns | 0.2642 ns |     - |     - |     - |         - |
|       TransformByMatrix3x2Benchmark | 3.1052 ns | 0.0555 ns | 0.0492 ns | 3.0895 ns | 3.0533 ns | 3.2131 ns |     - |     - |     - |         - |
|       TransformByMatrix4x4Benchmark | 4.4043 ns | 0.0631 ns | 0.0590 ns | 4.3825 ns | 4.3431 ns | 4.5028 ns |     - |     - |     - |         - |
|      TransformByQuaternionBenchmark | 2.3113 ns | 0.0467 ns | 0.0437 ns | 2.2935 ns | 2.2627 ns | 2.3983 ns |     - |     - |     - |         - |
| TransformNormalByMatrix3x2Benchmark | 2.5846 ns | 0.0219 ns | 0.0182 ns | 2.5827 ns | 2.5584 ns | 2.6302 ns |     - |     - |     - |         - |
| TransformNormalByMatrix4x4Benchmark | 4.0022 ns | 0.0546 ns | 0.0484 ns | 3.9959 ns | 3.9274 ns | 4.1117 ns |     - |     - |     - |         - |
