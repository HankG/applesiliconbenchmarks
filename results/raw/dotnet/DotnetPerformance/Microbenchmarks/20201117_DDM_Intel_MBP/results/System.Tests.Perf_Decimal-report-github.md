``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |      value |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |----------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|      **Add** |          **?** |  **7.390 ns** | **0.1115 ns** | **0.1043 ns** |  **7.359 ns** |  **7.266 ns** |  **7.606 ns** |      **-** |     **-** |     **-** |         **-** |
| Subtract |          ? |  7.650 ns | 0.0829 ns | 0.0775 ns |  7.657 ns |  7.534 ns |  7.806 ns |      - |     - |     - |         - |
| Multiply |          ? |  6.514 ns | 0.1122 ns | 0.1049 ns |  6.481 ns |  6.377 ns |  6.685 ns |      - |     - |     - |         - |
|   Divide |          ? | 59.930 ns | 0.8552 ns | 0.7999 ns | 59.615 ns | 58.824 ns | 61.434 ns |      - |     - |     - |         - |
|      Mod |          ? | 11.979 ns | 0.1508 ns | 0.1410 ns | 11.927 ns | 11.809 ns | 12.216 ns |      - |     - |     - |         - |
|    Floor |          ? |  9.440 ns | 0.1608 ns | 0.1504 ns |  9.358 ns |  9.277 ns |  9.666 ns |      - |     - |     - |         - |
|    Round |          ? | 14.460 ns | 0.1297 ns | 0.1214 ns | 14.416 ns | 14.264 ns | 14.678 ns |      - |     - |     - |         - |
| **ToString** | **123456.789** | **67.198 ns** | **1.1727 ns** | **1.0396 ns** | **67.625 ns** | **65.576 ns** | **68.536 ns** | **0.0113** |     **-** |     **-** |      **48 B** |
|    Parse | 123456.789 | 79.525 ns | 1.0702 ns | 1.0011 ns | 78.953 ns | 78.547 ns | 81.276 ns |      - |     - |     - |         - |
| TryParse | 123456.789 | 75.010 ns | 0.8869 ns | 0.7862 ns | 74.916 ns | 74.021 ns | 76.712 ns |      - |     - |     - |         - |
