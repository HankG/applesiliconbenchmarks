``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method | Size |       Mean |     Error |    StdDev |     Median |        Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|----------:|------:|------:|------:|----------:|
|                Dictionary |  512 |   9.902 μs | 0.1173 μs | 0.0980 μs |   9.873 μs |   9.747 μs |  10.05 μs |     - |     - |     - |         - |
|               IDictionary |  512 |  11.773 μs | 0.1720 μs | 0.1609 μs |  11.687 μs |  11.571 μs |  12.07 μs |     - |     - |     - |         - |
|                SortedList |  512 | 269.558 μs | 3.8123 μs | 3.3795 μs | 268.341 μs | 266.211 μs | 275.33 μs |     - |     - |     - |         - |
|          SortedDictionary |  512 | 295.171 μs | 2.9606 μs | 2.4723 μs | 295.697 μs | 290.047 μs | 299.23 μs |     - |     - |     - |         - |
|      ConcurrentDictionary |  512 |  13.599 μs | 0.1947 μs | 0.1821 μs |  13.625 μs |  13.356 μs |  13.95 μs |     - |     - |     - |         - |
|       ImmutableDictionary |  512 |  26.884 μs | 0.4556 μs | 0.3805 μs |  26.837 μs |  26.280 μs |  27.67 μs |     - |     - |     - |         - |
| ImmutableSortedDictionary |  512 | 271.888 μs | 4.1160 μs | 3.8501 μs | 270.316 μs | 267.016 μs | 277.65 μs |     - |     - |     - |         - |
