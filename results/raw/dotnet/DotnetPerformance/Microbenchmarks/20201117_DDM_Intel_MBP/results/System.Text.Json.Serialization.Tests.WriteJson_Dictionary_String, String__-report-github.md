``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                  Method |     Mean |    Error |   StdDev |   Median |       Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------ |---------:|---------:|---------:|---------:|----------:|---------:|-------:|------:|------:|----------:|
|       SerializeToString | 10.86 μs | 0.155 μs | 0.145 μs | 10.87 μs | 10.594 μs | 11.17 μs | 2.8390 |     - |     - |   12008 B |
|    SerializeToUtf8Bytes | 10.23 μs | 0.203 μs | 0.180 μs | 10.19 μs |  9.961 μs | 10.58 μs | 1.4423 |     - |     - |    6096 B |
|       SerializeToStream | 10.81 μs | 0.118 μs | 0.110 μs | 10.78 μs | 10.644 μs | 11.00 μs |      - |     - |     - |     152 B |
| SerializeObjectProperty | 11.16 μs | 0.143 μs | 0.120 μs | 11.14 μs | 11.020 μs | 11.42 μs | 2.9276 |     - |     - |   12336 B |
