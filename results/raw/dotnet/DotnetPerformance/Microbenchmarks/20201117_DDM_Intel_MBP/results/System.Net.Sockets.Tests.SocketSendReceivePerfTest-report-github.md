``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                         Method |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|                 SendAsyncThenReceiveAsync_Task | 212.9 ms | 3.96 ms | 3.70 ms | 212.7 ms | 206.2 ms | 219.7 ms |     - |     - |     - |     592 B |
|                 ReceiveAsyncThenSendAsync_Task | 226.4 ms | 2.04 ms | 1.80 ms | 226.5 ms | 224.2 ms | 230.3 ms |     - |     - |     - |     608 B |
| SendAsyncThenReceiveAsync_SocketAsyncEventArgs | 211.1 ms | 4.12 ms | 4.75 ms | 211.3 ms | 203.8 ms | 217.8 ms |     - |     - |     - |    1368 B |
| ReceiveAsyncThenSendAsync_SocketAsyncEventArgs | 222.5 ms | 3.57 ms | 3.34 ms | 223.4 ms | 217.5 ms | 227.5 ms |     - |     - |     - |    1376 B |
