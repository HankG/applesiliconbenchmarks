``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-NXBZBK : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|  CreateFromVector3WithScalarBenchmark |  3.9364 ns | 0.1028 ns | 0.0912 ns |  3.9061 ns |  3.8507 ns |  4.1399 ns |     - |     - |     - |         - |
|         CreateFromScalarXYZWBenchmark |  4.5817 ns | 0.0521 ns | 0.0488 ns |  4.5763 ns |  4.4947 ns |  4.6454 ns |     - |     - |     - |         - |
|                     IdentityBenchmark |  4.4455 ns | 0.1550 ns | 0.1785 ns |  4.3905 ns |  4.2435 ns |  4.9097 ns |     - |     - |     - |         - |
|                   IsIdentityBenchmark |  0.0000 ns | 0.0000 ns | 0.0000 ns |  0.0000 ns |  0.0000 ns |  0.0000 ns |     - |     - |     - |         - |
|                  AddOperatorBenchmark |  6.1996 ns | 0.0810 ns | 0.0758 ns |  6.1668 ns |  6.1024 ns |  6.3374 ns |     - |     - |     - |         - |
|             DivisionOperatorBenchmark | 22.8235 ns | 0.3818 ns | 0.3572 ns | 22.7172 ns | 22.3944 ns | 23.4773 ns |     - |     - |     - |         - |
|             EqualityOperatorBenchmark |  3.0306 ns | 0.1041 ns | 0.1198 ns |  3.0128 ns |  2.8864 ns |  3.2898 ns |     - |     - |     - |         - |
|           InequalityOperatorBenchmark |  2.9425 ns | 0.0483 ns | 0.0428 ns |  2.9311 ns |  2.8991 ns |  3.0303 ns |     - |     - |     - |         - |
| MultiplyByQuaternionOperatorBenchmark | 15.9239 ns | 0.1940 ns | 0.1815 ns | 15.9166 ns | 15.6648 ns | 16.2400 ns |     - |     - |     - |         - |
|     MultiplyByScalarOperatorBenchmark |  5.3894 ns | 0.1265 ns | 0.1406 ns |  5.3610 ns |  5.1944 ns |  5.6777 ns |     - |     - |     - |         - |
|          SubtractionOperatorBenchmark |  6.1821 ns | 0.0965 ns | 0.0855 ns |  6.1678 ns |  6.0715 ns |  6.3232 ns |     - |     - |     - |         - |
|             NegationOperatorBenchmark |  5.2705 ns | 0.0642 ns | 0.0600 ns |  5.2520 ns |  5.1865 ns |  5.3810 ns |     - |     - |     - |         - |
|                          AddBenchmark |  6.1643 ns | 0.0825 ns | 0.0772 ns |  6.1553 ns |  6.0729 ns |  6.3003 ns |     - |     - |     - |         - |
|                  ConcatenateBenchmark | 15.9889 ns | 0.2847 ns | 0.2663 ns | 15.8855 ns | 15.6596 ns | 16.5394 ns |     - |     - |     - |         - |
|                    ConjugateBenchmark |  5.3165 ns | 0.1309 ns | 0.1225 ns |  5.3033 ns |  5.1731 ns |  5.6156 ns |     - |     - |     - |         - |
|          CreateFromAxisAngleBenchmark | 10.2922 ns | 0.2065 ns | 0.1931 ns | 10.2165 ns | 10.0752 ns | 10.7609 ns |     - |     - |     - |         - |
|     CreateFromRotationMatrixBenchmark |  8.9886 ns | 0.1500 ns | 0.1403 ns |  8.9262 ns |  8.8446 ns |  9.2635 ns |     - |     - |     - |         - |
|       CreateFromYawPitchRollBenchmark | 26.8353 ns | 0.3889 ns | 0.3448 ns | 26.7446 ns | 26.2700 ns | 27.5883 ns |     - |     - |     - |         - |
|                       DivideBenchmark |  6.1831 ns | 0.0894 ns | 0.0792 ns |  6.1686 ns |  6.0817 ns |  6.3390 ns |     - |     - |     - |         - |
|                          DotBenchmark |  1.4173 ns | 0.0358 ns | 0.0335 ns |  1.4068 ns |  1.3764 ns |  1.4889 ns |     - |     - |     - |         - |
|                       EqualsBenchmark |  2.4076 ns | 0.0488 ns | 0.0433 ns |  2.3896 ns |  2.3683 ns |  2.5085 ns |     - |     - |     - |         - |
|                      InverseBenchmark | 17.3636 ns | 0.2089 ns | 0.1852 ns | 17.3158 ns | 17.0956 ns | 17.7596 ns |     - |     - |     - |         - |
|                       LengthBenchmark |  0.0060 ns | 0.0107 ns | 0.0100 ns |  0.0000 ns |  0.0000 ns |  0.0303 ns |     - |     - |     - |         - |
|                LengthSquaredBenchmark |  0.0061 ns | 0.0100 ns | 0.0094 ns |  0.0006 ns |  0.0000 ns |  0.0243 ns |     - |     - |     - |         - |
|                         LerpBenchmark | 24.0738 ns | 0.3685 ns | 0.3267 ns | 23.9760 ns | 23.6941 ns | 24.8895 ns |     - |     - |     - |         - |
|         MultiplyByQuaternionBenchmark | 15.6326 ns | 0.2123 ns | 0.1986 ns | 15.5820 ns | 15.3841 ns | 16.0247 ns |     - |     - |     - |         - |
|             MultiplyByScalarBenchmark |  5.2733 ns | 0.0847 ns | 0.0792 ns |  5.2648 ns |  5.1759 ns |  5.4059 ns |     - |     - |     - |         - |
|                       NegateBenchmark |  5.2347 ns | 0.0642 ns | 0.0536 ns |  5.2252 ns |  5.1706 ns |  5.3717 ns |     - |     - |     - |         - |
|                    NormalizeBenchmark | 19.9108 ns | 0.2563 ns | 0.2141 ns | 19.8793 ns | 19.6132 ns | 20.4678 ns |     - |     - |     - |         - |
|                        SlerpBenchmark | 24.2685 ns | 0.4556 ns | 0.4039 ns | 24.2474 ns | 23.6048 ns | 25.0767 ns |     - |     - |     - |         - |
|                     SubtractBenchmark |  6.1868 ns | 0.0736 ns | 0.0689 ns |  6.1791 ns |  6.0961 ns |  6.3129 ns |     - |     - |     - |         - |
