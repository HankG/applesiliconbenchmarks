``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |      Gen 0 | Gen 1 | Gen 2 |   Allocated |
|------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-----------:|------:|------:|------------:|
|        Where00LinqQueryX |   687.9 ms |  1.86 ms |  1.45 ms |   687.6 ms |   685.4 ms |   691.4 ms | 34000.0000 |     - |     - |  72000288 B |
|       Where00LinqMethodX |   834.5 ms |  0.86 ms |  0.67 ms |   834.3 ms |   833.3 ms |   835.7 ms | 34000.0000 |     - |     - |  72000288 B |
|              Where00ForX |   474.5 ms |  3.55 ms |  3.32 ms |   476.2 ms |   469.8 ms |   478.8 ms | 84000.0000 |     - |     - | 176000576 B |
|        Where01LinqQueryX |   324.5 ms |  0.49 ms |  0.41 ms |   324.5 ms |   323.6 ms |   325.3 ms |  8000.0000 |     - |     - |  18000576 B |
|       Where01LinqMethodX |   305.3 ms |  1.27 ms |  1.06 ms |   305.5 ms |   304.1 ms |   307.7 ms |  8000.0000 |     - |     - |  18000288 B |
| Where01LinqMethodNestedX |   550.8 ms |  8.12 ms |  6.78 ms |   547.6 ms |   541.4 ms |   559.5 ms | 28000.0000 |     - |     - |  60001328 B |
|              Where01ForX |   230.0 ms |  0.50 ms |  0.46 ms |   229.9 ms |   229.2 ms |   231.0 ms | 10000.0000 |     - |     - |  22005520 B |
|       Count00LinqMethodX | 1,881.3 ms | 25.35 ms | 19.79 ms | 1,887.6 ms | 1,856.3 ms | 1,917.6 ms | 19000.0000 |     - |     - |  40000288 B |
|              Count00ForX |   374.7 ms |  1.75 ms |  1.64 ms |   374.4 ms |   372.4 ms |   377.1 ms |          - |     - |     - |       288 B |
|        Order00LinqQueryX |   141.7 ms |  5.99 ms |  6.15 ms |   144.3 ms |   133.3 ms |   152.9 ms | 28000.0000 |     - |     - |  58600288 B |
|       Order00LinqMethodX |   145.6 ms |  3.93 ms |  3.86 ms |   147.7 ms |   138.9 ms |   149.8 ms | 28000.0000 |     - |     - |  58600288 B |
|           Order00ManualX |   139.0 ms |  2.70 ms |  2.11 ms |   139.5 ms |   132.6 ms |   140.9 ms |  7000.0000 |     - |     - |  16000288 B |
