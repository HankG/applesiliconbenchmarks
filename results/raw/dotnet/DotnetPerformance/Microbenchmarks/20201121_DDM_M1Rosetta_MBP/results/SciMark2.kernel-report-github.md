``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |   Error |  StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|--------:|--------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|        benchFFT |   752.8 ms | 0.25 ms | 0.22 ms |   752.8 ms |   752.4 ms |   753.1 ms |     - |     - |     - |     288 B |
|        benchSOR | 1,261.9 ms | 0.33 ms | 0.31 ms | 1,261.8 ms | 1,261.5 ms | 1,262.5 ms |     - |     - |     - |     288 B |
| benchMonteCarlo |   710.7 ms | 3.61 ms | 3.38 ms |   712.0 ms |   703.9 ms |   715.0 ms |     - |     - |     - |     464 B |
| benchSparseMult |   558.7 ms | 0.31 ms | 0.28 ms |   558.7 ms |   558.2 ms |   559.3 ms |     - |     - |     - |     288 B |
|     benchmarkLU |   570.3 ms | 0.86 ms | 0.76 ms |   570.1 ms |   569.1 ms |   571.7 ms |     - |     - |     - |     288 B |
