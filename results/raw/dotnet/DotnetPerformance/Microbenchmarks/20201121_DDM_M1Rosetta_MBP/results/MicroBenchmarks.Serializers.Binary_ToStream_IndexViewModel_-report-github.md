``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| BinaryFormatter | 17.18 μs | 1.134 μs | 1.061 μs | 17.54 μs | 15.40 μs | 18.83 μs | 4.4101 |     - |     - |   10684 B |
|    protobuf-net | 23.27 μs | 0.324 μs | 0.253 μs | 23.21 μs | 22.76 μs | 23.57 μs | 1.9627 |     - |     - |    5011 B |
|     MessagePack | 16.51 μs | 3.925 μs | 4.199 μs | 14.35 μs | 13.75 μs | 29.21 μs |      - |     - |     - |     288 B |
