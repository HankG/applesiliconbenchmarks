``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|---------:|--------:|------:|----------:|
|                        Jil |   512.5 μs |   1.83 μs |   1.71 μs |   512.9 μs |   509.6 μs |   514.9 μs |  58.4677 | 18.1452 |     - | 170.81 KB |
|                   JSON.NET |   655.8 μs |  10.74 μs |  10.05 μs |   661.9 μs |   637.0 μs |   668.0 μs | 111.9593 | 35.6234 |     - | 302.85 KB |
|                   Utf8Json |   319.8 μs |   2.25 μs |   1.99 μs |   320.6 μs |   316.0 μs |   321.9 μs |  78.1050 | 25.6082 |     - | 175.05 KB |
| DataContractJsonSerializer | 4,605.7 μs | 259.81 μs | 299.20 μs | 4,536.5 μs | 4,301.1 μs | 5,382.4 μs | 327.5862 | 86.2069 |     - | 784.65 KB |
