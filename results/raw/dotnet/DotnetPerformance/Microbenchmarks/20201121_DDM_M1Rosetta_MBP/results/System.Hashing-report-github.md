``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|            Method | BytesCount |         Mean |     Error |    StdDev |       Median |          Min |          Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------ |----------- |-------------:|----------:|----------:|-------------:|-------------:|-------------:|------:|------:|------:|----------:|
| **GetStringHashCode** |         **10** |     **2.877 ns** | **0.0835 ns** | **0.0697 ns** |     **2.834 ns** |     **2.826 ns** |     **2.990 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |        **100** |    **56.404 ns** | **0.0373 ns** | **0.0311 ns** |    **56.396 ns** |    **56.362 ns** |    **56.482 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |       **1000** |   **613.505 ns** | **0.5559 ns** | **0.4928 ns** |   **613.255 ns** |   **613.023 ns** |   **614.524 ns** |     **-** |     **-** |     **-** |         **-** |
| **GetStringHashCode** |      **10000** | **6,181.582 ns** | **5.1934 ns** | **4.6039 ns** | **6,180.037 ns** | **6,176.024 ns** | **6,190.959 ns** |     **-** |     **-** |     **-** |         **-** |
