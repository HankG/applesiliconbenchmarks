``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |        Mean |    Error |   StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |------------:|---------:|---------:|------------:|------------:|------------:|------:|------:|------:|----------:|
|    GetField |    72.73 μs | 2.638 μs | 3.037 μs |    73.93 μs |    65.61 μs |    74.18 μs |     - |     - |     - |         - |
|  GetMethod1 |   116.89 μs | 0.876 μs | 0.684 μs |   116.55 μs |   116.23 μs |   117.91 μs |     - |     - |     - |       1 B |
|  GetMethod2 |   220.53 μs | 0.506 μs | 0.473 μs |   220.42 μs |   219.93 μs |   221.44 μs |     - |     - |     - |       2 B |
|  GetMethod3 |   329.80 μs | 0.314 μs | 0.278 μs |   329.87 μs |   329.33 μs |   330.27 μs |     - |     - |     - |       4 B |
|  GetMethod4 |   440.75 μs | 0.917 μs | 0.766 μs |   440.57 μs |   439.85 μs |   442.54 μs |     - |     - |     - |       7 B |
|  GetMethod5 |   585.66 μs | 8.215 μs | 6.860 μs |   587.42 μs |   562.92 μs |   588.59 μs |     - |     - |     - |      11 B |
| GetMethod10 | 1,134.32 μs | 3.363 μs | 2.626 μs | 1,133.39 μs | 1,131.96 μs | 1,141.26 μs |     - |     - |     - |      40 B |
| GetMethod12 | 1,319.14 μs | 1.793 μs | 1.589 μs | 1,319.36 μs | 1,316.23 μs | 1,322.23 μs |     - |     - |     - |      56 B |
| GetMethod15 | 1,728.18 μs | 6.951 μs | 5.805 μs | 1,725.33 μs | 1,723.53 μs | 1,743.41 μs |     - |     - |     - |      93 B |
| GetMethod20 | 2,469.83 μs | 5.320 μs | 4.716 μs | 2,468.28 μs | 2,464.74 μs | 2,478.92 μs |     - |     - |     - |     161 B |
