``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |        Mean |        Error |       StdDev |     Median |        Min |         Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------------------------- |------------:|-------------:|-------------:|-----------:|-----------:|------------:|---------:|--------:|------:|----------:|
|                        Jil |    730.9 μs |      2.84 μs |      2.66 μs |   730.3 μs |   726.9 μs |    735.4 μs |  42.6136 | 11.3636 |     - | 106.21 KB |
|                   JSON.NET |  5,559.7 μs |  5,738.41 μs |  6,608.36 μs | 1,179.4 μs |   784.5 μs | 20,117.4 μs |  58.8235 | 19.6078 |     - | 159.63 KB |
|                   Utf8Json |    370.6 μs |      1.76 μs |      1.47 μs |   370.9 μs |   366.9 μs |    372.2 μs |  47.0588 | 11.7647 |     - | 107.97 KB |
| DataContractJsonSerializer | 17,364.3 μs | 18,562.13 μs | 21,376.17 μs | 3,837.8 μs | 3,691.8 μs | 59,273.6 μs | 200.0000 |       - |     - | 568.24 KB |
