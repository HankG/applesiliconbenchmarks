``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |           Mean |       Error |      StdDev |         Median |            Min |            Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------------:|------------:|------------:|---------------:|---------------:|---------------:|-------:|------:|------:|----------:|
| Interpol_Scalar |  7,946.6052 ns |   8.7111 ns |   8.1484 ns |  7,946.7581 ns |  7,928.6976 ns |  7,960.0194 ns | 3.8903 |     - |     - |    8216 B |
| Interpol_Vector | 15,971.2074 ns | 149.5036 ns | 139.8458 ns | 16,032.3846 ns | 15,805.5703 ns | 16,146.4375 ns | 3.9223 |     - |     - |    8256 B |
|    Interpol_AVX |      0.0002 ns |   0.0006 ns |   0.0005 ns |      0.0000 ns |      0.0000 ns |      0.0017 ns |      - |     - |     - |         - |
