``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | color |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |------ |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|         **ObjectGetType** |     **?** |  **0.0000 ns** | **0.0000 ns** | **0.0000 ns** |  **0.0000 ns** |  **0.0000 ns** |  **0.0000 ns** |      **-** |     **-** |     **-** |         **-** |
| ObjectGetTypeNoBoxing |     ? |  0.9611 ns | 0.0387 ns | 0.0362 ns |  0.9661 ns |  0.8875 ns |  1.0062 ns |      - |     - |     - |         - |
|            EnumEquals |     ? |  7.0599 ns | 0.0188 ns | 0.0176 ns |  7.0609 ns |  7.0261 ns |  7.0847 ns | 0.0229 |     - |     - |      48 B |
|         **EnumCompareTo** |   **Red** | **14.4576 ns** | **0.0205 ns** | **0.0192 ns** | **14.4559 ns** | **14.4220 ns** | **14.4895 ns** | **0.0229** |     **-** |     **-** |      **48 B** |
