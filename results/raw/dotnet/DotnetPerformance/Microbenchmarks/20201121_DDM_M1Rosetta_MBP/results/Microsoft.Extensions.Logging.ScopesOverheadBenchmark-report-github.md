``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method | HasISupportLoggingScopeLogger | CaptureScopes |       Mean |     Error |    StdDev |     Median |        Min |        Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |------------------------------ |-------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|--------:|-------:|------:|------:|----------:|
|             **FilteredByLevel** |                         **False** |         **False** |  **14.337 ns** | **0.0333 ns** | **0.0295 ns** |  **14.345 ns** |  **14.285 ns** |  **14.382 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                         False |         False |  20.145 ns | 0.0188 ns | 0.0176 ns |  20.138 ns |  20.110 ns |  20.171 ns |  1.41 |    0.00 |      - |     - |     - |         - |
|                 NotFiltered |                         False |         False |  43.944 ns | 0.5797 ns | 0.4526 ns |  43.829 ns |  43.731 ns |  45.372 ns |  3.06 |    0.03 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                         False |         False |  59.905 ns | 1.1642 ns | 0.9721 ns |  59.547 ns |  59.023 ns |  62.318 ns |  4.18 |    0.07 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                         **False** |          **True** |   **6.379 ns** | **0.0125 ns** | **0.0104 ns** |   **6.379 ns** |   **6.361 ns** |   **6.396 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                         False |          True |  22.281 ns | 0.0202 ns | 0.0189 ns |  22.275 ns |  22.250 ns |  22.312 ns |  3.49 |    0.01 |      - |     - |     - |         - |
|                 NotFiltered |                         False |          True |  44.854 ns | 0.0960 ns | 0.0801 ns |  44.824 ns |  44.795 ns |  45.103 ns |  7.03 |    0.02 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                         False |          True |  58.429 ns | 0.0539 ns | 0.0505 ns |  58.425 ns |  58.331 ns |  58.514 ns |  9.16 |    0.02 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                          **True** |         **False** |   **9.203 ns** | **0.0138 ns** | **0.0108 ns** |   **9.201 ns** |   **9.187 ns** |   **9.225 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                          True |         False |  18.505 ns | 0.0188 ns | 0.0166 ns |  18.506 ns |  18.476 ns |  18.537 ns |  2.01 |    0.00 |      - |     - |     - |         - |
|                 NotFiltered |                          True |         False |  42.738 ns | 0.0695 ns | 0.0543 ns |  42.739 ns |  42.605 ns |  42.816 ns |  4.64 |    0.01 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                          True |         False |  55.759 ns | 0.0889 ns | 0.0742 ns |  55.746 ns |  55.633 ns |  55.928 ns |  6.06 |    0.01 |      - |     - |     - |         - |
|                             |                               |               |            |           |           |            |            |            |       |         |        |       |       |           |
|             **FilteredByLevel** |                          **True** |          **True** |   **8.122 ns** | **0.0085 ns** | **0.0071 ns** |   **8.120 ns** |   **8.114 ns** |   **8.138 ns** |  **1.00** |    **0.00** |      **-** |     **-** |     **-** |         **-** |
| FilteredByLevel_InsideScope |                          True |          True |  91.395 ns | 1.2508 ns | 0.9766 ns |  90.971 ns |  90.796 ns |  94.244 ns | 11.25 |    0.12 | 0.0573 |     - |     - |     120 B |
|                 NotFiltered |                          True |          True |  40.136 ns | 0.0296 ns | 0.0277 ns |  40.130 ns |  40.101 ns |  40.179 ns |  4.94 |    0.00 |      - |     - |     - |         - |
|     NotFiltered_InsideScope |                          True |          True | 132.539 ns | 0.1541 ns | 0.1287 ns | 132.584 ns | 132.199 ns | 132.681 ns | 16.32 |    0.02 | 0.0571 |     - |     - |     120 B |
