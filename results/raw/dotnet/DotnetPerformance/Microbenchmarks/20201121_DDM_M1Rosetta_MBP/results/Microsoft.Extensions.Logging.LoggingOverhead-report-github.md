``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                     Method |      Mean |     Error |    StdDev |    Median |       Min |       Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|--------:|-------:|------:|------:|----------:|
|                NoArguments_FilteredByLevel | 17.265 ns | 0.0216 ns | 0.0202 ns | 17.256 ns | 17.241 ns | 17.297 ns |  3.60 |    0.00 |      - |     - |     - |         - |
|  NoArguments_DefineMessage_FilteredByLevel |  4.799 ns | 0.0042 ns | 0.0035 ns |  4.799 ns |  4.794 ns |  4.804 ns |  1.00 |    0.00 |      - |     - |     - |         - |
|                                NoArguments | 25.257 ns | 0.0229 ns | 0.0203 ns | 25.256 ns | 25.215 ns | 25.291 ns |  5.26 |    0.00 |      - |     - |     - |         - |
|                  NoArguments_DefineMessage | 32.385 ns | 0.0242 ns | 0.0215 ns | 32.380 ns | 32.347 ns | 32.428 ns |  6.75 |    0.00 |      - |     - |     - |         - |
|                               TwoArguments | 88.207 ns | 0.3534 ns | 0.2759 ns | 88.224 ns | 87.725 ns | 88.890 ns | 18.38 |    0.05 | 0.0305 |     - |     - |      64 B |
|               TwoArguments_FilteredByLevel | 80.321 ns | 0.3399 ns | 0.2654 ns | 80.366 ns | 79.697 ns | 80.557 ns | 16.74 |    0.05 | 0.0304 |     - |     - |      64 B |
|                 TwoArguments_DefineMessage | 41.750 ns | 0.0799 ns | 0.0748 ns | 41.721 ns | 41.669 ns | 41.912 ns |  8.70 |    0.02 |      - |     - |     - |         - |
| TwoArguments_DefineMessage_FilteredByLevel | 11.110 ns | 0.0176 ns | 0.0156 ns | 11.109 ns | 11.071 ns | 11.136 ns |  2.32 |    0.00 |      - |     - |     - |         - |
