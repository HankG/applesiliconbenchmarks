``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |----------:|----------:|----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                        Jil |  1.493 μs | 0.0021 μs | 0.0019 μs |  1.493 μs |  1.490 μs |  1.497 μs | 1.8234 |     - |     - |    3824 B |
|                   JSON.NET |  3.436 μs | 0.0526 μs | 0.0492 μs |  3.423 μs |  3.370 μs |  3.537 μs | 2.9051 |     - |     - |    6080 B |
|                   Utf8Json |  1.017 μs | 0.0022 μs | 0.0020 μs |  1.017 μs |  1.014 μs |  1.020 μs | 0.2107 |     - |     - |     448 B |
| DataContractJsonSerializer | 12.021 μs | 0.1147 μs | 0.0895 μs | 12.047 μs | 11.854 μs | 12.130 μs | 6.1242 |     - |     - |   14104 B |
