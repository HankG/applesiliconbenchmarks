``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |      Mean |      Error |     StdDev |    Median |       Min |         Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |----------:|-----------:|-----------:|----------:|----------:|------------:|--------:|------:|------:|----------:|
|                        Jil |  72.35 μs |   0.291 μs |   0.272 μs |  72.26 μs |  71.98 μs |    72.89 μs | 13.0208 |     - |     - |   26.7 KB |
|                   JSON.NET | 593.02 μs | 577.631 μs | 665.201 μs | 108.82 μs | 105.31 μs | 2,119.52 μs | 16.2602 |     - |     - |  34.27 KB |
|                   Utf8Json |  47.81 μs |   0.203 μs |   0.189 μs |  47.82 μs |  47.43 μs |    48.17 μs | 10.3786 |     - |     - |  21.58 KB |
| DataContractJsonSerializer | 431.58 μs |   6.450 μs |   5.036 μs | 430.84 μs | 425.45 μs |   443.10 μs | 31.2500 |     - |     - |  90.17 KB |
