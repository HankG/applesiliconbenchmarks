``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |      Mean |     Error |    StdDev |    Median |       Min |       Max |    Gen 0 |  Gen 1 | Gen 2 | Allocated |
|---------------- |----------:|----------:|----------:|----------:|----------:|----------:|---------:|-------:|------:|----------:|
| BinaryFormatter |  78.81 μs |  0.491 μs |  0.459 μs |  78.68 μs |  78.02 μs |  79.66 μs |  26.7296 |      - |     - |   55.2 KB |
|    protobuf-net | 759.18 μs | 17.709 μs | 18.186 μs | 745.89 μs | 741.14 μs | 788.92 μs | 107.6923 |      - |     - | 227.04 KB |
|     MessagePack | 102.54 μs |  0.251 μs |  0.210 μs | 102.50 μs | 102.27 μs | 102.97 μs |  34.9026 | 4.4643 |     - |  75.67 KB |
