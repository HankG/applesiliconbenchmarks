``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |   Gen 0 |   Gen 1 | Gen 2 | Allocated |
|----------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|--------:|--------:|------:|----------:|
|          XmlSerializer |   709.2 μs |  4.53 μs |  4.01 μs |   708.3 μs |   701.4 μs |   714.6 μs | 78.6517 |       - |     - | 165.51 KB |
| DataContractSerializer | 1,893.1 μs | 13.22 μs | 12.37 μs | 1,894.1 μs | 1,874.7 μs | 1,914.8 μs | 82.7068 | 22.5564 |     - | 210.16 KB |
