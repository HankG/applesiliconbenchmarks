``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                        Jil |   868.0 ns |  3.24 ns |  2.87 ns |   868.7 ns |   861.9 ns |   872.7 ns | 1.6948 |     - |     - |    3544 B |
|                   JSON.NET | 1,513.5 ns |  7.36 ns |  6.53 ns | 1,511.7 ns | 1,504.7 ns | 1,523.5 ns | 2.7670 |     - |     - |    5800 B |
|                   Utf8Json |   414.9 ns |  2.73 ns |  2.13 ns |   415.3 ns |   408.9 ns |   417.1 ns | 0.0802 |     - |     - |     168 B |
| DataContractJsonSerializer | 4,726.8 ns | 30.47 ns | 27.01 ns | 4,717.2 ns | 4,686.9 ns | 4,787.5 ns | 6.2882 |     - |     - |   13152 B |
