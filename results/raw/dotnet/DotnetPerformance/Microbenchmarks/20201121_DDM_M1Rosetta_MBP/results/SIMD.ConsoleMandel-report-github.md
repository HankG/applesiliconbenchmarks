``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |     Mean |   Error |  StdDev |   Median |      Min |      Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |---------:|--------:|--------:|---------:|---------:|---------:|------:|------:|------:|----------:|
|  ScalarFloatSinglethreadRaw | 850.3 ms | 0.32 ms | 0.28 ms | 850.2 ms | 849.9 ms | 850.8 ms |     - |     - |     - |     512 B |
|  ScalarFloatSinglethreadADT | 846.2 ms | 0.40 ms | 0.36 ms | 846.1 ms | 845.7 ms | 846.9 ms |     - |     - |     - |     512 B |
| ScalarDoubleSinglethreadRaw | 851.6 ms | 0.19 ms | 0.16 ms | 851.5 ms | 851.3 ms | 851.9 ms |     - |     - |     - |    5744 B |
| ScalarDoubleSinglethreadADT | 850.3 ms | 0.36 ms | 0.30 ms | 850.3 ms | 849.8 ms | 850.8 ms |     - |     - |     - |     512 B |
|  VectorFloatSinglethreadRaw | 270.9 ms | 0.35 ms | 0.31 ms | 270.9 ms | 270.5 ms | 271.3 ms |     - |     - |     - |  210992 B |
|  VectorFloatSinglethreadADT | 272.4 ms | 0.36 ms | 0.32 ms | 272.4 ms | 272.0 ms | 273.1 ms |     - |     - |     - |  216224 B |
| VectorDoubleSinglethreadRaw | 500.7 ms | 0.60 ms | 0.53 ms | 500.5 ms | 500.2 ms | 501.9 ms |     - |     - |     - |  210032 B |
| VectorDoubleSinglethreadADT | 500.6 ms | 0.44 ms | 0.39 ms | 500.5 ms | 500.1 ms | 501.4 ms |     - |     - |     - |  210032 B |
