``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                    Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------------ |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                    ReadStructAndReverseBE |  11.297 ns | 0.1111 ns | 0.0985 ns |  11.256 ns |  11.224 ns |  11.480 ns |     - |     - |     - |         - |
|                    ReadStructAndReverseLE |   1.512 ns | 0.0179 ns | 0.0140 ns |   1.515 ns |   1.469 ns |   1.526 ns |     - |     - |     - |         - |
|                  ReadStructFieldByFieldBE |   8.309 ns | 0.0445 ns | 0.0372 ns |   8.314 ns |   8.229 ns |   8.365 ns |     - |     - |     - |         - |
|                  ReadStructFieldByFieldLE |   8.166 ns | 0.0894 ns | 0.0836 ns |   8.177 ns |   8.055 ns |   8.269 ns |     - |     - |     - |         - |
| ReadStructFieldByFieldUsingBitConverterLE |  22.190 ns | 0.0312 ns | 0.0292 ns |  22.192 ns |  22.138 ns |  22.231 ns |     - |     - |     - |         - |
| ReadStructFieldByFieldUsingBitConverterBE |  24.794 ns | 0.0594 ns | 0.0496 ns |  24.799 ns |  24.682 ns |  24.861 ns |     - |     - |     - |         - |
|                  MeasureReverseEndianness | 604.291 ns | 7.0664 ns | 5.9008 ns | 608.040 ns | 597.377 ns | 610.993 ns |     - |     - |     - |         - |
|                   MeasureReverseUsingNtoH | 615.198 ns | 6.5995 ns | 5.8503 ns | 618.018 ns | 606.545 ns | 621.139 ns |     - |     - |     - |         - |
