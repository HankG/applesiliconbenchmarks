``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-OKJAVC : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=40  MinIterationCount=15  WarmupCount=1  

```
|        Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |       Gen 0 |      Gen 1 |     Gen 2 | Allocated |
|-------------- |---------:|---------:|---------:|---------:|---------:|---------:|------------:|-----------:|----------:|----------:|
| BinaryTrees_6 | 556.3 ms | 16.24 ms | 28.87 ms | 559.6 ms | 496.7 ms | 614.5 ms | 416000.0000 | 66000.0000 | 5000.0000 |   1.01 GB |
