``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|      Jil |   271.0 ns |  0.50 ns |  0.45 ns |   271.0 ns |   269.7 ns |   271.6 ns | 0.1259 |     - |     - |     264 B |
| JSON.NET | 1,283.0 ns | 16.77 ns | 13.09 ns | 1,284.0 ns | 1,258.7 ns | 1,307.6 ns | 1.2906 |     - |     - |    2776 B |
| Utf8Json |   468.3 ns |  1.49 ns |  1.32 ns |   468.1 ns |   464.8 ns |   470.6 ns | 0.1321 |     - |     - |     280 B |
