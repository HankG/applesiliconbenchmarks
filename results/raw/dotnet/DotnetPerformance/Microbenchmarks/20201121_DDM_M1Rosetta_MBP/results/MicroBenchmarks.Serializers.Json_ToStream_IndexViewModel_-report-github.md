``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |      Mean |     Error |    StdDev |   Median |      Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |----------:|----------:|----------:|---------:|---------:|----------:|------:|------:|------:|----------:|
|                        Jil |  58.18 μs |  0.274 μs |  0.229 μs | 58.08 μs | 57.99 μs |  58.73 μs |     - |     - |     - |      97 B |
|                   JSON.NET | 116.19 μs | 64.628 μs | 74.426 μs | 69.83 μs | 67.75 μs | 266.79 μs |     - |     - |     - |    2736 B |
|                   Utf8Json |  48.57 μs |  7.555 μs |  7.758 μs | 45.00 μs | 44.29 μs |  70.92 μs |     - |     - |     - |     288 B |
| DataContractJsonSerializer |  83.13 μs |  1.244 μs |  0.971 μs | 82.64 μs | 81.96 μs |  84.59 μs |     - |     - |     - |    2424 B |
