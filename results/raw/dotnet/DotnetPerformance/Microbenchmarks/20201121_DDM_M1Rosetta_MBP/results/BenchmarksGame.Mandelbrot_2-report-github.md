``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method | width |                             checksum |    Mean |    Error |   StdDev |  Median |     Min |     Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |------ |------------------------------------- |--------:|---------:|---------:|--------:|--------:|--------:|------:|------:|------:|----------:|
| Mandelbrot_2 |  4000 | C7-E6-66-43-6(...)7-2F-FC-A1-D3 [47] | 1.077 s | 0.0083 s | 0.0078 s | 1.080 s | 1.066 s | 1.085 s |     - |     - |     - |   1.91 MB |
