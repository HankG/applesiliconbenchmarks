``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                    Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |     Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|----------:|------:|------:|----------:|
|               ObjFooIsObj |   551.3 μs | 15.76 μs | 18.15 μs |   564.9 μs |   530.3 μs |   569.2 μs |         - |     - |     - |       1 B |
|               FooObjIsFoo | 1,017.7 μs |  5.64 μs |  5.27 μs | 1,020.1 μs | 1,003.0 μs | 1,021.2 μs |         - |     - |     - |       2 B |
|              FooObjIsNull | 1,389.3 μs | 11.62 μs | 10.87 μs | 1,386.1 μs | 1,376.8 μs | 1,404.6 μs |         - |     - |     - |       1 B |
|        FooObjIsDescendant |   764.5 μs |  2.39 μs |  2.24 μs |   765.1 μs |   759.6 μs |   767.8 μs |         - |     - |     - |       2 B |
|             IFooFooIsIFoo | 1,016.0 μs |  7.50 μs |  7.02 μs | 1,017.5 μs | 1,003.9 μs | 1,027.5 μs |         - |     - |     - |       2 B |
|             IFooObjIsIFoo |   737.9 μs |  4.64 μs |  4.34 μs |   738.4 μs |   724.4 μs |   742.2 μs |         - |     - |     - |       2 B |
|    IFooObjIsIFooInterAlia |   765.1 μs |  1.92 μs |  1.79 μs |   765.4 μs |   759.2 μs |   766.9 μs |         - |     - |     - |       2 B |
| IFooObjIsDescendantOfIFoo |   735.5 μs |  3.97 μs |  3.71 μs |   736.9 μs |   724.5 μs |   738.4 μs |         - |     - |     - |       2 B |
|                    ObjInt |   715.9 μs |  0.97 μs |  0.86 μs |   716.0 μs |   714.2 μs |   717.2 μs | 1147.7273 |     - |     - | 2400002 B |
|                    IntObj |   175.4 μs |  0.22 μs |  0.20 μs |   175.3 μs |   175.0 μs |   175.7 μs |         - |     - |     - |         - |
|        ObjScalarValueType |   722.3 μs |  1.18 μs |  1.04 μs |   722.1 μs |   720.7 μs |   724.1 μs | 1147.7273 |     - |     - | 2400002 B |
|        ScalarValueTypeObj |   275.1 μs |  0.40 μs |  0.37 μs |   275.1 μs |   274.2 μs |   275.6 μs |         - |     - |     - |         - |
|        ObjObjrefValueType | 1,237.1 μs |  1.44 μs |  1.35 μs | 1,237.4 μs | 1,233.1 μs | 1,238.9 μs | 1528.8462 |     - |     - | 3200003 B |
|        ObjrefValueTypeObj |   968.7 μs |  2.87 μs |  2.54 μs |   968.5 μs |   964.3 μs |   973.1 μs |         - |     - |     - |       2 B |
|           FooObjCastIfIsa |   509.3 μs |  1.19 μs |  1.12 μs |   509.1 μs |   507.0 μs |   511.0 μs |         - |     - |     - |       1 B |
