``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-LRHUTE : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  InvocationCount=1000  
IterationTime=250.0000 ms  MaxIterationCount=20  MinIterationCount=15  
MinWarmupIterationCount=6  UnrollFactor=1  WarmupCount=-1  

```
|          Method | Size |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |----- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|   QuickSortSpan |  512 |   6.101 μs | 0.0295 μs | 0.0276 μs |   6.090 μs |   6.075 μs |   6.163 μs |     - |     - |     - |         - |
|  BubbleSortSpan |  512 | 142.651 μs | 1.1016 μs | 1.0304 μs | 142.674 μs | 141.470 μs | 144.190 μs |     - |     - |     - |       1 B |
|  QuickSortArray |  512 |   5.946 μs | 0.0285 μs | 0.0267 μs |   5.938 μs |   5.912 μs |   5.993 μs |     - |     - |     - |         - |
| BubbleSortArray |  512 | 145.294 μs | 0.4987 μs | 0.3893 μs | 145.376 μs | 144.692 μs | 145.966 μs |     - |     - |     - |         - |
