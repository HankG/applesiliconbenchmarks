``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|      Jil |   646.2 ns |  1.47 ns |  1.31 ns |   646.5 ns |   644.0 ns |   648.0 ns | 0.2591 |     - |     - |     544 B |
| JSON.NET | 2,769.3 ns | 43.97 ns | 38.98 ns | 2,769.5 ns | 2,717.6 ns | 2,827.5 ns | 1.4506 |     - |     - |    3056 B |
| Utf8Json | 1,156.6 ns |  2.09 ns |  1.95 ns | 1,157.5 ns | 1,153.4 ns | 1,159.1 ns | 0.3168 |     - |     - |     672 B |
