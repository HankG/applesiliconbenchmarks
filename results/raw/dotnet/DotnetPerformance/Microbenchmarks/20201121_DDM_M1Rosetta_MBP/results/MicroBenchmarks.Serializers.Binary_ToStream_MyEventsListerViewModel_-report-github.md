``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |      Mean |    Error |   StdDev |    Median |       Min |       Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |----------:|---------:|---------:|----------:|----------:|----------:|--------:|------:|------:|----------:|
| BinaryFormatter |  50.39 μs | 2.504 μs | 2.572 μs |  51.56 μs |  47.12 μs |  55.47 μs | 10.2828 |     - |     - |   25439 B |
|    protobuf-net | 324.84 μs | 2.789 μs | 2.472 μs | 324.43 μs | 321.51 μs | 330.28 μs | 40.4172 |     - |     - |   86613 B |
|     MessagePack | 116.94 μs | 2.131 μs | 1.780 μs | 116.12 μs | 115.04 μs | 121.71 μs |       - |     - |     - |         - |
