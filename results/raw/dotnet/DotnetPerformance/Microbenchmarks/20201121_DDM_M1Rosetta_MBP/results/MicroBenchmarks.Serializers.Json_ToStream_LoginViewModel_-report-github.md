``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|                        Jil |   357.3 ns |   0.32 ns |   0.28 ns |   357.2 ns |   356.8 ns |   357.7 ns |      - |     - |     - |         - |
|                   JSON.NET |   565.7 ns |   6.90 ns |   6.45 ns |   564.4 ns |   556.5 ns |   579.1 ns | 0.2128 |     - |     - |     448 B |
|                   Utf8Json | 1,140.6 ns | 357.51 ns | 351.12 ns | 1,021.0 ns |   875.0 ns | 2,208.0 ns |      - |     - |     - |     288 B |
| DataContractJsonSerializer | 1,285.5 ns |   8.35 ns |   6.52 ns | 1,283.9 ns | 1,277.9 ns | 1,297.8 ns | 0.4760 |     - |     - |    1000 B |
