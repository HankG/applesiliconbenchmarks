``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |       Mean |    Error |  StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |-----------:|---------:|--------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|          XmlSerializer | 2,350.3 ns | 10.05 ns | 9.40 ns | 2,354.4 ns | 2,332.1 ns | 2,364.7 ns | 3.6640 |     - |     - |   7.49 KB |
| DataContractSerializer |   880.8 ns |  7.19 ns | 6.73 ns |   878.6 ns |   871.0 ns |   897.1 ns | 0.6834 |     - |     - |    1.4 KB |
