``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |       Error |      StdDev |     Median |        Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |-----------:|------------:|------------:|-----------:|-----------:|------------:|-------:|------:|------:|----------:|
|      Jil |   441.2 ns |     5.94 ns |     4.96 ns |   439.6 ns |   436.9 ns |    452.3 ns | 0.3506 |     - |     - |     736 B |
| JSON.NET |   769.9 ns |     3.73 ns |     3.30 ns |   770.4 ns |   762.7 ns |    775.0 ns | 0.7192 |     - |     - |    1504 B |
| Utf8Json | 6,807.1 ns | 7,992.03 ns | 8,883.12 ns | 1,667.0 ns | 1,500.0 ns | 28,083.0 ns |      - |     - |     - |     480 B |
