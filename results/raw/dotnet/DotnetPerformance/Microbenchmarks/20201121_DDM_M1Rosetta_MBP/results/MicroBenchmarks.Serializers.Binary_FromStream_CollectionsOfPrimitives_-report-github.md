``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |        Mean |    Error |   StdDev |      Median |         Min |         Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|---------------- |------------:|---------:|---------:|------------:|------------:|------------:|---------:|--------:|------:|----------:|
| BinaryFormatter | 1,204.48 μs | 7.231 μs | 6.410 μs | 1,204.16 μs | 1,195.08 μs | 1,214.81 μs | 192.3077 | 72.1154 |     - | 640.02 KB |
|    protobuf-net |   242.95 μs | 0.686 μs | 0.608 μs |   243.08 μs |   241.22 μs |   243.72 μs |  92.7734 | 35.1563 |     - | 265.15 KB |
|     MessagePack |    86.45 μs | 0.117 μs | 0.098 μs |    86.43 μs |    86.29 μs |    86.67 μs |  32.8039 |  6.9061 |     - |  75.36 KB |
