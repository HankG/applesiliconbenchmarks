``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 5,344.6 ns | 63.75 ns | 56.51 ns | 5,315.9 ns | 5,294.0 ns | 5,455.4 ns | 2.3734 |     - |     - |    5008 B |
|    protobuf-net |   869.1 ns |  8.53 ns |  7.56 ns |   869.3 ns |   854.7 ns |   878.7 ns | 0.2091 |     - |     - |     440 B |
|     MessagePack |   157.5 ns |  3.06 ns |  2.71 ns |   158.5 ns |   154.3 ns |   163.7 ns | 0.0797 |     - |     - |     168 B |
