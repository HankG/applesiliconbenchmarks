``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |     Error |    StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|                 Ctor_String |  0.0000 ns | 0.0000 ns | 0.0000 ns |  0.0000 ns |  0.0000 ns |  0.0000 ns |     - |     - |     - |         - |
|                  Ctor_Array |  0.0000 ns | 0.0000 ns | 0.0000 ns |  0.0000 ns |  0.0000 ns |  0.0000 ns |     - |     - |     - |         - |
| Indexer_FirstElement_String |  0.2474 ns | 0.0042 ns | 0.0039 ns |  0.2463 ns |  0.2405 ns |  0.2537 ns |     - |     - |     - |         - |
|  Indexer_FirstElement_Array |  0.4865 ns | 0.0552 ns | 0.0517 ns |  0.4978 ns |  0.3043 ns |  0.5199 ns |     - |     - |     - |         - |
|                Count_String |  0.5934 ns | 0.0019 ns | 0.0018 ns |  0.5933 ns |  0.5899 ns |  0.5968 ns |     - |     - |     - |         - |
|                 Count_Array |  0.5655 ns | 0.0020 ns | 0.0019 ns |  0.5649 ns |  0.5617 ns |  0.5687 ns |     - |     - |     - |         - |
|              ForEach_String |  0.3285 ns | 0.0074 ns | 0.0066 ns |  0.3268 ns |  0.3199 ns |  0.3395 ns |     - |     - |     - |         - |
|               ForEach_Array | 14.9895 ns | 0.1053 ns | 0.0985 ns | 15.0067 ns | 14.7439 ns | 15.1131 ns |     - |     - |     - |         - |
