``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|---------:|--------:|------:|----------:|
| BinaryFormatter | 1,250.9 μs | 11.80 μs | 11.04 μs | 1,251.7 μs | 1,234.0 μs | 1,275.0 μs | 174.1294 | 44.7761 |     - |  492032 B |
|    protobuf-net |   139.6 μs |  0.28 μs |  0.24 μs |   139.6 μs |   139.2 μs |   140.0 μs |        - |       - |     - |     209 B |
|     MessagePack |   267.1 μs |  4.59 μs |  4.51 μs |   264.5 μs |   263.7 μs |   279.0 μs |        - |       - |     - |     288 B |
