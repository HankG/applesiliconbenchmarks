``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------- |---------:|--------:|--------:|---------:|---------:|---------:|---------:|--------:|------:|----------:|
|      Jil | 288.1 μs | 0.37 μs | 0.33 μs | 288.1 μs | 287.3 μs | 288.6 μs |  60.2273 | 19.3182 |     - | 167.61 KB |
| JSON.NET | 569.5 μs | 2.34 μs | 2.19 μs | 568.7 μs | 566.4 μs | 573.8 μs | 111.3636 | 36.3636 |     - | 299.92 KB |
| Utf8Json | 352.2 μs | 2.41 μs | 2.25 μs | 352.6 μs | 349.1 μs | 356.3 μs |  82.5175 | 22.3776 |     - | 222.73 KB |
