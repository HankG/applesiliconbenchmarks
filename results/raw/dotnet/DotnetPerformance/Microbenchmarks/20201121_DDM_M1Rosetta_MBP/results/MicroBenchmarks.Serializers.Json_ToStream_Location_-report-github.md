``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |       Error |      StdDev |     Median |        Min |         Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|------------:|------------:|-----------:|-----------:|------------:|-------:|------:|------:|----------:|
|                        Jil |   513.3 ns |     0.50 ns |     0.45 ns |   513.2 ns |   512.3 ns |    514.0 ns | 0.0441 |     - |     - |      96 B |
|                   JSON.NET | 6,640.3 ns | 6,537.34 ns | 7,528.41 ns | 1,565.1 ns | 1,532.4 ns | 22,760.1 ns | 0.1968 |     - |     - |     448 B |
|                   Utf8Json | 1,993.1 ns |   911.28 ns |   975.06 ns | 1,542.0 ns | 1,250.0 ns |  4,333.0 ns |      - |     - |     - |     288 B |
| DataContractJsonSerializer | 2,764.3 ns |    45.85 ns |    38.29 ns | 2,768.0 ns | 2,702.8 ns |  2,841.9 ns | 0.3622 |     - |     - |    1000 B |
