``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |       Error |      StdDev |     Median |        Min |        Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|------------:|------------:|-----------:|-----------:|-----------:|--------:|------:|------:|----------:|
|                        Jil |   479.2 μs |     5.40 μs |     4.22 μs |   479.2 μs |   472.6 μs |   487.9 μs | 41.6667 |     - |     - | 123.86 KB |
|                   JSON.NET | 1,245.3 μs |    30.21 μs |    33.58 μs | 1,227.6 μs | 1,217.2 μs | 1,319.5 μs |       - |     - |     - | 146.73 KB |
|                   Utf8Json |   814.4 μs |    23.61 μs |    25.26 μs |   808.6 μs |   789.6 μs |   871.9 μs |       - |     - |     - |  245.3 KB |
| DataContractJsonSerializer | 2,614.3 μs | 2,119.67 μs | 2,441.02 μs |   857.2 μs |   846.5 μs | 7,839.9 μs |       - |     - |     - |  23.63 KB |
