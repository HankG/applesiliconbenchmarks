``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|               Method |      Mean |      Error |     StdDev |    Median |       Min |        Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------- |----------:|-----------:|-----------:|----------:|----------:|-----------:|------:|--------:|-------:|------:|------:|----------:|
|                 NoDI |  3.525 ns |  0.1135 ns |  0.1006 ns |  3.567 ns |  3.277 ns |   3.581 ns |  1.00 |    0.00 | 0.0114 |     - |     - |      24 B |
|            Transient | 31.797 ns |  0.0313 ns |  0.0262 ns | 31.797 ns | 31.740 ns |  31.841 ns |  8.98 |    0.22 | 0.0114 |     - |     - |      24 B |
|               Scoped | 82.175 ns | 11.9287 ns | 11.7155 ns | 76.205 ns | 75.453 ns | 118.287 ns | 23.57 |    3.68 |      - |     - |     - |         - |
|            Singleton | 34.517 ns |  2.7877 ns |  2.8628 ns | 32.672 ns | 32.114 ns |  42.452 ns |  9.93 |    0.88 |      - |     - |     - |         - |
|         ServiceScope | 61.695 ns |  0.5422 ns |  0.4527 ns | 61.510 ns | 61.312 ns |  62.685 ns | 17.42 |    0.44 | 0.0648 |     - |     - |     136 B |
| ServiceScopeProvider | 92.194 ns | 73.9586 ns | 79.1348 ns | 52.407 ns | 37.124 ns | 272.517 ns | 30.48 |   23.44 |      - |     - |     - |         - |
|      EmptyEnumerable | 32.243 ns |  0.0275 ns |  0.0257 ns | 32.244 ns | 32.209 ns |  32.300 ns |  9.15 |    0.28 |      - |     - |     - |         - |
