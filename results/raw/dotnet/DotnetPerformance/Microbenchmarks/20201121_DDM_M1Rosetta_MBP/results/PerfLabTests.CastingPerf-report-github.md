``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                        Method |              Mean |          Error |         StdDev |            Median |               Min |               Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------------------------- |------------------:|---------------:|---------------:|------------------:|------------------:|------------------:|------:|------:|------:|----------:|
|                                   ObjFooIsObj |   508,046.0853 ns |  3,915.0269 ns |  3,662.1186 ns |   509,818.8851 ns |   500,601.8972 ns |   511,178.9315 ns |     - |     - |     - |       1 B |
|                                  ObjFooIsObj2 |   634,169.8812 ns |  4,301.7855 ns |  4,023.8928 ns |   634,407.0825 ns |   625,780.1050 ns |   639,418.9575 ns |     - |     - |     - |       1 B |
|                                   ObjObjIsFoo |   812,512.2819 ns | 16,060.8065 ns | 16,493.2659 ns |   798,389.2578 ns |   796,798.3734 ns |   830,396.1609 ns |     - |     - |     - |       2 B |
|                                   FooObjIsFoo |   636,986.3823 ns |  1,484.9313 ns |  1,389.0057 ns |   637,414.5850 ns |   633,520.8325 ns |   638,402.2925 ns |     - |     - |     - |       1 B |
|                                  FooObjIsFoo2 |   478,818.0198 ns |    273.8600 ns |    256.1688 ns |   478,821.7339 ns |   478,330.5729 ns |   479,296.2434 ns |     - |     - |     - |       1 B |
|                                  FooObjIsNull |   509,197.1963 ns |  2,665.9419 ns |  2,493.7237 ns |   510,066.9388 ns |   500,841.5982 ns |   510,770.6480 ns |     - |     - |     - |       1 B |
|                            FooObjIsDescendant |   577,013.3801 ns |    952.3449 ns |    890.8240 ns |   576,929.7824 ns |   575,032.5023 ns |   578,361.4005 ns |     - |     - |     - |       1 B |
|                                 IFooFooIsIFoo |   284,588.0364 ns |    554.8331 ns |    518.9912 ns |   284,738.6364 ns |   283,565.5295 ns |   285,250.9000 ns |     - |     - |     - |       1 B |
|                                 IFooObjIsIFoo |   575,291.5687 ns |    975.7774 ns |    912.7428 ns |   575,335.8778 ns |   573,688.1506 ns |   576,987.5142 ns |     - |     - |     - |       1 B |
|                        IFooObjIsIFooInterAlia |   575,180.9370 ns |    400.4767 ns |    355.0121 ns |   575,166.3415 ns |   574,215.4018 ns |   575,757.8147 ns |     - |     - |     - |       1 B |
|                     IFooObjIsDescendantOfIFoo |   575,634.5318 ns |    618.0126 ns |    578.0894 ns |   575,580.7273 ns |   574,635.8892 ns |   576,744.5540 ns |     - |     - |     - |       2 B |
|                                        ObjInt |   318,868.5713 ns |    330.2903 ns |    308.9537 ns |   318,932.1059 ns |   318,152.1849 ns |   319,256.8304 ns |     - |     - |     - |       1 B |
|                                        IntObj |   449,154.2135 ns |    417.5991 ns |    390.6224 ns |   449,149.4420 ns |   448,465.3634 ns |   449,796.8384 ns |     - |     - |     - |       1 B |
|                            ObjScalarValueType |   820,262.9588 ns |  6,789.5763 ns |  6,350.9738 ns |   818,113.7894 ns |   813,635.0747 ns |   829,423.5720 ns |     - |     - |     - |       2 B |
|                            ScalarValueTypeObj |   478,609.1998 ns |    560.6240 ns |    468.1464 ns |   478,668.9453 ns |   477,415.6914 ns |   479,222.1680 ns |     - |     - |     - |       1 B |
|                            ObjObjrefValueType |   280,564.5620 ns |    450.6122 ns |    421.5029 ns |   280,594.6808 ns |   279,360.6775 ns |   281,124.4888 ns |     - |     - |     - |       1 B |
|                            ObjrefValueTypeObj |   763,367.8003 ns |  4,247.2412 ns |  3,972.8720 ns |   765,025.4498 ns |   754,407.7093 ns |   766,184.3835 ns |     - |     - |     - |       1 B |
|                               FooObjCastIfIsa | 1,337,773.9986 ns |  7,053.1234 ns |  6,597.4960 ns | 1,339,656.0312 ns | 1,315,151.0365 ns | 1,341,931.8542 ns |     - |     - |     - |       3 B |
|                        CheckObjIsInterfaceYes |   231,189.1307 ns |    131.7423 ns |    110.0107 ns |   231,215.1691 ns |   230,887.8869 ns |   231,322.3998 ns |     - |     - |     - |       1 B |
|                         CheckObjIsInterfaceNo |   272,083.7281 ns |    771.9831 ns |    722.1135 ns |   272,300.7802 ns |   271,013.9181 ns |   273,034.2134 ns |     - |     - |     - |       1 B |
|                  CheckIsInstAnyIsInterfaceYes |   231,210.6421 ns |    253.3017 ns |    224.5453 ns |   231,206.2656 ns |   230,730.2192 ns |   231,587.4122 ns |     - |     - |     - |         - |
|                   CheckIsInstAnyIsInterfaceNo |   271,999.2320 ns |    607.6462 ns |    568.3926 ns |   272,087.1724 ns |   271,149.3578 ns |   272,693.5388 ns |     - |     - |     - |         - |
|                      CheckArrayIsInterfaceYes |   434,527.2759 ns |    609.0610 ns |    569.7161 ns |   434,488.4253 ns |   433,218.8958 ns |   435,309.6059 ns |     - |     - |     - |       1 B |
|                       CheckArrayIsInterfaceNo |   551,854.7632 ns |    708.9414 ns |    628.4581 ns |   551,848.1950 ns |   550,489.2705 ns |   552,904.6778 ns |     - |     - |     - |       1 B |
|        CheckArrayIsNonvariantGenericInterface |         8.2012 ns |      0.0211 ns |      0.0176 ns |         8.2011 ns |         8.1756 ns |         8.2287 ns |     - |     - |     - |         - |
|      CheckArrayIsNonvariantGenericInterfaceNo |         6.2674 ns |      0.0154 ns |      0.0144 ns |         6.2636 ns |         6.2462 ns |         6.2963 ns |     - |     - |     - |         - |
|                   CheckArrayIsArrayByVariance |         0.9897 ns |      0.0076 ns |      0.0059 ns |         0.9876 ns |         0.9834 ns |         1.0030 ns |     - |     - |     - |         - |
|            CheckListIsVariantGenericInterface |         2.9019 ns |      0.0057 ns |      0.0050 ns |         2.9016 ns |         2.8944 ns |         2.9112 ns |     - |     - |     - |         - |
|         CheckArrayIsVariantGenericInterfaceNo |         3.1950 ns |      0.0064 ns |      0.0054 ns |         3.1938 ns |         3.1881 ns |         3.2054 ns |     - |     - |     - |         - |
|                  AssignArrayElementByVariance |         3.4390 ns |      0.0066 ns |      0.0055 ns |         3.4398 ns |         3.4262 ns |         3.4488 ns |     - |     - |     - |         - |
| CheckArrayIsVariantGenericInterfaceReflection |         0.0000 ns |      0.0000 ns |      0.0000 ns |         0.0000 ns |         0.0000 ns |         0.0000 ns |     - |     - |     - |         - |
