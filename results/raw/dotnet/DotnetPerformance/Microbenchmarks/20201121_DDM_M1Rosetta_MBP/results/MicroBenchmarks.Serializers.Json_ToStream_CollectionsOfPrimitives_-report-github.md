``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                     Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|--------:|------:|------:|----------:|
|                        Jil |   336.4 μs |  0.74 μs |  0.69 μs |   336.4 μs |   335.0 μs |   337.3 μs |       - |     - |     - |      97 B |
|                   JSON.NET |   640.6 μs | 14.83 μs | 15.87 μs |   631.4 μs |   625.0 μs |   673.0 μs |       - |     - |     - |  107424 B |
|                   Utf8Json |   470.1 μs |  9.23 μs |  7.71 μs |   467.3 μs |   462.0 μs |   483.1 μs |       - |     - |     - |    3048 B |
| DataContractJsonSerializer | 1,501.0 μs |  7.73 μs |  6.85 μs | 1,499.2 μs | 1,491.7 μs | 1,512.9 μs | 29.9401 |     - |     - |   74922 B |
