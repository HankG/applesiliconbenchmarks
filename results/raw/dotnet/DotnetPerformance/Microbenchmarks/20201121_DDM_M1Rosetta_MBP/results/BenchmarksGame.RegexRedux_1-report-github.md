``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |    Gen 0 |    Gen 1 |    Gen 2 | Allocated |
|------------- |---------:|---------:|---------:|---------:|---------:|---------:|---------:|---------:|---------:|----------:|
| RegexRedux_1 | 55.06 ms | 1.021 ms | 0.853 ms | 54.68 ms | 54.39 ms | 57.43 ms | 250.0000 | 250.0000 | 250.0000 |   2.84 MB |
