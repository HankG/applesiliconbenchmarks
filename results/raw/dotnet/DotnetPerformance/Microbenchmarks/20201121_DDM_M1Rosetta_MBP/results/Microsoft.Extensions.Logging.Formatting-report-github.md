``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |      Mean |    Error |   StdDev |    Median |       Min |       Max | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----------:|---------:|---------:|----------:|----------:|----------:|------:|--------:|-------:|------:|------:|----------:|
| TwoArguments | 164.61 ns | 0.872 ns | 0.728 ns | 164.28 ns | 163.69 ns | 165.61 ns |  5.12 |    0.02 | 0.0378 |     - |     - |      80 B |
|  NoArguments |  32.14 ns | 0.045 ns | 0.037 ns |  32.13 ns |  32.07 ns |  32.20 ns |  1.00 |    0.00 |      - |     - |     - |         - |
