``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 4,244.8 ns | 132.13 ns | 129.77 ns | 4,316.3 ns | 4,046.4 ns | 4,368.2 ns | 1.8858 |     - |     - |    4304 B |
|    protobuf-net |   641.3 ns |   2.15 ns |   1.68 ns |   641.7 ns |   638.8 ns |   644.7 ns | 0.0976 |     - |     - |     208 B |
|     MessagePack |   656.0 ns | 397.84 ns | 458.15 ns |   340.4 ns |   314.2 ns | 1,399.6 ns |      - |     - |     - |         - |
