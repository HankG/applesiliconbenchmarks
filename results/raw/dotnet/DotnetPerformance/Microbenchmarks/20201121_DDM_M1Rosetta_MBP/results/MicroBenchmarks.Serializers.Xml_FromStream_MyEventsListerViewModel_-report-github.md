``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |      Mean |      Error |     StdDev |   Median |      Min |       Max |    Gen 0 |   Gen 1 | Gen 2 | Allocated |
|----------------------- |----------:|-----------:|-----------:|---------:|---------:|----------:|---------:|--------:|------:|----------:|
|          XmlSerializer |  1.608 ms |  0.0903 ms |  0.0967 ms | 1.560 ms | 1.519 ms |  1.857 ms | 209.8765 | 67.9012 |     - | 536.97 KB |
| DataContractSerializer | 14.511 ms | 16.0133 ms | 18.4410 ms | 2.204 ms | 1.908 ms | 54.812 ms |        - |       - |     - | 189.38 KB |
