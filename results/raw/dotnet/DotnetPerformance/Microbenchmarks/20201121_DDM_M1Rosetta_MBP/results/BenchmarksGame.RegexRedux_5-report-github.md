``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method |  options |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |--------- |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| **RegexRedux_5** |     **None** | **36.106 ms** | **0.8705 ms** | **0.9675 ms** | **35.777 ms** | **35.047 ms** | **38.203 ms** |     **-** |     **-** |     **-** |   **2.67 MB** |
| **RegexRedux_5** | **Compiled** |  **8.496 ms** | **0.1593 ms** | **0.1565 ms** |  **8.538 ms** |  **8.138 ms** |  **8.732 ms** |     **-** |     **-** |     **-** |   **2.67 MB** |
