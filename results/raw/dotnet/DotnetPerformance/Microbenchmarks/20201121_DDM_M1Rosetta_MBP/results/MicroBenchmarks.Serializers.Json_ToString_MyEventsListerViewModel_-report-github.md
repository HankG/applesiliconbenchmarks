``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |    Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|--------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|---------:|--------:|--------:|----------:|
|      Jil |   515.4 μs |  1.95 μs |  1.63 μs |   515.7 μs |   511.1 μs |   517.6 μs | 135.0806 | 52.4194 | 44.3548 | 424.41 KB |
| JSON.NET | 1,339.9 μs | 25.88 μs | 26.58 μs | 1,334.6 μs | 1,299.7 μs | 1,412.8 μs |        - |       - |       - |  452.4 KB |
| Utf8Json |   946.5 μs | 67.72 μs | 77.98 μs |   903.1 μs |   890.5 μs | 1,132.2 μs |        - |       - |       - | 394.91 KB |
