``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|------:|------:|----------:|
|      Jil | 42.64 μs | 0.050 μs | 0.047 μs | 42.64 μs | 42.54 μs | 42.72 μs | 11.4413 |     - |     - |  23.49 KB |
| JSON.NET | 92.13 μs | 1.241 μs | 0.969 μs | 92.21 μs | 90.21 μs | 93.39 μs | 13.8889 |     - |     - |  31.31 KB |
| Utf8Json | 48.15 μs | 0.394 μs | 0.349 μs | 47.96 μs | 47.76 μs | 48.67 μs | 16.5305 |     - |     - |  33.87 KB |
