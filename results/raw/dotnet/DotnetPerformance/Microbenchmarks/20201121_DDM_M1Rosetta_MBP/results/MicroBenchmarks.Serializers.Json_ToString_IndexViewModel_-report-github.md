``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |      Mean |     Error |    StdDev |   Median |      Min |       Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |----------:|----------:|----------:|---------:|---------:|----------:|--------:|------:|------:|----------:|
|      Jil |  42.90 μs |  0.134 μs |  0.112 μs | 42.95 μs | 42.74 μs |  43.05 μs | 27.4725 |     - |     - |  56.65 KB |
| JSON.NET | 111.23 μs | 45.656 μs | 50.746 μs | 82.62 μs | 80.29 μs | 239.25 μs |       - |     - |     - |  59.53 KB |
| Utf8Json |  54.84 μs | 19.442 μs | 21.610 μs | 42.46 μs | 41.17 μs | 114.67 μs |       - |     - |     - |  24.83 KB |
