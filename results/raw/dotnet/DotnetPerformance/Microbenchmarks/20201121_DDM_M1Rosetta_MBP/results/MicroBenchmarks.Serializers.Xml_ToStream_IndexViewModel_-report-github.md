``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |     Mean |    Error |   StdDev |   Median |      Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---------:|---------:|---------:|---------:|---------:|----------:|-------:|------:|------:|----------:|
|          XmlSerializer | 36.36 μs | 0.096 μs | 0.080 μs | 36.35 μs | 36.24 μs |  36.52 μs | 5.2204 |     - |     - |  10.91 KB |
| DataContractSerializer | 85.67 μs | 6.354 μs | 7.317 μs | 81.71 μs | 79.98 μs | 101.75 μs |      - |     - |     - |  20.08 KB |
