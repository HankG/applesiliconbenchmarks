``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method | HasSubscribers |  Json |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |--------------- |------ |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| **NestedScopes_TwoMessages** |          **False** | **False** |   **108.0 ns** |  **0.18 ns** |  **0.15 ns** |   **108.0 ns** |   **107.7 ns** |   **108.2 ns** | **0.0115** |     **-** |     **-** |      **24 B** |
| **NestedScopes_TwoMessages** |          **False** |  **True** |   **115.5 ns** |  **0.16 ns** |  **0.14 ns** |   **115.5 ns** |   **115.4 ns** |   **115.8 ns** | **0.0111** |     **-** |     **-** |      **24 B** |
| **NestedScopes_TwoMessages** |           **True** | **False** | **2,285.0 ns** | **28.91 ns** | **22.57 ns** | **2,280.8 ns** | **2,253.3 ns** | **2,326.0 ns** | **0.9282** |     **-** |     **-** |    **1960 B** |
| **NestedScopes_TwoMessages** |           **True** |  **True** | **4,288.7 ns** | **52.45 ns** | **43.80 ns** | **4,280.3 ns** | **4,205.5 ns** | **4,354.9 ns** | **2.2374** |     **-** |     **-** |    **4888 B** |
