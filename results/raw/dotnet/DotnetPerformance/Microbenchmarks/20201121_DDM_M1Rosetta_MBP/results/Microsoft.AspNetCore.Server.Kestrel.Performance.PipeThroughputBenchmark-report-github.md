``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method | Length | Chunks |       Mean |       Error |      StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |------- |------- |-----------:|------------:|------------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|   **Parse_ParallelAsync** |    **128** |      **1** |   **201.5 ns** |     **1.48 ns** |     **1.31 ns** |   **201.3 ns** |   **199.9 ns** |   **203.9 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |    128 |      1 |   877.4 ns |   974.63 ns | 1,083.30 ns |   358.1 ns |   262.2 ns | 3,899.3 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |    **128** |     **16** | **1,207.5 ns** |     **8.92 ns** |     **8.35 ns** | **1,205.1 ns** | **1,193.5 ns** | **1,222.6 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |    128 |     16 |   553.4 ns |    10.03 ns |     8.37 ns |   550.1 ns |   546.6 ns |   576.6 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |   **4096** |      **1** | **1,218.7 ns** |    **14.87 ns** |    **13.91 ns** | **1,219.5 ns** | **1,193.3 ns** | **1,249.0 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |   4096 |      1 |   430.6 ns |   436.62 ns |   428.82 ns |   263.9 ns |   258.6 ns | 1,812.7 ns |     - |     - |     - |         - |
|   **Parse_ParallelAsync** |   **4096** |     **16** | **2,272.5 ns** |    **22.39 ns** |    **19.85 ns** | **2,271.7 ns** | **2,248.9 ns** | **2,313.5 ns** |     **-** |     **-** |     **-** |       **2 B** |
| Parse_SequentialAsync |   4096 |     16 | 2,639.5 ns | 2,198.96 ns | 2,532.33 ns |   960.1 ns |   611.3 ns | 8,600.6 ns |     - |     - |     - |         - |
