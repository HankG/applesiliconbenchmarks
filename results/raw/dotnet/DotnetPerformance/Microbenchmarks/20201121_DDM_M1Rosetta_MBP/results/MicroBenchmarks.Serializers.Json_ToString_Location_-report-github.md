``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |        Mean |        Error |       StdDev |     Median |        Min |          Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|--------- |------------:|-------------:|-------------:|-----------:|-----------:|-------------:|-------:|------:|------:|----------:|
|      Jil |    862.4 ns |      2.04 ns |      1.81 ns |   862.4 ns |   859.0 ns |     865.5 ns | 0.6637 |     - |     - |    1392 B |
| JSON.NET | 70,891.4 ns | 82,519.95 ns | 95,030.10 ns | 9,395.5 ns | 8,250.0 ns | 237,666.0 ns |      - |     - |     - |    2024 B |
| Utf8Json |  8,095.8 ns |  8,215.77 ns |  9,461.30 ns | 2,229.0 ns | 1,958.0 ns |  25,792.0 ns |      - |     - |     - |     712 B |
