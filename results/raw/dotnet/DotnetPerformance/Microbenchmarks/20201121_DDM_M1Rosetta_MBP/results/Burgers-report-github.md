``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|    Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |       Gen 0 | Gen 1 | Gen 2 |    Allocated |
|---------- |---------:|--------:|--------:|---------:|---------:|---------:|------------:|------:|------:|-------------:|
| Burgers_0 | 244.7 ms | 0.74 ms | 0.69 ms | 244.5 ms | 243.6 ms | 246.0 ms | 370000.0000 |     - |     - | 781646.05 KB |
| Burgers_1 | 183.5 ms | 0.20 ms | 0.17 ms | 183.5 ms | 183.3 ms | 183.9 ms |           - |     - |     - |    156.59 KB |
| Burgers_2 | 183.5 ms | 0.12 ms | 0.11 ms | 183.5 ms | 183.3 ms | 183.7 ms |           - |     - |     - |    156.59 KB |
| Burgers_3 | 191.4 ms | 2.99 ms | 2.80 ms | 193.6 ms | 188.1 ms | 194.2 ms |           - |     - |     - |    156.61 KB |
