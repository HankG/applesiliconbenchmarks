``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|------:|------:|----------:|
| BinaryFormatter | 28.51 μs | 0.385 μs | 0.322 μs | 28.49 μs | 28.08 μs | 29.14 μs |  9.6618 |     - |     - |  21.55 KB |
|    protobuf-net | 56.80 μs | 0.696 μs | 0.543 μs | 56.74 μs | 56.17 μs | 57.60 μs | 14.8515 |     - |     - |  31.04 KB |
|     MessagePack | 10.68 μs | 0.024 μs | 0.019 μs | 10.68 μs | 10.65 μs | 10.71 μs | 10.3661 |     - |     - |   21.2 KB |
