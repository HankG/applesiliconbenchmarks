``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|          XmlSerializer | 7.990 μs | 0.0929 μs | 0.0776 μs | 8.007 μs | 7.861 μs | 8.152 μs | 3.3592 |     - |     - |   6.88 KB |
| DataContractSerializer | 6.170 μs | 0.0975 μs | 0.0864 μs | 6.132 μs | 6.049 μs | 6.359 μs | 6.1044 |     - |     - |  12.48 KB |
