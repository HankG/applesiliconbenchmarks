``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|           Ctor_String |  4.1666 ns | 0.0572 ns | 0.0535 ns |  4.1343 ns |  4.1099 ns |  4.2276 ns |      - |     - |     - |         - |
|              GetValue |  2.9173 ns | 0.0078 ns | 0.0069 ns |  2.9187 ns |  2.9016 ns |  2.9247 ns |      - |     - |     - |         - |
|               Indexer |  7.0979 ns | 0.0205 ns | 0.0182 ns |  7.1066 ns |  7.0674 ns |  7.1200 ns |      - |     - |     - |         - |
| Equals_Object_Invalid |  0.1276 ns | 0.0040 ns | 0.0038 ns |  0.1256 ns |  0.1232 ns |  0.1346 ns |      - |     - |     - |         - |
|   Equals_Object_Valid |  4.8030 ns | 0.0059 ns | 0.0055 ns |  4.8014 ns |  4.7958 ns |  4.8130 ns |      - |     - |     - |         - |
|          Equals_Valid |  3.8689 ns | 0.0035 ns | 0.0031 ns |  3.8683 ns |  3.8633 ns |  3.8753 ns |      - |     - |     - |         - |
|         Equals_String |  1.9341 ns | 0.0063 ns | 0.0052 ns |  1.9334 ns |  1.9229 ns |  1.9457 ns |      - |     - |     - |         - |
|    GetSegmentHashCode | 15.6503 ns | 0.1563 ns | 0.1462 ns | 15.5487 ns | 15.5298 ns | 15.8753 ns |      - |     - |     - |         - |
|            StartsWith |  4.6733 ns | 0.0114 ns | 0.0089 ns |  4.6724 ns |  4.6588 ns |  4.6917 ns |      - |     - |     - |         - |
|              EndsWith |  5.2875 ns | 0.0175 ns | 0.0155 ns |  5.2886 ns |  5.2626 ns |  5.3143 ns |      - |     - |     - |         - |
|             SubString | 12.1920 ns | 0.0155 ns | 0.0137 ns | 12.1954 ns | 12.1639 ns | 12.2068 ns | 0.0153 |     - |     - |      32 B |
|            SubSegment |  3.7797 ns | 0.0493 ns | 0.0462 ns |  3.8096 ns |  3.7037 ns |  3.8254 ns |      - |     - |     - |         - |
|               IndexOf |  0.0000 ns | 0.0000 ns | 0.0000 ns |  0.0000 ns |  0.0000 ns |  0.0000 ns |      - |     - |     - |         - |
|            IndexOfAny |  6.0521 ns | 0.0091 ns | 0.0086 ns |  6.0513 ns |  6.0325 ns |  6.0647 ns |      - |     - |     - |         - |
|           LastIndexOf |  8.2912 ns | 0.0244 ns | 0.0216 ns |  8.2918 ns |  8.2529 ns |  8.3310 ns |      - |     - |     - |         - |
|                  Trim | 26.5423 ns | 0.3116 ns | 0.2914 ns | 26.7612 ns | 26.2125 ns | 26.8522 ns |      - |     - |     - |         - |
|             TrimStart |  5.9742 ns | 0.0047 ns | 0.0039 ns |  5.9757 ns |  5.9678 ns |  5.9792 ns |      - |     - |     - |         - |
|               TrimEnd |  4.4654 ns | 0.0108 ns | 0.0101 ns |  4.4693 ns |  4.4490 ns |  4.4798 ns |      - |     - |     - |         - |
