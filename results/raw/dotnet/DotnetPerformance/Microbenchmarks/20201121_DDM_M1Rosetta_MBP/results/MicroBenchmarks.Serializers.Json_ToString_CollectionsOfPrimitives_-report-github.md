``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 |   Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|--------:|--------:|----------:|
|      Jil | 425.7 μs |  1.00 μs |  0.93 μs | 425.8 μs | 424.0 μs | 427.0 μs | 64.1892 | 32.0946 | 32.0946 | 211.77 KB |
| JSON.NET | 720.4 μs | 22.61 μs | 24.19 μs | 707.7 μs | 698.2 μs | 786.6 μs |       - |       - |       - | 293.39 KB |
| Utf8Json | 481.6 μs | 12.87 μs | 14.30 μs | 473.5 μs | 468.2 μs | 512.2 μs |       - |       - |       - |  98.29 KB |
