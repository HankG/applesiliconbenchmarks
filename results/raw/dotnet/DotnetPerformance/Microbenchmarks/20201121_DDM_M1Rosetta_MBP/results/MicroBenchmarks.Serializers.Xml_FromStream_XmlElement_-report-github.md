``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                 Method |     Mean |     Error |    StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---------:|----------:|----------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
|          XmlSerializer | 3.567 μs | 0.0255 μs | 0.0226 μs | 3.576 μs | 3.524 μs | 3.596 μs | 2.8780 |     - |     - |    5.9 KB |
| DataContractSerializer | 2.876 μs | 0.0233 μs | 0.0182 μs | 2.876 μs | 2.841 μs | 2.916 μs | 3.4847 |     - |     - |   7.12 KB |
