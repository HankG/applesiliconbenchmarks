``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                   Method |      Mean |     Error |    StdDev |    Median |      Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------- |----------:|----------:|----------:|----------:|---------:|----------:|------:|------:|------:|----------:|
|   ValueTupleCompareNoOpt |  5.912 ns | 0.0743 ns | 0.0620 ns |  5.942 ns | 5.794 ns |  5.978 ns |     - |     - |     - |         - |
|        ValueTupleCompare |  3.299 ns | 0.0027 ns | 0.0022 ns |  3.298 ns | 3.294 ns |  3.304 ns |     - |     - |     - |         - |
|  ValueTupleCompareCached |  9.947 ns | 0.0870 ns | 0.0772 ns |  9.983 ns | 9.808 ns | 10.020 ns |     - |     - |     - |         - |
| ValueTupleCompareWrapped | 10.001 ns | 0.0700 ns | 0.0584 ns | 10.032 ns | 9.876 ns | 10.049 ns |     - |     - |     - |         - |
