``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-OKJAVC : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=40  MinIterationCount=15  WarmupCount=1  

```
|        Method |     Mean |   Error |  StdDev |   Median |      Min |      Max |      Gen 0 |      Gen 1 |     Gen 2 | Allocated |
|-------------- |---------:|--------:|--------:|---------:|---------:|---------:|-----------:|-----------:|----------:|----------:|
| BinaryTrees_5 | 132.9 ms | 2.69 ms | 4.63 ms | 132.4 ms | 123.2 ms | 144.3 ms | 74000.0000 | 13000.0000 | 4000.0000 | 227.34 MB |
