``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                                      Method |         Mean |      Error |     StdDev |       Median |          Min |          Max |     Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------------- |-------------:|-----------:|-----------:|-------------:|-------------:|-------------:|----------:|------:|------:|----------:|
|                         EmptyStaticFunction |  2,103.58 μs |  11.084 μs |  10.368 μs |  2,106.04 μs |  2,066.39 μs |  2,108.30 μs |         - |     - |     - |       4 B |
|                     EmptyStaticFunction5Arg |  2,046.87 μs |  12.188 μs |  11.401 μs |  2,049.55 μs |  2,021.14 μs |  2,058.67 μs |         - |     - |     - |       4 B |
|                       EmptyInstanceFunction |  2,120.22 μs |  20.697 μs |  19.360 μs |  2,113.73 μs |  2,100.20 μs |  2,142.19 μs |         - |     - |     - |       4 B |
|                    InterfaceInterfaceMethod |  6,692.53 μs |  59.890 μs |  46.758 μs |  6,724.24 μs |  6,604.21 μs |  6,733.83 μs |         - |     - |     - |       6 B |
|       InterfaceInterfaceMethodLongHierarchy |    779.02 μs |  16.519 μs |  18.361 μs |    765.80 μs |    762.57 μs |    822.60 μs |         - |     - |     - |       1 B |
|      InterfaceInterfaceMethodSwitchCallType |  1,114.96 μs |   3.348 μs |   3.132 μs |  1,116.11 μs |  1,106.54 μs |  1,118.06 μs |         - |     - |     - |       3 B |
|                          ClassVirtualMethod |    222.57 μs |   1.699 μs |   1.589 μs |    223.45 μs |    220.18 μs |    224.16 μs |         - |     - |     - |       1 B |
|                  SealedClassInterfaceMethod |    412.00 μs |   3.529 μs |   3.301 μs |    413.49 μs |    406.90 μs |    415.06 μs |         - |     - |     - |       1 B |
|          StructWithInterfaceInterfaceMethod |    211.06 μs |   1.705 μs |   1.595 μs |    210.31 μs |    209.53 μs |    213.63 μs |         - |     - |     - |         - |
|                               StaticIntPlus |    216.55 μs |   0.161 μs |   0.151 μs |    216.49 μs |    216.36 μs |    216.84 μs |         - |     - |     - |         - |
|                        ObjectStringIsString |     75.20 μs |   0.716 μs |   0.670 μs |     75.65 μs |     74.35 μs |     75.89 μs |         - |     - |     - |         - |
|             NewDelegateClassEmptyInstanceFn |    750.51 μs |   1.062 μs |   0.993 μs |    750.52 μs |    748.80 μs |    752.36 μs | 3059.5238 |     - |     - | 6400002 B |
|               NewDelegateClassEmptyStaticFn |    794.03 μs |   1.502 μs |   1.405 μs |    793.67 μs |    792.62 μs |    797.37 μs | 3059.3750 |     - |     - | 6400001 B |
|                            InstanceDelegate |    442.34 μs |   4.259 μs |   3.984 μs |    441.92 μs |    437.86 μs |    446.80 μs |         - |     - |     - |       1 B |
|                              StaticDelegate |    724.38 μs |   5.861 μs |   5.483 μs |    720.93 μs |    719.76 μs |    733.80 μs |         - |     - |     - |       2 B |
|                               MeasureEvents | 47,273.06 μs | 160.039 μs | 133.640 μs | 47,253.44 μs | 47,112.77 μs | 47,532.05 μs |         - |     - |     - |      72 B |
|     GenericClassWithIntGenericInstanceField |     51.41 μs |   0.475 μs |   0.445 μs |     51.60 μs |     50.85 μs |     51.93 μs |         - |     - |     - |         - |
|              GenericClassGenericStaticField |     46.99 μs |   0.138 μs |   0.108 μs |     46.97 μs |     46.92 μs |     47.32 μs |         - |     - |     - |         - |
|           GenericClassGenericInstanceMethod |    210.12 μs |   1.256 μs |   1.175 μs |    210.44 μs |    207.41 μs |    211.50 μs |         - |     - |     - |         - |
|             GenericClassGenericStaticMethod |    206.98 μs |   0.758 μs |   0.709 μs |    207.04 μs |    205.34 μs |    207.97 μs |         - |     - |     - |         - |
|                        GenericGenericMethod |    207.79 μs |   1.541 μs |   1.442 μs |    208.44 μs |    204.98 μs |    209.45 μs |         - |     - |     - |         - |
| GenericClassWithSTringGenericInstanceMethod |    139.45 μs |   0.137 μs |   0.122 μs |    139.48 μs |    139.21 μs |    139.65 μs |         - |     - |     - |         - |
|                  ForeachOverList100Elements | 25,448.94 μs |  79.923 μs |  70.850 μs | 25,470.95 μs | 25,275.76 μs | 25,515.71 μs |         - |     - |     - |      29 B |
|                 TypeReflectionObjectGetType |    211.82 μs |   0.164 μs |   0.145 μs |    211.78 μs |    211.63 μs |    212.15 μs |         - |     - |     - |         - |
|                  TypeReflectionArrayGetType |    211.80 μs |   0.231 μs |   0.217 μs |    211.83 μs |    211.38 μs |    212.09 μs |         - |     - |     - |         - |
|                           IntegerFormatting |  2,703.32 μs |  25.943 μs |  24.267 μs |  2,718.51 μs |  2,671.24 μs |  2,726.80 μs | 2291.6667 |     - |     - | 4800006 B |
