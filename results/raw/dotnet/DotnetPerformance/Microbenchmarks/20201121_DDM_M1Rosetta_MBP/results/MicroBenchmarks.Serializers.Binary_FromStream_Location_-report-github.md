``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 7,930.2 ns | 20.43 ns | 19.11 ns | 7,932.4 ns | 7,894.3 ns | 7,959.2 ns | 3.1827 |     - |     - |    6696 B |
|    protobuf-net | 1,501.5 ns | 11.66 ns |  9.74 ns | 1,504.1 ns | 1,478.9 ns | 1,512.8 ns | 0.7151 |     - |     - |    1496 B |
|     MessagePack |   481.9 ns |  5.49 ns |  4.29 ns |   480.7 ns |   478.2 ns |   493.9 ns | 0.2135 |     - |     - |     448 B |
