``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|       Method | RentalSize | UseSharedPool |     Mean |    Error |   StdDev |   Median |      Min |      Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------- |----------- |-------------- |---------:|---------:|---------:|---------:|---------:|---------:|-------:|------:|------:|----------:|
| **RentNoReturn** |         **64** |         **False** | **21.05 ns** | **0.050 ns** | **0.047 ns** | **21.04 ns** | **20.96 ns** | **21.11 ns** | **0.0421** |     **-** |     **-** |      **88 B** |
| **RentNoReturn** |         **64** |          **True** | **21.88 ns** | **0.050 ns** | **0.047 ns** | **21.89 ns** | **21.73 ns** | **21.94 ns** | **0.0420** |     **-** |     **-** |      **88 B** |
