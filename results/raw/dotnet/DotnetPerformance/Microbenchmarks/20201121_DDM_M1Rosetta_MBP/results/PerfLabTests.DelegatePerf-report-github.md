``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                         Method | length |         Mean |     Error |    StdDev |       Median |          Min |          Max |       Gen 0 | Gen 1 | Gen 2 |   Allocated |
|------------------------------- |------- |-------------:|----------:|----------:|-------------:|-------------:|-------------:|------------:|------:|------:|------------:|
|                 **DelegateInvoke** |      **?** |     **382.7 μs** |   **0.44 μs** |   **0.41 μs** |     **382.7 μs** |     **381.8 μs** |     **383.3 μs** |           **-** |     **-** |     **-** |           **-** |
| MulticastDelegateCombineInvoke |      ? | 195,172.1 μs | 394.27 μs | 349.51 μs | 195,198.6 μs | 194,492.4 μs | 195,849.2 μs | 189000.0000 |     - |     - | 400920192 B |
|        **MulticastDelegateInvoke** |    **100** |   **3,654.0 μs** |   **4.97 μs** |   **4.15 μs** |   **3,653.5 μs** |   **3,647.8 μs** |   **3,664.4 μs** |           **-** |     **-** |     **-** |         **9 B** |
|        **MulticastDelegateInvoke** |   **1000** |  **54,329.2 μs** |  **79.45 μs** |  **70.43 μs** |  **54,338.9 μs** |  **54,145.5 μs** |  **54,432.4 μs** |           **-** |     **-** |     **-** |        **72 B** |
