``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method |    n |         expected |     Mean |   Error |  StdDev |   Median |      Min |      Max |        Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----- |----------------- |---------:|--------:|--------:|---------:|---------:|---------:|-------------:|------:|------:|----------:|
| PiDigits_3 | 3000 | 8649423196	:3000 | 649.7 ms | 4.47 ms | 3.96 ms | 651.3 ms | 639.8 ms | 653.6 ms | 1277000.0000 |     - |     - |   2.49 GB |
