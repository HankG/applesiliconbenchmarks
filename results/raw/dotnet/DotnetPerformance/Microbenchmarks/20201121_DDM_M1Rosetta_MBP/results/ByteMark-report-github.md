``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |        Gen 0 |        Gen 1 |        Gen 2 |     Allocated |
|---------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------------:|-------------:|-------------:|--------------:|
|      BenchNumericSortJagged | 1,099.3 ms |  9.08 ms |  8.50 ms | 1,100.7 ms | 1,087.1 ms | 1,112.7 ms |   19000.0000 |    8000.0000 |    2000.0000 |   63438.63 KB |
| BenchNumericSortRectangular | 1,224.6 ms |  3.40 ms |  2.66 ms | 1,224.7 ms | 1,218.2 ms | 1,228.7 ms |    2000.0000 |    2000.0000 |    2000.0000 |   31684.63 KB |
|             BenchStringSort | 1,244.7 ms |  5.63 ms |  5.27 ms | 1,247.0 ms | 1,231.2 ms | 1,250.7 ms |   21000.0000 |    8000.0000 |    3000.0000 |   69638.67 KB |
|                 BenchBitOps |   555.9 ms |  3.75 ms |  3.32 ms |   555.4 ms |   550.7 ms |   564.3 ms | 4166000.0000 | 4166000.0000 | 4166000.0000 | 12805860.3 KB |
|                BenchEmFloat | 3,221.3 ms | 31.18 ms | 29.17 ms | 3,203.2 ms | 3,194.5 ms | 3,264.8 ms |   30000.0000 |    2000.0000 |    1000.0000 |  101717.92 KB |
|           BenchEmFloatClass |   651.3 ms |  6.13 ms |  5.73 ms |   648.8 ms |   644.4 ms |   658.1 ms |   13000.0000 |    1000.0000 |            - |   35216.42 KB |
|                BenchFourier |   340.2 ms |  0.24 ms |  0.23 ms |   340.2 ms |   339.8 ms |   340.6 ms |            - |            - |            - |     483.13 KB |
|           BenchAssignJagged |   818.5 ms |  7.23 ms |  6.76 ms |   815.0 ms |   813.5 ms |   829.8 ms |    1000.0000 |            - |            - |    5576.93 KB |
|      BenchAssignRectangular | 1,061.9 ms | 10.92 ms | 10.21 ms | 1,053.3 ms | 1,051.8 ms | 1,073.4 ms |    1000.0000 |            - |            - |    5214.11 KB |
|         BenchIDEAEncryption | 1,065.8 ms | 12.13 ms | 11.35 ms | 1,065.6 ms | 1,048.1 ms | 1,078.7 ms |            - |            - |            - |     610.48 KB |
|           BenchNeuralJagged |   649.6 ms |  0.79 ms |  0.70 ms |   649.5 ms |   648.2 ms |   650.7 ms |            - |            - |            - |      847.6 KB |
|                 BenchNeural |   580.7 ms |  2.43 ms |  2.15 ms |   581.5 ms |   576.2 ms |   583.0 ms |            - |            - |            - |    1434.55 KB |
|               BenchLUDecomp |   910.0 ms |  0.88 ms |  0.82 ms |   910.1 ms |   908.5 ms |   911.3 ms |   63000.0000 |   19000.0000 |    7000.0000 |   211161.7 KB |
