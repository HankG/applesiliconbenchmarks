``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|          Method |       Mean |     Error |    StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------- |-----------:|----------:|----------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
| BinaryFormatter | 2,251.8 ns |   8.55 ns |   7.58 ns | 2,250.7 ns | 2,239.8 ns | 2,266.7 ns | 1.6755 |     - |     - |    3520 B |
|    protobuf-net |   463.6 ns |   2.53 ns |   2.11 ns |   463.9 ns |   458.8 ns |   467.1 ns | 0.0992 |     - |     - |     208 B |
|     MessagePack |   489.8 ns | 315.33 ns | 363.14 ns |   387.0 ns |   139.1 ns | 1,276.8 ns |      - |     - |     - |         - |
