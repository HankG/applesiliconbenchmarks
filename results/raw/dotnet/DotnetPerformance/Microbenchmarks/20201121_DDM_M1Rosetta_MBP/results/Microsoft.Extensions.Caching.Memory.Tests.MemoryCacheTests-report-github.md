``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|      Method |      Mean |      Error |     StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------ |----------:|-----------:|-----------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|      GetHit | 154.31 ns |  53.672 ns |  55.117 ns | 130.11 ns | 120.06 ns | 312.73 ns |      - |     - |     - |         - |
|     GetMiss |  82.22 ns |   1.106 ns |   0.863 ns |  82.47 ns |  80.04 ns |  83.06 ns |      - |     - |     - |         - |
| SetOverride | 477.11 ns | 160.224 ns | 157.362 ns | 413.87 ns | 405.28 ns | 977.44 ns | 0.1875 |     - |     - |     424 B |
