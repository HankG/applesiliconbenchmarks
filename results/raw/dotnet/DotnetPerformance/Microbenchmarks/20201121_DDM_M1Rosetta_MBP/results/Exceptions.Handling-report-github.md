``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                              Method |               kind |      Mean |    Error |   StdDev |    Median |       Min |       Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------ |------------------- |----------:|---------:|---------:|----------:|----------:|----------:|-------:|------:|------:|----------:|
|                       **ThrowAndCatch** |           **Software** |  **15.78 μs** | **0.424 μs** | **0.488 μs** |  **15.88 μs** |  **15.13 μs** |  **16.35 μs** | **0.1291** |     **-** |     **-** |     **320 B** |
|       ThrowAndCatch_ManyCatchBlocks |           Software |  15.87 μs | 0.252 μs | 0.236 μs |  15.90 μs |  15.58 μs |  16.25 μs | 0.1305 |     - |     - |     320 B |
|                ThrowAndCatchFinally |           Software |  15.36 μs | 0.164 μs | 0.137 μs |  15.45 μs |  15.18 μs |  15.52 μs | 0.1310 |     - |     - |     320 B |
|                   ThrowAndCatchWhen |           Software |  15.69 μs | 0.256 μs | 0.240 μs |  15.53 μs |  15.48 μs |  16.19 μs | 0.1291 |     - |     - |     320 B |
|            ThrowAndCatchWhenFinally |           Software |  15.73 μs | 0.171 μs | 0.151 μs |  15.73 μs |  15.56 μs |  15.91 μs | 0.1357 |     - |     - |     320 B |
|                   ThrowAndCatchDeep |           Software |  29.42 μs | 0.365 μs | 0.341 μs |  29.19 μs |  29.15 μs |  30.24 μs | 0.7267 |     - |     - |    1736 B |
|          ThrowAndCatchDeepRecursive |           Software |  31.10 μs | 0.618 μs | 0.687 μs |  30.96 μs |  30.36 μs |  32.28 μs | 0.7606 |     - |     - |    1736 B |
| MultipleNestedTryCatch_FirstCatches |           Software |  15.22 μs | 0.230 μs | 0.215 μs |  15.26 μs |  15.00 μs |  15.65 μs | 0.1251 |     - |     - |     320 B |
|  MultipleNestedTryCatch_LastCatches |           Software |  15.66 μs | 0.150 μs | 0.126 μs |  15.59 μs |  15.55 μs |  15.86 μs | 0.1303 |     - |     - |     320 B |
|            MultipleNestedTryFinally |           Software |  15.26 μs | 0.167 μs | 0.140 μs |  15.19 μs |  15.11 μs |  15.44 μs | 0.1268 |     - |     - |     320 B |
|                 CatchAndRethrowDeep |           Software | 358.52 μs | 3.587 μs | 3.355 μs | 356.29 μs | 355.20 μs | 362.76 μs |      - |     - |     - |    1737 B |
|              CatchAndThrowOtherDeep |           Software | 368.86 μs | 2.883 μs | 2.697 μs | 370.11 μs | 363.57 μs | 370.82 μs | 1.4535 |     - |     - |    3520 B |
|                   TryAndFinallyDeep |           Software |  30.22 μs | 0.265 μs | 0.248 μs |  30.08 μs |  29.98 μs |  30.70 μs | 0.7198 |     - |     - |    1736 B |
|       TryAndCatchDeep_CaugtAtTheTop |           Software |  31.20 μs | 0.531 μs | 0.497 μs |  31.16 μs |  30.26 μs |  31.84 μs | 0.7500 |     - |     - |    1736 B |
|                       **ThrowAndCatch** |           **Hardware** |  **42.05 μs** | **0.042 μs** | **0.037 μs** |  **42.04 μs** |  **41.98 μs** |  **42.11 μs** |      **-** |     **-** |     **-** |     **320 B** |
|       ThrowAndCatch_ManyCatchBlocks |           Hardware |  42.60 μs | 0.064 μs | 0.057 μs |  42.60 μs |  42.51 μs |  42.68 μs |      - |     - |     - |     320 B |
|                ThrowAndCatchFinally |           Hardware |  42.14 μs | 0.087 μs | 0.082 μs |  42.12 μs |  42.03 μs |  42.29 μs |      - |     - |     - |     320 B |
|                   ThrowAndCatchWhen |           Hardware |  42.13 μs | 0.053 μs | 0.044 μs |  42.14 μs |  42.00 μs |  42.18 μs |      - |     - |     - |     320 B |
|            ThrowAndCatchWhenFinally |           Hardware |  41.77 μs | 0.062 μs | 0.052 μs |  41.78 μs |  41.68 μs |  41.88 μs |      - |     - |     - |     320 B |
|                   ThrowAndCatchDeep |           Hardware |  59.63 μs | 0.184 μs | 0.172 μs |  59.61 μs |  59.37 μs |  59.92 μs | 0.7156 |     - |     - |    1736 B |
|          ThrowAndCatchDeepRecursive |           Hardware |  60.43 μs | 0.091 μs | 0.076 μs |  60.43 μs |  60.27 μs |  60.51 μs | 0.7212 |     - |     - |    1736 B |
| MultipleNestedTryCatch_FirstCatches |           Hardware |  42.09 μs | 0.085 μs | 0.075 μs |  42.10 μs |  41.97 μs |  42.21 μs |      - |     - |     - |     320 B |
|  MultipleNestedTryCatch_LastCatches |           Hardware |  42.34 μs | 0.113 μs | 0.100 μs |  42.33 μs |  42.20 μs |  42.55 μs |      - |     - |     - |     320 B |
|            MultipleNestedTryFinally |           Hardware |  42.23 μs | 0.063 μs | 0.059 μs |  42.23 μs |  42.14 μs |  42.33 μs |      - |     - |     - |     320 B |
|                 CatchAndRethrowDeep |           Hardware | 492.88 μs | 0.953 μs | 0.796 μs | 492.83 μs | 491.87 μs | 494.74 μs |      - |     - |     - |    1737 B |
|              CatchAndThrowOtherDeep |           Hardware | 487.72 μs | 0.532 μs | 0.444 μs | 487.74 μs | 487.08 μs | 488.47 μs |      - |     - |     - |    3521 B |
|                   TryAndFinallyDeep |           Hardware |  61.06 μs | 0.181 μs | 0.169 μs |  61.05 μs |  60.73 μs |  61.33 μs | 0.7324 |     - |     - |    1736 B |
|       TryAndCatchDeep_CaugtAtTheTop |           Hardware |  60.48 μs | 0.118 μs | 0.110 μs |  60.51 μs |  60.24 μs |  60.62 μs | 0.7239 |     - |     - |    1736 B |
|                       **ThrowAndCatch** | **ReflectionSoftware** |  **57.41 μs** | **0.514 μs** | **0.481 μs** |  **57.13 μs** |  **56.87 μs** |  **58.05 μs** | **0.4545** |     **-** |     **-** |     **952 B** |
|       ThrowAndCatch_ManyCatchBlocks | ReflectionSoftware |  55.54 μs | 0.488 μs | 0.457 μs |  55.82 μs |  54.87 μs |  55.89 μs | 0.4386 |     - |     - |     952 B |
|                   ThrowAndCatchDeep | ReflectionSoftware |  68.07 μs | 0.713 μs | 0.667 μs |  67.67 μs |  67.45 μs |  68.99 μs | 0.8117 |     - |     - |    2152 B |
|          ThrowAndCatchDeepRecursive | ReflectionSoftware |  70.88 μs | 0.743 μs | 0.695 μs |  70.49 μs |  70.25 μs |  71.87 μs | 0.8484 |     - |     - |    2152 B |
|                       **ThrowAndCatch** | **ReflectionHardware** |  **85.99 μs** | **0.163 μs** | **0.152 μs** |  **85.96 μs** |  **85.80 μs** |  **86.35 μs** | **0.3434** |     **-** |     **-** |     **952 B** |
|       ThrowAndCatch_ManyCatchBlocks | ReflectionHardware |  86.02 μs | 0.109 μs | 0.102 μs |  86.03 μs |  85.88 μs |  86.25 μs | 0.3453 |     - |     - |     952 B |
|                   ThrowAndCatchDeep | ReflectionHardware |  99.59 μs | 0.105 μs | 0.099 μs |  99.60 μs |  99.46 μs |  99.79 μs | 0.7962 |     - |     - |    2152 B |
|          ThrowAndCatchDeepRecursive | ReflectionHardware | 101.00 μs | 0.140 μs | 0.124 μs | 100.98 μs | 100.85 μs | 101.21 μs | 0.8065 |     - |     - |    2152 B |
