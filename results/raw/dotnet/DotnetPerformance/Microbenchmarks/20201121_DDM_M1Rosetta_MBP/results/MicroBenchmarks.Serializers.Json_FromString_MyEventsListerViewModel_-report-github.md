``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|   Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |   Gen 0 |   Gen 1 | Gen 2 | Allocated |
|--------- |---------:|---------:|---------:|---------:|---------:|---------:|--------:|--------:|------:|----------:|
|      Jil | 324.1 μs |  1.50 μs |  1.41 μs | 324.1 μs | 322.0 μs | 326.3 μs | 45.9184 | 12.7551 |     - |    103 KB |
| JSON.NET | 871.3 μs | 12.35 μs | 10.94 μs | 866.6 μs | 860.1 μs | 896.6 μs |       - |       - |     - |  156.7 KB |
| Utf8Json | 394.4 μs |  2.95 μs |  2.61 μs | 395.2 μs | 388.1 μs | 396.9 μs | 65.0794 | 20.6349 |     - | 182.79 KB |
