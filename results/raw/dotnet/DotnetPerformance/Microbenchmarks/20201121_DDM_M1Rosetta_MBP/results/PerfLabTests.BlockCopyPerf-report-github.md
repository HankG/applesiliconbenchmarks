``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|        Method | numElements |      Mean |     Error |    StdDev |    Median |       Min |       Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------- |------------ |----------:|----------:|----------:|----------:|----------:|----------:|------:|------:|------:|----------:|
| **CallBlockCopy** |          **10** |  **4.535 ns** | **0.0060 ns** | **0.0056 ns** |  **4.536 ns** |  **4.526 ns** |  **4.545 ns** |     **-** |     **-** |     **-** |         **-** |
| **CallBlockCopy** |         **100** |  **6.514 ns** | **0.0063 ns** | **0.0056 ns** |  **6.516 ns** |  **6.503 ns** |  **6.521 ns** |     **-** |     **-** |     **-** |         **-** |
| **CallBlockCopy** |        **1000** | **16.285 ns** | **0.0377 ns** | **0.0353 ns** | **16.286 ns** | **16.230 ns** | **16.347 ns** |     **-** |     **-** |     **-** |         **-** |
