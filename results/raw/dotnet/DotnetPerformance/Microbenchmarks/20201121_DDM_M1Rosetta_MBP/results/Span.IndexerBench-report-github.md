``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method | length |       Mean |   Error |  StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |------- |-----------:|--------:|--------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
|              Ref |   1024 |   631.1 ns | 0.68 ns | 0.53 ns |   630.9 ns |   630.5 ns |   632.2 ns |     - |     - |     - |         - |
|           Fixed1 |   1024 |   631.0 ns | 0.58 ns | 0.48 ns |   630.8 ns |   630.4 ns |   631.8 ns |     - |     - |     - |         - |
|           Fixed2 |   1024 |   631.8 ns | 0.68 ns | 0.57 ns |   631.5 ns |   631.2 ns |   632.9 ns |     - |     - |     - |         - |
|         Indexer1 |   1024 |   630.5 ns | 0.81 ns | 0.64 ns |   630.3 ns |   629.2 ns |   631.4 ns |     - |     - |     - |         - |
|         Indexer2 |   1024 |   627.9 ns | 0.38 ns | 0.32 ns |   627.9 ns |   627.2 ns |   628.5 ns |     - |     - |     - |         - |
|         Indexer3 |   1024 |   626.5 ns | 0.36 ns | 0.30 ns |   626.4 ns |   626.2 ns |   627.1 ns |     - |     - |     - |         - |
|         Indexer4 |   1024 | 6,299.1 ns | 4.99 ns | 4.42 ns | 6,298.3 ns | 6,293.0 ns | 6,306.5 ns |     - |     - |     - |         - |
|         Indexer5 |   1024 |   630.6 ns | 0.50 ns | 0.42 ns |   630.5 ns |   630.1 ns |   631.4 ns |     - |     - |     - |         - |
|         Indexer6 |   1024 |   629.6 ns | 0.62 ns | 0.52 ns |   629.4 ns |   629.1 ns |   630.6 ns |     - |     - |     - |         - |
| ReadOnlyIndexer1 |   1024 |   631.6 ns | 0.37 ns | 0.31 ns |   631.6 ns |   631.1 ns |   632.1 ns |     - |     - |     - |         - |
| ReadOnlyIndexer2 |   1024 |   630.8 ns | 0.66 ns | 0.55 ns |   630.7 ns |   630.2 ns |   632.0 ns |     - |     - |     - |         - |
| WriteViaIndexer1 |   1024 | 2,124.3 ns | 1.54 ns | 1.29 ns | 2,124.2 ns | 2,122.6 ns | 2,127.0 ns |     - |     - |     - |         - |
| WriteViaIndexer2 |   1024 |   631.5 ns | 0.50 ns | 0.42 ns |   631.5 ns |   630.8 ns |   632.1 ns |     - |     - |     - |         - |
|   KnownSizeArray |   1024 |   629.7 ns | 0.34 ns | 0.29 ns |   629.7 ns |   629.2 ns |   630.2 ns |     - |     - |     - |         - |
|    KnownSizeCtor |   1024 |   629.0 ns | 0.52 ns | 0.44 ns |   628.8 ns |   628.6 ns |   629.8 ns |     - |     - |     - |         - |
|   KnownSizeCtor2 |   1024 |   628.9 ns | 0.52 ns | 0.43 ns |   628.7 ns |   628.4 ns |   629.8 ns |     - |     - |     - |         - |
|       SameIndex1 |   1024 |   705.1 ns | 0.47 ns | 0.39 ns |   705.0 ns |   704.7 ns |   705.7 ns |     - |     - |     - |         - |
|       SameIndex2 |   1024 |   700.7 ns | 8.11 ns | 6.77 ns |   705.2 ns |   693.3 ns |   707.6 ns |     - |     - |     - |         - |
|    CoveredIndex1 |   1024 |   876.5 ns | 7.98 ns | 6.66 ns |   872.2 ns |   869.9 ns |   886.7 ns |     - |     - |     - |         - |
|    CoveredIndex2 |   1024 |   711.0 ns | 6.37 ns | 5.32 ns |   707.6 ns |   705.7 ns |   719.2 ns |     - |     - |     - |         - |
|    CoveredIndex3 |   1024 |   823.1 ns | 8.94 ns | 7.47 ns |   817.9 ns |   816.8 ns |   832.7 ns |     - |     - |     - |         - |
