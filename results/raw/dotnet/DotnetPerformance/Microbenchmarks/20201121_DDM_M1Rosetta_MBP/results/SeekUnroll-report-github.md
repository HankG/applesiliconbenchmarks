``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-RGYCHM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|     Method | boxedIndex |       Mean |   Error |  StdDev |     Median |        Min |        Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------- |----------- |-----------:|--------:|--------:|-----------:|-----------:|-----------:|------:|------:|------:|----------:|
| **SeekUnroll** |          **1** |   **952.6 ms** | **7.30 ms** | **6.83 ms** |   **956.0 ms** |   **939.3 ms** |   **956.6 ms** |     **-** |     **-** |     **-** |     **328 B** |
| **SeekUnroll** |          **3** |   **953.8 ms** | **4.90 ms** | **4.59 ms** |   **955.5 ms** |   **942.5 ms** |   **956.7 ms** |     **-** |     **-** |     **-** |     **616 B** |
| **SeekUnroll** |         **11** | **1,141.6 ms** | **9.89 ms** | **9.25 ms** | **1,137.0 ms** | **1,134.0 ms** | **1,158.3 ms** |     **-** |     **-** |     **-** |     **328 B** |
| **SeekUnroll** |         **19** |   **951.5 ms** | **5.95 ms** | **5.56 ms** |   **953.4 ms** |   **938.6 ms** |   **956.4 ms** |     **-** |     **-** |     **-** |     **616 B** |
| **SeekUnroll** |         **27** |   **952.9 ms** | **5.86 ms** | **5.48 ms** |   **955.5 ms** |   **938.8 ms** |   **958.3 ms** |     **-** |     **-** |     **-** |    **5560 B** |
