``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-DRJFVP : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

MaxRelativeError=0.01  

```
|               Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |     Gen 0 |     Gen 1 | Gen 2 | Allocated |
|--------------------- |---------:|---------:|---------:|---------:|---------:|---------:|----------:|----------:|------:|----------:|
| BuildAnalyzerConfigs | 69.18 ms | 0.691 ms | 1.411 ms | 68.82 ms | 67.16 ms | 73.01 ms | 2000.0000 | 1000.0000 |     - |   8.12 MB |
