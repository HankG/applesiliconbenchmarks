``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-DRJFVP : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-DEHDEY : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

MaxRelativeError=0.01  InvocationCount=1  

```
|                      Method |        Job | UnrollFactor |        Mean |    Error |   StdDev |      Median |         Min |         Max |       Gen 0 |       Gen 1 |     Gen 2 |  Allocated |
|---------------------------- |----------- |------------- |------------:|---------:|---------:|------------:|------------:|------------:|------------:|------------:|----------:|-----------:|
|       CompileMethodsAndEmit | Job-DRJFVP |           16 |  5,300.9 ms | 31.47 ms | 27.89 ms |  5,296.9 ms |  5,255.8 ms |  5,349.4 ms | 143000.0000 |  42000.0000 |         - |  498.39 MB |
|           SerializeMetadata | Job-DRJFVP |           16 |    295.4 ms |  2.79 ms |  5.69 ms |    294.6 ms |    287.8 ms |    310.0 ms |   5000.0000 |   2000.0000 |         - |   33.36 MB |
|              GetDiagnostics | Job-DEHDEY |            1 |  3,779.8 ms | 18.45 ms | 17.26 ms |  3,775.3 ms |  3,756.1 ms |  3,814.3 ms | 145000.0000 |  38000.0000 |         - |  381.75 MB |
| GetDiagnosticsWithAnalyzers | Job-DEHDEY |            1 | 12,914.4 ms | 39.62 ms | 37.06 ms | 12,916.7 ms | 12,844.4 ms | 12,970.8 ms | 494000.0000 | 122000.0000 | 3000.0000 | 1515.63 MB |
