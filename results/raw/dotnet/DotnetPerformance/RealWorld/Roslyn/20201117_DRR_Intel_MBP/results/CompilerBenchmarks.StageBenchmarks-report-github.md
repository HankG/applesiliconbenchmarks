``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-OZETUM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-IIZQUN : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

MaxRelativeError=0.01  InvocationCount=1  

```
|                      Method |        Job | UnrollFactor |        Mean |    Error |   StdDev |      Median |         Min |         Max |       Gen 0 |      Gen 1 |     Gen 2 |  Allocated |
|---------------------------- |----------- |------------- |------------:|---------:|---------:|------------:|------------:|------------:|------------:|-----------:|----------:|-----------:|
|       CompileMethodsAndEmit | Job-OZETUM |           16 |  4,402.4 ms | 19.25 ms | 18.01 ms |  4,401.8 ms |  4,367.7 ms |  4,433.8 ms |  99000.0000 | 27000.0000 | 1000.0000 |  498.65 MB |
|           SerializeMetadata | Job-OZETUM |           16 |    285.4 ms |  2.70 ms |  3.32 ms |    284.9 ms |    279.9 ms |    292.2 ms |   3000.0000 |  1000.0000 |         - |   33.35 MB |
|              GetDiagnostics | Job-IIZQUN |            1 |  3,154.5 ms | 18.35 ms | 17.16 ms |  3,148.5 ms |  3,130.9 ms |  3,189.8 ms |  85000.0000 | 22000.0000 |         - |  381.66 MB |
| GetDiagnosticsWithAnalyzers | Job-IIZQUN |            1 | 11,226.7 ms | 58.71 ms | 52.04 ms | 11,222.5 ms | 11,162.4 ms | 11,340.6 ms | 325000.0000 | 81000.0000 | 3000.0000 | 1515.68 MB |
