``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-OZETUM : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

MaxRelativeError=0.01  

```
|               Method |     Mean |    Error |   StdDev |   Median |      Min |      Max |     Gen 0 |    Gen 1 | Gen 2 | Allocated |
|--------------------- |---------:|---------:|---------:|---------:|---------:|---------:|----------:|---------:|------:|----------:|
| BuildAnalyzerConfigs | 67.15 ms | 0.668 ms | 0.558 ms | 67.16 ms | 66.15 ms | 67.99 ms | 1375.0000 | 625.0000 |     - |   8.12 MB |
