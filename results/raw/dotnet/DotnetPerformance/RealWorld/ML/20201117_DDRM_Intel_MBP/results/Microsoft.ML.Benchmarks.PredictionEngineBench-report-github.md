``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-JFNXEN : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|         MakeIrisPredictions |   346.3 ns |  6.02 ns |  5.63 ns |   343.9 ns |   340.2 ns |   356.4 ns | 0.0152 |     - |     - |      64 B |
|    MakeSentimentPredictions | 4,218.9 ns | 82.14 ns | 80.68 ns | 4,187.5 ns | 4,118.4 ns | 4,371.0 ns | 0.0331 |     - |     - |     168 B |
| MakeBreastCancerPredictions |   124.7 ns |  2.16 ns |  2.02 ns |   124.2 ns |   121.7 ns |   128.2 ns | 0.0056 |     - |     - |      24 B |
