``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-JFNXEN : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method |        Mean |     Error |    StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |------------:|----------:|----------:|------------:|------------:|------------:|------:|------:|------:|----------:|
| HashScalarString |  1,975.7 μs |  39.03 μs |  43.38 μs |  1,961.8 μs |  1,921.4 μs |  2,054.5 μs |     - |     - |     - |       2 B |
|  HashScalarFloat |    566.3 μs |   9.37 μs |   8.30 μs |    564.3 μs |    557.1 μs |    585.9 μs |     - |     - |     - |       1 B |
| HashScalarDouble |    664.6 μs |  10.09 μs |   9.43 μs |    660.6 μs |    652.0 μs |    681.9 μs |     - |     - |     - |       1 B |
|    HashScalarKey |    450.6 μs |   8.50 μs |   7.95 μs |    446.8 μs |    443.1 μs |    466.6 μs |     - |     - |     - |         - |
| HashVectorString | 22,413.3 μs | 379.97 μs | 336.83 μs | 22,267.4 μs | 22,107.0 μs | 23,189.5 μs |     - |     - |     - |      85 B |
|  HashVectorFloat |  8,948.4 μs | 175.16 μs | 172.03 μs |  8,985.7 μs |  8,710.7 μs |  9,250.5 μs |     - |     - |     - |      58 B |
| HashVectorDouble |  9,456.5 μs | 126.17 μs | 105.36 μs |  9,432.2 μs |  9,311.7 μs |  9,768.5 μs |     - |     - |     - |      59 B |
|    HashVectorKey |  8,282.1 μs | 164.82 μs | 161.87 μs |  8,210.5 μs |  8,120.0 μs |  8,582.3 μs |     - |     - |     - |      58 B |
