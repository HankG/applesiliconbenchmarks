``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-JFNXEN : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|              Method |                Mean |             Error |            StdDev |              Median |                 Min |                 Max |        Gen 0 |       Gen 1 |       Gen 2 |    Allocated |
|-------------------- |--------------------:|------------------:|------------------:|--------------------:|--------------------:|--------------------:|-------------:|------------:|------------:|-------------:|
|           TrainIris |  3,750,329,850.3 ns |  28,666,462.05 ns |  26,814,626.39 ns |  3,749,030,694.0 ns |  3,699,946,389.0 ns |  3,795,526,088.0 ns |  610000.0000 | 113000.0000 |  11000.0000 | 2718120760 B |
|      TrainSentiment | 18,240,090,057.8 ns | 420,326,134.96 ns | 484,048,202.08 ns | 18,027,301,350.0 ns | 17,700,985,707.0 ns | 19,071,458,720.0 ns | 1915000.0000 | 549000.0000 | 118000.0000 | 8561925328 B |
|         PredictIris |            343.7 ns |           3.71 ns |           3.29 ns |            342.3 ns |            339.2 ns |            349.8 ns |       0.0151 |           - |           - |         64 B |
| PredictIrisBatchOf1 |        387,248.4 ns |       4,677.17 ns |       3,651.63 ns |        388,376.1 ns |        379,773.5 ns |        391,506.5 ns |      47.6190 |      5.9524 |           - |     201329 B |
| PredictIrisBatchOf2 |        394,529.3 ns |       7,759.61 ns |       8,624.78 ns |        393,686.1 ns |        383,593.5 ns |        412,626.9 ns |      47.2561 |      6.0976 |           - |     201808 B |
| PredictIrisBatchOf5 |        384,309.2 ns |       6,130.20 ns |       5,434.27 ns |        383,888.5 ns |        375,404.5 ns |        395,965.9 ns |      45.9559 |      1.8382 |           - |     199334 B |
