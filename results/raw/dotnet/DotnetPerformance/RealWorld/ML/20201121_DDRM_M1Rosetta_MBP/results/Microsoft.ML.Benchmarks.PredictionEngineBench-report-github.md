``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QMKCTP : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|                      Method |       Mean |    Error |   StdDev |     Median |        Min |        Max |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------------- |-----------:|---------:|---------:|-----------:|-----------:|-----------:|-------:|------:|------:|----------:|
|         MakeIrisPredictions |   435.1 ns |  4.89 ns |  4.57 ns |   434.5 ns |   428.4 ns |   443.1 ns | 0.0290 |     - |     - |      64 B |
|    MakeSentimentPredictions | 4,391.1 ns | 15.05 ns | 11.75 ns | 4,389.3 ns | 4,373.2 ns | 4,413.0 ns | 0.0710 |     - |     - |     168 B |
| MakeBreastCancerPredictions |   134.0 ns |  1.17 ns |  1.04 ns |   133.9 ns |   132.4 ns |   135.6 ns | 0.0114 |     - |     - |      24 B |
