``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QMKCTP : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|              Method |                Mean |            Error |           StdDev |              Median |                 Min |                 Max |        Gen 0 |       Gen 1 |       Gen 2 |    Allocated |
|-------------------- |--------------------:|-----------------:|-----------------:|--------------------:|--------------------:|--------------------:|-------------:|------------:|------------:|-------------:|
|           TrainIris |  4,269,575,352.5 ns | 15,861,865.11 ns | 13,245,375.75 ns |  4,275,341,875.0 ns |  4,247,194,417.0 ns |  4,288,845,375.0 ns |  904000.0000 | 183000.0000 |  16000.0000 | 2776464232 B |
|      TrainSentiment | 16,564,535,430.6 ns | 71,922,811.45 ns | 67,276,642.45 ns | 16,577,264,541.0 ns | 16,407,264,875.0 ns | 16,683,797,125.0 ns | 2958000.0000 | 793000.0000 | 186000.0000 | 8598670608 B |
|         PredictIris |            419.4 ns |          1.22 ns |          1.02 ns |            419.3 ns |            418.0 ns |            421.7 ns |       0.0290 |           - |           - |         64 B |
| PredictIrisBatchOf1 |      1,039,990.9 ns |     30,835.77 ns |     32,993.93 ns |      1,049,601.4 ns |        956,597.2 ns |      1,075,476.1 ns |      97.2222 |      3.4722 |           - |     205061 B |
| PredictIrisBatchOf2 |        808,439.8 ns |     13,653.99 ns |     11,401.70 ns |        807,609.3 ns |        790,039.6 ns |        827,257.9 ns |      93.7500 |      5.6818 |           - |     195730 B |
| PredictIrisBatchOf5 |        866,336.4 ns |     29,966.32 ns |     33,307.49 ns |        869,463.5 ns |        816,857.4 ns |        928,388.5 ns |      93.7500 |      3.4722 |           - |     196084 B |
