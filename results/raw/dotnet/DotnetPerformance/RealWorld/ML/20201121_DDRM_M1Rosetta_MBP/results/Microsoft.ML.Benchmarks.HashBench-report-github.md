``` ini

BenchmarkDotNet=v0.12.1.1405-nightly, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=5.0.100
  [Host]     : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT
  Job-QMKCTP : .NET Core 5.0.0 (CoreCLR 5.0.20.51904, CoreFX 5.0.20.51904), X64 RyuJIT

PowerPlanMode=00000000-0000-0000-0000-000000000000  Arguments=/p:DebugType=portable  IterationTime=250.0000 ms  
MaxIterationCount=20  MinIterationCount=15  WarmupCount=1  

```
|           Method |        Mean |       Error |      StdDev |      Median |         Min |         Max | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |------------:|------------:|------------:|------------:|------------:|------------:|------:|------:|------:|----------:|
| HashScalarString |  2,078.3 μs |     3.99 μs |     3.74 μs |  2,077.5 μs |  2,072.7 μs |  2,086.6 μs |     - |     - |     - |       3 B |
|  HashScalarFloat |    940.9 μs |     3.01 μs |     2.66 μs |    939.7 μs |    938.6 μs |    946.4 μs |     - |     - |     - |       2 B |
| HashScalarDouble |    828.6 μs |     2.99 μs |     2.65 μs |    827.9 μs |    825.1 μs |    834.3 μs |     - |     - |     - |       6 B |
|    HashScalarKey |    641.0 μs |     6.35 μs |     5.94 μs |    638.5 μs |    631.3 μs |    648.8 μs |     - |     - |     - |       1 B |
| HashVectorString | 24,160.1 μs | 6,681.47 μs | 6,861.37 μs | 19,570.1 μs | 19,505.8 μs | 41,759.5 μs |     - |     - |     - |     200 B |
|  HashVectorFloat |  9,672.4 μs |    81.33 μs |    76.08 μs |  9,636.1 μs |  9,595.7 μs |  9,797.1 μs |     - |     - |     - |      60 B |
| HashVectorDouble | 10,422.6 μs |    94.03 μs |    87.96 μs | 10,372.7 μs | 10,351.4 μs | 10,558.9 μs |     - |     - |     - |      60 B |
|    HashVectorKey |  9,013.2 μs |    86.62 μs |    81.03 μs |  8,968.5 μs |  8,922.6 μs |  9,132.8 μs |     - |     - |     - |      58 B |
