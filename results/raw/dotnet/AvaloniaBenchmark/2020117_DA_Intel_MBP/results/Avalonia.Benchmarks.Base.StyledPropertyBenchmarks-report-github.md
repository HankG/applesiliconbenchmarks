``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|                                Method |       Mean |     Error |    StdDev |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------- |-----------:|----------:|----------:|--------:|------:|------:|----------:|
|           Set_Int_Property_LocalValue |  12.487 μs | 0.2211 μs | 0.2068 μs |  1.5869 |     - |     - |   6.49 KB |
|  Set_Int_Property_Multiple_Priorities | 203.877 μs | 3.6003 μs | 3.3677 μs | 15.6250 |     - |     - |  64.67 KB |
|      Set_Int_Property_TemplatedParent |  31.766 μs | 0.6258 μs | 1.0627 μs |  4.7607 |     - |     - |  19.56 KB |
|          Bind_Int_Property_LocalValue |   8.109 μs | 0.1596 μs | 0.3469 μs |  1.6174 |     - |     - |   6.63 KB |
| Bind_Int_Property_Multiple_Priorities |  52.869 μs | 0.8231 μs | 0.7699 μs |  9.5215 |     - |     - |  38.96 KB |
| Set_Validated_Int_Property_LocalValue |  12.521 μs | 0.1517 μs | 0.1267 μs |  1.5869 |     - |     - |   6.49 KB |
|   Set_Coerced_Int_Property_LocalValue |  17.238 μs | 0.3428 μs | 0.7079 μs |  3.1128 |     - |     - |  12.83 KB |
