``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|         Method |        Mean |     Error |    StdDev |    Gen 0 |    Gen 1 |   Gen 2 |  Allocated |
|--------------- |------------:|----------:|----------:|---------:|---------:|--------:|-----------:|
| CreateCalendar | 21,708.4 μs | 433.98 μs | 825.69 μs | 843.7500 | 281.2500 | 62.5000 | 4047.88 KB |
|   CreateButton |    166.0 μs |   3.32 μs |   4.43 μs |  10.0098 |   0.9766 |       - |    41.7 KB |
|  CreateTextBox |  1,430.9 μs |  28.13 μs |  37.55 μs |  56.6406 |  29.2969 |  1.9531 |  227.57 KB |
