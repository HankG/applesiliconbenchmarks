``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|                Method |     Mean |    Error |   StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |---------:|---------:|---------:|-------:|------:|------:|----------:|
|    IsSelector_NoMatch | 50.86 ns | 0.439 ns | 0.389 ns |      - |     - |     - |         - |
|      IsSelector_Match | 41.41 ns | 0.784 ns | 0.733 ns |      - |     - |     - |         - |
| ClassSelector_NoMatch | 48.89 ns | 0.949 ns | 0.888 ns | 0.0153 |     - |     - |      64 B |
|   ClassSelector_Match | 48.96 ns | 0.981 ns | 1.129 ns | 0.0153 |     - |     - |      64 B |
