``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|                 Method |        Mean |     Error |    StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |------------:|----------:|----------:|-------:|------:|------:|----------:|
| ClearAndSetIntProperty |    273.6 ns |   5.38 ns |   9.43 ns | 0.0591 |     - |     - |     248 B |
|        BindIntProperty | 15,393.6 ns | 306.66 ns | 633.30 ns | 3.6926 |     - |     - |   15544 B |
