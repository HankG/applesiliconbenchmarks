``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host] : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|              Method |        Mean |     Error |    StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |------------:|----------:|----------:|-------:|------:|------:|----------:|
|   InpcAccessorMatch |    59.60 ns |  1.203 ns |  1.763 ns |      - |     - |     - |         - |
|   InpcAccessorStart |    73.24 ns |  1.466 ns |  2.717 ns | 0.0114 |     - |     - |      48 B |
| MethodAccessorMatch |    60.21 ns |  1.210 ns |  1.884 ns |      - |     - |     - |         - |
| MethodAccessorStart | 2,201.49 ns | 42.309 ns | 55.014 ns | 0.0305 |     - |     - |     128 B |
