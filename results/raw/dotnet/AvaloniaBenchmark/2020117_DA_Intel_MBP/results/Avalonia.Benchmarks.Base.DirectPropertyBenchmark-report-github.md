``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|              Method |     Mean |     Error |    StdDev | Ratio | RatioSD |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |---------:|----------:|----------:|------:|--------:|-------:|------:|------:|----------:|
| SetAndRaiseOriginal | 3.883 μs | 0.0763 μs | 0.0749 μs |  1.00 |    0.00 | 1.5411 |     - |     - |   6.32 KB |
|   SetAndRaiseSimple | 3.751 μs | 0.0748 μs | 0.1863 μs |  1.00 |    0.05 | 1.5411 |     - |     - |   6.32 KB |
