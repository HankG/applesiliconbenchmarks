``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host] : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|                          Method |       Mean |     Error |    StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |-----------:|----------:|----------:|-------:|------:|------:|----------:|
|       TwoWayBinding_Via_Binding |   4.158 μs | 0.0827 μs | 0.1965 μs | 0.6638 |     - |     - |   2.72 KB |
| UpdateTwoWayBinding_Via_Binding | 113.202 μs | 1.8744 μs | 1.5652 μs | 8.3008 |     - |     - |  33.92 KB |
