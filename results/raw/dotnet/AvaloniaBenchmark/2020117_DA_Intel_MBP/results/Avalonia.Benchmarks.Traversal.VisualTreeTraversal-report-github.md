``` ini

BenchmarkDotNet=v0.12.1, OS=macOS Catalina 10.15.7 (19H15) [Darwin 19.6.0]
Intel Core i7-8559U CPU 2.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.404
  [Host]     : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT
  DefaultJob : .NET Core 3.1.10 (CoreCLR 4.700.20.51601, CoreFX 4.700.20.51901), X64 RyuJIT


```
|                       Method |         Mean |       Error |       StdDev |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------- |-------------:|------------:|-------------:|--------:|------:|------:|----------:|
|      FindAncestorOfType_Linq |     621.2 μs |    12.42 μs |     22.72 μs | 83.0078 |     - |     - |  348640 B |
| FindAncestorOfType_Optimized |     187.1 μs |     3.08 μs |      2.88 μs |       - |     - |     - |         - |
|     FindCommonVisualAncestor | 399,642.8 μs | 7,905.23 μs | 14,652.85 μs |       - |     - |     - |      48 B |
