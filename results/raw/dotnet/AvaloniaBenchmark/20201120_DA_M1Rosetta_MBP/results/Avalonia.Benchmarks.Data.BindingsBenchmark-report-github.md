``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host] : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|                          Method |       Mean |     Error |    StdDev |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------- |-----------:|----------:|----------:|--------:|------:|------:|----------:|
|       TwoWayBinding_Via_Binding |   4.539 μs | 0.0706 μs | 0.0590 μs |  1.2817 |     - |     - |   2.72 KB |
| UpdateTwoWayBinding_Via_Binding | 131.130 μs | 1.3306 μs | 1.1795 μs | 16.6016 |     - |     - |  33.92 KB |
