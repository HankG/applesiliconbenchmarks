``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host]     : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT
  DefaultJob : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT


```
|              Method |     Mean |     Error |    StdDev | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |---------:|----------:|----------:|------:|-------:|------:|------:|----------:|
| SetAndRaiseOriginal | 4.072 μs | 0.0154 μs | 0.0121 μs |  1.00 | 3.0899 |     - |     - |   6.32 KB |
|   SetAndRaiseSimple | 3.794 μs | 0.0077 μs | 0.0068 μs |  0.93 | 3.0937 |     - |     - |   6.32 KB |
