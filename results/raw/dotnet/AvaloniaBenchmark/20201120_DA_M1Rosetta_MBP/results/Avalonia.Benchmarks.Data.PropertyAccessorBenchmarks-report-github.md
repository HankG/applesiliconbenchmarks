``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host] : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|              Method |        Mean |    Error |   StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------- |------------:|---------:|---------:|-------:|------:|------:|----------:|
|   InpcAccessorMatch |    88.68 ns | 0.046 ns | 0.038 ns |      - |     - |     - |         - |
|   InpcAccessorStart |   104.01 ns | 0.184 ns | 0.172 ns | 0.0229 |     - |     - |      48 B |
| MethodAccessorMatch |    88.20 ns | 0.387 ns | 0.343 ns |      - |     - |     - |         - |
| MethodAccessorStart | 2,878.48 ns | 7.791 ns | 6.083 ns | 0.0610 |     - |     - |     128 B |
