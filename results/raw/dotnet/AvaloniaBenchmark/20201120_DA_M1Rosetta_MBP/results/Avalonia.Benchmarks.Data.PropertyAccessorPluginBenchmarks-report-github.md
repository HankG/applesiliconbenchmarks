``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host] : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|           Method |     Mean |   Error |  StdDev | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |---------:|--------:|--------:|------:|------:|------:|----------:|
| MatchAccessorOld | 205.8 ns | 0.31 ns | 0.27 ns |     - |     - |     - |         - |
| MatchAccessorNew | 112.6 ns | 0.54 ns | 0.51 ns |     - |     - |     - |         - |
