``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host]     : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT
  DefaultJob : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT


```
|                       Method |         Mean |       Error |      StdDev |    Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------------- |-------------:|------------:|------------:|---------:|------:|------:|----------:|
|      FindAncestorOfType_Linq |     721.6 μs |     7.83 μs |     6.54 μs | 164.0625 |     - |     - |  348640 B |
| FindAncestorOfType_Optimized |     194.9 μs |     0.16 μs |     0.14 μs |        - |     - |     - |         - |
|     FindCommonVisualAncestor | 761,609.9 μs | 3,629.84 μs | 3,395.35 μs |        - |     - |     - |         - |
