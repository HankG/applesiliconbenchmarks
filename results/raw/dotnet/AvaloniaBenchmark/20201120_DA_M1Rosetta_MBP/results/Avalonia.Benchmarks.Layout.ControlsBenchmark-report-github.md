``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host]     : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT
  DefaultJob : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT


```
|         Method |        Mean |     Error |      StdDev |      Median |     Gen 0 | Gen 1 | Gen 2 |  Allocated |
|--------------- |------------:|----------:|------------:|------------:|----------:|------:|------:|-----------:|
| CreateCalendar | 19,600.8 μs | 648.83 μs | 1,709.27 μs | 19,009.9 μs | 1000.0000 |     - |     - | 4045.99 KB |
|   CreateButton |    148.5 μs |   2.20 μs |     1.84 μs |    148.2 μs |   19.5313 |     - |     - |   41.71 KB |
|  CreateTextBox |  2,233.5 μs | 143.68 μs |   381.02 μs |  2,166.9 μs |         - |     - |     - |  228.76 KB |
