``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host]     : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT
  DefaultJob : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT


```
|                                Method |       Mean |     Error |    StdDev |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|-------------------------------------- |-----------:|----------:|----------:|--------:|------:|------:|----------:|
|           Set_Int_Property_LocalValue |  12.352 μs | 0.0382 μs | 0.0339 μs |  3.1738 |     - |     - |   6.49 KB |
|  Set_Int_Property_Multiple_Priorities | 248.813 μs | 0.6492 μs | 0.5421 μs | 31.2500 |     - |     - |  64.67 KB |
|      Set_Int_Property_TemplatedParent |  36.595 μs | 0.0635 μs | 0.0530 μs |  9.5215 |     - |     - |  19.56 KB |
|          Bind_Int_Property_LocalValue |   8.412 μs | 0.0335 μs | 0.0280 μs |  3.2349 |     - |     - |   6.63 KB |
| Bind_Int_Property_Multiple_Priorities |  60.538 μs | 0.7649 μs | 0.5972 μs | 19.0430 |     - |     - |  38.96 KB |
| Set_Validated_Int_Property_LocalValue |  13.636 μs | 0.0407 μs | 0.0360 μs |  3.1738 |     - |     - |   6.49 KB |
|   Set_Coerced_Int_Property_LocalValue |  17.265 μs | 0.0306 μs | 0.0255 μs |  6.2561 |     - |     - |  12.83 KB |
