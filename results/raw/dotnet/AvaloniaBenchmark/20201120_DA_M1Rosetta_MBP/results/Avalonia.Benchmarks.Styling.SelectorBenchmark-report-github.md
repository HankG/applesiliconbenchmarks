``` ini

BenchmarkDotNet=v0.12.1, OS=macOS 11.0 (20A2411) [Darwin 20.1.0]
VirtualApple 2.50GHz processor (Max: 2.40GHz), 1 CPU, 8 logical and 8 physical cores
.NET Core SDK=3.1.403
  [Host]     : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT
  DefaultJob : .NET Core 3.1.9 (CoreCLR 4.700.20.47201, CoreFX 4.700.20.47203), X64 RyuJIT


```
|                Method |     Mean |    Error |   StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|---------------------- |---------:|---------:|---------:|-------:|------:|------:|----------:|
|    IsSelector_NoMatch | 56.20 ns | 0.432 ns | 0.361 ns |      - |     - |     - |         - |
|      IsSelector_Match | 60.59 ns | 0.985 ns | 0.823 ns |      - |     - |     - |         - |
| ClassSelector_NoMatch | 49.61 ns | 0.439 ns | 0.389 ns | 0.0306 |     - |     - |      64 B |
|   ClassSelector_Match | 54.45 ns | 0.481 ns | 0.427 ns | 0.0306 |     - |     - |      64 B |
