# Apple Silicon Benchmarks

As an Apple hardware user I am interested in the relative performance of 
Apple Silicon compared to x86-64 offerings from AMD and Intel. My 
particular area of interest is the performance of my main development 
platforms, the .NET CLR and the JVM. This is especially true while those
platforms will need to run under Rosetta x86-64 emulation initially.

This project is an attempt to quantify the performance of these platforms
in a reproducable and fair way. Any custom source code generated for 
running or processing these results is released under an AGPL 3.0 license.
All data is being released under a Creative Commons Attribution (CC-BY)
license.