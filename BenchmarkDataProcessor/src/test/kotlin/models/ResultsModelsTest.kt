package models

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ResultsModelsTest {
    @Test
    fun testHasProperty() {
        assertFalse(TestResult("blank").hasMax)
        assertFalse(TestResult("blank").hasMean)
        assertFalse(TestResult("blank").hasMedian)
        assertFalse(TestResult("blank").hasMin)
        assertFalse(TestResult("blank").hasSamples)
        assertFalse(TestResult("blank").hasUnitName)
        assertTrue(TestResult("blank", max = 1.0).hasMax)
        assertTrue(TestResult("blank", mean = 1.0).hasMean)
        assertTrue(TestResult("blank", median = 1.0).hasMedian)
        assertTrue(TestResult("blank", min = 1.0).hasMin)
        assertTrue(TestResult("blank", samples = 1).hasSamples)
        assertTrue(TestResult("blank", unitName = "fps").hasUnitName)
    }

}