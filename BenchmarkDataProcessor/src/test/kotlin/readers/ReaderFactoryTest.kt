package readers

import com.github.michaelbull.result.unwrap
import models.TestType
import readers.resultset.DopeTestResultSetReader
import java.io.File
import java.lang.IllegalArgumentException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ReaderFactoryTest {
    private val implementedTypes = listOf(TestType.DopeTest)
    @Test
    fun testKnownValues() {
        assertEquals(DopeTestResultSetReader, ResultSetReaderFactory.getReader(TestType.DopeTest).unwrap())
    }

    @Test
    fun testUnknownValues() {
        TestType.values()
            .filter { !implementedTypes.contains(it)}
            .forEach { assertFailsWith(IllegalArgumentException::class, { ResultSetReaderFactory.getReader(it) }) }
    }

}