package readers.file

import com.github.michaelbull.result.unwrap
import models.MachineInfo
import models.TestResultSet
import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals

class OrekitSummaryFileReaderTest {

    @Test
    fun testUnixFormatRead() {
        val result = getResult(this.unixFormat)
        val expectedCompileTime = 24.057
        val expectedTestTime = 13*60.0 + 13.653

        println(result)

        assertEquals(expectedCompileTime, result.results[0].mean)
        assertEquals(expectedTestTime, result.results[1].mean)
    }

    @Test
    fun testBigSurFormatRead() {
        val result = getResult(this.bigSurFormat)
        val expectedCompileTime = 41.810
        val expectedTestTime = 14*60 + 27.30

        println(result)

        assertEquals(expectedCompileTime, result.results[0].mean)
        assertEquals(expectedTestTime, result.results[1].mean)

    }

    private fun getResult(text:String): TestResultSet {
        val tmpFile = Files.createTempFile("resultsTest", ".txt")
        Files.writeString(tmpFile, text)
        val resultSet = OrekitSummaryFileReader.parse(tmpFile.toAbsolutePath().toString(), MachineInfo.NO_INFO).unwrap()
        Files.deleteIfExists(tmpFile)
        return resultSet
    }

    val unixFormat = """
        
        Compile
        real	0m24.057s
        user	1m29.724s
        sys	0m3.895s

        Test
        real	13m13.653s
        user	20m28.013s
        sys	0m26.755s
        
    """.trimIndent()

    val bigSurFormat = """
        
        Compile
        87.55s user 6.38s system 224% cpu 41.810 total

        Test
        1243.89s user 37.73s system 147% cpu 14:27.30 total
        
    """.trimIndent()
}