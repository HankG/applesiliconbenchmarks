package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.unwrap
import kotlinx.serialization.json.Json
import readers.LocalDateTimeSerializer
import java.time.LocalDateTime
import kotlin.test.Test
import kotlin.test.assertEquals

class ReaderUtilsTest {
    @Test
    fun testSimpleSplitter() {
        val initialValue = "Model Name: Apple Development Platform"
        val expectedKey = "Model Name"
        val expectedValue = " Apple Development Platform"
        val (key, value) = initialValue.splitKeyValue(':').unwrap()
        println("|$key| |$value|")
        assertEquals(expectedKey, key)
        assertEquals(expectedValue, value)
    }

    @Test
    fun testExtractValue() {
        assertEquals(2.4, "Processor Speed: 2.4 GHz".findAndExtractValue().unwrap().toDouble())
        assertEquals(4, "Total Number of Cores: 4".findAndExtractValue().unwrap().toInt())
        assertEquals("000000-0000-0000-0000-0000000000", "Hardware UUID: 000000-0000-0000-0000-0000000000".findAndExtractValue().unwrap())
    }

    @Test
    fun testFindAndExtractValue() {
        val lines = listOf(
            "Model Name: Apple Development Platform"
        )
        assertEquals("Apple Development Platform",
            lines.findAndExtractValue("Model Name", true).unwrap().trim())
        assertEquals(Err("Can't find key"), lines.findAndExtractValue("SomethingElse"))
    }

    @Test
    fun testLocalDateTimeSerializerRoundTrip() {
        val now = LocalDateTime.now()
        val json = Json.encodeToString(LocalDateTimeSerializer, now)
        println(json)
        val nowFromjson = Json.decodeFromString(LocalDateTimeSerializer, json)
        assertEquals(now, nowFromjson)
    }
}