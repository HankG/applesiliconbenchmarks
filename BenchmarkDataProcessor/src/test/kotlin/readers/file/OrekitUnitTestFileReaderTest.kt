package readers.file

import com.github.michaelbull.result.unwrap
import models.MachineInfo
import org.junit.Test
import java.nio.file.Files
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.exp
import kotlin.test.assertEquals

class OrekitUnitTestFileReaderTest {
    @Test
    fun testReader() {
        val tmpFile = Files.createTempFile("resultsTest", ".txt")
        Files.writeString(tmpFile, fileSegment)
        val resultSet = OrekitUnitTestFileReader.parse(tmpFile.toAbsolutePath().toString(), MachineInfo.NO_INFO).unwrap()
        Files.deleteIfExists(tmpFile)
        val expectedTime = OffsetDateTime
                .parse("2020-11-20T22:19:43-05:00", DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                .toLocalDateTime()

        assertEquals(expectedTime, resultSet.testDate)
        assertEquals(12, resultSet.results.size)
        assertEquals("org.orekit.data.ExceptionalDataContextTest", resultSet.results.first().testName)
        assertEquals(0.064, resultSet.results.first().mean)
        assertEquals("org.orekit.compiler.plugin.DefaultDataContextPluginTest", resultSet.results.last().testName)
        assertEquals(1.272, resultSet.results.last().mean)
    }

    val fileSegment = """
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< org.orekit:orekit >--------------------------
[INFO] Building ORbit Extrapolation KIT 10.3-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:3.1.0:resources (default-resources) @ orekit ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 69 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:compile (plugin-compile) @ orekit ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 4 source files to /Users/hankdev/tmp/orekit/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 8
[INFO] 
[INFO] --- maven-resources-plugin:3.1.0:copy-resources (default) @ orekit ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] 
[INFO] --- build-helper-maven-plugin:3.2.0:add-resource (add-resource) @ orekit ---
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ orekit ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 942 source files to /Users/hankdev/tmp/orekit/target/classes
[WARNING] bootstrap class path not set in conjunction with -source 8
[WARNING] /Users/hankdev/tmp/orekit/src/main/java/org/orekit/data/DataProvider.java:[108,16] Use of the default data context from a scope not annotated with @DefaultDataContext. This code may be unintentionally using the default data context. Used: METHOD feed(java.util.regex.Pattern,org.orekit.data.DataLoader)
[INFO] 
[INFO] --- maven-bundle-plugin:4.2.1:manifest (bundle-manifest) @ orekit ---
[INFO] 
[INFO] --- maven-resources-plugin:3.1.0:testResources (default-testResources) @ orekit ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 534 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ orekit ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 570 source files to /Users/hankdev/tmp/orekit/target/test-classes
[WARNING] bootstrap class path not set in conjunction with -source 8
[INFO] 
[INFO] --- jacoco-maven-plugin:0.8.5:prepare-agent (prepare-agent) @ orekit ---
[INFO] argLine set to -javaagent:/Users/hankdev/.m2/repository/org/jacoco/org.jacoco.agent/0.8.5/org.jacoco.agent-0.8.5-runtime.jar=destfile=/Users/hankdev/tmp/orekit/target/jacoco.exec,excludes=fr/cs/examples/**/*.class
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ orekit ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit4/2.22.2/surefire-junit4-2.22.2.pom
Progress (1): 2.7/3.1 kB
Progress (1): 3.1 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit4/2.22.2/surefire-junit4-2.22.2.pom (3.1 kB at 8.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-providers/2.22.2/surefire-providers-2.22.2.pom
Progress (1): 2.5 kB
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-providers/2.22.2/surefire-providers-2.22.2.pom (2.5 kB at 58 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit4/2.22.2/surefire-junit4-2.22.2.jar
Progress (1): 2.7/85 kB
Progress (1): 5.5/85 kB

Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit4/2.22.2/surefire-junit4-2.22.2.jar (85 kB at 1.2 MB/s)
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running org.orekit.data.ExceptionalDataContextTest
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.064 s - in org.orekit.data.ExceptionalDataContextTest
[INFO] Running org.orekit.data.FilesListCrawlerTest
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.008 s - in org.orekit.data.FilesListCrawlerTest
[INFO] Running org.orekit.data.PoissonSeriesParserTest
[INFO] Tests run: 28, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 4.535 s - in org.orekit.data.PoissonSeriesParserTest
[INFO] Running org.orekit.data.ZipJarCrawlerTest
[INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.005 s - in org.orekit.data.ZipJarCrawlerTest
[INFO] Running org.orekit.data.UnixCompressFilterTest
[INFO] Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.003 s - in org.orekit.data.UnixCompressFilterTest
[INFO] Running org.orekit.data.NetworkCrawlerTest
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 s - in org.orekit.data.NetworkCrawlerTest
[INFO] Running org.orekit.data.DataProvidersManagerTest
[INFO] Tests run: 12, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.015 s - in org.orekit.data.DataProvidersManagerTest
[INFO] Running org.orekit.data.NutationCodecTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.068 s - in org.orekit.data.NutationCodecTest
[INFO] Running org.orekit.data.ClasspathCrawlerTest
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 s - in org.orekit.data.ClasspathCrawlerTest
[INFO] Running org.orekit.data.DirectoryCrawlerTest
[INFO] Tests run: 7, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 s - in org.orekit.data.DirectoryCrawlerTest
[INFO] Running org.orekit.data.FundamentalNutationArgumentsTest
[INFO] Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.247 s - in org.orekit.data.FundamentalNutationArgumentsTest
[INFO] Running org.orekit.compiler.plugin.DefaultDataContextPluginTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.272 s - in org.orekit.compiler.plugin.DefaultDataContextPluginTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 4156, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  14:25 min
[INFO] Finished at: 2020-11-20T22:19:43-05:00
[INFO] ------------------------------------------------------------------------
    """.trimIndent()
}