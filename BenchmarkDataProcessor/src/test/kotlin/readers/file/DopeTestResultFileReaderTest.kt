package readers.file

import com.github.michaelbull.result.unwrap
import models.MachineInfo
import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals

class DopeTestResultFileReaderTest {

    @Test
    fun testResultsParsing() {
        val tmpFile = Files.createTempFile("resultsTest", ".csv")
        Files.writeString(tmpFile, sampleResultsFile)
        val resultSet = DopeTestResultFileReader.parse(tmpFile.toAbsolutePath().toString(), MachineInfo.NO_INFO).unwrap()
        println(resultSet)
        assertEquals(3, resultSet.results.size)
        val first = resultSet.results.first()
        assertEquals("build", first.testName)
        assertEquals(DopeTestResultFileReader.UNITS, first.unitName)
        assertEquals(1060.48, first.mean)
        Files.deleteIfExists(tmpFile)
    }

    val sampleResultsFile =
        """
            test, avg dopes/s
            build, 1060.48
            binding 2, 21900.07
            change, 204446.71
        """.trimIndent()
}