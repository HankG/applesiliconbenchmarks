import com.github.michaelbull.result.*
import kotlinx.serialization.json.Json
import models.AppSettings
import models.CategoryOutputData
import models.FolderMapping
import models.TestType
import readers.ResultSetReaderFactory
import writers.ChartWriter
import writers.HtmlWriter
import writers.WriterUtils
import java.nio.file.Files
import java.nio.file.Paths

class MainProcessor(
        val appSettings:AppSettings,
        val specificMachines:List<String>,
        val specificTestTypes:List<TestType>,
        val maxEntriesPerSet:Int
    ) {
    companion object {
        val DEFAULT_MAX_ENTRIES_PER_SET = 10
    }

    private val DEFAULT_SETTINGS_FILE_NAME = "settings.json"
    val resultsStorage = ResultsStorage()

    fun bootstrap(): Result<String, String> {
        val baseFolder = appSettings.baseFolderPath
        val baseFolderPath = Paths.get(appSettings.baseFolderPath)
        if (Files.notExists(baseFolderPath)) {
            return Err("Base folder for bootstrapping doesn't exist: $baseFolder")
        }

        try {
            val storagePath = Paths.get(baseFolder, appSettings.storageFolderName)
            Files.createDirectory(storagePath)

            val outputFolder = Paths.get(baseFolder, appSettings.outputBaseFolderName)
            Files.createDirectory(outputFolder)

            val machineMappingsPath = Paths.get(storagePath.toString(), appSettings.machineMappingsFilename)
            val resultsFilePath = Paths.get(storagePath.toString(), appSettings.resultsStorageFilename)
            resultsStorage.save(machineMappingsPath.toString(), resultsFilePath.toString())

            appSettings.testFolders.add(FolderMapping("java/JavaFXBouncyBallsBenchmark", TestType.JavaFXBouncyBalls))
            val settingsJson = Json{prettyPrint = true}.encodeToString(AppSettings.serializer(), appSettings)
            val settingsPath = Paths.get(baseFolder, DEFAULT_SETTINGS_FILE_NAME)
            Files.writeString(settingsPath, settingsJson)
            return Ok("New configuration bootstrapped, settings file at: ${settingsPath.toAbsolutePath()}")
        } catch (e:Exception) {
            return Err("Error bootstrapping at $baseFolder : $e")
        }
    }

    fun initializeFromStorage(): Result<String, String> {
        return resultsStorage.initializeFromDisk(machinePath, resultsPath, true, true)
    }

    fun processResults(): List<String> {
        val status = mutableListOf<String>()

        appSettings.testFolders.map {
            processResult(it)
        }.forEach {
            it.mapBoth(
                {success -> status.addAll(success)},
                {error -> status.add("Error $error")}
            )
        }
        return status
    }

    fun outputAll(): List<String> {
        val results = mutableListOf<String>()

        specificTestTypes.forEach {
            outputDataSet(it).mapBoth(
                    {success -> results.add("Success: $success")},
                    {error -> results.add("Error: $error")}
            )
        }

        return results
    }

    fun outputDataSet(type: TestType):Result<String, String> {
        val (fileNameBase, fileNameError) = WriterUtils.getFilenameBase(type)
        if (fileNameBase == null || fileNameError != null) {
            return Err(fileNameError?:"")
        }

        val (data, dataError) = generateCategoryDataSet(type)
        if (data == null || dataError != null) {
            return Err(dataError?:"")
        }

        val htmlFileName = "$fileNameBase.html"

        val pngFileNames = data.mapIndexed { index, catData ->
            val pngFileName = "$fileNameBase$index.png"
            val fullPngPath = Paths.get(outputPath, pngFileName).toAbsolutePath().toString()
            ChartWriter.generateChart(catData, fullPngPath)
            pngFileName
        }

        val fullHtmlPath = Paths.get(outputPath, htmlFileName).toAbsolutePath().toString()
        HtmlWriter.writeCategoryDataOutput(fullHtmlPath, data, pngFileNames)

        return Ok(fullHtmlPath)
    }

    fun save(): Result<String, String> {
        return resultsStorage.save(machinePath, resultsPath)
    }

    private val machinePath:String
        get() = Paths.get(appSettings.baseFolderPath, appSettings.storageFolderName, appSettings.machineMappingsFilename).toString()

    private val resultsPath:String
        get() = Paths.get(appSettings.baseFolderPath, appSettings.storageFolderName, appSettings.resultsStorageFilename).toString()

    private val outputPath:String
        get() = Paths.get(appSettings.baseFolderPath, appSettings.outputBaseFolderName).toString()

    private fun processResult(folderMapping: FolderMapping):Result<List<String>, String> {
        val testBaseFolder = Paths.get(appSettings.baseFolderPath, appSettings.inputBaseFolderName, folderMapping.relativePath).toFile()
        if (!testBaseFolder.exists() || !testBaseFolder.isDirectory) {
            return Err("Requested test base folder does not exist: ${testBaseFolder.toPath().toAbsolutePath()}")
        }

        val (reader, error) = ResultSetReaderFactory.getReader(folderMapping.expectedType)
        if (reader == null && error != null) {
            Err(error)
        }

        val resultFolders = testBaseFolder.listFiles().filter { it.isDirectory }
        val statuses = mutableListOf<String>()
        val results  = resultFolders.map { reader?.process(it.absolutePath, maxEntriesPerSet) }
        results.forEach {
            val result = it?.component1()
            val resultError = it?.component2()

            if (result != null) {
                val filteredResults = result
                        .filter { specificMachines.isEmpty() || it.machineInfo.id in specificMachines }
                resultsStorage.addResults(filteredResults, true).forEach { r ->
                    r.mapBoth(
                            {success -> statuses.add(success)},
                            {error -> statuses.add(error)}
                    )
                }
            }

            if (resultError != null) {
                statuses.add(resultError)
            }
        }

        return Ok(statuses)
    }

    private fun generateCategoryDataSet(testType: TestType): Result<List<CategoryOutputData>, String> {
        val resultSetByTitle = resultsStorage.results.filter { it.testType == testType }.groupBy { it.title }
        if (resultSetByTitle.isEmpty()) {
            return Err("No results for type $testType")
        }

        val rval = mutableListOf<CategoryOutputData>()
        resultSetByTitle.forEach { title, results ->
            val categoryTitle = results.first().categoryTitle
            val datasetTitle = results.first().dataSetTitle
            val unitName = results.first().results.firstOrNull()?.unitName?:""
            val data = results
                    .flatMap{testSet ->
                        testSet.results.map {result ->
                            val value = if (result.hasMedian) result.median else result.mean
                            CategoryOutputData.DataSet(testSet.machineInfo.hardwareInfo.processorName,result.testName, value)
                        }
                    }
            rval.add(CategoryOutputData(title = title, categoryTitle = categoryTitle, datasetTitle = datasetTitle, valueUnits = unitName, data = data))
        }
        return Ok(rval)
    }

}