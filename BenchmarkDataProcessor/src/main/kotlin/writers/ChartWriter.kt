package writers

import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.CategoryOutputData
import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartUtils
import org.jfree.chart.labels.CategoryItemLabelGenerator
import org.jfree.chart.labels.ItemLabelAnchor
import org.jfree.chart.labels.ItemLabelPosition
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator
import org.jfree.chart.plot.CategoryPlot
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.renderer.category.BarRenderer
import org.jfree.chart.renderer.category.StandardBarPainter
import org.jfree.chart.ui.TextAnchor
import org.jfree.data.category.CategoryDataset
import org.jfree.data.category.DefaultCategoryDataset
import java.awt.Font
import java.io.File
import java.text.NumberFormat


object ChartWriter {
    fun generateChart(data: CategoryOutputData, filePath: String, width: Int = 1024, height: Int = 600): Result<String, String> {
        val outputFile = File(filePath)
        val dataset = data.toJfreeChartData()
        //ChartFactory.setChartTheme(StandardChartTheme.createDarknessTheme())
        val chart = ChartFactory.createBarChart(
            data.title,
            data.categoryTitle,
            data.valueUnits,
            dataset,
            PlotOrientation.HORIZONTAL,
            true,
            false,
            false
        )

        val plot = chart.plot as CategoryPlot
        val renderer = plot.renderer as BarRenderer
        renderer.barPainter = StandardBarPainter()
        val generator: CategoryItemLabelGenerator =
            StandardCategoryItemLabelGenerator("{2}", NumberFormat.getInstance())

        val categoryItemRender = plot.renderer
        categoryItemRender.setDefaultItemLabelGenerator(generator)

        categoryItemRender.setDefaultItemLabelsVisible(true)
        ChartUtils.saveChartAsPNG(outputFile, chart, width, height)
        return Ok(filePath)
    }


}

fun CategoryOutputData.toJfreeChartData(): CategoryDataset {
    val dataset = DefaultCategoryDataset()
    this.data.sortedBy { it.setName }.forEach {
        dataset.addValue(it.value, it.setName, it.valueName)
    }
    return dataset

}