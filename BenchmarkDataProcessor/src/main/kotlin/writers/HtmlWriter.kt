package writers

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import models.CategoryOutputData
import org.jfree.data.category.CategoryDataset
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths

object HtmlWriter {
    fun writeCategoryDataOutput(htmlOutputPath:String, datasets: List<CategoryOutputData>, graphicsPaths:List<String>) {
        val outputPath = Paths.get(htmlOutputPath)
        val outputString = categoryDataOutputToString(datasets, graphicsPaths)
        Files.writeString(outputPath, outputString)
    }

    fun categoryDataOutputToString(datasets: List<CategoryOutputData>, graphicsPaths:List<String>):String {
        return buildString {
            makeHtml(this, datasets, graphicsPaths)
        }
    }
}

fun FlowContent.categoryDataTable(dataset: CategoryOutputData) {
    table(classes = "table table-striped") {
        thead {
            tr {
                th { +dataset.datasetTitle }
                th { +dataset.categoryTitle }
                th { +dataset.valueUnits }
            }
        }
        tbody{
            dataset.data.sortedWith(Comparator<CategoryOutputData.DataSet>{ a, b ->
                if (a.valueName.toDoubleOrNull() != null && b.valueName.toDoubleOrNull() != null) {
                    a.valueName.toDouble().compareTo(b.valueName.toDouble())
                } else {
                    a.valueName.compareTo(b.valueName)
                }
            }).forEach {
                tr {
                    td { +it.setName }
                    td { +it.valueName }
                    td { +"${it.value}" }
                }
            }
        }
    }
}

fun FlowContent.categoryDataSet(dataset: CategoryOutputData, graphicsPath:String) {
    div(classes = "panel panel-default .col-md-4") {
        div(classes = "panel-heading") {
            +dataset.title
        }
        div(classes = "panel-body") {
            img {
                alt = "Graph"
                src = graphicsPath
            }
        }
        categoryDataTable(dataset)
    }
}

fun makeHtml(stream: Appendable, datasets: List<CategoryOutputData>, graphicsPaths:List<String>) {
    stream.appendHTML().html {
        head {
            styleLink("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css")
        }
        body {
            datasets.forEachIndexed { index, data ->
                div(classes = "container") {
                    categoryDataSet(data, graphicsPaths[index])
                }
            }
        }
    }
}
