package writers

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.TestType

object WriterUtils {
    fun getFilenameBase(testType: TestType):Result<String, String> {
        return when(testType) {
            TestType.AvaloniaCompile -> Ok("AvaloniaCompile")
            TestType.AvaloniaTest -> Ok("AvaloniaBenchmarkTest")
            TestType.DotnetPerformanceRealWorldML -> Ok("DotNetPerfRealWorldML")
            TestType.DotnetPerformanceRealWorldCompiler -> Ok("DotNetPerfRealWorldCompiler")
            TestType.DotnetPerformanceMicro -> Ok("DotNetPerfMicro")
            TestType.DopeTest -> Ok("DopeTest")
            TestType.JavaFXBouncyBalls -> Ok("JavaFXBouncyBalls")
            TestType.OrekitCompile -> Ok("OrekitCompile")
            TestType.OrekitUnitTest -> Ok("OrekitUnitTest")
            TestType.Renaissance -> Ok("RenaissanceBenchmarkTest")
        }
    }
}