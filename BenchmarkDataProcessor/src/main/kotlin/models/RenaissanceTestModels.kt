package models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RenaissanceResultSet(
    val data: Map<String, RenaissanceResult>
)

@Serializable
data class RenaissanceResult(
    val results: List<RenaissanceResultEntry>
)

@Serializable
data class RenaissanceResultEntry(
    @SerialName("duration_ns") val durationNanos: Long,
    @SerialName("uptime_ns") val uptimeNanos: Long
)