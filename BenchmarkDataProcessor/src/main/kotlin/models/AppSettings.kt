package models

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.nio.file.Files
import java.nio.file.Paths

@Serializable
data class AppSettings(
    val baseFolderPath: String,
    val storageFolderName: String,
    val inputBaseFolderName: String,
    val outputBaseFolderName: String,
    val machineMappingsFilename: String,
    val resultsStorageFilename: String,
    val testFolders: MutableList<FolderMapping> = mutableListOf()
) {
    companion object {
        const val DEFAULT_INPUT_FOLDER_NAME = "raw"
        const val DEFAULT_STORAGE_FOLDER_NAME = "storage"
        const val DEFAULT_OUTPUT_FOLDER_NAME = "output"
        const val DEFAULT_MACHINE_MAPPINGS_FILE_NAME = "machines.json"
        const val DEFAULT_RESULTS_FILE_NAME = "results.json"

        val DEFAULT = AppSettings(
            baseFolderPath = "",
            storageFolderName = DEFAULT_STORAGE_FOLDER_NAME,
            inputBaseFolderName = DEFAULT_INPUT_FOLDER_NAME,
            outputBaseFolderName = DEFAULT_OUTPUT_FOLDER_NAME,
            machineMappingsFilename = DEFAULT_MACHINE_MAPPINGS_FILE_NAME,
            resultsStorageFilename = DEFAULT_RESULTS_FILE_NAME
        )

        fun loadFromJsonFile(filePath:String):Result<AppSettings, String> {
            return try {
                val settingsJson = Files.readString(Paths.get(filePath))
                val settings = Json.decodeFromString<AppSettings>(settingsJson)
                Ok(settings)
            } catch (e:Exception) {
                Err("Error loading settings file $filePath : $e")
            }
        }
    }
}

@Serializable
data class FolderMapping(
    val relativePath:String,
    val expectedType: TestType
)

enum class TestType {
    AvaloniaCompile,
    AvaloniaTest,
    DopeTest,
    DotnetPerformanceRealWorldML,
    DotnetPerformanceRealWorldCompiler,
    DotnetPerformanceMicro,
    JavaFXBouncyBalls,
    OrekitCompile,
    OrekitUnitTest,
    Renaissance,
}

