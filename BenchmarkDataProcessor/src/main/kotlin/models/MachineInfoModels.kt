package models

import kotlinx.serialization.Serializable

@Serializable
data class HardwareInfo(
    val modelName: String,
    val modelIdentifier: String,
    val processorName: String,
    val processorSpeed: Double,
    val numberProcessors: Int,
    val numberPhysicalCores: Int,
    val numberTotalCores: Int,
    val memoryGB: Double,
) {
    companion object {
        val NO_INFO = HardwareInfo ("", "", "", 0.0, 0, 0,  0, 0.0)
    }
}

@Serializable
data class GraphicsInfo(
    val chipset: String,
    val vendor: String,
    val displayResolution: String
) {
    companion object {
        val NO_INFO = GraphicsInfo ("", "", "")
    }
}

@Serializable
data class MachineInfo(
    val id: String,
    val hardwareInfo: HardwareInfo,
    val graphicsInfo: GraphicsInfo = GraphicsInfo.NO_INFO
) {
    companion object {
        val NO_INFO = MachineInfo("", HardwareInfo.NO_INFO, GraphicsInfo.NO_INFO)
    }
}

@Serializable
data class RuntimeInfo(
    val title: String
) {
    companion object {
        val NO_INFO = RuntimeInfo("")
    }
}