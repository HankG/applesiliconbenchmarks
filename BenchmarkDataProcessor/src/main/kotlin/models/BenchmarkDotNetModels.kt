package models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BenchmarkResultSet(
    @SerialName("Benchmarks") val data:List<BenchmarkData>
)

@Serializable
data class BenchmarkData(
        @SerialName("DisplayInfo") val title:String,
        @SerialName("MethodTitle") val methodName:String,
        @SerialName("Statistics") val statistics: BenchmarkStatistics = BenchmarkStatistics.NO_STATS
)

@Serializable
data class BenchmarkStatistics(
    @SerialName("N") val count: Int,
    @SerialName("Min") val min: Double,
    @SerialName("Median") val median: Double,
    @SerialName("Mean") val mean: Double,
    @SerialName("Max") val max:Double,
    @SerialName("StandardDeviation") val stddev: Double,
    @SerialName("OriginalValues") val values: List<Double>
) {
    companion object {
        val NO_STATS = BenchmarkStatistics(0, 0.0, 0.0, 0.0, 0.0, 0.0, listOf())
    }
}