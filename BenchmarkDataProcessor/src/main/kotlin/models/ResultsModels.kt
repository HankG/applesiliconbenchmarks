package models

import kotlinx.serialization.Serializable
import readers.LocalDateTimeSerializer
import java.time.LocalDateTime

@Serializable
data class TestResultSet(
    val title: String,
    val categoryTitle: String = "Test",
    val dataSetTitle: String = "System",
    val testType: TestType,
    val results: List<TestResult>,
    val runSetRootFolder: String = NO_PATH,
    @Serializable(with = LocalDateTimeSerializer::class) val testDate: LocalDateTime = LocalDateTime.MIN,
    val machineInfo: MachineInfo = MachineInfo.NO_INFO,
    val runtimeInfo: RuntimeInfo = RuntimeInfo.NO_INFO
) {
    fun breakUp(maxEntriesPerTest: Int): List<TestResultSet> {
        if (results.size <= maxEntriesPerTest) {
            return listOf(this)
        }

        val chunkedResults = results.chunked(maxEntriesPerTest)
        val numberSets = chunkedResults.size
        return chunkedResults.mapIndexed { index, list ->
            this.copy(title = "${this.title} (${index + 1}/$numberSets)", results = list)
        }
    }

    companion object {
        val NO_PATH = ""
    }
}

@Serializable
data class TestResult(
    val testName: String,
    val unitName: String = UNSET_UNITS_VALUE,
    val median: Double = UNSET_DOUBLE_VALUE,
    val mean: Double = UNSET_DOUBLE_VALUE,
    val min: Double = UNSET_DOUBLE_VALUE,
    val max: Double = UNSET_DOUBLE_VALUE,
    val samples: Int = UNSET_INT_VALUE
) {
    companion object {
        val UNSET_DOUBLE_VALUE = Double.MIN_VALUE
        val UNSET_INT_VALUE = Int.MIN_VALUE
        val UNSET_UNITS_VALUE = ""
    }

    val hasUnitName get() = unitName != UNSET_UNITS_VALUE
    val hasMedian get() = median != UNSET_DOUBLE_VALUE
    val hasMean get() = mean != UNSET_DOUBLE_VALUE
    val hasMin get() = min != UNSET_DOUBLE_VALUE
    val hasMax get() = max != UNSET_DOUBLE_VALUE
    val hasSamples get() = samples != UNSET_INT_VALUE
}

data class CategoryOutputData(
        val title: String,
        val categoryTitle: String,
        val datasetTitle: String,
        val valueUnits: String,
        val data: List<DataSet>,
) {
    data class DataSet(
        val setName: String,
        val valueName: String,
        val value:Double,
    )
}