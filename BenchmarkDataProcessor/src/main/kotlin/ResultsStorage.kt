import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import models.MachineInfo
import models.TestResultSet
import java.nio.file.Files
import java.nio.file.Paths

class ResultsStorage() {
    private val machineMappings: MutableList<MachineInfo> = mutableListOf()
    private val resultsStorage: MutableList<TestResultSet> = mutableListOf()

    val machines: List<MachineInfo> get() = machineMappings.toList()
    val results: List<TestResultSet> get() = resultsStorage.toList()

    fun initializeFromDisk(
        machineMappingFilePath:String,
        resultStorageFilePath:String,
        updateResultMachineInfo:Boolean,
        clearOldValues:Boolean,
    ):Result<String, String> {
        val machineMappingFile = Paths.get(machineMappingFilePath)
        if (Files.notExists(machineMappingFile)) {
            return Err("Machine mapping file doesn't exist: $machineMappingFilePath")
        }

        val resultStorageFile = Paths.get(resultStorageFilePath)
        if (Files.notExists(resultStorageFile)) {
            return Err("Result storage file does not exist: $resultStorageFilePath")
        }

        val newMachinesData = Files.readString(machineMappingFile)?:""
        val newMachines= Json.decodeFromString<List<MachineInfo>>(newMachinesData)

        val newResultsData = Files.readString(resultStorageFile)?:""
        val newResults = Json.decodeFromString<List<TestResultSet>>(newResultsData)

        val finalResults = if (updateResultMachineInfo) {
            val newMachinesById = newMachines.associateBy { it.id }
            newResults.map {
                it.copy(machineInfo = newMachinesById[it.machineInfo.id]?:it.machineInfo)
            }
        } else {
            newResults
        }

        if (clearOldValues) {
            machineMappings.clear()
            resultsStorage.clear()
        }

        machineMappings.addAll(newMachines)
        resultsStorage.addAll(finalResults)
        return Ok("Initialized with ${newMachines.size} machines and ${finalResults.size} results")
    }

    fun getResultsForMachineId(id:String): Result<List<TestResultSet>, String> {
        val data = resultsStorage.filter { it.machineInfo.id == id }
        if (data.isEmpty()) {
            return Err("Machine ID not found: $id")
        } else {
            return Ok(data)
        }
    }

    fun addResults(newResults:List<TestResultSet>, allowOverwrite: Boolean): List<Result<String, String>> {
        return newResults.map {
            addResult(it, allowOverwrite)
        }
    }

    fun addResult(newResult:TestResultSet, allowOverwrite:Boolean): Result<String, String> {
        var status = "Added new result"

        val existingTestResult = resultsStorage.firstOrNull { it.runSetRootFolder == newResult.runSetRootFolder && it.title == newResult.title }
        if (!allowOverwrite && existingTestResult != null) {
            return Err("Data set from this path already processed: ${newResult.runSetRootFolder}")
        }

        if (existingTestResult != null) {
            resultsStorage.remove(existingTestResult)
            status = "Updating result "
        }

        resultsStorage.add(newResult)
        if (machineMappings.firstOrNull { it.id == newResult.machineInfo.id } == null) {
            machineMappings.add(newResult.machineInfo)
        }

        return Ok("$status from: ${newResult.runSetRootFolder}")
    }

    fun save(
        machineMappingFilePath:String,
        resultStorageFilePath:String,
    ): Result<String, String> {
        try {
            val machineStoragePath = Paths.get(machineMappingFilePath)
            val machineJson = Json{prettyPrint = true}.encodeToString(ListSerializer(MachineInfo.serializer()), machineMappings)
            Files.writeString(machineStoragePath, machineJson)
            val resultStoragePath = Paths.get(resultStorageFilePath)
            val resultsJson = Json{prettyPrint = true}.encodeToString(ListSerializer(TestResultSet.serializer()), resultsStorage)
            Files.writeString(resultStoragePath, resultsJson)
        } catch (e:Exception) {
            Err("Error attempting to store data to $machineMappingFilePath and $resultStorageFilePath: $e")
        }

        return Ok("Results stored successfully to $machineMappingFilePath and $resultStorageFilePath")
    }
}