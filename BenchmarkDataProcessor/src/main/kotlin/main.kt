import com.github.michaelbull.result.mapBoth
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.multiple
import models.AppSettings
import models.TestType


fun listKnownResultsTypes() {
    println("Known test types")
    TestType.values().forEach { println(it) }
}

fun bootstrap(path:String) {
    val bootstrappedAppSettings = AppSettings.DEFAULT.copy(path)
    val result = MainProcessor(bootstrappedAppSettings, listOf(),
            TestType.values().toList(), MainProcessor.DEFAULT_MAX_ENTRIES_PER_SET).bootstrap()
    result.mapBoth(
            { success -> println("Bootstrap successful. $success")},
            { failure -> println("Bootstrapping failed: $failure")}
    )
}

fun main(args: Array<String>) {
    val argParser = ArgParser("BenchmarkDataProcessor")
    val settingsPath = argParser.option(
        ArgType.String,
        fullName = "filename",
        description = "Path to the settings JSON file, including filename"
    )

    val bootstrap = argParser.option(
            ArgType.String,
            fullName = "bootstrap",
            description = "Bootstrap storage at this location"
    )

    val listKnownResultsType = argParser.option(
            ArgType.Boolean,
            fullName = "list-known-types",
            description = "List known test types"
    )

    val processRuns = argParser.option(
            ArgType.Boolean,
            fullName = "process",
            description = "Process all new run data in the configured input folder and specified test folders"
    )

    val verboseProcessing = argParser.option(
            ArgType.Boolean,
            fullName = "verbose-processing",
            description = "Output verbose logging of processing results"
    )

    val dumpMachineList = argParser.option(
            ArgType.Boolean,
            fullName = "list-machines",
            description = "List all known machines to the command line output"
    )

    val dumpResultsList = argParser.option(
            ArgType.Boolean,
            fullName = "list-results",
            description = "List all known test result sets to the command line"
    )

    val outputAll = argParser.option(
            ArgType.Boolean,
            fullName = "output-all",
            description = "Generate output files for all known test types"
    )

    val explicitMachines = argParser.option(
            ArgType.String,
            fullName = "machine",
            description = "Only process machines with the listed UUID(s), if ommitted all are run"
    ).multiple().default(listOf(""))

    val maxTestsPerGraph = argParser.option(
            ArgType.Int,
            fullName = "max-tests-per-graph",
            description = "Maximum number of tests that will be created per graph"
    ).default(MainProcessor.DEFAULT_MAX_ENTRIES_PER_SET)

    val testTypes = argParser.option(
            ArgType.Choice<TestType>(),
            fullName = "test-type",
            description = "Only run specific test type(s), if omitted all are run"
    ).multiple().default(TestType.values().toList())


    argParser.parse(args)

    if (listKnownResultsType.value?:false) {
        listKnownResultsTypes()
        return
    }

    if (bootstrap.value?.isNotEmpty()?:false) {
        bootstrap(bootstrap.value?:"")
        return
    }

    val settingsFilePath = settingsPath.value?:""
    if (settingsFilePath.isEmpty()) {
        println("Settings value must be specified if not bootstrapping")
        return
    }

    val (appSettings, error) = AppSettings.loadFromJsonFile(settingsFilePath)
    if (appSettings == null || error != null) {
        println("Error creating main processor: $error")
        return
    }

    val specificMachines = explicitMachines.value.filter { it.isNotEmpty() }
    val mainProcessor = MainProcessor(appSettings, specificMachines, testTypes.value, maxTestsPerGraph.value)

    if (processRuns.value?:false) {
        println("Processing new results")
        val results = mainProcessor.processResults()
        if (verboseProcessing.value?:false) {
            results.forEach { println(it) }
        }
        println("Post-processing machine count: ${mainProcessor.resultsStorage.machines.size}")
        println("Post-processing results count: ${mainProcessor.resultsStorage.results.size}")
    }

    if (dumpMachineList.value?:false) {
        println("UUID, Model Name, Model ID, Processor Name, Processor Speed, Processor Total Cores")
        mainProcessor.resultsStorage.machines.forEach {
            print("${it.id}, ")
            with(it.hardwareInfo) {
                print("${this.modelName}, ${this.modelIdentifier}, ${this.processorName}, ${this.processorSpeed}")
            }
            println()
        }
    }

    if (dumpResultsList.value?:false) {
        println("Title, # Results, System, Source Folder")
        mainProcessor.resultsStorage.results.forEach {
            println("${it.title}, ${it.results.size}, ${it.machineInfo.hardwareInfo.processorName}, ${it.runSetRootFolder}")
        }
    }

    if (outputAll.value?:false) {
        mainProcessor.outputAll().forEach { println(it) }
    }

}