package readers.resultset

import com.github.michaelbull.result.Result
import models.TestResultSet

interface ResultSetReader {
    fun process(resultSetFolderPath: String, maxEntriesPerSet: Int): Result<List<TestResultSet>, String>
}