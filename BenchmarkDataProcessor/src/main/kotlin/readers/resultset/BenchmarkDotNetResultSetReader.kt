package readers.resultset

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import kotlinx.serialization.json.Json
import models.*
import readers.file.MachineInfoReader
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths

class BenchmarkDotNetResultSetReader(val type:TestType) : ResultSetReader {
    val UNITS = "nanosec"

    override fun process(resultSetFolderPath: String, maxEntriesPerSet: Int): Result<List<TestResultSet>, String> {
        val testResultSets = mutableListOf<TestResultSet>()
        val title = "$type BenchmarkDotNet Test"
        val basePath = Paths.get(resultSetFolderPath)
        if (Files.notExists(basePath)) {
            return Err("Base path does not exist: $basePath")
        }

        val machineInfoPath = MachineInfoReader.buildMachineInfoPath(basePath)
        if (Files.notExists(machineInfoPath)) {
            return Err("Machine info not found: $machineInfoPath")
        }
        val machineInfo = MachineInfoReader.parse(machineInfoPath.toString())

        val resultsSubFolder = Paths.get(resultSetFolderPath, "results")
        val benchmarkResultJsonFiles = resultsSubFolder.toFile().listFiles().filter { it.extension == "json" }

        try {
            var totalTime = 0.0
            val testResults = mutableListOf<TestResult>()
            benchmarkResultJsonFiles.forEach {jsonFile ->
                val benchmarkResultJson = Files.readString(jsonFile.toPath())
                try {
                    val benchmarkResult = Json{ignoreUnknownKeys = true; coerceInputValues = true}
                            .decodeFromString(BenchmarkResultSet.serializer(), benchmarkResultJson)
                    benchmarkResult.data.forEach {
                        totalTime += it.statistics.values.sumByDouble { it.toDouble() } * 1e-9
                        testResults.add(TestResult(
                                testName = it.methodName,
                                unitName = UNITS,
                                mean = it.statistics.mean,
                                min = it.statistics.min,
                                max = it.statistics.max,
                                samples = it.statistics.count
                        ))
                    }
                } catch (e:Exception) {
                    println(e)
                }
            }

            val testSummary = TestResultSet(
                    title = "$title Summary",
                    testType = type,
                    machineInfo = machineInfo,
                    runSetRootFolder = resultSetFolderPath,
                    results = listOf(TestResult(
                            testName = "Total",
                            unitName = "sec",
                            mean = totalTime,
                            samples = 1
                    ))
            )

            val test = TestResultSet(
                title = title,
                testType = type,
                machineInfo = machineInfo,
                runSetRootFolder = resultSetFolderPath,
                results = testResults
            )
            testResultSets.addAll(test.breakUp(maxEntriesPerSet) + testSummary)
            return Ok(testResultSets)
        } catch (e: Exception) {
            return Err("Exception thrown processing $resultSetFolderPath: ${e.toString()}")
        }

    }
}