package readers.resultset

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import com.github.michaelbull.result.andThen
import models.TestResultSet
import readers.file.MachineInfoReader
import readers.file.AvaloniaCompileFileReader
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths

object AvaloniaCompileResultSetReader : ResultSetReader {
    val RESULT_FILE_NAME = "results.txt"

    override fun process(resultSetFolderPath: String, maxEntriesPerSet: Int): Result<List<TestResultSet>, String> {
        val basePath = Paths.get(resultSetFolderPath)
        if (Files.notExists(basePath)) {
            return Err("Base path does not exist: $basePath")
        }

        val machineInfoPath = MachineInfoReader.buildMachineInfoPath(basePath)
        if (Files.notExists(machineInfoPath)) {
            return Err("Machine info not found: $machineInfoPath")
        }

        val resultFilePath = Paths.get(resultSetFolderPath, RESULT_FILE_NAME)
        if (Files.notExists(resultFilePath)) {
            return Err("Result set file not found: $resultFilePath")
        }

        try {
            val machineInfo = MachineInfoReader.parse(machineInfoPath.toString())
            return AvaloniaCompileFileReader.parse(resultFilePath.toString(), machineInfo)
                    .andThen { Ok(it.copy(runSetRootFolder = resultSetFolderPath)) }
                    .andThen { Ok(listOf(it)) }
        } catch (e: Exception) {
            return Err("Exception thrown processing $resultSetFolderPath: ${e.toString()}")
        }

    }
}