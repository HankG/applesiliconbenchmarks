package readers.resultset

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import kotlinx.serialization.json.Json
import models.RenaissanceResultSet
import models.TestResult
import models.TestResultSet
import models.TestType
import readers.file.MachineInfoReader
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths

object RenaissanceTestResultSetReader : ResultSetReader {
    val TITLE = "Java Renaissance Benchmark"
    val UNITS = "sec"
    val testsToSkip = listOf("db-shootout.json", "")

    override fun process(resultSetFolderPath: String, maxEntriesPerSet: Int): Result<List<TestResultSet>, String> {
        val basePath = Paths.get(resultSetFolderPath)
        if (Files.notExists(basePath)) {
            return Err("Base path does not exist: $basePath")
        }

        val machineInfoPath = MachineInfoReader.buildMachineInfoPath(basePath)
        if (Files.notExists(machineInfoPath)) {
            return Err("Machine info not found: $machineInfoPath")
        }

        val benchmarkResultJsonFiles = basePath.toFile()
                .listFiles()
                .filter { it.extension == "json" }
                .filter { !(it.name in testsToSkip) }
                .filter { it.name != MachineInfoReader.FILE_NAME}

        try {
            var totalRunTime = 0.0
            val machineInfo = MachineInfoReader.parse(machineInfoPath.toString())
            val testResults = mutableListOf<TestResult>()
            benchmarkResultJsonFiles.forEach {jsonFile ->
                try {
                    val benchmarkResultJson = Files.readString(jsonFile.toPath())
                    val benchmarkResult = Json{ignoreUnknownKeys = true}
                            .decodeFromString(RenaissanceResultSet.serializer(), benchmarkResultJson)
                    benchmarkResult.data.forEach { name, renaissanceResult ->
                        val sum = renaissanceResult.results.sumByDouble { it.durationNanos.toDouble() } * 1e-9
                        totalRunTime += sum
                        val average = renaissanceResult.results.map { it.durationNanos.toDouble() }.average() * 1e-9
                        val min = renaissanceResult.results.minOf { it.durationNanos } * 1e-9
                        val max = renaissanceResult.results.maxOf { it.durationNanos } * 1e-9
                        testResults.add(TestResult(
                                testName = name,
                                unitName = UNITS,
                                mean = average,
                                min = min,
                                max = max,
                                samples = renaissanceResult.results.size
                        ))
                    }
                } catch(e:Exception) {
                    println(e)
                }
            }

            val testSummary = TestResultSet(
                    title = "$TITLE Summary",
                    testType = TestType.Renaissance,
                    machineInfo = machineInfo,
                    runSetRootFolder = resultSetFolderPath,
                    results = listOf(TestResult(
                        testName = "Total",
                        unitName = UNITS,
                        mean = totalRunTime,
                        samples = 1
                    ))
            )

            val test = TestResultSet(
                    title = TITLE,
                    testType = TestType.Renaissance,
                    machineInfo = machineInfo,
                    runSetRootFolder = resultSetFolderPath,
                    results = testResults
            )


            return Ok(test.breakUp(maxEntriesPerSet) + testSummary)
        } catch (e: Exception) {
            return Err("Exception thrown processing $resultSetFolderPath: ${e.toString()}")
        }
    }

    private fun resultJsonToResultSet() {

    }
}