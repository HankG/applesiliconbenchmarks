package readers.file

import com.github.michaelbull.result.*

fun String.splitKeyValue(separator:Char = ':'): Result<Pair<String, String>, String> {
    val keyIndex = this.indexOfFirst { it ==  separator}
    return if (keyIndex < 0) {
        Err("Key separator doesn't exist")
    } else {
        Ok(Pair(this.substring(0, keyIndex), this.substring(keyIndex + 1)))
    }
}

fun String.findAndExtractValue(keyValueSeparator:Char = ':', valueUnitSeparator:Char = ' '): Result<String, String> {
    return this.splitKeyValue(keyValueSeparator)
        .andThen {
            val fullValue = it.second.trim()
            if (fullValue.contains(valueUnitSeparator))
                fullValue.trim().splitKeyValue(valueUnitSeparator)
            else
                Pair(fullValue, "").toResultOr { "" }
        }
        .andThen { it.first.trim().toResultOr { "Error Pulling first value" } }
}

fun List<String>.findAndExtractValue(keyName:String, rawValue:Boolean = false): Result<String, String> {
    return this.firstOrNull { it.startsWith(keyName) }
        .toResultOr { "Can't find key" }
        .andThen {
            if (rawValue)
                it.splitKeyValue().map { it.second.trim() }
            else
                it.findAndExtractValue()
        }
}

fun extractSecondsFromBigSurLine(line:String): Double {
    val elements = line.split("\\s+".toRegex())
    val timeString = elements[elements.size - 2]

    return if (timeString.contains(":")) {
        val timeElements = timeString.split(":")
        val seconds = timeElements.last().toDouble()
        val (hoursSeconds, minutesSeconds) = if (timeElements.size == 2) {
            Pair(0.0, 60.0 * timeElements[0].toDouble())
        } else {
            Pair(3600 * timeElements[0].toDouble(), 60 * timeElements[1].toDouble())
        }

        seconds + minutesSeconds + hoursSeconds
    } else {
        timeString.toDouble()
    }
}

fun extractSecondsFromUnixLine(line:String): Double {
    val (_, timeValue) = line.split("\\s+".toRegex())
    val mIndex = timeValue.indexOf("m")
    val minutes = timeValue.substring(0, mIndex).toDouble()
    val seconds = timeValue.substring(mIndex + 1, timeValue.length - 1).toDouble()
    return minutes * 60.0 + seconds
}