package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.MachineInfo
import models.TestResult
import models.TestResultSet
import models.TestType
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

object OrekitUnitTestFileReader : ResultFileReader {
    val TITLE = "Orekit Unit Tests Summary"
    val UNITS = "sec"

    override fun parse(filePath: String, machineInfo: MachineInfo): Result<TestResultSet, String> {
        val path = Paths.get(filePath)
        if (Files.notExists(path)) {
            return Err("File not found: $path")
        }

        val lines = Files.readAllLines(path)
        val runTimeLine = lines.filter { it.contains("Finished at:") }.firstOrNull()
        val results = lines
                .filter { it.contains("Tests run:") && it.contains("-")}
                .map { testLineToResult(it) }

        val testDate = if (runTimeLine != null) {
            runTimeLineToLocalDateTime(runTimeLine)
        } else {
            val attributes = Files.readAttributes(path, BasicFileAttributes::class.java)
            LocalDateTime.ofInstant(attributes.creationTime().toInstant(), ZoneId.systemDefault())
        }

        return Ok(TestResultSet(
            title = TITLE,
            testType = TestType.OrekitUnitTest,
            machineInfo = machineInfo,
            runSetRootFolder = filePath,
            testDate = testDate,
            results = results
        ))
    }

    private fun runTimeLineToLocalDateTime(line:String):LocalDateTime {
        val elements = line.split("\\s+".toRegex())
        return OffsetDateTime.parse(elements.last(), DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDateTime()
    }

    private fun testLineToResult(line:String):TestResult {
        val elements = line.trim().split("\\s+".toRegex())
        val name = elements.last().split(".").last()

        val runtimeElement = elements.indexOf("elapsed:")
        val runtimeString = elements[runtimeElement + 1]
        val unitsString = elements[runtimeElement + 1]

        val multiplier = when (unitsString){
            "min" -> 60.0
            "hr" -> 3600.0
            else -> 1.0
        }

        val runtime = runtimeString.toDouble() * multiplier
        return TestResult(
            testName = name,
            unitName = UNITS,
            mean = runtime,
            samples = 1
        )
    }
}