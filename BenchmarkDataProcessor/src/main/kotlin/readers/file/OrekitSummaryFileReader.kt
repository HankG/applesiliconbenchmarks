package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.MachineInfo
import models.TestResult
import models.TestResultSet
import models.TestType
import java.nio.file.Files
import java.nio.file.Paths

object OrekitSummaryFileReader : ResultFileReader {
    val TITLE = "Orekit Compilation & Unit Test Summary"
    val UNITS = "sec"

    override fun parse(filePath: String, machineInfo: MachineInfo): Result<TestResultSet, String> {
        val path = Paths.get(filePath)
        if (Files.notExists(path)) {
            return Err("File not found: $path")
        }

        val lines = Files.readAllLines(path)
        val bigSurSyntaxLines = lines.filter { it.contains("%") }
        val (compileSeconds, testSeconds) = if (bigSurSyntaxLines.isNotEmpty()) {
            Pair(extractSecondsFromBigSurLine(bigSurSyntaxLines[0]),extractSecondsFromBigSurLine(bigSurSyntaxLines[1]))
        } else {
            val timeLines = lines.filter { it.startsWith("real")}
            Pair(extractSecondsFromUnixLine(timeLines[0]), extractSecondsFromUnixLine(timeLines[1]))
        }

        val result = listOf(
            TestResult(
                testName = "Compile",
                unitName = UNITS,
                mean = compileSeconds,
                samples = 1
            ),
            TestResult(
                testName = "Unit Tests",
                unitName = UNITS,
                mean = testSeconds,
                samples = 1
            )
        )

        return Ok(TestResultSet(
            title = TITLE,
            testType = TestType.OrekitCompile,
            machineInfo = machineInfo,
            runSetRootFolder = filePath,
            results = result
        ))
    }
}