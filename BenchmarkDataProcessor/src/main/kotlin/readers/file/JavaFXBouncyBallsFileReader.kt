package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import kotlinx.html.TITLE
import models.MachineInfo
import models.TestResult
import models.TestResultSet
import models.TestType
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.time.LocalDateTime
import java.time.ZoneId

object JavaFXBouncyBallsFileReader : ResultFileReader {
    val TITLE = "JavaFX Bouncy Balls Render Test"
    val UNITS = "fps"
    override fun parse(filePath: String, machineInfo: MachineInfo): Result<TestResultSet, String> {
        val ballsIndex = 1
        val fpsIndex = 2
        val testNumberIndex = 5
        val path = Paths.get(filePath)
        if (Files.notExists(path)) {
            return Err("File not found: $path")
        }

        val attributes = Files.readAttributes(path, BasicFileAttributes::class.java)
        val testDate = LocalDateTime.ofInstant(attributes.creationTime().toInstant(), ZoneId.systemDefault())

        val results = Files.readAllLines(path)
                .map {
                    it.trim().split(",")
                }
                .filter {
                    it.size == 6
                }
                .filter {
                    it[ballsIndex].toIntOrNull() != null && it[testNumberIndex].toInt() == 0
                }
                .map {
                    TestResult(
                        testName = it[ballsIndex],
                        unitName = UNITS,
                        mean = it[fpsIndex].toDouble())
                }

        return Ok(TestResultSet(title = TITLE, testType = TestType.JavaFXBouncyBalls, categoryTitle = "Ball Count",  results = results, runSetRootFolder = filePath,  testDate = testDate, machineInfo = machineInfo))
    }
}