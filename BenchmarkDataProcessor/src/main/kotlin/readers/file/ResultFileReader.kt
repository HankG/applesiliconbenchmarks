package readers.file

import com.github.michaelbull.result.Result
import models.MachineInfo
import models.TestResultSet

interface ResultFileReader {
    fun parse(filePath: String, machineInfo: MachineInfo): Result<TestResultSet, String>
}