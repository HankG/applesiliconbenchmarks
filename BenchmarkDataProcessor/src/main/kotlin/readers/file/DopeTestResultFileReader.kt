package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.MachineInfo
import models.TestResult
import models.TestResultSet
import models.TestType
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.time.LocalDateTime
import java.time.ZoneId

object DopeTestResultFileReader : ResultFileReader {
    val TITLE = "Dope Test Performance Study"
    val UNITS = "ops/sec"

    override fun parse(filePath: String, machineInfo: MachineInfo) : Result<TestResultSet, String> {
        val path = Paths.get(filePath)
        if (Files.notExists(path)) {
            return Err("File not found: $path")
        }

        val attributes = Files.readAttributes(path, BasicFileAttributes::class.java)
        val testDate = LocalDateTime.ofInstant(attributes.creationTime().toInstant(), ZoneId.systemDefault())

        val results = Files.readAllLines(path)
            .map { it.trim().split(",")}
            .filter { it.size == 2 }
            .filter { it[1].toDoubleOrNull() != null}
            .map { TestResult(testName = it[0], unitName = UNITS, mean = it[1].toDouble())}

        return Ok(TestResultSet(title = TITLE, testType = TestType.DopeTest, results = results, runSetRootFolder = filePath,  testDate = testDate, machineInfo = machineInfo))
    }
}