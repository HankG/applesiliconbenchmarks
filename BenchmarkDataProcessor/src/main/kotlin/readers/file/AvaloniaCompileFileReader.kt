package readers.file

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.MachineInfo
import models.TestResult
import models.TestResultSet
import models.TestType
import java.nio.file.Files
import java.nio.file.Paths

object AvaloniaCompileFileReader : ResultFileReader {
    val TITLE = "Avalonia Compilation"
    val UNITS = "sec"

    override fun parse(filePath: String, machineInfo: MachineInfo): Result<TestResultSet, String> {
        val path = Paths.get(filePath)
        if (Files.notExists(path)) {
            return Err("File not found: $path")
        }

        val lines = Files.readAllLines(path)
        val bigSurSyntaxLines = lines.filter { it.contains("%") }
        val compileSeconds = if (bigSurSyntaxLines.isNotEmpty()) {
            extractSecondsFromBigSurLine(bigSurSyntaxLines[0])
        } else {
            val timeLines = lines.filter { it.startsWith("real")}
            extractSecondsFromUnixLine(timeLines[0])
        }

        val result = listOf(
                TestResult(
                        testName = "Compile",
                        unitName = UNITS,
                        mean = compileSeconds,
                        samples = 1
                )
        )

        return Ok(TestResultSet(
                title = TITLE,
                testType = TestType.AvaloniaCompile,
                machineInfo = machineInfo,
                runSetRootFolder = filePath,
                results = result
        ))
    }

}