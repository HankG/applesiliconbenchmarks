package readers.file

import models.GraphicsInfo
import models.HardwareInfo
import models.MachineInfo
import com.github.michaelbull.result.*
import kotlinx.serialization.json.Json
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

object MachineInfoReader {
    const val FILE_NAME = "machine-info.json"

    fun buildMachineInfoPath(basePath: Path) : Path {
        return Paths.get(basePath.toString(), FILE_NAME)
    }

    fun parse(filePath: String): MachineInfo {
        val json = Files.readString(Paths.get(filePath))
        return Json{ignoreUnknownKeys = true; isLenient = true}
                .decodeFromString(MachineInfo.serializer(), json)
    }

}