package readers

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import models.TestType
import readers.resultset.*

object ResultSetReaderFactory {
    fun getReader(type: TestType) : Result<ResultSetReader, String> {
        return when(type) {
            TestType.AvaloniaCompile -> Ok(AvaloniaCompileResultSetReader)
            TestType.AvaloniaTest -> Ok(BenchmarkDotNetResultSetReader(TestType.AvaloniaTest))
            TestType.DotnetPerformanceRealWorldML -> Ok(BenchmarkDotNetResultSetReader(TestType.DotnetPerformanceRealWorldML))
            TestType.DotnetPerformanceRealWorldCompiler -> Ok(BenchmarkDotNetResultSetReader(TestType.DotnetPerformanceRealWorldCompiler))
            TestType.DotnetPerformanceMicro -> Ok(BenchmarkDotNetResultSetReader(TestType.DotnetPerformanceMicro))
            TestType.DopeTest -> Ok(DopeTestResultSetReader)
            TestType.JavaFXBouncyBalls -> Ok(JavaFXBouncyBallsResultSetReader)
            TestType.OrekitCompile -> Ok(OrekitCompileResultSetReader)
            TestType.OrekitUnitTest -> Ok(OrekitUnitTestResultSetReader)
            TestType.Renaissance -> Ok(RenaissanceTestResultSetReader)
        }
    }
}