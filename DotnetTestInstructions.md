- Uno Dope Test
  - `system_profiler SPHardwareDataType SPMemoryDataType SPDisplaysDataType > machine_info.txt`
  - Download the DopeTest from [here](https://github.com/unoplatform/uno.dopesbench)
    - `git clone https://github.com/unoplatform/uno.dopesbench.git`
  - Compile in release mode in Visual Studio Code
  - Run the application and record the steady state value for
    - Build, binding2 and change, restarting the app between runs
    - Run each of the stages for at least 60 seconds then record the average value listed
- Avalonia Compilation
- `system_profiler SPHardwareDataType SPMemoryDataType SPDisplaysDataType > machine_info.txt`

  ```
  git clone https://github.com/AvaloniaUI/Avalonia.git
  cd Avalonia
  git submodule update --init --recursive
  ./build.sh CompileNative
  cd samples/ControlCatalog.NetCore
  time dotnet build
  ```
- Avalonia Benchmarks  all projects
   - Download source from [here](https://github.com/AvaloniaUI/Avalonia) and follow build instructions (or start off from end of Avalonia Compilation benchmark)
   - Run all tests
   - `dotnet run -c Release --exporters json csv`
- Microsoft .NET runtime performance test projects all projects
   - Results or stored in the artifacts/bin/* directories by test type names.
   - checkout commit aa79d8ceb6bed637ca1937e79c1eb30097a019ab
   - https://github.com/dotnet/performance
   - Microbenchmarks
   - Run all tests
      - `dotnet run -c Release --framework netcoreapp5.0 --exporters=json csv`
   - Real World
      - ML Benchmarks (multicore) (>400% CPU on 2018 MBP)
        - Run all tests
        - `dotnet run -c Release --framework netcoreapp5.0 --exporters=json csv` 
      - Roslyn
        - Run all tests
        - `dotnet run -c Release --framework netcoreapp5.0 --exporters=json csv`
      
   
