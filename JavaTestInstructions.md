- JavaFXBouncyBallsBenchmark
  - Record system configuration from within that directory
  ```
  ./gradlew --version > jvm_info.txt
  system_profiler SPHardwareDataType SPMemoryDataType SPDisplaysDataType > machine_info.txt
  ```
  - Compile the executable
    - `./gradlew jlink`
  - Run the test
	```
	./build/image/bin/balls \
	--initial_balls 2500 --max_balls 100000 --ball_increment 2500 \
	--initial_width 480 --initial_height 267  --max_width 480  --max_height 267 \
	--scale 0.0 --test_count 3 --warmup 10 
	  
	mv statistics.csv statistics.small_warmup.csv

	./build/image/bin/balls \
	--initial_balls 2500 --max_balls 100000 --ball_increment 2500 \
	--initial_width 1440 --initial_height 800  --max_width 1440  --max_height 800 \
	--scale 0.2 --test_count 1 --warmup 10 

	mv statistics.csv statistics.1.csv
	```

- Java Renaissance Benchmarks
  - Checkout the Renaissance Benchmark Suite and follow configuration instructions
  https://github.com/renaissance-benchmarks/renaissance
  - ```
git clone https://github.com/renaissance-benchmarks/renaissance.git
cd renaissance
tools/sbt/bin/sbt assembly

  ```
  - Run each benchmark below 
  - fj-kmeans
  - `scala-dotty`, `mnemonics`, `par-mnemonics`, `rx-scrabble`, `scala-doku`, `scala-kmeans`, single core or less than 150% CPU on average
  - Copy below into a shell script file and run as script
  - Note, first line is warm up since CPUs ultimately need thermal control and don't everything warmed up for all tests
	```bash
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 10 akka-uct
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv akka-uct.csv --json akka-uct.json akka-uct
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv als.csv --json als.json als
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv chi-square.csv --json chi-square.json chi-square
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv db-shootout.csv --json db-shootout.json db-shootout
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv dec-tree.csv --json dec-tree.json dec-tree
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv dotty.csv --json dotty.json dotty
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv finagle-chirper.csv --json finagle-chirper.json finagle-chirper
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv finagle-http.csv --json finagle-http.json finagle-http
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv fj-kmeans.csv --json fj-kmeans.json fj-kmeans
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv future-genetic.csv --json future-genetic.json future-genetic
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv gauss-mix.csv --json gauss-mix.json gauss-mix
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv log-regression.csv --json log-regression.json log-regression
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv mnemonics.csv --json mnemonics.json mnemonics
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv movie-lens.csv --json movie-lens.json movie-lens
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv naive-bayes.csv --json naive-bayes.json naive-bayes
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv neo4j-analytics.csv --json neo4j-analytics.json neo4j-analytics
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv page-rank.csv --json page-rank.json page-rank
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv par-mnemonics.csv --json par-mnemonics.json par-mnemonics
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv philosophers.csv --json philosophers.json philosophers
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv reactors.csv --json reactors.json reactors
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv rx-scrabble.csv --json rx-scrabble.json rx-scrabble
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv scala-doku.csv --json scala-doku.json scala-doku
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv scala-kmeans.csv --json scala-kmeans.json scala-kmeans
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv scala-stm-bench7.csv --json scala-stm-bench7.json scala-stm-bench7
java -jar target/renaissance-gpl-0.11.0.jar --repetitions 100 --csv scrabble.csv --json scrabble.json scrabble
	```

- Orekit Build Benchmark
  - Clone, build, and run tests to time the compilation time and test execution time
  - Clone 
    - `git clone https://gitlab.orekit.org/orekit/orekit.git`
  - Build once to pull down dependencies and "warm up" then clean and do a timed build and record results
    ```
git clone https://gitlab.orekit.org/orekit/orekit.git
cd orekit
mvn clean package -DskipTests
mvn clean
time mvn package -DskipTests
    ```
    - Copy the status output line to a timing file similar to the ones in the sample folder
  - Time execution of tests and record results
    ```
    time mvn test | sed $'s,\x1b\\[[0-9;]*[a-zA-Z],,g' > testlog.txt
    ```
