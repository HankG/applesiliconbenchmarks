# JavaFX Benchmark

This Benchmark is heavily based on the 
[FXBenchmark01](https://github.com/miho/FXBenchmark01) project by 
[Michael Hoffer](https://github.com/miho) whom based it on the 
Bubblemark benchmark. 

I only made some small changes to get it compiling against the
latest OpenJFX version of JavaFX and some other small changes
for my own purposes.