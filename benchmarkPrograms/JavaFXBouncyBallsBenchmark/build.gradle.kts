plugins {
    java
    application
    id("org.openjfx.javafxplugin") version "0.0.9"
    id("org.beryx.jlink") version "2.22.2"
}

group = "me.hankg"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

java {
    modularity.inferModulePath.set(true)
}
modularity.disableEffectiveArgumentsAdjustment()

dependencies {
    implementation("net.sourceforge.argparse4j", "argparse4j", "0.8.1")
    testImplementation("junit", "junit", "4.12")
}

javafx {
    version = "15"
    modules("javafx.controls", "javafx.base", "javafx.graphics", "javafx.media", "javafx.swing", "javafx.web")
}

application {
    mainModule.set("me.hankg.javaballsopt")
    mainClass.set("me.hankg.javaballsopt.FXBenchmark01")
}

jlink {
    launcher {
        name = "balls"
    }
    imageZip.set(project.file("${project.buildDir}/image-zip/JavaFXBouncyBallsBenchmark.zip"))
}