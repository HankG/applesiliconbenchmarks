module JavaFXBenchmark {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.media;
    requires javafx.swing;
    requires javafx.web;

    requires java.logging;
    requires argparse4j;

    exports me.hankg.javaballsopt;
}