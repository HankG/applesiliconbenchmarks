package me.hankg.javaballsopt;

import java.awt.*;

public class BallMovementModel {
    private Insets walls;
    private double elastity;
    private double ballRadius;

    public Insets getWalls() {
        return walls;
    }

    public void setWalls(Insets walls) {
        this.walls = walls;
    }

    public double getElastity() {
        return elastity;
    }

    public void setElastity(double elastity) {
        this.elastity = elastity;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getBallRadius() {
        return ballRadius;
    }

    public void setBallRadius(double ballRadius) {
        this.ballRadius = ballRadius;
    }

    private double maxSpeed;

    private BallMovementModel() {
        this(null, 0, 0, 0);
    }

    public BallMovementModel(Insets walls, double elastity, double ballRadius, double maxSpeed) {
        this.walls = walls;
        this.elastity = elastity;
        this.ballRadius = ballRadius;
        this.maxSpeed = maxSpeed;
    }

}
