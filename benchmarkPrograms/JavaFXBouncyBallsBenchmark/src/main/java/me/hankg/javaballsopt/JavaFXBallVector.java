/*
 * JavaBall.java
 * 
 * License: The code is released under Creative Commons Attribution 2.5 License
 * (http://creativecommons.org/licenses/by/2.5/)
 */

package me.hankg.javaballsopt;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rbair
 */
public class JavaFXBallVector extends JavaFXBall {
    final List<Stop> stops = new ArrayList<Stop>(List.of(
            new Stop(0.2, Color.RED),
            new Stop(0.8, Color.RED.darker().darker()),
            new Stop(1.0, Color.BLACK)
    ));
    final double gradientCenterX = 0.3;
    final double gradientCenterY = 0.3;
    final double gradientRadius = 1.0;
    final RadialGradient gradient = new RadialGradient(
            0.0, 0.0, gradientCenterX, gradientCenterY, gradientRadius, true, CycleMethod.NO_CYCLE, stops);
    final double circleCenterX = 0.3;
    final double circleCenterY = 0.3;
    final double circleRadius = 20.0;
    public Circle img = new Circle(circleCenterX, circleCenterY, circleRadius, gradient);

    public JavaFXBallVector(BallMovementModel model) {
        super(model);
        move();
    }
    
    @Override public void move() {
        super.move();
        
        img.setTranslateX(this._x);
        img.setTranslateY(this._y);
    }
    
    @Override public JavaFXBallVector clone() {
    	JavaFXBallVector lJavaFXBall = new JavaFXBallVector(model);
        ((Group)img.getParent()).getChildren().add(lJavaFXBall.img);
        return lJavaFXBall;
    }
    
    public void remove() {
    	((Group)img.getParent()).getChildren().remove(img);
    }
}
