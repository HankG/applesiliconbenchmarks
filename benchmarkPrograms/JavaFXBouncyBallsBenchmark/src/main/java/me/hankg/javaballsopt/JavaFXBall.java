package me.hankg.javaballsopt;

public abstract class JavaFXBall extends Ball {

    public JavaFXBall(BallMovementModel model) {
        super(model);
    }

    public abstract void remove();
}
