package me.hankg.javaballsopt;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class FXBenchmark01 extends Application {
    final static int MAX_BALLS_DEFAULT = 10000;
    final static int INIT_BALLS_DEFAULT = 1000;
    final static int INC_BALLS_DEFAULT = 1000;
    final static int NUM_TESTS_DEFAULT = 5;
    final static double SIZE_INCREMENT_DEFAULT = 0.2;
    final static int WARM_UP_DEFAULT = 10;
    final static int INITIAL_WIDTH_DEFAULT = 640;
    final static int INITIAL_HEIGHT_DEFAULT = 480;
    final static int MAX_WIDTH_DEFAULT = 1920;
    final static int MAX_HEIGHT_DEFAULT = 1280;
    static int maxBalls = MAX_BALLS_DEFAULT;
    static int initialBalls = INIT_BALLS_DEFAULT;
    static int ballInc  = INC_BALLS_DEFAULT;
    static int numTests = NUM_TESTS_DEFAULT;
    static double windowIncrement = SIZE_INCREMENT_DEFAULT;
    static int warmUpCount = WARM_UP_DEFAULT;
    static double stageWidth = INITIAL_WIDTH_DEFAULT;
    static double stageHeight = INITIAL_HEIGHT_DEFAULT;
    static double maxWidth = MAX_WIDTH_DEFAULT;
    static double maxHeight = MAX_HEIGHT_DEFAULT;

    public static void calcMaxBallMoveUpdateRate(double ballCount) {
        System.out.println("Estimating overhead of move operations on max # balls...");
        double elastity = -.02;
        double ballRadius = 26;
        double maxSpeed = 3.0;
        Insets walls = new Insets(0, 0, 1280, 1920);
        BallMovementModel model = new BallMovementModel(walls, elastity, ballRadius, maxSpeed);
        List<Ball> balls = new ArrayList<Ball>();

        for(int i = 0; i < ballCount; i++) {
            balls.add(new Ball(model));
        }

        int updatesCount = 0;
        long minElapsedTime = 10000;
        long start = System.currentTimeMillis();
        long stop = System.currentTimeMillis();
        while ((stop - start) <= minElapsedTime) {
            for(int b = 0; b < ballCount; b++) {
                balls.get(b).move();
            }
            updatesCount++;
            stop = System.currentTimeMillis();
        }
        double elapsed = stop - start;
        double updatesPerSec = updatesCount / elapsed * 1000.0;
        System.out.format("Max #Balls/frame: %e, Estimated max fps: %f", ballCount, updatesPerSec);
        System.out.println();
    }
    public static void main(String[] args) {
        final String maxBallsKeyword = "max_balls";
        final String initBallsKeyword = "initial_balls";
        final String incBallsKeyword = "ball_increment";
        final String numTestsKeyword = "test_count";
        final String scaleKeyword = "scale";
        final String warmupKeyword = "warmup";
        final String initialWidthKeyword = "initial_width";
        final String initialHeightKeyword = "initial_height";
        final String maxWidthKeyword = "max_width";
        final String maxHeightKeyword = "max_height";
        ArgumentParser parser = ArgumentParsers.newFor("FXBenchmark01").build()
                .defaultHelp(true)
                .description("JavaFX Performance Benchmark");
        parser.addArgument("--" + maxBallsKeyword)
                .type(Integer.class)
                .setDefault(MAX_BALLS_DEFAULT)
                .help("Max number of balls to render for each main iteration");
        parser.addArgument("--" + initBallsKeyword)
                .type(Integer.class)
                .setDefault(INIT_BALLS_DEFAULT)
                .help("Initial number of balls to render for each main iteration");
        parser.addArgument("--" + incBallsKeyword)
                .type(Integer.class)
                .setDefault(INC_BALLS_DEFAULT)
                .help("Number of balls to add per iteration");
        parser.addArgument("--" + numTestsKeyword)
                .type(Integer.class)
                .setDefault(NUM_TESTS_DEFAULT)
                .help("Number of test iterations (each test is different scaled up resolution)");
        parser.addArgument("--" + scaleKeyword)
                .type(Double.class)
                .setDefault(SIZE_INCREMENT_DEFAULT)
                .help("How much to scale window size by per test (e.g. 0.1 increases size by 10%)");
        parser.addArgument("--" + warmupKeyword)
                .type(Integer.class)
                .setDefault(WARM_UP_DEFAULT)
                .help("How long in seconds to let the test to warm up before measuring");
        parser.addArgument("--" + initialWidthKeyword)
                .type(Integer.class)
                .setDefault(INITIAL_WIDTH_DEFAULT)
                .help("Initial Window width in pixels");
        parser.addArgument("--" + initialHeightKeyword)
                .type(Integer.class)
                .setDefault(INITIAL_HEIGHT_DEFAULT)
                .help("Initial Window height in pixels");
        parser.addArgument("--" + maxWidthKeyword)
                .type(Integer.class)
                .setDefault(MAX_WIDTH_DEFAULT)
                .help("Max Window width in pixels");
        parser.addArgument("--" + maxHeightKeyword)
                .type(Integer.class)
                .setDefault(MAX_HEIGHT_DEFAULT)
                .help("Max Window height in pixels");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
            initialBalls = ns.getInt(initBallsKeyword);
            maxBalls = ns.getInt(maxBallsKeyword);
            ballInc = ns.getInt(incBallsKeyword);
            numTests = ns.getInt(numTestsKeyword);
            windowIncrement = ns.getDouble(scaleKeyword);
            stageWidth = ns.getInt(initialWidthKeyword);
            stageHeight = ns.getInt(initialHeightKeyword);
            maxWidth = ns.getInt(maxWidthKeyword);
            maxHeight = ns.getInt(maxHeightKeyword);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        calcMaxBallMoveUpdateRate(maxBalls);
        launch(args);
    }

    @Override
    public void start(final Stage stage) {
        try {
            statistics.add(header);
            // get the image
            URL url = FXBenchmark01.class.getResource("ball.png");
            Image lImage = new Image(url.openStream());

            // create the ball object
			//final JavaFXBallBitmap lJavaFXBall = new JavaFXBallBitmap(model, lImage);
            final JavaFXBallVector lJavaFXBall = new JavaFXBallVector(model);

            // create the JavaFX container to hold the balls and add the first ball
            Group root = new Group();
            root.getChildren().add(lJavaFXBall.img);
            javaFXBalls.add(lJavaFXBall);

            // create scene
            Scene scene = new Scene(root, maxWidth, maxHeight);

            // create and show stage
            stage.setTitle("Bubblemark (Window Size)");
            stage.setScene(scene);
            
            stage.setX(0);
            stage.setY(0);
            stage.setWidth(stageWidth);
            stage.setHeight(stageHeight);
            
            stage.show();

            // add more balls by cloning it (this is the way the Swing test does it... clone also adds the ball to the group)
            for (int i = 0; i < initialBalls - 1; i++) {
                javaFXBalls.add(lJavaFXBall.clone());
            }
            currentBalls = initialBalls;

            // create the incrementing test scenarion
            // let's be bold and time using nanoseconds; expectations are high
            starttimeNano = System.nanoTime();
            new AnimationTimer() {
                @Override
                public void handle(long arg0) {
                    // for all balls
                    int s = javaFXBalls.size();
                    for (int i = 0; i < s; i++) {
                        // move the ball
                        javaFXBalls.get(i).move();
                    }

//					// for all possible ball interactions
//					for (int i = 0; i < s; i++)
//					{
//						for (int j = i+1; j < s; j++)
//						{
//							// detect collision and change movement accordingly
//							javaFXBalls.get(i).doCollide( javaFXBalls.get(j) );
//						}
//					}

                    // count the frame
                    frameCnt++;

                    // check if a second has passed
                    long currenttimeNano = System.nanoTime();
                    if (currenttimeNano > lasttimeFPS + 1000000000) {
                        double dt = (currenttimeNano - lasttimeFPS) * 1e-9;
                        double frameRate = frameCnt / dt;
                        String stats = "t: " 
                                + currenttimeNano 
                                + "; balls: " 
                                + javaFXBalls.size() 
                                + "; fps: " + frameRate
                                + "; size: " + stageWidth + " : " + stageHeight
                                + "; test-count: " + numberOfFullTests;
                        
                        // print out each FPS on stdout
                        System.out.println(stats);

                        // increase the test counter
                        measurementTest++;

                        // after 5 tests (warm up period) 
                        if (measurementTest == warmUpCount) {
                            statistics.add(String.format("%d,%d,%f,%f,%f,%d", currenttimeNano, javaFXBalls.size(), frameRate, stageWidth, stageHeight, numberOfFullTests));
                            // print the result on stderr in a Excel readable form
                            System.err.println(javaFXBalls.size() + ";" + frameCnt + ";");
                            int previousBalls = currentBalls;
                            currentBalls += ballInc;

                            for (int i = previousBalls; i < currentBalls; i++) {
                                javaFXBalls.add(lJavaFXBall.clone());
                            }

                            // clear the test counter
                            measurementTest = 0;

                            if (javaFXBalls.size() > maxBalls) {
                                numberOfFullTests++;

                                for (JavaFXBall javaFXBall : javaFXBalls) {
                                    if (javaFXBall != lJavaFXBall) {
                                        javaFXBall.remove();
                                    }
                                }

                                javaFXBalls.clear();
                                javaFXBalls.add(lJavaFXBall);
                                currentBalls = initialBalls;
                                
                                boolean sameSize = incStageWidth(stage, windowIncrement);

                                // add balls
                                for (int i = 0; i < currentBalls - 1; i++) {
                                    javaFXBalls.add(lJavaFXBall.clone());
                                }
                                
                                if (numberOfFullTests == numTests || sameSize) {
                                    try {
                                        FileWriter writer = new FileWriter(new File("statistics.csv"));
                                        
                                        for (String st : statistics) {
                                            writer.append(st + "\n");
                                        }
                                        
                                        writer.flush();
                                        writer.close();
                                        
                                        System.exit(0);
                                        
                                    } catch (IOException ex) {
                                        Logger.getLogger(FXBenchmark01.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }

                        // reset frame count and time
                        frameCnt = 0;
                        lasttimeFPS = currenttimeNano;
                    }
                }
            }.start();
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }

    private boolean incStageWidth(Stage stage, double inc) {
        double oldWidth = stageWidth;
        double oldHeight = stageHeight;
        stageWidth += Math.floor(stageWidth * inc);
        stageHeight += Math.floor(stageHeight * inc);

        if (stageWidth > maxWidth) {
            stageWidth = maxWidth;
        }

        if (stageHeight > maxHeight) {
            stageHeight = maxHeight;
        }

        stage.setWidth(stageWidth);
        stage.setHeight(stageHeight);

        model.getWalls().set(0, 0, (int)(stageHeight), (int)(stageWidth));

        return (stageWidth == oldWidth) && (stageHeight == oldHeight);
    } 


    List<JavaFXBall> javaFXBalls = new ArrayList<JavaFXBall>();
    int frameCnt = 0;
    long starttimeNano = 0;
    long lasttimeFPS = 0;
    int measurementTest = 0;
    int currentBalls = 0;
    int numberOfFullTests = 0;
    String header = "time, balls, fps, width, height, test count";
    List<String> statistics = new ArrayList<String>();
    double elastity = -.02;
    private double ballRadius = 26;
    private double maxSpeed = 3.0;
    private Insets walls = new Insets(0, 0, (int)(stageHeight), (int)(stageWidth));
    private BallMovementModel model = new BallMovementModel(walls, elastity, ballRadius, maxSpeed);
}
